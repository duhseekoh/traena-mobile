// @flow
export const getQueryString = (
  params: { [string]: string | number },
): string => {
  const pairs: Array<[string, string | number]> = (Object.entries(params): any);
  return pairs.reduce((qs, pair) => {
    if (pair[1] === null || pair[1] === undefined) {
      return qs;
    }

    if (qs === '') {
      return `?${pair[0]}=${encodeURIComponent(pair[1]+'')}`;
    }
    return `${qs}&${pair[0]}=${encodeURIComponent(pair[1]+'')}`;
  }, '');
};
