import 'react-native';
import userReducer from './reducer';
import { userRetrieved } from './actions';
jest.mock('app/logging/logger');

it('action USER_RETRIEVED sets the user exactly as it is passed to the action creator', () => {
  const mockUser = {
    id: 'doesnt matter'
  };
  expect(userReducer(undefined, userRetrieved(mockUser)).user).toEqual(mockUser);
});
