// @flow
import { NavigationActions } from 'react-navigation';
import type { NavigationParams, NavigationContainer } from 'react-navigation';

class NavigationService {
  _navContainer: NavigationContainer<*, *, *>;

  constructor() {
    this._navContainer;
  }

  setTopLevelNavigator(navContainerRef: *) {
    this._navContainer = navContainerRef;
  }

  navigate(routeName: string, params?: NavigationParams, key?: string) {
    // $FlowFixMe - `dispatch` exists on a NavigationContainer, but type bindings are wrong
    this._navContainer.dispatch(
      NavigationActions.navigate({
        type: NavigationActions.NAVIGATE,
        routeName,
        params,
        key,
      })
    );
  }

  pop() {
    // $FlowFixMe - `dispatch` exists on a NavigationContainer, but type bindings are wrong
    this._navContainer.dispatch(
      // $FlowFixMe - `pop` exists, but the type bindings don't have it listed.
      NavigationActions.back(),
    );
  }

  popToTop() {
    // $FlowFixMe - `dispatch` exists on a NavigationContainer, but type bindings are wrong
    this._navContainer.dispatch(
      // $FlowFixMe - `popToTop` exists, but the type bindings don't have it listed.
      NavigationActions.popToTop(),
    );
  }
}

export default NavigationService;
