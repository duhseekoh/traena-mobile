// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  ButtonFloat,
} from 'app/components';
import { goToActivity } from 'app/redux/modules/navigation/actions';
import type { Activity } from 'app/redux/modules/activity/types';

type Props = {
  activity: Activity,
  goToActivity: (activity: Activity) => void,
};

class ActivityButton extends Component<Props> {
  props: Props;

  render() {
    const { activity, goToActivity } = this.props;

    return (
      <ButtonFloat
        icon="bolt"
        title="Start Activity"
        onPress={() => goToActivity(activity)}
        style={{ borderRadius: 8 }}
      />
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    goToActivity: (activity) => {
      return dispatch(goToActivity(activity));
    },
  };
};

const ConnectedActivityButtonContainer = connect(
  null,
  mapDispatchToProps
)(ActivityButton);

export default ConnectedActivityButtonContainer;
