// @flow
import React from 'react';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import {
  TraenaIcon
} from 'app/components';
import GenericContentPreview from './GenericContentPreview';
import styleVars from 'app/style/variables';
import type { DailyActionContentItem } from 'app/redux/modules/content/types';

type Props = {
  contentItem: DailyActionContentItem,
  style?: ViewStyleProp,
};

const DailyActionPreview = (props: Props) => {
  const typeIcon = <TraenaIcon name='checklist' fontSize={styleVars.iconSize.small} color={styleVars.color.traenaOne} />
  return (
    <GenericContentPreview
      {...props}
      typeIcon={typeIcon}
    />
  );
}

export default DailyActionPreview;
