// @flow
import { getSubscribedChannels } from 'app/redux/modules/channel/actions';
import { currentChannelListPageSelector } from './selectors';
import type { IndexedPage } from '../../../types/Pagination';
import type { Channel } from '../channel/types';
import type { Dispatch, GetState } from 'app/redux/constants';

export const REFRESH_CHANNEL_LIST_REQUEST_STARTED = 'UI_CHANNEL_LIST/REFRESH_CHANNEL_LIST_REQUEST_STARTED';
const startedRefreshChannelListRequest = () => {
  return {
    type: REFRESH_CHANNEL_LIST_REQUEST_STARTED,
    payload: null,
  };
};

export const REFRESH_CHANNEL_LIST_REQUEST_FAILED = 'UI_CHANNEL_LIST/REFRESH_CHANNEL_LIST_REQUEST_FAILED';
const failedRefreshChannelListRequest = () => {
  return {
    type: REFRESH_CHANNEL_LIST_REQUEST_FAILED,
    payload: null,
  };
};

export const LOAD_MORE_CHANNEL_LIST_REQUEST_STARTED = 'UI_CHANNEL_LIST/LOAD_MORE_CHANNEL_LIST_REQUEST_STARTED';
const startedLoadMoreChannelListRequest = () => {
  return {
    type: LOAD_MORE_CHANNEL_LIST_REQUEST_STARTED,
    payload:  null,
  };
};

export const LOAD_MORE_CHANNEL_LIST_REQUEST_FAILED = 'UI_CHANNEL_LIST/LOAD_MORE_CHANNEL_LIST_REQUEST_FAILED';
const failedLoadMoreChannelListRequest = () => {
  return {
    type: LOAD_MORE_CHANNEL_LIST_REQUEST_FAILED,
    payload: null,
  };
};

export const LOADED_MORE_CHANNEL_LIST = 'UI_CHANNEL_LIST/LOADED_MORE_CHANNEL_LIST';
function loadedMoreChannelList(channelPage: IndexedPage<number, Channel>) {
  return {
    type: LOADED_MORE_CHANNEL_LIST,
    payload: { channelPage }
  };
}

export const REFRESHED_CHANNEL_LIST = 'UI_CHANNEL_LIST/REFRESHED_CHANNEL_LIST';
function refreshedChannelList(channelPage: IndexedPage<number, Channel>) {
  return {
    type: REFRESHED_CHANNEL_LIST,
    payload: { channelPage }
  };
}

export const refreshChannelList = () => {
  return async (dispatch: Dispatch) => {
    dispatch(startedRefreshChannelListRequest());
    try {
      const result: IndexedPage<number, Channel> = await dispatch(getSubscribedChannels());
      return dispatch(refreshedChannelList(result));
    } catch(error) {
      dispatch(failedRefreshChannelListRequest());
    }
  };
}

export const loadMoreChannelList = () => {
  return async (dispatch: Dispatch, getState: GetState) => {
    dispatch(startedLoadMoreChannelListRequest());
    const pageNumber = currentChannelListPageSelector(getState()) + 1;

    try {
      const result: IndexedPage<number, Channel> = await dispatch(getSubscribedChannels(pageNumber));
      return dispatch(loadedMoreChannelList(result));
    } catch(error) {
      dispatch(failedLoadMoreChannelListRequest());
    }
  };
};
