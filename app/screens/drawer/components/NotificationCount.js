// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { TRATextStrong } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  style?: ViewStyleProp,
  count: string | number,
};

/**
 * Displays some text in a circular badge.
 */
const NotificationCount = (props: Props) => {
  return (
    <View style={[styles.container, props.style]}>
      <TRATextStrong style={styles.text}>{props.count}</TRATextStrong>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 100,
    padding: 5,
    aspectRatio: 1, // keeps background lookin like a circle
    backgroundColor: styleVars.color.paleGreen,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: styleVars.color.white,
  },
});

export default NotificationCount;
