#!/usr/bin/env bash

ENVJS_FILE="./env-config.js"

# Echo what's happening to the build logs
echo Creating environment config file

# Create `env.js` file in project root
touch $ENVJS_FILE

# Write environment config to file, hiding from build logs
tee $ENVJS_FILE <<EOF
const envConfig = {
  API_BASE_URL: '$TRAENA_API_BASE_URL',
  BUGSNAG_API_KEY: '$BUGSNAG_API_KEY',
  STAGE_NAME: '$STAGE_NAME',
  AUTH0_CLIENT_ID: '$AUTH0_CLIENT_ID',
  AUTH0_DOMAIN: '$AUTH0_DOMAIN',
  AUTH0_DB_CONNECTION_NAME: '$AUTH0_DB_CONNECTION_NAME',
  AUTH0_AUDIENCE: '$AUTH0_AUDIENCE',
  AUTH0_USER_CLAIM_NAMESPACE: '$AUTH0_USER_CLAIM_NAMESPACE',
  CODEPUSH_DEPLOYMENT_KEY: '$CODEPUSH_DEPLOYMENT_KEY',
  DISPLAY_ENVIRONMENT_DETAILS: '$DISPLAY_ENVIRONMENT_DETAILS',
  FCM_SENDER_ID: '$FCM_SENDER_ID',
  FEEDBACK_URI: '$FEEDBACK_URI',
};

export default envConfig;
EOF
