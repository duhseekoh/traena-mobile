// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Keyboard,
} from 'react-native';
import SearchBar from 'app/components/search-bar/SearchBar';
import SearchResults from './components/SearchResults';
import {
  getMoreSearchResults,
  getRecommendedSearches,
  getContentTags,
  getSearchResults,
  addRecentSearch,
  uiSearchTextUpdated,
  clearSearch,
} from 'app/redux/modules/search/actions';
import { goToContentStandalone, goToTagStandalone } from 'app/redux/modules/navigation/actions';
import {
  isLoadingSelector,
  hasNoResultsSelector,
  searchResultsSelector,
  queryTextSelector,
  recentSearchesSelector,
  commonSearchesSelector,
  contentTagsSelector,
  canLoadMoreSelector,
  displayNoMoreResultsSelector,
} from 'app/redux/modules/search/selectors';
import type { SearchResults as SearchResultsType, Suggestion } from 'app/redux/modules/search/types';
import styleVars from 'app/style/variables';

type Props = {|
  searchResults: SearchResultsType,
  searchQuery?: string,
  addRecentSearch: Function,
  recentSearches: Suggestion[],
  commonSearches: Suggestion[],
  contentTags: Suggestion[],
  isLoading?: boolean,
  queryHasNoResults?: boolean,
  updateSearchQuery: Function,
  getMoreSearchResults: () => void,
  getRecommendedSearches: () => void,
  getContentTags: () => void,
  getSearchResults: () => void,
  getChannels: () => void,
  goToContentStandalone: Function,
  goToTagStandalone: Function,
  goToChannelStandalone: Function,
  clearSearch: () => void,
  displayNoMoreResults?: boolean,
  canLoadMore?: boolean,
|};

class SearchContainer extends Component<Props> {
  props: Props;
  componentDidMount() {
    this.props.getRecommendedSearches();
    this.props.getContentTags();
  }

  render() {
    const {
      commonSearches,
      searchResults,
      recentSearches,
      isLoading,
      queryHasNoResults,
      displayNoMoreResults,
      getMoreSearchResults,
      contentTags,
      canLoadMore,
      searchQuery,
    } = this.props;

    return (
      <View style={{flex: 1, backgroundColor: styleVars.color.white}}>
        {this._renderSearchBar()}
        <SearchResults
          results={searchResults}
          onSuggestionSelected={this._onSuggestionSelected}
          onTagSelected={this._onTagSelected}
          onSearchResultSelected={this._onSearchResultSelected}
          onPressLoadMore={getMoreSearchResults}
          recentSearches={recentSearches}
          commonSearches={commonSearches}
          contentTags={contentTags}
          isLoading={isLoading}
          queryHasNoResults={queryHasNoResults}
          displayNoMoreResults={displayNoMoreResults}
          canLoadMore={canLoadMore}
          hasQueryText={!!searchQuery}
        />
      </View>
    );
  }

  _renderSearchBar = () => {
    const {
      clearSearch,
      searchQuery,
      getSearchResults,
      updateSearchQuery,
    } = this.props;

    return (
      <View>
        <SearchBar
          onClearSearch={clearSearch}
          onSearchQueryTyping={updateSearchQuery}
          onSubmit={getSearchResults}
          searchQuery={searchQuery}
        />
      </View>
    )
  }

  _onSearchResultSelected = (contentId) => {
    const { searchQuery, addRecentSearch, goToContentStandalone } = this.props;
    goToContentStandalone(contentId);
    addRecentSearch(searchQuery);
  }

  _onSuggestionSelected = (searchQuery) => {
    const { updateSearchQuery, getSearchResults } = this.props;
    updateSearchQuery(searchQuery);
    getSearchResults();
    Keyboard.dismiss();
  }

  _onTagSelected = (tag) => {
    this.props.goToTagStandalone(tag);
    Keyboard.dismiss();
  }
}

const mapStateToProps = (state) => {
  return {
    searchResults: searchResultsSelector(state),
    searchQuery: queryTextSelector(state),
    queryHasNoResults: hasNoResultsSelector(state),
    recentSearches: recentSearchesSelector(state),
    commonSearches: commonSearchesSelector(state),
    contentTags: contentTagsSelector(state),
    isLoading: isLoadingSelector(state),
    canLoadMore: canLoadMoreSelector(state),
    displayNoMoreResults: displayNoMoreResultsSelector(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateSearchQuery: (searchQuery) => {
      if(searchQuery.length === 0) {
        return dispatch(clearSearch());
      }
      return dispatch(uiSearchTextUpdated(searchQuery));
    },
    getSearchResults: () => {
      return dispatch(getSearchResults());
    },
    getMoreSearchResults: () => {
      return dispatch(getMoreSearchResults());
    },
    addRecentSearch: (searchQuery) => {
      return dispatch(addRecentSearch(searchQuery));
    },
    clearSearch: () => {
      return dispatch(clearSearch());
    },
    getRecommendedSearches: () => {
      return dispatch(getRecommendedSearches());
    },
    getContentTags: () => {
      return dispatch(getContentTags());
    },
    goToContentStandalone: (contentId) => {
      return dispatch(goToContentStandalone(contentId));
    },
    goToTagStandalone: (tag) => {
      return dispatch(goToTagStandalone(tag));
    },
  };
};

const ConnectedSearchContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchContainer);

export default ConnectedSearchContainer;
