// @flow
import type { Dispatch } from 'app/redux/constants';
import * as whatsNewActions from 'app/redux/modules/whats-new/actions';
import * as uiBookmarksWidgetActions from 'app/redux/modules/ui-bookmarks-widget/actions';
import * as trendingTagsActions from 'app/redux/modules/trending-tags/actions';
import * as seriesProgressWidgetActions from 'app/redux/modules/series-progress-widget/actions';

export const REFRESH_FEED_REQUEST_STARTED = 'REFRESH_FEED_REQUEST_STARTED';
const startedRefreshFeedRequest = () => {
  return {
    type: REFRESH_FEED_REQUEST_STARTED,
  };
};

export const REFRESH_FEED_REQUEST_FINISHED = 'REFRESH_FEED_REQUEST_FINISHED';
const finishedRefreshFeedRequest = () => {
  return {
    type: REFRESH_FEED_REQUEST_FINISHED,
  };
};

export const refreshFeed = () => {
  return async (dispatch: Dispatch) => {
    dispatch(startedRefreshFeedRequest());
    try {
      await Promise.all([
        dispatch(whatsNewActions.initialize()),
        dispatch(uiBookmarksWidgetActions.initialize()),
        dispatch(trendingTagsActions.initialize()),
        dispatch(seriesProgressWidgetActions.initialize()),
      ]);
    } catch(error) {
      console.log(error); // fetchCatch will notify bugsnag, so just extra dev log here
    } finally {
      dispatch(finishedRefreshFeedRequest());
    }
  };
}
