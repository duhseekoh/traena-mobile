// @flow
import api from 'app/redux/api';
import logger from 'app/logging/logger';
import type { Dispatch } from 'app/redux/constants';

const COMPLETION_CREATED = 'COMPLETION_CREATED';
const COMPLETIONS_RETREIVED = 'COMPLETIONS_RETREIVED';

const completionsRetreived = (contentIds: number[]) => {
  return {
    type: COMPLETIONS_RETREIVED,
    payload: { contentIds },
  }
};

const completionCreated = (contentId: number) => {
  return {
    type: COMPLETION_CREATED,
    payload: { contentId },
  }
};

const addCompletion = (contentId: number) => {
  return (dispatch: Dispatch) => {
    return api.traenaList.addCompletedTraenaListItem(contentId)
      .then(() => {
        dispatch(completionCreated(contentId));
      })
      .catch(() => {
        logger.logBreadcrumb(`Error creating completion for content ${contentId}`);
      });
  };
};

const getContentCompletions = () => {
  return (dispatch: Dispatch) => {
    api.traenaList.retrieveContentCompletions()
      .then((contentIds: number[]) => dispatch(completionsRetreived(contentIds)));
  };
};

export {
  COMPLETION_CREATED,
  COMPLETIONS_RETREIVED,
  getContentCompletions,
  addCompletion,
}
