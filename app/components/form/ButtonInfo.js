// @flow
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { FeatherIcon } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  style?: ViewStyleProp,
  onPress?: () => any,
}

const ButtonInfo = ({style, onPress}: Props) => {
  const view = onPress ?
    <TouchableOpacity onPress={onPress}
      hitSlop={{top: 10, left: 10, right: 10, bottom: 10}}>
      <View style={styles.button}><FeatherIcon name={'info'} color={styleVars.color.traenaOne} fontSize={styleVars.iconSize.medlar}/></View>
    </TouchableOpacity> : null

  return (
    <View style={style}>
      {view}
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: styleVars.color.transparent,
    marginRight: 7,
  },
});

export default ButtonInfo;
