// @flow
import Icon from 'react-native-vector-icons/Feather';
import React from 'react';
import type { TextStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import type { Color, Size } from './types';

type Props = {
  style?: TextStyleProp,
  name: | 'menu'
    | 'user'
    | 'search'
    | 'bell'
    | 'bookmark'
    | 'bookmark-filled'
    | 'heart'
    | 'heart-filled'
    | 'message-square'
    | 'check-circle'
    | 'copy'
    | 'zap'
    | 'bar-chart-2'
    | 'info'
    | 'book'
    | 'image'
    | 'video'
    | 'calendar'
    | 'play'
    | 'pause'
    | 'rotate-cw'
    | 'maximize-2'
    | 'chevron-right'
    | 'chevron-left'
    | 'x',
  fontSize: Size,
  color: Color,
};

const FeatherIcon = ({ fontSize, color, style, name }: Props) => {
  return <Icon style={[{ fontSize, color }, style]} name={name} />;
};

export default FeatherIcon;
