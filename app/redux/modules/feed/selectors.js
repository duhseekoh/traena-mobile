// @flow
import type { ReduxState } from 'app/redux/constants';
const STATE_KEY = 'feed';
const stateFromRoot = (state: ReduxState) => state[STATE_KEY];
const isRefreshingSelector = (state: ReduxState) => stateFromRoot(state).isRefreshing;

export {
  STATE_KEY,
  isRefreshingSelector,
};
