// @flow
import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import styleVars from 'app/style/variables';
import { TRAText, TraenaIcon, FeatherIcon } from 'app/components';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style?: ViewStyleProp,
  count?: number,
  onPress: () => *,
  isActive?: boolean,
};

const LikesCounter = ({style, isActive = false, count = 0, onPress}: Props) => {
  const iconStyle = isActive ? [styles.icon, styles.iconActive] : styles.icon;
  const icon = isActive ?
    <TraenaIcon name='heart-filled' style={iconStyle}/> :
    <FeatherIcon name='heart' style={iconStyle} />;

  return (
    <View style={style}>
      <TouchableOpacity onPress={onPress} style={styles.button}
        hitSlop={{top: 10, left: 10, right: 10, bottom: 10}}>
        {icon}
        <TRAText style={styles.text}>{count}</TRAText>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
  },
  icon: {
    fontSize: styleVars.iconSize.large,
    color: styleVars.color.darkGrey,
    marginRight: 5
  },
  iconActive: {
    color: styleVars.color.candyRed,
  },
  text: {
    paddingTop: 3,
    color: styleVars.color.darkGrey
  }
});

export default LikesCounter;
