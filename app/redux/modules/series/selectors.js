// @flow
import { contentItemsSelector } from 'app/redux/modules/content/selectors';
import type { SeriesWithPreview, SeriesProgress } from './types';
import type { ReduxState } from 'app/redux/constants';

const STATE_KEY = 'series';
const stateFromRoot = (state: ReduxState) => state[STATE_KEY];

const selectSeriesById = (
  state: ReduxState,
  props: { seriesId: number },
) => {
  const seriesById = stateFromRoot(state).series;
  return seriesById[props.seriesId];
};

/**
 * Get an individual series + content tuple
 */
const seriesWithContentSelector = (
  state: ReduxState,
  props: { seriesId: number },
): ?SeriesWithPreview => {
  const reducerState = stateFromRoot(state);
  const series = reducerState.series[props.seriesId];
  if (!series) {
    return null;
  }

  const previewContent = series.contentPreview
    ? contentItemsSelector(state, { contentIds: series.contentPreview })
    : [];

  return {
    series,
    previewContent,
  };
};

/**
 * Get a list of series and content tuples
 */
const seriesListWithContentSelectorv2 = (
  state: ReduxState,
  props: { seriesIds: number[] },
): SeriesWithPreview[] => {
  return props.seriesIds.reduce((list, seriesId) => {
    const seriesWithContent = seriesWithContentSelector(state, { seriesId });
    if (!seriesWithContent) {
      return list;
    }
    return [...list, seriesWithContent];
  }, []);
};

const seriesProgressSelector = (
  state: ReduxState,
  props: { id: number },
): SeriesProgress => stateFromRoot(state).seriesProgress[props.id];

const seriesProgressByIdSelector = (state: ReduxState) => stateFromRoot(state).seriesProgress;

export {
  STATE_KEY,
  selectSeriesById,
  seriesWithContentSelector,
  seriesListWithContentSelectorv2,
  seriesProgressSelector,
  seriesProgressByIdSelector,
};
