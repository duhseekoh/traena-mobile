// @flow
import moment from 'moment';
import {
  getNotificationsTpl,
  getUnacknowledgedNotificationsCountTpl,
  deleteDeregisterNotificationsDeviceTpl,
  putAcknowledgeNotificationsTpl,
  putRegisterNotificationsDeviceTpl,
} from './util/endpoints';

import { getAuthHeaders } from 'app/redux/api/util/headers';

const retrieveNotifications = (maxDate?: moment) => {
  const timestamp = maxDate ? maxDate.toISOString() : null;
  return fetch(getNotificationsTpl({maxDateTimestamp: timestamp}), {
    method: 'GET',
  });
};

const acknowledgeNotifications = () => {
  return fetch(putAcknowledgeNotificationsTpl(), {
      method: 'PUT',
    });
};

const registerNotificationsForDevice = (deviceToken: string, platform: string) => {
  return fetch(putRegisterNotificationsDeviceTpl({
      deviceToken,
      platform,
    }), {
      method: 'PUT',
    });
};

const deregisterNotificationsForDevice = (deviceToken: string) => {
  return fetch(deleteDeregisterNotificationsDeviceTpl({
      deviceToken,
    }), {
      method: 'DELETE',
      headers: getAuthHeaders(),
    });
};

const retrieveUnacknowledgedNotificationsCount = () => {
  return fetch(getUnacknowledgedNotificationsCountTpl(), {
    method: 'GET',
  });
};

export default {
  acknowledgeNotifications,
  deregisterNotificationsForDevice,
  registerNotificationsForDevice,
  retrieveNotifications,
  retrieveUnacknowledgedNotificationsCount,
}
