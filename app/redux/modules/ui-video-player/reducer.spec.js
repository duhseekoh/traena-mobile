import 'react-native';
import videoPlayerReducer from './reducer';
import { setActiveVideoPlayer, deactivateVideoPlayer } from './actions';
jest.mock('app/logging/logger');

it('action SET_ACTIVE_VIDEO sets the correct activeVideoId', () => {
  const mockVideoId = 1;
  expect(videoPlayerReducer(undefined, setActiveVideoPlayer(mockVideoId)).activeVideoId).toEqual(mockVideoId);
});

it('action CLEAR_ACTIVE_VIDEO clears the activeVideoId', () => {
  expect(videoPlayerReducer(undefined, deactivateVideoPlayer()).activeVideoId).toEqual(null);
});
