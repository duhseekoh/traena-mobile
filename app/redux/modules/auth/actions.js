// @flow
import jwtDecode from 'jwt-decode';
import * as authApi from 'app/redux/api/auth';
import { isJwtValidSelector } from './selectors';
import AppStorage from 'app/repository/AppStorage';
import { errors } from 'app/redux/api/util/errorHandler';
import {
  deregisterPushNotifications,
  resetBadgeNotificationCount,
} from 'app/redux/modules/notification/actions';
import Keychain from 'app/repository/Keychain';
import config from 'app/config';
import logger from 'app/logging/logger';
import {
  createLoggedInEvent,
  createLoggedOutEvent,
} from 'app/redux/middleware/eventFactory';
import type { Auth0UserClaim } from 'app/redux/modules/auth/types';
import type { Dispatch, GetState } from 'app/redux/constants';

export const LOGIN_SUCCEEDED = 'LOGIN_SUCCEEDED';
/**
 * Successfully authed (via login or refresh).
 * 1. Decode the jwt, grab the user profile info, and save that
 * 2. Save the jwt
 * @param  {JWT} jwt id token from auth0
 */
function loginSucceeded() {
  return {
    type: LOGIN_SUCCEEDED,
    meta: {
      event: createLoggedInEvent()
    }
  }
}

export const TOKENS_RECEIVED = 'TOKENS_RECEIVED';
/**
 * Successfully authed (via login or refresh).
 */
const jwtReceived = ({ accessToken, idToken }, auth0User: Auth0UserClaim) => {
  logger.setupUser(auth0User.details.traenaId, auth0User.email); // user id not provided on token
  return {
    type: TOKENS_RECEIVED,
    payload: { idToken, accessToken, auth0User },
  }
};

export const LOGIN_REQUESTING = 'LOGIN_REQUESTING';
export const LOGIN_BAD_CREDENTIALS = 'LOGIN_BAD_CREDENTIALS';
export const LOGIN_UNKNOWN_FAILURE = 'LOGIN_UNKNOWN_FAILURE';
export const LOGGED_OUT = 'LOGGED_OUT';

/**
 * Log the user in by authenticating and getting the user profile.
 * @param  {string} username
 * @param  {string} password
 * @return {Promise}
 */
const login = (username: string, password: string) => {
  return (dispatch: Dispatch): Promise<void> => {
    dispatch({type: LOGIN_REQUESTING});
    logger.logBreadcrumb(`logging in with ${username}`);
    return authApi.loginDB(username, password)
      .then(({ accessToken, idToken, refreshToken }) => {
        if (!accessToken || !idToken, !refreshToken) {
          logger.logBreadcrumb('invalid login response', { accessToken, idToken, refreshToken});
          return dispatch({type: LOGIN_UNKNOWN_FAILURE});
        }
        // Keychain gets updated
        Keychain.setRefreshToken(refreshToken);
        // Redux state gets updated
        const auth0User: Auth0UserClaim = jwtDecode(idToken)[config.auth0.userClaimNamespace];
        dispatch(loginSucceeded());
        return dispatch(jwtReceived({ accessToken, idToken }, auth0User));
      })
      .catch((error) => {
        if(error === errors.BAD_CREDENTIALS) {
          return dispatch({type: LOGIN_BAD_CREDENTIALS});
        } else {
          logger.log(error);
          return dispatch({type: LOGIN_UNKNOWN_FAILURE});
        }
      });
  };
};

/**
 * First dispatch an analytics event for logging the user out. This allows us to
 * send the event with authorization headers before they get cleared out.
 * Then we dispatch the event that clears all of the redux state.
 */
const loggedOut = () => {
  return (dispatch: Dispatch): void => {
    dispatch({
      type: 'ANALYTICS_ONLY_ACTION',
      meta: {
        event: createLoggedOutEvent(),
      },
    });
    dispatch({ type: LOGGED_OUT });
  }
}

/**
 * Clear redux state. Clear stored state. Clear credentials.
 */
const logout = () => {
  return (dispatch: Dispatch): void => {
    logger.logBreadcrumb('logging out');
    dispatch(deregisterPushNotifications()); // even if this fails, should be ok...
    dispatch(resetBadgeNotificationCount());

    // Must do anything that relies on being logged in before this.
    dispatch(loggedOut());

    // Must do anything that relies on state before this.
    AppStorage.clearState();
    Keychain.clearRefreshToken();
  };
};

/**
 * Refresh the ID TOKEN.
 * If refresh fails but the access token is still valid (not expired), then swallow the error.
 * If refresh fails AND the access token is invalid, do a full logout.
 */
const refreshJwt = () => {
  return (dispatch: Dispatch, getState: GetState): Promise<void> => {
    logger.logBreadcrumb('Refreshing JWT');

    return Keychain.getRefreshToken()
      .then((refreshToken) => {
        return authApi.refreshIdToken(refreshToken)
          .then(({ idToken, accessToken }) => {
            logger.logBreadcrumb('Refreshed JWT', { idToken, accessToken });
            const auth0User: Auth0UserClaim = jwtDecode(idToken)[config.auth0.userClaimNamespace];
            return dispatch(jwtReceived({ idToken, accessToken }, auth0User));
          });
      })
      .catch(() => {
        if(!isJwtValidSelector(getState())) {
          return dispatch(logout());
        }
      });
  };
};

export {
  login,
  logout,
  refreshJwt,
}
