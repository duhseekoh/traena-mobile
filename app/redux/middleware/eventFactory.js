// @flow
// Series of factory functions that are used throughout redux action creators to attach analytics
// events to actions. They get picked up by tracking middleware to send the analytics to our api.

const EVENT_TYPES = {
  USER_LOGGED_IN: 'USER_LOGGED_IN',
  USER_LOGGED_OUT: 'USER_LOGGED_OUT',
  APP_LAUNCHED: 'APP_LAUNCHED',
  APP_ACTIVATED: 'APP_ACTIVATED',
  APP_DEACTIVATED: 'APP_DEACTIVATED',
  SCREEN: 'SCREEN',
  VIDEO_PROGRESSED: 'VIDEO_PROGRESSED',
  CONTENT_VIEWED: 'CONTENT_VIEWED',
  CONTENT_IMPRESSIONED: 'CONTENT_IMPRESSIONED',
};

export type Event = {|
  type: $Values<typeof EVENT_TYPES>,
  data?: {
    timestamp: string,
    /*allows any other properties that we'd like to pass along*/
  },
|};

function getTimestamp() {
  return new Date().toISOString();
}

function createLoggedInEvent(): Event {
  return {
    type: EVENT_TYPES.USER_LOGGED_IN,
    data: {
      timestamp: getTimestamp(),
    },
  };
}

function createLoggedOutEvent(): Event {
  return {
    type: EVENT_TYPES.USER_LOGGED_OUT,
    data: {
      timestamp: getTimestamp(),
    },
  };
}

function createAppLaunchedEvent(): Event {
  return {
    type: EVENT_TYPES.APP_LAUNCHED,
    data: {
      timestamp: getTimestamp(),
    },
  };
}

function createAppActivatedEvent(): Event {
  return {
    type: EVENT_TYPES.APP_ACTIVATED,
    data: {
      timestamp: getTimestamp(),
    },
  };
}

function createAppDeactivatedEvent(): Event {
  return {
    type: EVENT_TYPES.APP_DEACTIVATED,
    data: {
      timestamp: getTimestamp(),
    },
  };
}

type ContentViewedEventParams = {
  contentId: number,
};
function createContentViewedEvent(params: ContentViewedEventParams): Event {
  return {
    type: EVENT_TYPES.CONTENT_VIEWED,
    data: {
      ...params,
      timestamp: getTimestamp(),
    },
  };
}

export type VideoProgressedEventParams = {
  contentId?: number,
  isAndroidFullscreen: boolean,
  percent: number,
};
function createVideoProgressedEvent(params: VideoProgressedEventParams): Event {
  return {
    type: EVENT_TYPES.VIDEO_PROGRESSED,
    data: {
      ...params,
      timestamp: getTimestamp(),
    }
  }
}

type ScreenChangedEventParams = {
  screenName: string,
  screenProperties: {},
};
function createScreenChangedEvent(params: ScreenChangedEventParams): Event {
  return {
    type: EVENT_TYPES.SCREEN,
    data: {
      name: params.screenName,
      ...params.screenProperties,
      timestamp: getTimestamp(),
    }
  };
}

export {
  EVENT_TYPES,
  createAppActivatedEvent,
  createAppDeactivatedEvent,
  createAppLaunchedEvent,
  createContentViewedEvent,
  createLoggedInEvent,
  createLoggedOutEvent,
  createScreenChangedEvent,
  createVideoProgressedEvent,
}
