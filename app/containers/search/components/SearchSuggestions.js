// @flow
import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import { TRATextStrong } from 'app/components';
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import type { Suggestion } from 'app/redux/modules/search/types';

type Props = {
  style?: ViewStyleProp,
  title: string,
  suggestions: Suggestion[],
  onSuggestionSelected: (suggestion: any) => void,
};

const SearchSuggestions = ({style, title, suggestions = [], onSuggestionSelected}: Props) => {
  return (
    <View style={style}>
      <TRATextStrong style={styles.title}>{title}</TRATextStrong>
      <View>
        {
          suggestions.map((suggestion) => {
            return _renderSuggestion(suggestion, onSuggestionSelected);
          })
        }
      </View>
    </View>
  )
}

const _renderSuggestion = (suggestion, onSuggestionSelected) => {
  return (
    <TouchableOpacity
      style={styles.suggestionButton}
      onPress={() => onSuggestionSelected(suggestion.value)}
      key={suggestion.key}>
        <TRATextStrong style={styles.suggestionTitle}>
          {suggestion.title}
        </TRATextStrong>
        {suggestion.subtitle && <TRATextStrong style={styles.suggestionSubtitle}>
          {suggestion.subtitle}
        </TRATextStrong>}
    </TouchableOpacity>)
};

const styles = StyleSheet.create({
  title: {
    color: styleVars.color.darkGrey
  },
  suggestionButton: {
    marginTop: 8,
    marginLeft: 2
  },
  suggestionTitle: {
    fontSize: styleVars.fontSize.large,
    color: styleVars.color.darkGrey
  },
  suggestionSubtitle: {
    fontSize: styleVars.fontSize.medlar,
    color: styleVars.color.mediumGrey
  },
});

export default SearchSuggestions;
