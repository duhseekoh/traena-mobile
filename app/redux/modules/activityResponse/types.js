// @flow

type PollBody = {
  selectedChoiceId: string,
};

type QuestionSetBody = {
  progress: number,
};

type ResponseBody = QuestionSetBody | PollBody;

type ActivityResponse = {
  id: number,
  activityId: number,
  userId: number,
  version: number,
  body: ResponseBody
};

type PollResponse = {
  id: number,
  activityId: number,
  userId: number,
  version: number,
  body: PollBody
};

type QuestionSetResponseData = {
  correctFirstTry: boolean[],
};

type ActivityResponseData = QuestionSetResponseData;

export type { ActivityResponse, PollResponse, ResponseBody, ActivityResponseData } ;
