// @flow
import api from 'app/redux/api';
import type { Activity } from 'app/redux/modules/activity/types';
import type { Dispatch } from 'app/redux/constants';

const RETRIEVED_ACTIVITIES = 'RETRIEVED_ACTIVITIES';

const activitiesRetrieved = (activities: { index: Activity[] }) => {
  return {
    type: RETRIEVED_ACTIVITIES,
    payload: activities.index
  };
};

const RETRIEVED_ACTIVITY_RESULTS = 'RETRIEVED_ACTIVITY_RESULTS';

const activityResultsRetrieved = (activityResults) => {
  return {
    type: RETRIEVED_ACTIVITY_RESULTS,
    payload: activityResults
  };
};

const getActivitiesByContentId = (contentId: number) => {
  return async (dispatch: Dispatch) => {
    const activities = await api.activity.retrieveActivitiesByContentId(contentId);
    return dispatch(activitiesRetrieved(activities));
  };
};

const getActivityResults = (contentId: number, activityId: number) => {
  return async (dispatch: Dispatch) => {
    const results = await api.activity.retrieveActivitiesResults(contentId, activityId);
    return dispatch(activityResultsRetrieved({[activityId]: results}));
  };
};

export {
  RETRIEVED_ACTIVITIES,
  RETRIEVED_ACTIVITY_RESULTS,
  getActivitiesByContentId,
  getActivityResults,
}
