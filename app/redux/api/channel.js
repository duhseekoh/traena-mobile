// @flow
import { getChannelsTpl } from './util/endpoints';

const retrieveChannels = (page: number = 0, size: number = 100) => {
  return fetch(getChannelsTpl({ page, size }));
};

export default {
  retrieveChannels,
}
