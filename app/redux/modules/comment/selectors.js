import _ from 'lodash';
import moment from 'moment';
import { createSelector } from 'reselect';

const STATE_KEY = 'comment';
const stateFromRoot = (state) => state[STATE_KEY];

const commentsSelector = (state) => state.comment.comments;

const commentsGroupByContentSelector = createSelector([
  commentsSelector
], (comments) => {
  // Sort all comments by created date, in ascending order
  const commentsDateSorted = _.sortBy(Object.values(comments), [
    ({ createdTimestamp }) => -moment(createdTimestamp).valueOf()
  ]);

  // Group comments by content id (siloed comments retain sort order)
  const groups = _.groupBy(Object.values(commentsDateSorted), 'contentId');
  return groups;
});

const hasOlderSelector = createSelector([
  (state) => stateFromRoot(state).hasOlder
], (hasOlder) => hasOlder);

export {
  commentsGroupByContentSelector,
  hasOlderSelector
};
