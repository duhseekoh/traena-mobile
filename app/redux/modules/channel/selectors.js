// @flow
import type { ReduxState } from 'app/redux/constants';

const STATE_KEY = 'channel';
const stateFromRoot = (state: ReduxState) => state[STATE_KEY];

export const selectChannelsById = (state: ReduxState) => stateFromRoot(state).channels;

export const selectChannels = (state: ReduxState, props: { channelIds: number[] }) => {
  const channelsById = selectChannelsById(state);
  return props.channelIds.map(channelId => channelsById[channelId]);
};
