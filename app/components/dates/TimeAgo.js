// @flow
import React from 'react';
import {
  StyleSheet,
} from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import type moment from 'moment';
import {
  TRATextStrong,
} from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  style?: ViewStyleProp,
  time: moment;
};

const TimeAgo = ({ style, time }: Props) => {
  return (
    <TRATextStrong style={[styles.time, style]}>
      {time.fromNow()}
    </TRATextStrong>
  );
}

export default TimeAgo;

const styles = StyleSheet.create({
  time: {
    color: styleVars.color.mediumGrey,
    fontSize: styleVars.fontSize.regular,
  }
});
