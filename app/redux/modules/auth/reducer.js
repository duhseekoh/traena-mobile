// @flow
import { TOKENS_RECEIVED, LOGIN_REQUESTING, LOGIN_BAD_CREDENTIALS, LOGIN_UNKNOWN_FAILURE } from './actions';
import requestStatesEnum from './requestStatesEnum';
import type { Action } from 'app/types/Action';

export type ReduxStateAuth = {
  isLoggedIn: boolean,
  loginRequestState: any,
  idToken: any,
  accessToken: any,
};

const ACTION_HANDLERS = {
  [LOGIN_REQUESTING]: (state: ReduxStateAuth) => {
    return {
      ...state,
      loginRequestState: requestStatesEnum.IN_PROGRESS,
    }
  },
  [TOKENS_RECEIVED]: (state: ReduxStateAuth, action: Action) => {
    const { idToken, accessToken } = action.payload;
    return {
      isLoggedIn: !!accessToken,
      accessToken: accessToken,
      idToken: idToken,
      loginRequestState: requestStatesEnum.SUCCESS,
    };
  },
  [LOGIN_BAD_CREDENTIALS]: (state: ReduxStateAuth) => {
    return {
      ...state,
      loginRequestState: requestStatesEnum.BAD_CREDENTIALS_FAILURE,
    };
  },
  [LOGIN_UNKNOWN_FAILURE]: (state: ReduxStateAuth) => {
    return {
      ...state,
      loginRequestState: requestStatesEnum.UNKNOWN_FAILURE,
    };
  }

};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isLoggedIn: false,
  loginRequestState: null,
  idToken: null,
  accessToken: null,
};

export default function auth(state: ReduxStateAuth = initialState, action: Action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}
