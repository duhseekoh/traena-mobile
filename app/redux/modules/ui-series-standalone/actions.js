// @flow
import api from 'app/redux/api';
import { contentsFetchSucceeded } from 'app/redux/modules/content/actions';
import { getSeriesProgress } from 'app/redux/modules/series/actions';
import { currentPageSelector } from './selectors';
import type { IndexedPage } from '../../../types/Pagination';
import type { ContentItem } from '../content/types';
import type { Dispatch, GetState } from 'app/redux/constants';

export const REFRESH_SERIES_CONTENT_REQUEST_STARTED = 'REFRESH_SERIES_CONTENT_REQUEST_STARTED';
const startedRefreshSeriesContentRequest = (seriesId: number) => {
  return {
    type: REFRESH_SERIES_CONTENT_REQUEST_STARTED,
    payload: seriesId,
  };
};

export const REFRESH_SERIES_CONTENT_REQUEST_FAILED = 'REFRESH_SERIES_CONTENT_REQUEST_FAILED';
const failedRefreshSeriesContentRequest = (seriesId: number) => {
  return {
    type: REFRESH_SERIES_CONTENT_REQUEST_FAILED,
    payload: seriesId,
  };
};

export const LOAD_MORE_SERIES_CONTENT_REQUEST_STARTED = 'LOAD_MORE_SERIES_CONTENT_REQUEST_STARTED';
const startedLoadMoreSeriesContentRequest = (seriesId: number) => {
  return {
    type: LOAD_MORE_SERIES_CONTENT_REQUEST_STARTED,
    payload: seriesId,
  };
};

export const LOAD_MORE_SERIES_CONTENT_REQUEST_FAILED = 'LOAD_MORE_SERIES_CONTENT_REQUEST_FAILED';
const failedLoadMoreSeriesContentRequest = (seriesId: number) => {
  return {
    type: LOAD_MORE_SERIES_CONTENT_REQUEST_FAILED,
    payload: seriesId,
  };
};

export const LOADED_MORE_SERIES_CONTENT = 'LOADED_MORE_SERIES_CONTENT';
function loadedMoreSeriesContent(contentPage: IndexedPage<number, ContentItem>, seriesId: number) {
  return {
    type: LOADED_MORE_SERIES_CONTENT,
    payload: { contentPage, seriesId }
  };
}

export const REFRESHED_SERIES_CONTENT = 'REFRESHED_SERIES_CONTENT';
function refreshedSeriesContent(contentPage: IndexedPage<number, ContentItem>, seriesId: number) {
  return {
    type: REFRESHED_SERIES_CONTENT,
    payload: { contentPage, seriesId }
  };
}

export const refreshSeriesContent = (seriesId: number) => {
  return async (dispatch: Dispatch) => {
    dispatch(startedRefreshSeriesContentRequest(seriesId));
    try {
      // set and forget a call to get users progress for a series
      dispatch(getSeriesProgress(seriesId));
      // get all that content, to be displayed in the list
      const results = await api.series.retrieveContentForSeries(seriesId);
      dispatch(contentsFetchSucceeded(results.index));
      dispatch(refreshedSeriesContent(results, seriesId));
    } catch (err) {
      dispatch(failedRefreshSeriesContentRequest(seriesId));
    }
  };
}

export const loadMoreSeriesContent = (seriesId: number) => {
  return (dispatch: Dispatch, getState: GetState) => {
    dispatch(startedLoadMoreSeriesContentRequest(seriesId));
    const page = currentPageSelector(getState(), { id: seriesId }) + 1;
    return api.series.retrieveContentForSeries(seriesId, page)
      .then(results => {
        dispatch(contentsFetchSucceeded(results.index));
        dispatch(loadedMoreSeriesContent(results, seriesId));
      })
      .catch(() => {
      dispatch(failedLoadMoreSeriesContentRequest(seriesId));
      });
  };
};
