// @flow
import _ from 'lodash';
import { CREATED_ACTIVITY_RESPONSE, RETRIEVED_ACTIVITY_RESPONSE } from './actions';
import type { Action } from 'app/types/Action';
import type { ActivityResponse } from './types';

export type ReduxStateActivityResponse = {
  activityResponses: ActivityResponse[],
};

const ACTION_HANDLERS = {
  [CREATED_ACTIVITY_RESPONSE]: (state: ReduxStateActivityResponse, action: Action) => {
    return {
      ...state,
      activityResponses: [...state.activityResponses, action.payload],
    };
  },
  [RETRIEVED_ACTIVITY_RESPONSE]: (state: ReduxStateActivityResponse, action: Action) => {
    const mergedResponses = _.uniqBy([...state.activityResponses, ...action.payload], 'id');
    return {
      ...state,
      activityResponses: mergedResponses,
    };
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  activityResponses: [],
};

const activityResponseReducer = (state: ReduxStateActivityResponse = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default activityResponseReducer;
