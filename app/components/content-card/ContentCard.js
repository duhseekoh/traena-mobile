// @flow
import React from 'react';
import moment from 'moment';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import { TRATextStrong, TRAText, TimeAgo } from 'app/components';
import styleVars from 'app/style/variables';
import type { ContentItem } from 'app/redux/modules/content/types';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { CONTENT_TYPES } from 'app/redux/modules/content/constants';

type Props = {
  style?: ViewStyleProp,
  contentItem: ContentItem,
  onPress: (contentId: number) => any,
};

const ContentCard = ({ onPress, style, contentItem }: Props) => {
  const { title, id, text, publishedTimestamp } = contentItem;

  let cardBgSource;
  if (contentItem.type === CONTENT_TYPES.TRAINING_VIDEO) {
    cardBgSource = { uri: contentItem.video.previewImageURI };
  } else if (
    contentItem.type === CONTENT_TYPES.IMAGE &&
    contentItem.images[0]
  ) {
    const image = contentItem.images[0];
    cardBgSource = {
      uri: `${image.baseCDNPath}${image.variations.large.filename}`,
    };
  }

  return (
    <View style={[styles.container, style]}>
      <TouchableOpacity
        style={styles.touchable}
        onPress={() => onPress(id)}
        delayPressIn={100}
        activeOpacity={0.8}
      >
        {/* When its video or image content, this gets that image */}
        <ImageBackground
          style={styles.card}
          source={cardBgSource}
          resizeMode={'cover'}
        >
          {/* Always display a translucent gradient, and when there is no image
            put a solid blue-green behind it
          */}
          <ImageBackground
            style={[
              styles.card,
              !cardBgSource && { backgroundColor: styleVars.color.blueGreen },
            ]}
            source={require('app/images/overlay.png')}
            resizeMode={'cover'}
          >
            <View style={styles.cardContent}>
              <TRATextStrong
                numberOfLines={1}
                ellipsizeMode={'tail'}
                style={styles.cardTitle}
              >
                {title}
              </TRATextStrong>
              {text && (
                <TRAText
                  style={styles.cardText}
                  numberOfLines={2}
                  ellipsizeMode={'tail'}
                >
                  {text}
                </TRAText>
              )}
              <TimeAgo
                time={moment(publishedTimestamp)}
                style={styles.cardTime}
              />
            </View>
          </ImageBackground>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    aspectRatio: 1.6,
    borderRadius: 8,
    marginVertical: 5,
    ...styleVars.shadow.shadowThree,
  },
  touchable: {
    borderRadius: 8,
    overflow: 'hidden',
  },
  card: {
    width: '100%',
    height: '100%',
  },
  cardContent: {
    padding: 15,
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  cardTitle: {
    fontFamily: styleVars.fontFamily.medium,
    fontSize: styleVars.fontSize.xlarge,
    color: styleVars.color.white,
    marginBottom: 5,
  },
  cardText: {
    fontFamily: styleVars.fontFamily.medium,
    fontSize: styleVars.fontSize.medium,
    color: styleVars.color.white,
    marginBottom: 5,
  },
  cardTime: {
    fontFamily: styleVars.fontFamily.base,
    fontSize: styleVars.fontSize.small,
    color: styleVars.color.white,
  },
});

export default ContentCard;
