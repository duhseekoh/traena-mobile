// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import {
  TRAText,
} from 'app/components';
import type { User } from 'app/redux/modules/user/types';
import { getFullName } from 'app/redux/modules/user/util';
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style: ViewStyleProp,
  user: User,
};

function UserSummary(props: Props) {
  const { user } = props;
  return (
    <View style={props.style}>
      <View style={styles.nameContainer}>
        <TRAText numberOfLines={3} style={styles.name}>{getFullName(user)}</TRAText>
        {user.organization &&
          <View style={styles.positionLine}>
            <TRAText style={styles.position}>{user.organization.name}</TRAText>
          </View>
        }
        <View style={styles.positionLine}>
          <TRAText style={styles.position}>{user.position}</TRAText>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  nameContainer: {
    justifyContent: 'flex-start',
    flexGrow: 1,
    paddingHorizontal: 10,
    paddingTop: 13,
  },
  name: {
    fontSize: styleVars.fontSize.xlarger,
    fontFamily: styleVars.fontFamily.bold,
    color: styleVars.color.white,
    paddingBottom: 5,
  },
  positionLine: {
    marginTop: 5,
    flexDirection: 'row',
  },
  position: {
    marginBottom: 0,
    marginLeft: 1,
    fontSize: styleVars.fontSize.medlar,
    fontFamily: styleVars.fontFamily.medium,
    color: styleVars.color.black,
  },
});

export default UserSummary;
