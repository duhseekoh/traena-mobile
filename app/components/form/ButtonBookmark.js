// @flow
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { FeatherIcon, TraenaIcon } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  style?: ViewStyleProp,
  onPress: () => *,
  isBookmarked?: boolean,
}

const ButtonBookmark = ({style, onPress, isBookmarked = false}: Props) => {
  const iconStyles = isBookmarked ? [styles.icon, styles.iconBookmarked] : [styles.icon];

  const icon = isBookmarked ?
    <TraenaIcon name='bookmark-filled' style={iconStyles}/> :
    <FeatherIcon name='bookmark' style={iconStyles} />;

  return (
    <TouchableOpacity onPress={onPress} style={style}
      hitSlop={{top: 10, left: 10, right: 10, bottom: 10}}>
      <View style={styles.button}>{icon}</View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: styleVars.color.transparent,
  },
  icon: {
    color: styleVars.color.regular,
    fontSize: styleVars.iconSize.large,
  },
  iconBookmarked: {
    color: styleVars.color.traenaPrimaryGreen,
    fontSize: styleVars.iconSize.large,
  },
});

export default ButtonBookmark;
