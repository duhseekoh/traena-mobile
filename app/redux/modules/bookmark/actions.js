// @flow
import api from 'app/redux/api';
import type { Dispatch } from 'app/redux/constants';

export const RETRIEVED_BOOKMARK_IDS = 'RETRIEVED_BOOKMARK_IDS';
function retrievedBookmarkIds(bookmarks) {
  return {
    type: RETRIEVED_BOOKMARK_IDS,
    payload: bookmarks
  };
}

export const ADDED_BOOKMARK = 'ADDED_BOOKMARK';
function addedBookmark(contentId) {
  return {
    type: ADDED_BOOKMARK,
    payload: contentId
  };
}

export const REMOVED_BOOKMARK = 'REMOVED_BOOKMARK';
function removedBookmark(contentId) {
  return {
    type: REMOVED_BOOKMARK,
    payload: contentId
  };
}


export const retrieveBookmarkIds = () => {
  return async (dispatch: Dispatch) => {
    try {
      const results = await api.bookmark.retrieveBookmarkIds();
      return dispatch(retrievedBookmarkIds(results));
    } catch(error) {
      console.log(error); // fetchCatch will notify bugsnag, so just extra dev log here
    }
  }
}

export const addBookmark = (contentId: number) => {
  return (dispatch: Dispatch) => {
    dispatch(addedBookmark(contentId));
    return api.bookmark.addBookmark(contentId)
      .catch(() => {
        dispatch(removedBookmark(contentId));
    });
  };
};

export const deleteBookmark = (contentId: number) => {
  return (dispatch: Dispatch) => {
    dispatch(removedBookmark(contentId));
    return api.bookmark.deleteBookmark(contentId)
      .catch(() => {
        dispatch(addedBookmark(contentId));
    });
  };
};
