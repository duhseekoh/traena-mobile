// @flow
import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  View,
} from 'react-native';
import {
  TRATextStrong,
} from 'app/components';
import SeriesPreviewContainer from 'app/containers/series/SeriesPreviewContainer';
import type { ContentItem } from 'app/redux/modules/content/types';
import type { Series } from 'app/redux/modules/series/types';
import styleVars from 'app/style/variables';

type Props = {
  loadNextPage: () => void,
  refreshSeriesList: () => void,
  canLoadMore: boolean,
  isLoadingMore: boolean,
  isRefreshing: boolean,
  displayNoMoreResults: boolean,
  // TODO: `listProp` should have a type of FlatListProps; fix the import of this type
  // something like `import type { Props as FlatListProps } from 'react-native/Libraries/Lists/FlatList';`, maybe
  listProps?: any,
  refreshIconOffset: number,
  seriesListWithContent: { series: Series, previewContent: ContentItem[]}[],
};

class SeriesPreviewList extends Component<Props> {
  props: Props;
  render() {
    const { refreshSeriesList, isRefreshing = false, listProps, seriesListWithContent, refreshIconOffset } = this.props;
    return (
      <View style={{flex: 1}}>
        <FlatList
          horizontal={false}
          scrollEnabled={true}
          data={seriesListWithContent}
          keyExtractor={this._keyExtractor}
          removeClippedSubviews={true}
          // ItemSeparatorComponent={() => <ListItemSeparator />}
          // needs to be greater than 0 or android wont trigger onEndReached
          onEndReachedThreshold={0.1}
          scrollEventThrottle={1}
          // disable virtualization because this unmounts views
          // that are off screen causing glitchy jumps
          disableVirtualization={true}
          refreshControl={
            <RefreshControl
              // android only && only if there is a top banner
              progressViewOffset={refreshIconOffset}
              refreshing={isRefreshing}
              onRefresh={refreshSeriesList}
            />
          }
          ListFooterComponent={this._renderFooter}
          renderItem={this._renderItem}
          style={styles.list}
          onEndReached={this._loadMoreItems}
          {...listProps}
        />
      </View>
    );
  }

  _renderItem = ({item}: {item: { series: Series, previewContent: ContentItem[]}}) => {
    return (
      <View style={styles.itemHolder}>
        <SeriesPreviewContainer
          series={item.series}
          contentItems={item.previewContent}
        />
      </View>
  )}

  _renderFooter = () => {
    const { isLoadingMore, displayNoMoreResults } = this.props;
    return (
      <View>
        { isLoadingMore && (
          <ActivityIndicator
            size="small"
            color={styleVars.color.traenaPrimaryGreen}
            style={{paddingVertical: 50}}
          />
        )}
        { displayNoMoreResults && <TRATextStrong style={styles.endOfList}>no more results</TRATextStrong> }
      </View>
    )
  }

  _loadMoreItems = () => {
    if(!this.props.isRefreshing && !this.props.isLoadingMore && this.props.canLoadMore) {
      this.props.loadNextPage();
    }
  }

  _keyExtractor = (item) => Number(item.series.id).toString();

}

export default SeriesPreviewList;

const styles = StyleSheet.create({
  itemHolder: {
    marginVertical: 10,

  },
  list: {
    flex: 1,
    backgroundColor: styleVars.color.white,
  },
  endOfList: {
    textAlign: 'center',
    paddingVertical: 50,
    color: styleVars.color.mediumGrey,
  }
});
