// @flow
import bookmark from './bookmark';
import activity from './activity';
import activityResponse from './activityResponse';
import contents from './contents';
import feed from './feed';
import search from './search';
import series from './series';
import traenaList from './traenaList';
import notifications from './notifications';
import user from './user';

export default {
  activity,
  activityResponse,
  bookmark,
  contents,
  feed,
  notifications,
  search,
  series,
  traenaList,
  user,
}
