// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import {
  FeatherIcon,
} from 'app/components';
import QuestionSetContainer from './question-set/QuestionSetContainer';
import PollContainer from './poll/PollContainer';
import styleVars from 'app/style/variables';
import { exitActivity } from 'app/redux/modules/navigation/actions';
import { createActivityResponse } from 'app/redux/modules/activityResponse/actions';
import { activityProgressSelector } from 'app/redux/modules/ui-activity/questionSetSelectors';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import type { ResponseBody } from 'app/redux/modules/activityResponse/types';
import type { Activity } from 'app/redux/modules/activity/types';
import { ACTIVITY_TYPES } from 'app/redux/modules/activity/types';
import type { Dispatch } from 'app/redux/constants';

type OwnProps = {
  activity: Activity,
  style?: ViewStyleProp,
};

type ConnectedProps = {
  createActivityResponse: (activityId: number, body: ResponseBody) => any,
  exitActivity: () => any,
  progress: number,
};

type Props = OwnProps & ConnectedProps;
/*
 *  Container for an Activity that can be added to a ContentItem
 *  Can be one of the following types: QuestionSet
 */
class ActivityContainer extends Component<Props> {
  props: Props;

  _exitActivity = () => {
    const { activity, exitActivity, progress, createActivityResponse} = this.props;
    if(activity.type === ACTIVITY_TYPES.QUESTION_SET) {
      createActivityResponse(activity.id, { progress });
    }
    exitActivity();
  }

  render() {
    const { activity } = this.props;
    let content, barStyle, iconColor;

    if(activity.type === ACTIVITY_TYPES.QUESTION_SET) {
      barStyle = 'dark-content';
      iconColor = styleVars.color.black;

      content = (<View style={{flex: 1}}>
          <QuestionSetContainer activityId={activity.id} exitActivity={this._exitActivity} />
        </View>);
    } else if(activity.type === ACTIVITY_TYPES.POLL) {
      barStyle = 'light-content';
      iconColor = styleVars.color.white;

      content = <PollContainer
        activityId={activity.id}
        exitActivity={this._exitActivity} />
    } else { console.log ("im not a known type", activity.type)}

    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor={styleVars.color.translucentBGgreen} translucent={true} barStyle={barStyle} />
        {content}
        <View style={{position: 'absolute', top: 34, right: 20}}>
          <TouchableOpacity onPress={this._exitActivity}
            hitSlop={{top: 20, left: 20, right: 20, bottom: 20}}>
              <FeatherIcon name='x' fontSize={styleVars.iconSize.medlar} color={iconColor} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state, props: OwnProps) => {
  return {
    progress: activityProgressSelector(state, { activityId: props.activity.id }),
  }
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    createActivityResponse: (activityId, body) =>
      dispatch(createActivityResponse(activityId, body)),
    exitActivity: () =>
      dispatch(exitActivity()),
  };
};

const ConnectedActivityContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ActivityContainer);

export default ConnectedActivityContainer;
