// @flow
import type { User } from 'app/redux/modules/user/types';

type SearchResult = User;
type SearchResults = SearchResult[];

export type {
  SearchResult,
  SearchResults,
};
