// @flow
import _ from 'lodash';
import {
  COMPLETION_CREATED,
  COMPLETIONS_RETREIVED,
} from './actions';

import type { Action } from 'app/types/Action';

export type ReduxStateTraenaTask = {
  completeContentIds: number[],
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  completeContentIds: [],
};

const traenaTaskReducer = (state: ReduxStateTraenaTask = initialState, action: Action) => {
  switch(action.type) {
    case COMPLETION_CREATED: {
      const { contentId } = action.payload;
      return Object.assign({}, state, {
        completeContentIds: _.uniq([contentId, ...state.completeContentIds])
      });
    }
    case COMPLETIONS_RETREIVED: {
      const { contentIds } = action.payload;
      const mergedContentIds = _.uniq([...state.completeContentIds, ...contentIds]);
      return Object.assign({}, state, {
        completeContentIds: mergedContentIds,
      });
    }
    default:
      return state;
  }
}

export default traenaTaskReducer;
