// @flow
import React from 'react';
import {
  View,
  StyleSheet
} from 'react-native';
import styleVars from 'app/style/variables';

const SectionSeparator = () => {
  return (
    <View style={styles.container}>
      <View style={styles.separator}/>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 25
  },
  separator: {
    backgroundColor: styleVars.color.lightGrey2,
    width: 160,
    height: 5,
    borderRadius: 5
  }
});

export default SectionSeparator;
