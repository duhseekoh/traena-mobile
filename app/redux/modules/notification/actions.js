// @flow
import notificationsApi from 'app/redux/api/notifications';
import moment from 'moment';
import _ from 'lodash';
import PushNotification from 'react-native-push-notification';
import { goToContentStandalone } from 'app/redux/modules/navigation/actions';
import { notificationsSelector, deviceTokenSelector } from 'app/redux/modules/notification/selectors';
import logger from 'app/logging/logger';
import config from 'app/config';
import type { Notification } from 'app/redux/modules/notification/types';
import type { Dispatch, GetState } from 'app/redux/constants';

const GET_NOTIFICATION_REQUEST_STARTED = 'GET_NOTIFICATION_REQUEST_STARTED';
const startedNotificationRequest = () => {
  return {
    type: GET_NOTIFICATION_REQUEST_STARTED,
  };
};

const GET_NOTIFICATION_REQUEST_FINISHED = 'GET_NOTIFICATION_REQUEST_FINISHED';
const finishedNotificationRequest = () => {
  return {
    type: GET_NOTIFICATION_REQUEST_FINISHED,
  };
};

const RETRIEVED_NOTIFICATIONS = 'RETRIEVED_NOTIFICATIONS';
const notificationsRetrieved = (notifications: {[number]: Notification}) => {
  return {
    type: RETRIEVED_NOTIFICATIONS,
    payload: notifications
  };
};

const REFRESHED_NOTIFICATIONS = 'REFRESHED_NOTIFICATIONS';
const notificationsRefreshed = (notifications: {[number]: Notification}) => {
  return {
    type: REFRESHED_NOTIFICATIONS,
    payload: notifications
  };
};

const UNACKNOWLEDGED_COUNT_RETRIEVED = 'UNACKNOWLEDGED_COUNT_RETRIEVED';
const unacknowledgedCountRetrieved = (count: Object) => {
  return {
    type: UNACKNOWLEDGED_COUNT_RETRIEVED,
    payload: count
  };
};

const getRecentNotifications = () => {
  return (dispatch: Dispatch) => {
    dispatch(startedNotificationRequest());
    return notificationsApi.retrieveNotifications()
      .then((notifications) => {
        dispatch(notificationsRefreshed(notifications));
      })
      .finally(() => {
        dispatch(finishedNotificationRequest());
      });
  };
};

const getOlderNotifications = () => {
  return (dispatch: Dispatch, getState: GetState) => {
    const allNotifications = notificationsSelector(getState());
    const oldestNotification = _.last(allNotifications);
    const maxDate = moment(oldestNotification.createdTimestamp);

    dispatch(startedNotificationRequest());
    return notificationsApi.retrieveNotifications(maxDate)
      .then((notifications) => {
        dispatch(notificationsRetrieved(notifications));
      })
      .finally(() => {
        dispatch(finishedNotificationRequest());
      });
  };
};

const openNotification = (notification: Notification) => {
  return (dispatch: Dispatch) => {
    // Open a notification that has a piece of content associated to it
    if(notification.contentId) {
      dispatch(goToContentStandalone(notification.contentId));
    }
  }
};

const getUnacknowledgedNotificationsCount = () => {
  return (dispatch: Dispatch) => {
    return notificationsApi.retrieveUnacknowledgedNotificationsCount()
      .then(({ count }) => {
        dispatch(unacknowledgedCountRetrieved(count));
        if(count > 0) {
          dispatch(getRecentNotifications());
        }
      });
  };
};

// --------------------
// Push Notifications
// --------------------

const DEVICE_TOKEN_RECEIVED = 'DEVICE_TOKEN_RECEIVED';
const deviceTokenReceived = (token: string) => {
  return {
    type: DEVICE_TOKEN_RECEIVED,
    payload: token,
  }
};

const configurePushNotifications = () => {

  return (dispatch: Dispatch) => {
    PushNotification.configure({
      // Called when Token is generated (iOS and Android)
      onRegister: ({token, os}) => {
        console.log( 'TOKEN:', token, os );
        dispatch(deviceTokenReceived(token));
        notificationsApi.registerNotificationsForDevice(token, os);
      },
      // Called when a remote or local notification is opened or received
      onNotification: (notification) => {
        console.log( 'NOTIFICATION:', notification );
        dispatch(pushNotificationReceived());
      },
      senderID: config.fcm.senderId, // ANDROID ONLY
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },
      popInitialNotification: true,
      requestPermissions: true,
    });
  };
};

/**
 * User is probably logging out, so this is called to deregister the device they are
 * using from the db and SNS.
 */
const deregisterPushNotifications = () => {
  return (dispatch: Dispatch, getState: GetState) => {
    const deviceToken = deviceTokenSelector(getState());
    if(deviceToken) {
      return notificationsApi.deregisterNotificationsForDevice(deviceToken);
    }
  };
};

/**
 * When a push notification is received, we will request the most recent notifications.
 * We also request the count of unack notifications so we can display that there
 * are some notifications that are not read yet.
 */
const pushNotificationReceived = () => {
  return (dispatch: Dispatch) => {
    return dispatch(getUnacknowledgedNotificationsCount());
  };
};

const PUSH_NOTIFICATIONS_ACKNOWLEDGED = 'PUSH_NOTIFICATIONS_ACKNOWLEDGED';
/**
 * Reset the badge count to 0.
 * Let the api know this user has seen their notifications.
 */
const acknowledgeNotifications = () => {
  return (dispatch: Dispatch) => {
    dispatch(resetBadgeNotificationCount());
    dispatch({
      type: PUSH_NOTIFICATIONS_ACKNOWLEDGED
    });
    return notificationsApi.acknowledgeNotifications().catch((err) => {
      logger.logBreadcrumb('Failed acknowledging notifications', err);
    });
  };
};

const resetBadgeNotificationCount = () => {
  return () => {
    PushNotification.setApplicationIconBadgeNumber(0);
  };
};

export {
  DEVICE_TOKEN_RECEIVED,
  GET_NOTIFICATION_REQUEST_STARTED,
  GET_NOTIFICATION_REQUEST_FINISHED,
  PUSH_NOTIFICATIONS_ACKNOWLEDGED,
  RETRIEVED_NOTIFICATIONS,
  REFRESHED_NOTIFICATIONS,
  UNACKNOWLEDGED_COUNT_RETRIEVED,
  acknowledgeNotifications,
  configurePushNotifications,
  deregisterPushNotifications,
  getOlderNotifications,
  getRecentNotifications,
  getUnacknowledgedNotificationsCount,
  openNotification,
  resetBadgeNotificationCount,
}
