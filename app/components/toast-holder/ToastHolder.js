/* @flow */
import React from 'react';
import type { Node } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';

type Props = {
  children: Node,
};

const ToastHolder = ({ children }: Props) => (
  <View style={styles.toastHolder}>
    {children}
  </View>
);

const styles = StyleSheet.create({
  toastHolder: {
    position: 'absolute',
    top:0,
    left:0,
    paddingTop: 20,
    width: '100%',
  },
});

export default ToastHolder;
