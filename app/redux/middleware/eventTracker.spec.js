import 'react-native';
import { EVENT_TYPES, createScreenChangedEvent, createLoggedInEvent } from './eventFactory';
import eventTracker from './eventTracker';

describe('sending segment events', () => {

  const trackFn = jest.fn();
  const mw = eventTracker(trackFn);
  const nextFnMock = jest.fn();

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should call track for an action with an event on it', () => {
    const action = {
      type: 'DOESNT_MATTER',
      meta: {
        event: createLoggedInEvent(),
      },
    };
    mw()(nextFnMock)(action);
    expect(trackFn).toHaveBeenCalledTimes(1);
    expect(trackFn).lastCalledWith({
      type: EVENT_TYPES.USER_LOGGED_IN,
      data: {
        timestamp: expect.any(String),
      }
    });
    // Action should curry through
    expect(nextFnMock).lastCalledWith(action);
  });

  it('should call track for an action with an event containing a payload', () => {
    const action = {
      type: 'DOESNT_MATTER',
      meta: {
        event: createScreenChangedEvent({ screenName: 'SOME_SCREEN', screenProperties: { isBestScreen: true }}),
      }
    };
    mw()(nextFnMock)(action);
    expect(trackFn).toHaveBeenCalledTimes(1);
    expect(trackFn).lastCalledWith({
      type: 'SCREEN',
      data: {
        name: 'SOME_SCREEN',
        isBestScreen: true,
        timestamp: expect.any(String),
      },
    });
  });

  it('should not call track if no event exists on the action', () => {
    const action = {
      type: 'DOESNT_MATTER',
      meta: {/*empty*/}
    };
    mw()(nextFnMock)(action);
    expect(trackFn).toHaveBeenCalledTimes(0);
    expect(nextFnMock).lastCalledWith(action);
  });

});
