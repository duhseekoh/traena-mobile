// @flow
import { REFRESHED_COMMENTS, RETRIEVED_COMMENTS, ADDED_COMMENT, DELETED_COMMENT } from './actions';
import type { Action } from 'app/types/Action';
import type { Comment } from './types';

export type ReduxStateComment = {
  comments: {[number]: Comment},
  hasOlder: boolean,
};

const ACTION_HANDLERS = {
  [REFRESHED_COMMENTS]: (state: ReduxStateComment, action: Action) => {
    const { index: comments, last } = action.payload;

    return {
      ...state,
      comments,
      hasOlder: !last,
    }
  },
  [RETRIEVED_COMMENTS]: (state: ReduxStateComment, action: Action) => {
    const { index: comments, last } = action.payload;

    return {
      ...state,
      comments: {
        ...state.comments,
        ...comments,
      },
      hasOlder: !last,
    }
  },
  [ADDED_COMMENT]: (state: ReduxStateComment, action: Action) => {
    const comment = action.payload;
    const mergedComments = Object.assign({},
      state.comments,
      {[comment.id]: comment});

    return Object.assign({}, state, {
      comments: mergedComments
    });
  },
  [DELETED_COMMENT]: (state: ReduxStateComment, action: Action) => {
    const commentId = action.payload;
    const comments = {
      ...state.comments
    }
    delete comments[commentId];
    return {
      ...state,
      comments,
    };
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  // ----- COMMENTS
  /**
   * e.g. {
   *  [commentId]: {comment object}
   * }
   */
  comments: {},

  // ----- NEW COMMENT
  newPostText: null,
  /**
   * e.g. [
   *   {id, firstName, lastName}
   * ]
   * @type {Array}
   */
  newPostTaggedUsers: [],
  hasOlder: true,
};

const commentReducer = (state: ReduxStateComment = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default commentReducer;
