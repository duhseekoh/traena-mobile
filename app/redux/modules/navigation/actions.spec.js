import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
import { NavigationActions } from 'react-navigation';
import { NavigationService } from 'app/redux/modules/navigation/NavigationService';
import { navigateUniversalLink } from './actions';

jest.unmock('redux');
jest.mock('app/redux/modules/ui-content-standalone/selectors');
jest.mock('react-navigation');

const { Linking } = require('react-native');
// const { Actions: RNRFActions } = require('react-native-router-flux');


describe('navigation actions', () => {
  describe('handling deep linking', () => {
    it('passes for dev testing, while we rebuild these tests with react-navigation', () => {
      expect(1).toEqual(1);
    });
    // const middlewares = [thunk];
    // const mockStore = configureStore(middlewares);
    // const store = mockStore({});
    // const navigationService = new NavigationService();
    // let openURLSpy;
    //
    // beforeEach(() => {
    //   // Mock scene selector because its used in the function that routes the user
    //   const mockCurrentSceneSelector = jest.fn().mockImplementation(() => ({sceneKey: 'doesnt-matter'}));
    //   navigationSelectors.currentSceneSelector = mockCurrentSceneSelector;
    //   // Mock the call to the dynamically built RNRF contentStandalone call
    //   RNRFActions.contentStandalone = jest.fn();
    //   // Allows us to check for the case where universal links dont match
    //   openURLSpy = jest.spyOn(Linking, 'openURL');
    // });
    //
    // afterEach(() => {
    //   store.clearActions();
    // });
    //
    // it('should navigate to contentStandalone (prod content url)', () => {
    //   store.dispatch(navigateUniversalLink('https://traenaapp.io/content/128/view'));
    //   expect(RNRFActions.contentStandalone).toHaveBeenCalledWith({
    //     contentId: 128,
    //   });
    // });
    //
    // it('should navigate to contentStandalone (dev content url)', () => {
    //   store.dispatch(navigateUniversalLink('https://development.traenaapp.io/content/128/view'))
    //   expect(RNRFActions.contentStandalone).toHaveBeenCalledWith({
    //     contentId: 128,
    //   });
    // });
    //
    // it('should navigate to contentStandalone (staging content url)', () => {
    //   store.dispatch(navigateUniversalLink('https://staging.traenaapp.io/content/128/view'))
    //   expect(RNRFActions.contentStandalone).toHaveBeenCalledWith({
    //     contentId: 128,
    //   });
    // });
    //
    // it('should navigate to contentStandalone (prod www content url)', () => {
    //   store.dispatch(navigateUniversalLink('https://www.traenaapp.io/content/128/view'))
    //   expect(RNRFActions.contentStandalone).toHaveBeenCalledWith({
    //     contentId: 128,
    //   });
    // });
    //
    // it('should let the OS handle the url when the host has a typo', () => {
    //   store.dispatch(navigateUniversalLink('https://www.traenapp.io/content/128/view'))
    //   expect(openURLSpy).toHaveBeenCalledWith('https://www.traenapp.io/content/128/view');
    // });
    //
    // it('should let the OS handle the url when the subdomain isnt something we handle', () => {
    //   store.dispatch(navigateUniversalLink('https://ww.traenapp.io/content/128/view'))
    //   expect(openURLSpy).toHaveBeenCalledWith('https://ww.traenapp.io/content/128/view');
    // });
    //
    // it('should let the OS handle the url when the path has a typo', () => {
    //   store.dispatch(navigateUniversalLink('https://www.traenapp.io/cntent/128/view'))
    //   expect(openURLSpy).toHaveBeenCalledWith('https://www.traenapp.io/cntent/128/view');
    // });
    //
    // it('should let the OS handle the url when the path is to a content edit url', () => {
    //   store.dispatch(navigateUniversalLink('https://www.traenapp.io/content/128/edit'))
    //   expect(openURLSpy).toHaveBeenCalledWith('https://www.traenapp.io/content/128/edit');
    // });
  });
});
