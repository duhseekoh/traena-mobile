// @flow
import React from 'react';
import {
  TRAText,
} from 'app/components';
import {
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style?: ViewStyleProp,
  value: string,
  onPress: (value: string) => any,
};

function Tag(props: Props) {
  return (
    <TouchableOpacity style={styles.tag} onPress={() => props.onPress(props.value)}>
      <TRAText style={styles.tagText}>{props.value}</TRAText>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  tag: {
    backgroundColor: styleVars.color.blueGreen15percent,
    marginRight: 7,
    padding: 5,
    paddingHorizontal: 16,
    borderRadius: 6,
    marginVertical: 4,
  },
  tagText: {
    fontSize: styleVars.fontSize.regular,
    fontFamily: styleVars.fontFamily.bold,
    color: styleVars.color.blueGreen,
  }
});

export default Tag;
