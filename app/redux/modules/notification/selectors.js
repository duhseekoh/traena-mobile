// @flow
import moment from 'moment';
import _ from 'lodash';
import { createSelector } from 'reselect';
import type { Notification } from './types';
import type { ReduxStateNotification } from './reducer';

const STATE_KEY = 'notification';
const stateFromRoot = (state): ReduxStateNotification => state[STATE_KEY];

const notificationsSelector = createSelector([
  (state: *) => stateFromRoot(state).notifications,
], (notificationsKeyValues): Notification[] => {
  const notifications = Object.values(notificationsKeyValues);

  // Sort all comments by created date, in ascending order
  const notificationsArray: Notification[] = (Object.values(notifications): any);
  const notificationsDateSorted = (_.sortBy(notificationsArray, [
    ({createdTimestamp}: Notification) => -moment(createdTimestamp).valueOf()
  ]): any);

  return notificationsDateSorted;
});

const isLoadingSelector = createSelector([
  (state) => stateFromRoot(state).isLoading
], (isLoading) => isLoading);

const hasOlderSelector = createSelector([
  (state) => stateFromRoot(state).hasOlder
], (hasOlder) => hasOlder);

const deviceTokenSelector = createSelector([
  (state) => stateFromRoot(state).deviceToken
], (deviceToken) => deviceToken);

const unacknowledgedCountSelector = (state: *) => stateFromRoot(state).unacknowledgedCount;

const hasUnacknowledgedSelector = (state: *) => !!stateFromRoot(state).unacknowledgedCount;

export {
  deviceTokenSelector,
  hasOlderSelector,
  hasUnacknowledgedSelector,
  unacknowledgedCountSelector,
  isLoadingSelector,
  notificationsSelector
};
