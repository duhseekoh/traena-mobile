// @flow
import {
  getCompletionsTpl,
  postCompletionTpl,
} from './util/endpoints';

const retrieveContentCompletions = () => {
  return fetch(getCompletionsTpl());
};

const addCompletedTraenaListItem = (contentId: number) => {
  return fetch(postCompletionTpl({ contentId }), {
    method: 'POST',
  });
};

export default {
  addCompletedTraenaListItem,
  retrieveContentCompletions,
}
