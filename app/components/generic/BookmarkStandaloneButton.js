// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addBookmark, deleteBookmark } from 'app/redux/modules/bookmark/actions';
import {
  publishedContentSelector,
} from 'app/redux/modules/ui-content-standalone/selectors';

import { ButtonBookmark } from 'app/components';
import type { ContentItem } from 'app/redux/modules/content/types';

type OwnProps = {
  contentId: number,
}

type ConnectedProps = {
  addBookmark: (id: number) => void,
  deleteBookmark: (id: number) => void,
  contentItem: ContentItem,
}

type Props = OwnProps & ConnectedProps;

class BookmarkStandaloneButton extends Component<Props> {
  props: Props;
  _onBookmarkPressed = () => {
    const { contentItem, addBookmark, deleteBookmark } = this.props;
    const { id, isBookmarked } = contentItem;
    if (isBookmarked) {
      deleteBookmark(id);
    } else {
      addBookmark(id);
    }
  }

  render() {
    const { contentItem } = this.props;
    if(contentItem) {
      const { isBookmarked } = contentItem;
      return (
        <ButtonBookmark
          onPress={this._onBookmarkPressed}
          isBookmarked={isBookmarked} />
      );
    }
    return null;
  }
}

const mapStateToProps = (state, props) => {
  return {
    contentItem: publishedContentSelector(state, { contentId: props.contentId }),
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    addBookmark: (contentId: number) => {
      return dispatch(addBookmark(contentId));
    },
    deleteBookmark: (contentId: number) => {
      return dispatch(deleteBookmark(contentId));
    },
  };
};

const ConnectedBookmarkStandaloneButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(BookmarkStandaloneButton);

export default ConnectedBookmarkStandaloneButton;
