// @flow
import { SHOW_MODAL, HIDE_MODAL } from './actions';

import type { Action } from 'app/types/Action';

export type ReduxStateModal = {
  modalType: ?string,
  modalProps: Object,
};

const ACTION_HANDLERS = {
  [SHOW_MODAL]: (state: ReduxStateModal, action: Action) => {
    return {
      modalType: action.payload.modalType,
      modalProps: action.payload.modalProps,
    }
  },
  [HIDE_MODAL]: () => {
    return initialState;
  }
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  modalType: null,
  modalProps: {}
};

export default function finance(state: ReduxStateModal = initialState, action: Action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}
