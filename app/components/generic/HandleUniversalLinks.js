// @flow
import React, { Component } from 'react';
import { View, Linking } from 'react-native';
import { connect } from 'react-redux';
import { navigateUniversalLink } from 'app/redux/modules/navigation/actions';
import type { Dispatch } from 'app/redux/constants';

type Props = {
  navigateUniversalLink: (url: string) => Promise<*>,
};

type State = {};

class HandleUniversalLinks extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    //this._handleOpenUrl = this._handleOpenUrl.bind(this);
  }
  componentDidMount() {
    Linking.getInitialURL().then((url) => {
      if (url) {
        this._handleOpenUrl({ url });
      }
    });
    Linking.addEventListener('url', this._handleOpenUrl);
  }

  componentWillUnmount() {
    Linking.removeEventListener('url', this._handleOpenUrl);
  }

  _handleOpenUrl = (event) => {
    if(event.url) {
      this.props.navigateUniversalLink(event.url)
    }
  }
  render() {
    return(<View></View>);
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    navigateUniversalLink: (url: string) => {
      return dispatch(navigateUniversalLink(url));
    }
  };
}

const ConnectedHandleUniversalLinks = connect(
  null,
  mapDispatchToProps
)(HandleUniversalLinks);

export default ConnectedHandleUniversalLinks;
