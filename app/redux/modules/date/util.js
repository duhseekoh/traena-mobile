// @flow
import moment from 'moment';
import { PARTS_OF_DAY } from 'app/redux/modules/date/constants';
/**
 * Is the given date in the morning / evening timeframe
 * @param  {moment}  date date to check
 * @return {int} 0 = morning, 1 = afternoon, 2 = evening
 */
function getPartOfDay(date: moment) {
  let afternoonHour = 12;
  let evening = 18;
  if (date.hour() < afternoonHour) {
    return PARTS_OF_DAY.MORNING;
  } else if(date.hour() < evening) {
    return PARTS_OF_DAY.AFTERNOON;
  }
  return PARTS_OF_DAY.EVENING;
}

/**
 * Is the given date the same day of the year that today is
 * @param  {moment}  date date to check
 * @return {Boolean}      true if same day of year
 */
function isDateToday(date: moment) {
  return date.isSame(moment(), 'days');
}

/**
 * Get the date one day prior to the given date
 * @param  {moment} date base date
 * @return {moment}      day before base date
 */
function getDayBefore(date: moment) {
  return date.add(-1, 'days');
}

/**
 * Get the date one day after the given date
 * @param  {moment} date base date
 * @return {moment}      day after base date
 */
function getDayAfter(date: moment) {
  return date.add(1, 'days');
}

const dateUtil = {
  getPartOfDay: getPartOfDay,
  isDateToday: isDateToday,
  getDayBefore: getDayBefore,
  getDayAfter: getDayAfter
};

export default dateUtil;
