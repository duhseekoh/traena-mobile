// @flow
import { STATS_RECEIVED, STATS_LOADING } from './actions';

import type { Activity } from './types';
import type { Action } from 'app/types/Action';

export type ReduxStateStats = {
  isLoading: boolean,
  activityStreak: number,
  activity: Activity[],
  comments: Activity[],
  completedTasks: Activity[],
};

const ACTION_HANDLERS = {
  [STATS_RECEIVED]: (state: ReduxStateStats, action: Action) => {
    return Object.assign({}, state, {
      ...action.payload,
    });
  },
  [STATS_LOADING]: (state: ReduxStateStats, action: Action) => {
    return Object.assign({}, state, {
      isLoading: action.payload,
    });
  }
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isLoading: false,
  activityStreak: 0,
  activity: [],
  comments: [],
  completedTasks: [],
};

export default function user(state: ReduxStateStats = initialState, action: Action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}
