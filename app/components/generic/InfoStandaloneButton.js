// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showInfoModal } from 'app/redux/modules/modal/actions';

import { ButtonInfo } from 'app/components';

type OwnProps = {
  title: string,
  description: string,
}

type ConnectedProps = {
  showInfoModal: (props: {title: string, description: string}) => any,
}

type Props = OwnProps & ConnectedProps;

class InfoStandaloneButton extends Component<Props> {
  props: Props;
  _onInfoPressed = () => {
    const { title, description } = this.props;
    this.props.showInfoModal({title, description});
  }

  render() {
    return (
      <ButtonInfo
        onPress={this._onInfoPressed} />
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    showInfoModal: (props: {title: string, description: string}) => {
      return dispatch(showInfoModal(props));
    },
  };
};

const ConnectedInfoStandaloneButton = connect(
  null,
  mapDispatchToProps
)(InfoStandaloneButton);

export default ConnectedInfoStandaloneButton;
