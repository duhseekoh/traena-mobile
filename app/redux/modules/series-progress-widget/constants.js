import styleVars from 'app/style/variables';

const blackPattern = require('app/images/black-pattern.png');
const whitePattern = require('app/images/white-pattern.png');

export const SERIES_CARD_STYLES = {
  0: {
    color: styleVars.color.seriesCard1,
    opacity: 0.03,
    imageBackground: blackPattern,
  },
  1: {
    color: styleVars.color.seriesCard1,
    opacity: 0.03,
    imageBackground: blackPattern,
  },
  2: {
    color: styleVars.color.seriesCard2,
    opacity: 0.03,
    imageBackground: blackPattern,
  },
  3: {
    color: styleVars.color.seriesCard2,
    opacity: 0.03,
    imageBackground: blackPattern,
  },
  4: {
    color: styleVars.color.seriesCard3,
    opacity: 0.07,
    imageBackground: whitePattern,
  },
  5: {
    color: styleVars.color.seriesCard3,
    opacity: 0.07,
    imageBackground: whitePattern,
  },
  6: {
    color: styleVars.color.seriesCard4,
    opacity: 0.03,
    imageBackground: blackPattern,
  },
  7: {
    color: styleVars.color.seriesCard4,
    opacity: 0.03,
    imageBackground: blackPattern,
  },
  8: {
    color: styleVars.color.seriesCard5,
    opacity: 0.07,
    imageBackground: blackPattern,
  },
  9: {
    color: styleVars.color.seriesCard5,
    opacity: 0.07,
    imageBackground: blackPattern,
  },
};
