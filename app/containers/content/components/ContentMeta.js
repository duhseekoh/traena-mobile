// @flow
import React from 'react';
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  IconBar,
  TRAText,
  TraenaIcon,
  FeatherIcon,
} from 'app/components'
import styleVars from 'app/style/variables';

type Props = {
  onPressTitle?: () => *,
  isCompleted?: boolean,
  isSeries?: boolean,
  hasActivity?: boolean,
  title: string,
};

const ContentMeta = ({onPressTitle, isCompleted, isSeries, hasActivity, title}: Props) => {
  return (
    <View style={styles.containerView}>
      <TouchableWithoutFeedback onPress={onPressTitle}>
        <TRAText style={styles.text}>{title}</TRAText>
      </TouchableWithoutFeedback>
      <IconBar style={{marginHorizontal: 8}}>
        { isSeries && <FeatherIcon name='copy' fontSize={styleVars.iconSize.medlar} color={styleVars.color.lightGrey} />}
        { hasActivity && <TraenaIcon name='bolt' fontSize={styleVars.iconSize.medlar} color={styleVars.color.lightGrey} />}
        { isCompleted && <FeatherIcon name='check-circle' fontSize={styleVars.iconSize.medlar} color={styleVars.color.lightGrey} />}
      </IconBar>
    </View>
  )
};
export default ContentMeta;

const styles = StyleSheet.create({
  containerView: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  text: {
    marginHorizontal: 8,
    color: styleVars.color.mediumGrey,
    fontSize: styleVars.fontSize.medium,
  }
});
