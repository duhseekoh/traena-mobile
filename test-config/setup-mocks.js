jest.mock('redux');
jest.mock('bugsnag-react-native');

// Ugh.. https://github.com/facebook/jest/issues/2208
jest.mock('PushNotificationIOS', () => {
  return {
    addEventListener: jest.fn(),
    requestPermissions: jest.fn(() => Promise.resolve()),
    getInitialNotification: jest.fn(() => Promise.resolve()),
  }
});
jest.mock('Linking', () => {
  return {
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    openURL: jest.fn(),
    canOpenURL: jest.fn(),
    getInitialURL: jest.fn(),
  }
});
// Dont care about logger throughout tests, unless specifically testing the logger
jest.mock('app/logging/logger', () => ({
  setupUser: jest.fn(),
  logBreadcrumb: jest.fn(),
  logError: jest.fn(),
}));
