import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
} from 'react-native';
import { FeatherIcon, TRATextStrong } from 'app/components';
import styleVars from 'app/style/variables';
import { connect } from 'react-redux';

class Toast extends Component {
  render() {
    const { toast } = this.props;
    if (!toast) {
      return null;
    }
    return (
      <View style={styles.toast}>
        <TRATextStrong style={styles.toastText}>
          <FeatherIcon
             fontSize={styleVars.iconSize.small}
             color={styleVars.color.white}
             name={'alert-triangle'}
           /> {`${toast.title} ${toast.message}`}
        </TRATextStrong>
      </View>
    )
  }
}

Toast.defaultProps = {
  toast: null,
};

Toast.propTypes = {
  toast: PropTypes.arrayOf(PropTypes.shape()),
};

function mapStateToProps(state) {
  return {
    toast: state.toast.toast,
  };
}

export default connect(mapStateToProps, null)(Toast);

const styles = StyleSheet.create({
  toast: {
    width: '100%',
    padding: 20,
    backgroundColor: styleVars.color.negative,
    shadowColor: styleVars.color.black,
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius:2,
    shadowOpacity: .7
  },
  toastText: {
    color: styleVars.color.white,
    fontSize: styleVars.fontSize.medlar,
  },
});
