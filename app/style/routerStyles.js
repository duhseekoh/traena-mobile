import { Platform } from 'react-native';
import styleVars from './variables';

const navigationBarStyle = {
  // Android needs some extra padding to avoid overlapping with the status bar
  paddingTop: Platform.OS === 'ios' ? 0 : 10,
  backgroundColor: styleVars.color.white,
  borderBottomColor: styleVars.color.lightGrey3,
};

const navigationBarTitleStyle = {
  color: styleVars.color.black,
  fontSize: styleVars.fontSize.small,
  paddingTop: 3,
  fontFamily: styleVars.fontFamily.medium,
};

const navigationBarLeftButtonIconStyle = {
  tintColor: styleVars.color.black,
};

export {
  navigationBarLeftButtonIconStyle,
  navigationBarStyle,
  navigationBarTitleStyle,
}
