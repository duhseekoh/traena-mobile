// @flow
import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import { TRATextStrong } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  text: string,
};

const ScreenTitle = ({text = ''}: Props) => {
  return (
    <View style={styles.container}>
      <TRATextStrong style={styles.text}>
        {text}
      </TRATextStrong>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: .5,
    borderColor: styleVars.color.lightGrey3,
    backgroundColor: styleVars.color.white,
    paddingTop: 30,
    padding: 15,
  },
  text: {
    textAlign: 'center',
    fontSize: styleVars.fontSize.small,
  }
});

export default ScreenTitle;
