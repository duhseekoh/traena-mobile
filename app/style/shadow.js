export default {
  shadowColor: '#D0D0D0',
  shadowOffset: {
    width: 0,
    height: -1,
  },
  shadowOpacity: 1,
  shadowRadius: 6,
}
