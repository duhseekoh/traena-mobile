// @flow
import _ from 'lodash';
import {
  REFRESH_TAG_CONTENT_REQUEST_STARTED,
  REFRESH_TAG_CONTENT_REQUEST_FAILED,
  LOAD_MORE_TAG_CONTENT_REQUEST_STARTED,
  LOAD_MORE_TAG_CONTENT_REQUEST_FAILED,
  LOADED_MORE_TAG_CONTENT,
  REFRESHED_TAG_CONTENT,
} from './actions';
import { REQUEST_STATUSES, type RequestStatus } from 'app/redux/constants';
import type { Action } from 'app/types/Action';

export type ReduxStateTagStandalone = {
  uiSlices: {
    [tag: string]: {
      loadingStatus: RequestStatus,
      refreshingState: RequestStatus,
      contentIds: string[],
      currentPage: number,
      canLoadMore: boolean,
    }
  }
};

const ACTION_HANDLERS = {
  [REFRESHED_TAG_CONTENT]: (state: ReduxStateTagStandalone, action: Action) => {
    const { ids, totalPages, number, tag } = action.payload;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [tag]: {
          contentIds: ids,
          canLoadMore: number < totalPages - 1,
          currentPage: number,
          loadingStatus: REQUEST_STATUSES.COMPLETE,
          refreshingStatus: REQUEST_STATUSES.COMPLETE,
        }
      }
    }
  },
  [LOADED_MORE_TAG_CONTENT]: (state: ReduxStateTagStandalone, action: Action) => {
    const { ids, totalPages, number, tag } = action.payload;
    const mergedContentIds = _.uniq([...state.uiSlices[tag].contentIds, ...ids]);
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [tag]: {
          contentIds: mergedContentIds,
          canLoadMore: number < totalPages - 1,
          currentPage: number,
          loadingStatus: REQUEST_STATUSES.COMPLETE,
          refreshingStatus: REQUEST_STATUSES.COMPLETE,
        }
      }
    }
  },
  [REFRESH_TAG_CONTENT_REQUEST_STARTED]: (state: ReduxStateTagStandalone, action: Action) => {
    const tag = action.payload;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [tag]: {
          ...state.uiSlices[tag],
          refreshingStatus: REQUEST_STATUSES.IN_PROGRESS,
        }
      }
    }
  },
  [REFRESH_TAG_CONTENT_REQUEST_FAILED]: (state: ReduxStateTagStandalone, action: Action) => {
    const tag = action.payload;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [tag]: {
          ...state.uiSlices[tag],
          refreshingStatus: REQUEST_STATUSES.ERROR,
        }
      }
    };
  },
  [LOAD_MORE_TAG_CONTENT_REQUEST_STARTED]: (state: ReduxStateTagStandalone, action: Action) => {
    const tag = action.payload;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [tag]: {
          ...state.uiSlices[tag],
          loadingStatus: REQUEST_STATUSES.IN_PROGRESS,
        }
      }
    };
  },
  [LOAD_MORE_TAG_CONTENT_REQUEST_FAILED]: (state: ReduxStateTagStandalone, action: Action) => {
    const tag = action.payload;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [tag]: {
          ...state.uiSlices[tag],
          loadingStatus: REQUEST_STATUSES.ERROR,
        }
      }
    };
  },
};

const initialState: ReduxStateTagStandalone = {
  uiSlices: {}
};

const tagStandaloneReducer = (state: ReduxStateTagStandalone = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

export default tagStandaloneReducer;
