// @flow
import React, { Component } from 'react';
import {
  StyleSheet,
} from 'react-native';
import type { TextStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { connect } from 'react-redux';
import ParsedText from 'react-native-parsed-text';
import styleVars from 'app/style/variables';
import { navigateUniversalLink } from 'app/redux/modules/navigation/actions';
import type { Dispatch } from 'app/redux/constants';

type Props = {
  style?: TextStyleProp,
  children: Node,
  openURL: (url: string) => Promise<*>,
}

type State = {};
/**
 * Renders text but also make any URLs inside the text touchable.
 * What to do with the link is handled like universal links are handled.
 */
class TRATextParsed extends Component<Props, State> {
  props: Props;
  render() {
    const { style, children, openURL } = this.props;
    if (!children) {
      return null;
    }
    return (<ParsedText
        {...this.props}
        style={[styles.text, style]}
        parse={
          [
            {
              type: 'url',
              style: styles.url,
              onPress: openURL,
            },
          ]
        }
      >{children}</ParsedText>);
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    openURL: (url: string) => dispatch(navigateUniversalLink(url)),
  }
}

export default connect(null, mapDispatchToProps)(TRATextParsed);

const styles = StyleSheet.create({
  text: {
    fontFamily: styleVars.fontFamily.base,
    fontSize: styleVars.fontSize.regular,
  },
  url: {
    fontFamily: styleVars.fontFamily.bold,
    color: styleVars.color.traenaOne,
  }
});
