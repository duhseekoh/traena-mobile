// @flow
import api from 'app/redux/api';
import type { Dispatch } from 'app/redux/constants';
import type { Channel } from 'app/redux/modules/channel/types';

const CHANNEL_LIST_FETCH_STARTED = 'CHANNEL_LIST_FETCH_STARTED';
function channelListFetchStarted() {
  return {
    type: CHANNEL_LIST_FETCH_STARTED,
  };
}

const CHANNEL_LIST_FETCH_SUCCEEDED = 'CHANNEL_LIST_FETCH_SUCCEEDED';
function channelListFetchSucceeded(channels: {[number]: Channel}[]) {
  return {
    type: CHANNEL_LIST_FETCH_SUCCEEDED,
    payload: channels,
  };
}

const CHANNEL_LIST_FETCH_FAILED = 'CHANNEL_LIST_FETCH_FAILED';
function channelListFetchFailed(error: string | Error) {
  return {
    type: CHANNEL_LIST_FETCH_FAILED,
    payload: error,
  };
}

const getSubscribedChannels = (pageNumber?: number) => {
  return (dispatch: Dispatch) => {
    dispatch(channelListFetchStarted())
    return api.user.retrieveChannels(pageNumber)
      .then(results => {
        if(results) {
          dispatch(channelListFetchSucceeded(results.index));
        }
        return results;
      })
      .catch((error) => {
        dispatch(channelListFetchFailed(error));
      });
  }
};

export {
  CHANNEL_LIST_FETCH_SUCCEEDED,
  getSubscribedChannels,
}
