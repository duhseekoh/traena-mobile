// @flow
import React from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
} from 'react-native';
import { TRATextStrong, TRAText, ProgressBar, FeedCard } from 'app/components';
import styleVars from 'app/style/variables';
import { seriesProgressSelector } from 'app/redux/modules/series/selectors'
import type { Series, SeriesProgress } from 'app/redux/modules/series/types';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { SERIES_CARD_STYLES } from 'app/redux/modules/series-progress-widget/constants';

type Props = {
  style?: ViewStyleProp,
  series: Series,
  onPress: (seriesId: number, description: string) => any,
  seriesProgress: SeriesProgress,
};

/**
* Given the last digit of a seriesId, we pick a style for its card representation
*/
function getSeriesCardStyles(
  seriesId: number,
): { color: string, opacity: number, imageBackground: string } {
  const lastDigit = seriesId.toString().split('').pop();
  return SERIES_CARD_STYLES[lastDigit];
}

const SeriesProgressCard = ({ onPress, style, series, seriesProgress }: Props) => {
  const { title, id, description } = series;
  const {
    color,
    opacity,
    imageBackground,
  } = getSeriesCardStyles(id);

  const { completedContentCount, contentCount } = seriesProgress;
  const percent = (completedContentCount / contentCount) * 100;

  return (
    <FeedCard
      style={style}
      onPress={() => onPress(id, description)}
      cardColor={color}
      imageBackground={imageBackground}
      imageOpacity={opacity}
    >
      <TRATextStrong
        numberOfLines={1}
        ellipsizeMode={'tail'}
        style={styles.cardTitle}
      >
        {title}
      </TRATextStrong>
      {description && (
        <TRAText
          style={styles.cardText}
          numberOfLines={2}
          ellipsizeMode={'tail'}
        >
          {description}
        </TRAText>
      )}
      <ProgressBar
        percent={percent}
        style={styles.progressBar}
        barBackgroundColor={styleVars.color.translucentWhite}
        barColor={styleVars.color.white}
      />
    </FeedCard>
  );
};

const styles = StyleSheet.create({
  cardTitle: {
    fontFamily: styleVars.fontFamily.medium,
    fontSize: styleVars.fontSize.xlarge,
    color: styleVars.color.white,
    marginBottom: 5,
  },
  cardText: {
    fontFamily: styleVars.fontFamily.medium,
    fontSize: styleVars.fontSize.medium,
    color: styleVars.color.white,
    marginBottom: 5,
  },
  progressBar: {
    marginTop: 5,
  },
});

const mapStateToProps = (state, props) => {
  return {
    seriesProgress: seriesProgressSelector(state, { id: props.series.id }),
  }
};

export default connect(mapStateToProps, null)(SeriesProgressCard);
