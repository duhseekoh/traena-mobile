// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';
import ButtonQuiz from './components/ButtonQuiz';
import {
  TRAText,
  TRATextStrong,
  FeatherIcon,
} from 'app/components';
import ProgressBar from '../components/ProgressBar';
import MultipleChoiceQuestion from 'app/containers/activity/question-set/components/MultipleChoiceQuestion';
import { correctFirstTrySelector, currentQuestionIndexSelector, showSummarySelector, questionsSelector, totalQuestionsSelector, activityProgressSelector } from 'app/redux/modules/ui-activity/questionSetSelectors';
import styleVars from 'app/style/variables';
import { setCurrentQuestion, setCorrectFirstTry, resetQuestionSet } from 'app/redux/modules/ui-activity/actions';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import type { Question } from 'app/redux/modules/activity/types';
import type { Dispatch } from 'app/redux/constants';

type OwnProps = {
  style?: ViewStyleProp,
  exitActivity: () => void,
  activityId: number,
};

type ConnectedProps = {
  currentQuestionIndex: number,
  questions: Question[],
  totalQuestions: number,
  showSummary: boolean,
  correctFirstTry: boolean[],
  setCurrentQuestion: (questionIndex: number) => void,
  resetQuestionSet: (questionLen: number) => void,
  setCorrectFirstTry: (questionIndex: number) => void,
  progress: number,
};

type Props = OwnProps & ConnectedProps;

type State = {
  scrollWidth: number,
};

class QuestionSetContainer extends Component<Props, State> {
  scrollRef: ScrollView;

  constructor(props: Props) {
    super(props);

    this.state = {
      scrollWidth: 0,
    };
  }

  componentDidMount() {
    this.props.resetQuestionSet(this.props.totalQuestions);
  }

  _onLayout = ({nativeEvent}: *) => {
    if (nativeEvent.layout.width === this.state.scrollWidth) {
      return;
    }

    this.setState({
      scrollWidth: nativeEvent.layout.width,
    });
  }

  _getCorrectChoiceText = (questionIndex: number) => {
    const correctChoice = this.props.questions[questionIndex].choices.find(c => c.correct);
    return correctChoice ? correctChoice.choiceText : false;
  }

  _getQuestionSummary = (questionIndex: number) => {
    const summary = this.props.questions[questionIndex].summary;
    return summary ? summary : false;
  }

  _scrollToNext = () => {
    if(this.scrollRef) {
      this.scrollRef.scrollTo({x: this.state.scrollWidth * this.props.currentQuestionIndex});
    }
  }

  _renderSummary() {
    const { questions, totalQuestions, correctFirstTry, exitActivity } = this.props;
    const correctFirstTryTotal = correctFirstTry ? correctFirstTry.reduce((acc, val) => val ? acc + 1 : acc, 0) : 0;
    return (<View style={{flex: 1, alignSelf: 'stretch'}}>
      <ScrollView style={styles.summaryCard} showsVerticalScrollIndicator={false}>
        <TRATextStrong style={styles.summaryTitle}>Your Quiz Summary</TRATextStrong>
        <View style={styles.imageContainer}>
          <Image
            style={[styles.summaryImage]}
            source={require('app/images/activity-complete.png')} />
        </View>
        <View style={styles.firstTryIconContainer} >{questions.map((question, i) => {
          const style = correctFirstTry && correctFirstTry[i] ? styles.correctIcon : styles.incorrectIcon;
          return (<View key={question.id}><FeatherIcon style={style} name='check-circle' /></View>);
        })}
        </View>
        <TRAText style={{textAlign: 'center'}}>{correctFirstTryTotal} of {totalQuestions} correct on the first attempt</TRAText>
        <View style={styles.horizontalSeparator}></View>
        <View style={styles.questionReviewContainer}>
        {questions.map((question, i) => {
          const correctChoiceText = this._getCorrectChoiceText(i);
          const correctExplanationText = this._getQuestionSummary(i);
          const style = correctFirstTry && correctFirstTry[i] ? styles.correctIcon : styles.incorrectIcon;
          return(
            <View style={styles.questionReviewItem} key={question.id}>
              <FeatherIcon style={[style, { marginLeft: 0 }]} name='check-circle' />
              <View style={styles.questionReview}>
                <View><TRATextStrong style={styles.reviewText}>Question {i+1}: {question.questionText}</TRATextStrong></View>
                {correctChoiceText && <View><TRATextStrong style={[styles.reviewText, styles.reviewCorrectText]}>{correctChoiceText}</TRATextStrong></View>}
                {correctExplanationText && <View><TRAText style={styles.reviewText}>{correctExplanationText}</TRAText></View>}
              </View>
            </View>)
        })}
        </View>
      </ScrollView>
      <ButtonQuiz
        style={{marginBottom: 20, marginHorizontal: 28}}
        text={'Finished!'}
        onPress={exitActivity}
        focused={true}
        disabled={false}
        loading={false} />
    </View>);
  }

  _setCurrentQuestion = (questionIndex) => {
    this.props.setCurrentQuestion(questionIndex);
  }

  _setCorrectFirstTry = (questionIndex) => {
    this.props.setCorrectFirstTry(questionIndex);
  }

  render() {
    const { questions, currentQuestionIndex, totalQuestions, showSummary, progress } = this.props;
    return (
      <View style={styles.container}>
        <ProgressBar
          progress={progress}
          containerStyle={styles.progressBar}/>
        <ScrollView
          ref={(ref) => this.scrollRef = ref}
          horizontal
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          onLayout={this._onLayout}
          bounce={questions.length > 0 || this.props.showSummary}
          pagingEnabled>
          {
            questions.map((question, questionIndex) => {
              if(currentQuestionIndex < questionIndex) {
                // can't block the scrolling of this View. Instead
                // don't render questions that aren't available yet
                return;
              }
              return (<View key={question.id} style={[styles.scrollPage, { width: this.state.scrollWidth }]}>
                <MultipleChoiceQuestion
                  question={question}
                  questionIndex={questionIndex}
                  totalQuestions={totalQuestions}
                  setCurrentQuestion={this._setCurrentQuestion}
                  setCorrectFirstTry={this._setCorrectFirstTry}
                  currentQuestionIndex={currentQuestionIndex}
                  scrollToNext={this._scrollToNext} />
              </View>)
            })
          }
          {showSummary && <View style={[styles.scrollPage, { width: this.state.scrollWidth }]}>
              {this._renderSummary()}
            </View>}
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props: OwnProps) => {
  return {
    questions: questionsSelector(state, props),
    showSummary: showSummarySelector(state, props),
    currentQuestionIndex: currentQuestionIndexSelector(state, props),
    totalQuestions: totalQuestionsSelector(state, props),
    correctFirstTry: correctFirstTrySelector(state, props),
    progress: activityProgressSelector(state, props),
  }
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    setCurrentQuestion: (newIndex) =>
      dispatch(setCurrentQuestion(newIndex)),
    setCorrectFirstTry: (questionIndex) =>
      dispatch(setCorrectFirstTry(questionIndex)),
    resetQuestionSet: (questionsLen) =>
      dispatch(resetQuestionSet(questionsLen)),
  };
};

const ConnectedQuestionSetContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(QuestionSetContainer);

export default ConnectedQuestionSetContainer;

const imageRatio = 1.842;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  summaryCard: {
    marginVertical: 15,
    padding: 28,
    height: 410,
  },
  summaryTitle: {
    fontSize: styleVars.fontSize.large,
    textAlign: 'center',
  },
  imageContainer: {
    flex: 1,
    aspectRatio: imageRatio,
  },
  summaryImage: {
    resizeMode: 'contain',
    width: null,
    flex: 1,
    height: null,
  },
  firstTryIconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 18,
    marginTop: 20,
  },
  correctIcon: {
    fontSize: styleVars.fontSize.xlarge,
    color: styleVars.color.traenaPrimaryGreen,
    marginHorizontal: 10,
  },
  incorrectIcon: {
    fontSize: styleVars.fontSize.xlarge,
    color: styleVars.color.lightGrey,
    marginHorizontal: 10,
  },
  horizontalSeparator: {
    borderBottomColor: styleVars.color.lightGrey,
    borderBottomWidth: 2,
    marginVertical: 25,
  },
  questionReviewContainer: {
    paddingBottom: 40,
  },
  questionReviewItem: {
    flexDirection: 'row',
    paddingBottom: 10,
  },
  questionReview: {
    flex: 1,
  },
  reviewText: {
    paddingBottom: 8,
  },
  reviewCorrectText: {
    color: styleVars.color.traenaPrimaryGreen,
  },
  scrollPage: {
    flex: 1,
    alignItems: 'center',
    paddingBottom: 10,
  },
  progressBar: {
    marginRight: 75,
    top: 38,
    left: 20,
  },
});
