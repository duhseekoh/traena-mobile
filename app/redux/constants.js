// @flow
import type { ReduxStateAuth } from './modules/auth/reducer';
import type { ReduxStateChannelStandalone } from './modules/ui-channel-standalone/reducer';
import type { ReduxStateComment } from './modules/comment/reducer';
import type { ReduxStateContent } from './modules/content/reducer';
import type { ReduxStateUiContentStandalone } from './modules/ui-content-standalone/reducer';
import type { ReduxStateFeed } from './modules/feed/reducer';
import type { ReduxStateModal } from './modules/modal/reducer';
import type { ReduxStateNotification } from './modules/notification/reducer';
import type { ReduxStateSearch } from './modules/search/reducer';
import type { ReduxStateSeriesStandalone } from './modules/ui-series-standalone/reducer';
import type { ReduxStateSubscribedSeriesList } from './modules/ui-subscribed-series-list/reducer';
import type { ReduxStateStats } from './modules/stats/reducer';
import type { ReduxStateTagStandalone } from './modules/tag-standalone/reducer';
import type { ReduxStateToast } from './modules/toast/reducer';
import type { ReduxStateTraenaTask } from './modules/traena-task/reducer';
import type { ReduxStateUser } from './modules/user/reducer';
import type { ReduxStateUserSearch } from './modules/user-search/reducer';
import type { ReduxStateVideo } from './modules/ui-video-player/reducer';
import type { ReduxStateSeries } from './modules/series/reducer';
import type { ReduxStateChannel } from './modules/channel/reducer';
import type { ReduxStateChannelList } from './modules/ui-channel-list/reducer';
import type { ReduxStateUiSeriesContentPosition } from './modules/ui-series-content-position/reducer';
import type { ReduxStateSeriesProgressWidget } from './modules/series-progress-widget/reducer';
import type { Action } from 'app/types/Action';
import config from 'app/config';

// REQUEST STATUSES
export const REQUEST_STATUSES = {
  COMPLETE: 'COMPLETE',
  IN_PROGRESS: 'IN_PROGRESS',
  ERROR: 'ERROR',
};

export const FEEDBACK_URI = config.feedbackUri || '';

export type RequestStatus = $Values<typeof REQUEST_STATUSES>;

export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type PromiseAction = Promise<Action>;
export type Dispatch = (action: Action | ThunkAction | PromiseAction) => any; // eslint-disable-line

export type ReduxState = {
  auth: ReduxStateAuth,
  channelStandalone: ReduxStateChannelStandalone,
  comment: ReduxStateComment,
  content: ReduxStateContent,
  contentStandalone: ReduxStateUiContentStandalone,
  feed: ReduxStateFeed,
  modal: ReduxStateModal,
  network: *,
  notification: ReduxStateNotification,
  search: ReduxStateSearch,
  series: ReduxStateSeries,
  seriesStandalone: ReduxStateSeriesStandalone,
  subscribedSeriesList: ReduxStateSubscribedSeriesList,
  stat: ReduxStateStats,
  tagStandalone: ReduxStateTagStandalone,
  toast: ReduxStateToast,
  traenaTask: ReduxStateTraenaTask,
  user: ReduxStateUser,
  userSearch: ReduxStateUserSearch,
  videoPlayer: ReduxStateVideo,
  channel: ReduxStateChannel,
  uiChannelList: ReduxStateChannelList,
  uiSeriesContentPosition: ReduxStateUiSeriesContentPosition,
  seriesProgressWidget: ReduxStateSeriesProgressWidget,
};

export type GetState = () => ReduxState;
