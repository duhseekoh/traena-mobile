// @flow
import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import Comments from './components/Comments';
import { commentsGroupByContentSelector, hasOlderSelector } from 'app/redux/modules/comment/selectors';
import { addComment, getRecentComments, getMoreComments } from 'app/redux/modules/comment/actions';
import { goToUserSearch, popScene } from 'app/redux/modules/navigation/actions';
import type { Dispatch } from 'app/redux/constants';
import type { Comment } from 'app/redux/modules/comment/types';
import type { User } from 'app/redux/modules/user/types';

type OwnProps = {
  contentId: number,
};

type ConnectedProps ={
  comments: Comment[],
  hasOlder: boolean,
  addComment: (contentId: number, text: string, taggedUsers: User[]) => any,
  getRecentComments: (contentId: number) => Promise<*>,
  getOlderComments: (contentId: number, lastComment: ?Object) => any,
  goToUserSearch: (onUserSelected: Function) => Promise<*>,
  popScene: () => Promise<*>,
};

type State = {
  isLoadingRecent: boolean,
  isLoadingOlder: boolean,
  isPosting: boolean,
  newCommentText: string,
  newCommentTaggedUsers: User[],
};

class CommentsContainer extends Component<OwnProps & ConnectedProps, State> {
  constructor(props: OwnProps & ConnectedProps) {
    super(props);

    this.state = {
      isLoadingRecent: false,
      isLoadingOlder: false,
      isPosting: false,
      newCommentText: '',
      newCommentTaggedUsers: [],
    };
  }

  componentDidMount() {
    this._getRecentComments();
  }

  _getRecentComments = async () => {
    await this.props.getRecentComments(this.props.contentId)
    this.setState({
      isLoadingRecent: false,
    });
  }

  _getOlderComments = async () =>  {
    const { contentId, getOlderComments, comments } = this.props;

    this.setState({
      isLoadingOlder: true,
    });

    const oldestComment = comments && comments.length > 0 ? comments[comments.length-1] : null;

    getOlderComments(contentId, oldestComment);
    this.setState({
      isLoadingOlder: false,
    });
  }

  _postTextTyping = (text) => {
    this.setState({
      newCommentText: text
    });
  }

  _postCommentPressed = () =>  {
    const { addComment, contentId } = this.props;
    const { newCommentText, newCommentTaggedUsers } = this.state;

    if(newCommentText) {
      this.setState({
        isPosting: true,
      });
      addComment(contentId, newCommentText, newCommentTaggedUsers)
        .then(() => {
          this.setState({
            newCommentText: '',
            newCommentTaggedUsers: [],
          });
        })
        .finally(() => {
          this.setState({
            isPosting: false,
          });
        });
    }
  }

  _tagUserPressed = () =>  {
    const { goToUserSearch } = this.props;
    goToUserSearch(this._taggedUserSelected);
  }

  _taggedUserSelected = (user) => {
    const { popScene } = this.props;

    this.setState({
      newCommentTaggedUsers: _.uniqWith([...this.state.newCommentTaggedUsers, user], _.isEqual)
    });
    popScene(); // Gets rid of the user search screen
  }

  _removeUserPressed = (userToRemove) => {
    this.setState({
      newCommentTaggedUsers: this.state.newCommentTaggedUsers
        .filter(taggedUser => taggedUser.id !== userToRemove.id)
    });
  }

  render() {
    const { comments, hasOlder } = this.props;
    const { isLoadingOlder, isLoadingRecent, isPosting,
      newCommentText, newCommentTaggedUsers,
    } = this.state;
    return (
      <View style={{flex: 1}}>
        <Comments
          comments={comments}
          isLoadingRecent={isLoadingRecent}
          isLoadingOlder={isLoadingOlder}
          isPosting={isPosting}
          hasOlderComments={hasOlder}
          newCommentText={newCommentText}
          newCommentTaggedUsers={newCommentTaggedUsers}
          onPressLoadOlderComments={this._getOlderComments}
          onPressPostComment={this._postCommentPressed}
          onPressTagUser={this._tagUserPressed}
          onPressRemoveUser={this._removeUserPressed}
          onTypingNewComment={this._postTextTyping}
          onRefresh={this._getRecentComments}
        />
      </View>
    );
  }

}

const mapStateToProps = (state, { contentId }) => {
  return {
    comments: commentsGroupByContentSelector(state)[contentId] || [],
    hasOlder: hasOlderSelector(state),
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    addComment: (contentId, text, taggedUsers) => {
      return dispatch(addComment(contentId, text, taggedUsers));
    },
    getRecentComments: (contentId) => {
      return dispatch(getRecentComments(contentId));
    },
    getOlderComments: (contentId, lastComment) => {
      return dispatch(getMoreComments(contentId, lastComment));
    },
    goToUserSearch: (onUserSelected) => {
      return dispatch(goToUserSearch({
        onSearchResultSelected: onUserSelected
      }));
    },
    popScene: () => {
      return dispatch(popScene());
    }
  };
};

const ConnectedCommentsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CommentsContainer);

export default ConnectedCommentsContainer;
