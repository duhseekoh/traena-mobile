// @flow

type Activity = {
  date: string,
  count: number,
};

type Stats = {
  activity: Activity,
  activityStreak: number,
  comments: Activity[],
  completedTasks: Activity[],
};

export type {
  Activity,
  Stats,
};
