// @flow
import React from 'react';
import type { DailyActionContentItem } from 'app/redux/modules/content/types';
import GenericContentItem from '../GenericContentItem';

type Props = {
  contentItem: DailyActionContentItem,
  onLikePressed: (updatedDoesLike: boolean) => void,
  onCommentPressed: () => void,
  onBookmarkPressed: () => void,
  onCompletePressed: () => void,
};

function DailyAction(props: Props) {
  return (
    <GenericContentItem {...props} />
  );
}

export default DailyAction;
