// @flow
import { Platform } from 'react-native';

const black = '#000',
  white = '#FFF',
  lightGrey = '#e3e3e3',
  lightGrey2 = '#999',
  lightGrey3 = '#ebebeb',
  lightGrey4 = '#fbfbfb',
  lightGrey5 = '#f7f7f7',
  lightGrey6 = '#d8d9da',
  mediumGrey = '#B1B3B5', // in traena color palette
  mediumGrey2 = '#e1e1e1',
  mediumGrey3 = '#666',
  darkGrey2 = '#383839',
  darkGrey = '#404041',
  darkGrey3 = '#a8aaac',
  darkGrey4 = '#595959',
  traenaPrimaryGreen = '#00B49C',
  traenaPrimaryGreenLighter = '#afe6de',
  traenaSecondaryGreen = '#8CC63E',
  traenaLightRed= '#f97f6a',
  forestGreen = '#0b5a65',
  blueGreen = '#008A97',
  blueGreen15percent = '#D9EEF0',
  paleGreen='#076570',
  pureGreen = '#0F0',
  smoothRed = '#C44',
  pureRed = '#da4453',
  candyRed = '#FE0000',
  gradientBlue= '#9aadd4',
  darkSlateGrey = '#404041',
  seriesCard1 = '#00ab94',
  seriesCard2 = '#4cc7b6',
  seriesCard3 = '#7dd2cd',
  seriesCard4 = '#008a97',
  seriesCard5 = '#034552';

const color = {
  // TODO - replace with better names once we define a color system
  candyRed,
  traenaOne: traenaPrimaryGreen,
  traenaPrimaryGreen,
  traenaSecondaryGreen,
  traenaPrimaryGreenLighter,
  black,
  white,
  lightGrey,
  lightGrey2,
  lightGrey3,
  lightGrey4,
  lightGrey5,
  lightGrey6,
  mediumGrey,
  mediumGrey2,
  mediumGrey3,
  darkSlateGrey,
  darkGrey2,
  darkGrey3,
  darkGrey4,
  forestGreen,
  blueGreen,
  blueGreen15percent,
  paleGreen,
  gradientBlue,
  darkGrey,
  positive: pureGreen,
  negative: pureRed,
  chartPositive: traenaPrimaryGreen,
  chartNegative: traenaLightRed,
  formError: smoothRed,
  translucentBGLight: 'rgba(0,0,0,0.3)',
  translucentWhite: 'rgba(255,255,255,0.3)',
  textShadow: 'rgba(0,0,0,0.1)',
  translucentBGgreen: 'rgba(0, 180, 156, .2)',
  transparent: 'transparent',
  regular: darkGrey2,
  seriesCard1,
  seriesCard2,
  seriesCard3,
  seriesCard4,
  seriesCard5,
};

const fontSize = {
  xxxlarge: 55,
  xxlarge: 40,
  xlarger: 32,
  xlarge: 24,
  large: 20,
  medlar: 16,
  regular: 14,
  medium: 14, // default font size (at least what react native has as default)
  small: 12,
  xsmall: 10,
  xxsmall: 8
};

// ios: font families are referenced by the logical font name
// android: font families are referenced by the filenames in android/app/src/main/assets/fonts
const fontFamily = {
  base: Platform.OS === 'ios' ? 'Halcom' : 'The Northern Block Ltd - Halcom',
  bold: Platform.OS === 'ios' ? 'Halcom-Bold' : 'The Northern Block Ltd - Halcom Bold',
  medium: Platform.OS === 'ios' ? 'Halcom-Medium' : 'The Northern Block Ltd - Halcom Medium',
  mono: Platform.OS === 'ios' ? 'Menlo' : 'monospace'
};

const iconSize = {
  xxxlarge: 50,
  xxlarge: 44,
  xlarge: 32,
  large: 24,
  medlar: 20,
  medium: 18,
  small: 16,
  xsmall: 12
};

const padding = {
  // 'content' blocks should use this to stay consistently away from screen
  edgeOfScreen: 10,
  // the tab bar has a set height, some views may need to pad the bottom so the
  // tab bar does not overlap
  tabBarHeight: 50,
};

const textInputHeights = {
  medium: 40,
};

const shadow = {
  shadowOne: {
    shadowColor: color.black,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
  shadowTwo: {
    shadowColor: color.black,
    shadowOpacity: .7,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 2,
    elevation: 2,
  },
  shadowThree: {
    shadowColor: color.black,
    shadowOpacity: .5,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 2,
    elevation: 2,
  },
  shadowFour: {
    shadowColor: color.black,
    shadowOpacity: .2,
    shadowOffset: {
      width: 0,
      height: -2,
    },
    shadowRadius: 3,
    elevation: 2,
  },
  none: {
    shadowColor: color.transparent,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
  }
}

const opacity = {
  modalOpacity: .5,
  disabledButton: .3,
};

export default {
  color,
  fontSize,
  fontFamily,
  iconSize,
  padding,
  textInputHeights,
  opacity,
  shadow,
}
