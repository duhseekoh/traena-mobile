// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import type {
  NavigationNavigatorProps,
  NavigationEventSubscription
} from 'react-navigation';
import {
  View
} from 'react-native';
import Notifications from './components/Notifications';
import {
  acknowledgeNotifications,
  openNotification,
  getOlderNotifications,
  getRecentNotifications,
} from 'app/redux/modules/notification/actions';
import {
  hasOlderSelector,
  isLoadingSelector,
  notificationsSelector
} from 'app/redux/modules/notification/selectors';
import type { Notification } from 'app/redux/modules/notification/types';
import logger from 'app/logging/logger';
import type { Dispatch } from 'app/redux/constants';

type Props = {
  acknowledgeNotifications: () => void,
  getOlderNotifications: () => void,
  openNotification: () => void,
  refreshNotifications: () => void,
  notifications: Notification[],
  isLoading?: boolean,
  hasOlder?: boolean,
  isSelected?: boolean,
} & NavigationNavigatorProps<*, *>;

class NotificationsContainer extends Component<Props> {
  _navigationListener: NavigationEventSubscription;
  componentDidMount() {
    this._navigationListener = this.props.navigation.addListener(
      'didFocus',
      () => {
        logger.logBreadcrumb('Notifications displayed');
        this.props.acknowledgeNotifications();
        this.props.refreshNotifications();
      }
    );
  }

  componentWillUnmount() {
    this._navigationListener.remove();
  }

  render() {
    const { notifications, refreshNotifications, getOlderNotifications,
      openNotification, isLoading, hasOlder } = this.props;

    return (
      <View style={{flex: 1}}>
        <Notifications
          notifications={notifications}
          isLoading={isLoading}
          hasOlder={hasOlder}
          onOpenNotification={openNotification}
          onRefresh={refreshNotifications}
          onGetOlderNotifications={getOlderNotifications}
        />
      </View>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    notifications: notificationsSelector(state),
    isLoading: isLoadingSelector(state),
    hasOlder: hasOlderSelector(state),
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    acknowledgeNotifications: () => {
      return dispatch(acknowledgeNotifications());
    },
    openNotification: (notification) => {
      return dispatch(openNotification(notification));
    },
    refreshNotifications: () => {
      // TODO - We could miss some notifications if more than 10 have been sent since
      // the last request. Could use a minDate on the endpoint instead and pass in the
      // most recent notification.
      return dispatch(getRecentNotifications());
    },
    getOlderNotifications: () => {
      return dispatch(getOlderNotifications());
    }
  };
};

const ConnectedNotificationsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(withNavigation(NotificationsContainer));

export default ConnectedNotificationsContainer;
