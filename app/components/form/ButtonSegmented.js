// @flow
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { TRATextStrong } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  title: string,
  style?: ViewStyleProp,
  onPress?: () => *,
  isSelected?: boolean,
};

const ButtonSegmented = ({title, style, onPress, isSelected}: Props) => {
  const buttonStyles = [style, styles.button, isSelected ? styles.buttonSelected : null];
  const buttonTextStyles = [styles.buttonText, isSelected ? styles.buttonTextSelected : null];

  return (
    <TouchableOpacity style={buttonStyles} onPress={onPress}>
      <View>
        <TRATextStrong style={buttonTextStyles}>{title}</TRATextStrong>
      </View>
    </TouchableOpacity>
  )
};

const styles = StyleSheet.create({
  button: {
    flex: 1,
    paddingVertical: 10,
  },
  buttonSelected: {
    borderBottomWidth: 3,
    borderBottomColor: styleVars.color.traenaOne,
  },
  buttonText: {
    textAlign: 'center',
    fontSize: 10,
    color: styleVars.color.mediumGrey,
    fontFamily: styleVars.fontFamily.bold,
  },
  buttonTextSelected: {
    color: styleVars.color.traenaOne,
  },
});

export default ButtonSegmented;
