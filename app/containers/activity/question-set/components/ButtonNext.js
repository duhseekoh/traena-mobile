// @flow
import React from 'react';
import {
  StyleSheet,
} from 'react-native';
import {
  ButtonStyled,
} from 'app/components';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import styleVars from 'app/style/variables';

type Props = {
  text: string,
  style?: ViewStyleProp,
  onPress: () => any,
  disabled: boolean,
  loading: boolean,
  focused: boolean,
};


const ButtonQuiz = (props: Props) =>
  <ButtonStyled
    defaultTextStyle={styles.text}
    defaultButtonStyle={styles.button}
    focusedTextStyle={styles.activeText}
    focusedButtonStyle={styles.activeButton}
    disabledTextStyle={styles.disabledText}
    disabledButtonStyle={styles.disabledButton}
    loadingTextStyle={{}}
    loadingButtonStyle={{}}
    {...props} />

export default ButtonQuiz;

const styles = StyleSheet.create({
  text: {
    textAlign: 'center',
    fontSize: styleVars.fontSize.medium,
    color: styleVars.color.traenaOne,
    padding: 16,
    backgroundColor: styleVars.color.transparent,
  },
  activeText: {
    color: styleVars.color.white,
  },
  disabledText: {
    color: styleVars.color.lightGrey,
  },
  button: {
    backgroundColor: styleVars.color.white,
    borderColor: styleVars.color.traenaOne,
    borderWidth: 2,
    borderRadius: 30,
  },
  activeButton: {
    backgroundColor: styleVars.color.traenaOne,
  },
  disabledButton: {
    borderColor: styleVars.color.lightGrey,
  }
});
