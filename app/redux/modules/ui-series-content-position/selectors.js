// @flow
import { seriesProgressSelector } from 'app/redux/modules/series/selectors';
import type { ReduxState } from 'app/redux/constants';

const STATE_KEY = 'uiSeriesContentPosition';
const stateFromRoot = (state) => state[STATE_KEY];

export const isLoadingSelector = (state: ReduxState) => stateFromRoot(state).isLoading;

type Ids = {
  seriesId: number,
  contentId?: number,
};

export const selectSeriesLength = (state: ReduxState, { seriesId }: Ids) => {
  const seriesProgress = seriesProgressSelector(state, { id: seriesId });
  if (!seriesProgress || !seriesProgress.contentIds) {
    return null;
  }
  return seriesProgress.contentIds.length;
}

export const selectSeriesContentPosition = (state: ReduxState, { seriesId, contentId }: Ids) => {
  const seriesProgress = seriesProgressSelector(state, { id: seriesId });
  if (!seriesProgress || !seriesProgress.contentIds) {
    return null;
  }
  const seriesContentOrder = seriesProgress.contentIds;
  return seriesContentOrder.indexOf(contentId) + 1;
}

export const selectNextSeriesContentId = (state: ReduxState, { seriesId, contentId }: Ids) => {
  const seriesProgress = seriesProgressSelector(state, { id: seriesId });
  if (!seriesProgress || !seriesProgress.contentIds) {
    return null;
  }
  const seriesContentOrder = seriesProgress.contentIds;
  const contentPosition = seriesContentOrder.indexOf(contentId);
  if (contentPosition === seriesContentOrder.length - 1) {
    return null;
  }
  return seriesContentOrder[contentPosition + 1];
}

export const selectPrevSeriesContentId = (state: ReduxState, { seriesId, contentId }: Ids) => {
  const seriesProgress = seriesProgressSelector(state, { id: seriesId });
  if (!seriesProgress || !seriesProgress.contentIds) {
    return null;
  }
  const seriesContentOrder = seriesProgress.contentIds;
  const contentPosition = seriesContentOrder.indexOf(contentId);
  if (contentPosition === 0) {
    return null;
  }
  return seriesContentOrder[contentPosition - 1];
}
