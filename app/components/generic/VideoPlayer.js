// @flow
import _ from 'lodash';
import React, { Component } from 'react';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { connect } from 'react-redux';
import {
  ActivityIndicator,
  ImageBackground,
  LayoutAnimation,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Orientation from 'react-native-orientation';
import {
  TRAText,
  TRATextStrong,
  FeatherIcon,
} from 'app/components';
import type { Dispatch } from 'app/redux/constants';
import moment from 'moment';
import Video from 'react-native-video';
import type { Video as VideoType} from 'app/types/Video';
import styleVars from 'app/style/variables';
import logger from 'app/logging/logger';
import { goToAndroidFullscreenVideo, exitAndroidFullscreenVideo } from 'app/redux/modules/navigation/actions';
import { setActiveVideoPlayer, videoMajorProgress } from 'app/redux/modules/ui-video-player/actions';
import { activeVideoIdSelector } from 'app/redux/modules/ui-video-player/selectors';

type OwnProps = {
  videoURI: string,
  previewImageURI: string,
  style?: ViewStyleProp,
  isAndroidFullscreen: boolean,
  eventMeta?: Object,
};

type State = {
  isVideoVisible: boolean,
  paused: boolean,
  isControlsVisible: boolean,
  isLoading: boolean,
  hasError: boolean,
  videoId: string,
  atTime?: moment$MomentDuration,
  timeLoaded?: moment$MomentDuration,
  totalTime?: moment$MomentDuration,
};

class VideoPlayer extends Component<OwnProps & ConnectedProps, State> {
  static defaultProps = {
    isAndroidFullscreen: false,
  };
  recordIntervalId: IntervalID | typeof undefined;
  fadeawayTimeout: TimeoutID;
  player: any;

  constructor(props: OwnProps & ConnectedProps) {
    super(props);
    this.state = {
      isVideoVisible: false,
      paused: true,
      isControlsVisible: false,
      isLoading: false,
      hasError: false,
      videoId: _.uniqueId(),
    };
    this.recordIntervalId = undefined;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.activeVideoId !== this.state.videoId) {
      if (Platform.OS === 'android') {
        // On android, if a video is displayed in the fullscreen route but there is a video
        // behind it on a different route, the video behind it shows through. This will
        // set the video to be hidden and the preview image displayed instead.
        this.setState({
          isVideoVisible: false,
        });
      } else {
        this.setState({
          paused: true,
        });
      }
    }
  }

  componentWillUnmount() {
    clearInterval(this.recordIntervalId);
  }

  render() {
    const { videoURI, style, previewImageURI } = this.props;

    if(!videoURI || !previewImageURI) {
      return(
        <View style={[style, styles.containerErrorView]}>
          <TRATextStrong style={styles.errorText}>Oops. Looks like this video is missing.</TRATextStrong>
        </View>
      );
    }

    if(this.props.isAndroidFullscreen) {
      return(this._renderAndroidFullscreen({videoURI, previewImageURI}));
    }
    else {
      return(this._renderInline(style, {videoURI, previewImageURI}));
    }
  }

  _renderAndroidFullscreen = (video) => {
    return (
      <View style={{flex: 1}}>
        <ImageBackground
          style={stylesFullscreen.backgroundImage}
          resizeMode='contain'
          source={{uri: video.previewImageURI}}>

          {this.state.isVideoVisible ?
            <TouchableWithoutFeedback onPress={this._onPressVideo}>
              <View style={stylesFullscreen.videoArea}>
                {this._renderVideo(video)}
              </View>
            </TouchableWithoutFeedback> :

            <TouchableOpacity onPress={this._onPressPlay} delayPressIn={50} style={stylesFullscreen.videoPlayIcon}>
              <FeatherIcon name='play' style={styles.playIcon} />
            </TouchableOpacity>
          }

          {(this.state.isVideoVisible && this.state.isControlsVisible) ?
            (this._renderControls()) : this._renderMessaging()}
          </ImageBackground>
      </View>
    );
  }

  _renderInline = (style, video) => {
    return (
        <View style={[style, stylesInline.containerView]}>
          <ImageBackground style={stylesInline.backgroundImage} source={{uri: video.previewImageURI}}>

            {this.state.isVideoVisible ?
              <TouchableWithoutFeedback onPress={this._onPressVideo}>
                <View style={{flex: 1}}>

                  <View style={stylesInline.videoArea}>
                    {this._renderVideo(video)}
                  </View>

                  {this.state.isControlsVisible ?
                    this._renderControls() : this._renderMessaging()}
                </View>
              </TouchableWithoutFeedback> :

              <TouchableOpacity onPress={this._onPressPlay} delayPressIn={50} style={stylesInline.videoArea}>
                <FeatherIcon name='play' style={styles.playIcon} />
              </TouchableOpacity>
            }

          </ImageBackground>
        </View>
      );
  }

  /**
   * Before the video is shown, this is the button that launches it.
   * Also used when pressing the play button on the controls overlay.
   */
  _onPressPlay = () => {
    this.props.setActiveVideoPlayer(this.state.videoId);
    this.setState({
      isVideoVisible: true,
      paused: false,
    }, () => {
      this._fadeawayControls();
    });
  }

  /**
   * Used when pressing the pause button on the controls overlay.
   */
  _onPressPause = () =>  {
    this.setState({
      paused: true,
    }, () => {
      clearTimeout(this.fadeawayTimeout);
    });
  }

  /**
   * Once the video is shown, this is called whenever it is tapped
   * Toggles Pause and Resume
   */
  _onPressVideo = () =>  {
    LayoutAnimation.easeInEaseOut();
    this.setState({
      isControlsVisible: true,
    }, () => {
      this._fadeawayControls();
    });
  }

  /**
   * When a video is tapped, the overal controls are shown. This is called at the same time
   * to make sure the overlay controls disappear after a short duration.
   */
  _fadeawayControls = () =>  {
    clearTimeout(this.fadeawayTimeout);
    this.fadeawayTimeout = setTimeout(() => {
      LayoutAnimation.easeInEaseOut();
      this.setState({
        isControlsVisible: false,
      });
    }, 3000);
  }

  _renderVideo = (video) => {
    return (
      <Video source={{uri: video.videoURI}}   // Can be a URL or a local file.
         ref={(ref) => {
           this.player = ref
         }}                             // Store reference
         rate={1.0}                     // 0 is paused, 1 is normal.
         volume={1.0}                   // 0 is muted, 1 is normal.
         muted={false}
         paused={this.state.paused}     // Pauses playback entirely.
         resizeMode="contain"           // 'contain' - Entire video is shown in container, maintains aspect ratio, so if not 16:9, then bars will be seen
         repeat={true}                  // Combo of this and onEnd seeking to beginnning to true enables video to be played multiple times on android
         controls={false}
         playInBackground={false}       // Audio continues to play when app entering background.
         playWhenInactive={false}       // [iOS] Video continues to play when control or notification center are shown.
         progressUpdateInterval={250} // [iOS] Interval to fire onProgress (default to ~250ms)
         style={styles.video}
         onLoadStart={this._onVideoStartLoad}
         onLoad={this._onVideoLoad}
         onProgress={this._onProgress}
         onEnd={this._onVideoEnd}
         onError={this._onError}
         />
    );
  }

  _renderMessaging = () => {
    if(this.state.isLoading) {
      return (
        <View style={styles.overlay}>
          <ActivityIndicator
            size="small"
            color={styleVars.color.traenaPrimaryGreen}
          />
        </View>
      );
    }
    if(this.state.hasError) {
      return (
        <View style={styles.overlay}>
          <TRATextStrong style={styles.errorText}>Error loading video.</TRATextStrong>
        </View>
      );
    }
  }

  _renderControls = () => {
    return (
      <View style={styles.controls}>
        {this.state.paused ?
          <TouchableOpacity onPress={this._onPressPlay}
            hitSlop={{top: 100, left: 100, right: 100, bottom: 100}}>
            <FeatherIcon name='play' style={styles.bigActionIcon} fontSize={styleVars.iconSize.xxxlarge}/>
          </TouchableOpacity>
          :
          <TouchableOpacity onPress={this._onPressPause}
            hitSlop={{top: 100, left: 100, right: 100, bottom: 100}}>
            <FeatherIcon name='pause' style={styles.bigActionIcon} fontSize={styleVars.iconSize.xxxlarge}/>
          </TouchableOpacity>
        }

        { !this.props.isAndroidFullscreen ?
          <View style={stylesInline.extraControlsHolder}>
            <TouchableOpacity onPress={this._onPressEnterFullscreen}
              hitSlop={{top: 20, left: 20, right: 20, bottom: 20}}>
              <FeatherIcon name='maximize-2' style={styles.extraControlIcon} fontSize={styleVars.iconSize.large} color={styleVars.color.white}/>
            </TouchableOpacity>
          </View> :
          (<View style={stylesFullscreen.extraControlsHolder}>
            <TouchableOpacity onPress={this._onPressExitAndroidFullscreen}
              hitSlop={{top: 20, left: 20, right: 20, bottom: 20}}>
              <FeatherIcon name='x' style={styles.extraControlIcon} fontSize={styleVars.iconSize.large} color={styleVars.color.white} />
            </TouchableOpacity>
          </View>)
        }

        <View style={styles.videoMetaHolder}>
          <TRAText style={styles.videoMetaText}>
            {this.state.atTime ?
              moment.utc(this.state.atTime.asMilliseconds()).format("m:ss") :
              '--:--'
            } /
            <Text> {this.state.totalTime ?
              moment.utc(this.state.totalTime.asMilliseconds()).format("m:ss") :
              '--:--'
            }</Text>
          </TRAText>
          <TouchableOpacity onPress={this._onPressReplay}
            hitSlop={{top: 20, left: 20, right: 20, bottom: 20}}>
            <FeatherIcon name='rotate-cw' style={styles.extraControlIcon} fontSize={styleVars.iconSize.large} color={styleVars.color.white}/>
          </TouchableOpacity>
        </View>
      </View>
    );

  }

  _onVideoStartLoad = () => {
    const { previewImageURI, videoURI } = this.props;
    logger.logBreadcrumb('video starting to load', { previewImageURI, videoURI });
    LayoutAnimation.easeInEaseOut();

    // restart android at previous time. Since we are hiding videos to avoid
    // overlapping video leak they don't hold their own state. This does not
    // set time in the full screen view.
    if(Platform.OS === 'android' && this.state.atTime) {
      this.player.seek(this.state.atTime.asSeconds());
    }
    this.setState({
      isLoading: true,
    });
  }

  _onVideoLoad = ({ duration }) => {
    const { previewImageURI, videoURI } = this.props;
    logger.logBreadcrumb('video loaded', { previewImageURI, videoURI, duration });
    LayoutAnimation.easeInEaseOut();
    this.setState({
      totalTime: moment.duration(duration, 'seconds'),
      isLoading: false,
    });

    // Now that the video has loaded, record an event every 10 seconds
    if (!this.recordIntervalId) {
      this.recordIntervalId = setInterval(this._recordMajorProgress, 10000);
      this._recordMajorProgress();
    }
  }

  _onProgress = ({ currentTime, playableDuration }) => {
    this.setState({
      atTime: moment.duration(currentTime, 'seconds'),
      timeLoaded: moment.duration(playableDuration, 'seconds'),
    });
  }

  // if the video isn't paused and its loaded, then we record that as analytics
  _recordMajorProgress = () => {
    let atTime = this.state.atTime ? this.state.atTime.asMilliseconds() : 0;
    let timeLoaded = this.state.timeLoaded ? this.state.timeLoaded.asMilliseconds() : 0;
    if (!this.state.paused && atTime >= 0 && timeLoaded > 0) {
      const percentComplete = (atTime / timeLoaded) * 100;
      this.props.videoMajorProgress(percentComplete, this.props.isAndroidFullscreen, this.props.eventMeta);
    }
  }

  _onVideoEnd = () => {
    this.props.videoMajorProgress(100, this.props.isAndroidFullscreen, this.props.eventMeta);
    // android exoplayer doesnt work with repeat=true, so seek to beginning manually
    if (Platform.OS === 'android') {
      this.player.seek(0);
    }
  }

  _onError = (errorParams) => {
    const { previewImageURI, videoURI } = this.props;
    logger.logBreadcrumb('error playing video', { previewImageURI, videoURI, errorParams });
    this.setState({
      hasError: true,
      isLoading: false,
    });
  }

  _onPressEnterFullscreen = () => {
    if(Platform.OS === 'android') {
      this.props.goToAndroidFullscreenVideo({videoURI: this.props.videoURI, previewImageURI: this.props.previewImageURI});
    }
    else {
      this.player.presentFullscreenPlayer();
    }
  }

  _onPressExitAndroidFullscreen = () => {
    // locking here to avoid seeing feed in landscape
    Orientation.lockToPortrait();
    this.props.exitAndroidFullscreenVideo();
  }

  _onPressReplay = () => {
    if(this.player) {
      this.player.seek(0);
      this.player.seek(0); //iOS won't seek to beginning without a sec call
    }
  }
}

type ConnectedProps = {
  activeVideoId?: string,
  setActiveVideoPlayer: (videoId: string) => Promise<*>,
  goToAndroidFullscreenVideo: (video: VideoType) => Promise<*>,
  exitAndroidFullscreenVideo: () => Promise<*>,
  videoMajorProgress: (percent: number, isAndroidFullscreen: boolean, eventMeta: any) => Promise<*>,
};

const mapStateToProps = (state) => {
  return {
    activeVideoId: activeVideoIdSelector(state),
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    setActiveVideoPlayer: (videoId) => {
      return dispatch(setActiveVideoPlayer(videoId));
    },
    goToAndroidFullscreenVideo: (video) => {
      return dispatch(goToAndroidFullscreenVideo(video));
    },
    exitAndroidFullscreenVideo: () => {
      return dispatch(exitAndroidFullscreenVideo());
    },
    videoMajorProgress: (percent, isAndroidFullscreen, eventMeta) => {
      return dispatch(videoMajorProgress(percent, isAndroidFullscreen, eventMeta));
    },
  };
}

const ConnectedVideoPlayer = connect(
  mapStateToProps,
  mapDispatchToProps
)(VideoPlayer);

export default ConnectedVideoPlayer;
const styles = StyleSheet.create({
  containerErrorView: {
    marginVertical: 15,
  },
  video: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    backgroundColor: styleVars.color.black,
  },
  videoMetaHolder: {
    position: 'absolute',
    top: 5,
    right: 5,
    alignItems: 'flex-end'
  },
  videoMetaText: {
    textShadowColor: styleVars.color.black,
    color: styleVars.color.white,
    backgroundColor: styleVars.color.transparent,
    fontSize: styleVars.fontSize.small,
    fontFamily: styleVars.fontFamily.mono,
  },
  playIcon: {
    fontSize: styleVars.iconSize.xxxlarge,
    opacity: .95,
    color: styleVars.color.white,
    backgroundColor: styleVars.color.transparent,
  },
  controls: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    backgroundColor: styleVars.color.translucentBGLight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bigActionIcon: {
    color: styleVars.color.white,
    padding: 30,
  },
  extraControlIcon: {
  },
  overlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    backgroundColor: styleVars.color.transparent,
    justifyContent: 'center',
    alignItems: 'center',
  },
  errorText: {
    textAlign: 'center',
  },
});

const stylesInline = StyleSheet.create({
  containerView: {
  },
  videoArea: {
    flex: 1,
    aspectRatio: 1.7777777778, // 16/9 container size for all videos
    justifyContent: 'center',
    alignItems: 'center',
  },
  extraControlsHolder: {
    position: 'absolute',
    bottom: 5,
    right: 5,
    alignItems: 'flex-end',
  },
  backgroundImage: {
    position: 'relative',
    aspectRatio: 1.7777777778,
    width: null,
    height: null,
    backgroundColor: styleVars.color.darkGrey3,
   },
});

const stylesFullscreen = StyleSheet.create({
  videoArea: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: styleVars.color.black,
  },
  videoPlayIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  extraControlsHolder: {
    position: 'absolute',
    left: 5,
    top: 5,
  },
  backgroundImage: {
    flex: 1,
    backgroundColor: styleVars.color.black,
   },
});
