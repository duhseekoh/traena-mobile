// @flow
import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import {
  TRATextStrong,
  UserInitials,
 } from 'app/components';
import styleVars from 'app/style/variables';
import type { User } from 'app/redux/modules/user/types';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  user: User,
  style?: ViewStyleProp,
  size?: 'tiny' | 'small' | 'medium' | 'large';
};

const UserAvatar = ({user, style, size = 'medium'}: Props) => {
  const sizeSettings = {
    tiny: { widthHeight: 20, borderRadius: 10, fontSize: styleVars.fontSize.xsmall },
    small: { widthHeight: 36, borderRadius: 18, fontSize: styleVars.fontSize.large },
    medium: { widthHeight: 64, borderRadius: 32, fontSize: styleVars.fontSize.xlarge },
    large: { widthHeight: 100, borderRadius: 50, fontSize: styleVars.fontSize.xxxlarge },
  };
  const selectedSize = sizeSettings[size];

  return (
    <View style={[styles.container, style]}>
      <View style={[styles.avatar, {
        width: selectedSize.widthHeight,
        height: selectedSize.widthHeight,
        borderRadius: selectedSize.borderRadius,
      }]}>
        <TRATextStrong style={[styles.avatarInitials, {
          fontSize: selectedSize.fontSize
        }]}>
          <UserInitials user={user} style={styles.avatarInitials} />
        </TRATextStrong>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatar: {
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: styleVars.color.darkGrey2
  },
  avatarInitials: {
    color: styleVars.color.white,
    textAlign: 'center',
    fontFamily: styleVars.fontFamily.bold,
  },
});

export default UserAvatar;
