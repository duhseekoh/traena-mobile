// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import { TRATextStrong, FeatherIcon, Tag } from 'app/components';
import { trendingTagsSelector, isLoadingSelector } from 'app/redux/modules/trending-tags/selectors';
import * as trendingTagsActions from 'app/redux/modules/trending-tags/actions';
import styleVars from 'app/style/variables';
import type { TagAggregation } from 'app/redux/modules/trending-tags/types';
import { goToTagStandalone } from 'app/redux/modules/navigation/actions';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style?: ViewStyleProp,
  tags: TagAggregation[],
  initialize: () => any,
  isLoading: boolean,
  goToTagStandalone: (value: string) => any,
};

class TrendingTagsContainer extends Component<Props> {
  componentDidMount() {
    this.props.initialize();
  }

  render() {
    const { tags, isLoading, goToTagStandalone, style } = this.props;
    const noNewContent = !isLoading &&
      !tags.length;
    const displayLoading = isLoading &&
      !tags.length;

    if (noNewContent) {
      return null;
    }

    return (
      <View style={style}>
        <TRATextStrong style={styles.title}>
          <FeatherIcon
            fontSize={styleVars.iconSize.medlar}
            color={styleVars.color.traenaPrimaryGreen}
            name={'trending-up'}
          /> Trending Tags
        </TRATextStrong>
        {displayLoading ?
          <ActivityIndicator
            size="small"
            color={styleVars.color.traenaPrimaryGreen}
          /> :
          <View style={styles.tagsWrapper}>
            {tags.map(tag => <Tag key={tag.key} value={tag.key} onPress={goToTagStandalone}/>)}
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: styleVars.fontSize.large,
    paddingHorizontal: styleVars.padding.edgeOfScreen,
    marginTop: 15,
  },
  tagsWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: styleVars.padding.edgeOfScreen,
  },
});

const mapStateToProps = (state) => ({
  tags: trendingTagsSelector(state),
  isLoading: isLoadingSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  initialize: () => dispatch(trendingTagsActions.initialize()),
  goToTagStandalone: tag => dispatch(goToTagStandalone(tag)),
})

export default connect(mapStateToProps, mapDispatchToProps)(TrendingTagsContainer);
