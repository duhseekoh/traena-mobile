// @flow
import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { TRAText, TRATextStrong } from 'app/components';
import styleVars from 'app/style/variables';
import { goToChannelStandalone } from 'app/redux/modules/navigation/actions';
import type { Channel } from 'app/redux/modules/channel/types';
import * as fromUser from 'app/redux/modules/user/selectors'

type Props = {
  channel: Channel,
  selfOrgId: number,
  style?: ViewStyleProp,
  goToChannelStandalone: (channel: Channel) => any,
};

const ChannelPreview = ({ channel, style, goToChannelStandalone, selfOrgId }: Props) => {
  const { name, description, organization } = channel;
  return (
    <View style={[styles.itemContainer, style]}>
      <TouchableOpacity onPress={() => goToChannelStandalone(channel)}>
        <View style={styles.content}>
          <View style={{ flex: 1 }}>
            <View style={styles.titleHolder}>
              <TRATextStrong
                style={styles.titleText}
                numberOfLines={1}
                ellipsizeMode={'tail'}
              >
                {name}
              </TRATextStrong>
              {organization && (organization.id !== selfOrgId) &&
                <TRAText
                  style={styles.organizationText}
                  numberOfLines={1}
                  ellipsizeMode={'tail'}
                >
                  {organization.name}
                </TRAText>
              }
              {description ?
                <TRAText
                  style={styles.descriptionText}
                  numberOfLines={3}
                  ellipsizeMode={'tail'}
                >
                  {description}
                </TRAText> : null
              }
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const mapDispatchToProps = dispatch => ({
  goToChannelStandalone: (channel: Channel) => {
    return dispatch(goToChannelStandalone(channel));
  },
});

const mapStateToProps = state => ({
  selfOrgId: fromUser.selectOrganizationId(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChannelPreview);

const styles = StyleSheet.create({
  itemContainer: {
    backgroundColor: styleVars.color.white,
    paddingVertical: 20,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 12,
  },
  titleHolder: {},
  titleText: {
    fontSize: styleVars.fontSize.large,
  },
  descriptionText: {
    fontSize: styleVars.fontSize.medium,
    color: styleVars.color.lightGrey2,
  },
  organizationText: {
    fontSize: styleVars.fontSize.medium,
    fontFamily: styleVars.fontFamily.bold,
    color: styleVars.color.darkGrey,
    marginBottom: 5,
  },
});
