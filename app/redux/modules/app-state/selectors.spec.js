import { persistedAppStateSelector } from './selectors';

it('persistedAppStateSelector will select data from several branches of the state tree', () => {

  const auth = { someAuthField: 'authField' },
    user = { someUserField: 'someUserField' },
    shouldNotBeIncluded = { shouldNotBeIncluded: 'shouldNotBeIncluded' };

  const state = {
    auth,
    user,
    shouldNotBeIncluded,
  };

  const selectedState = persistedAppStateSelector(state);
  expect(selectedState).toBeInstanceOf(Object);
  expect(selectedState.shouldNotBeIncluded).toBeUndefined();
  expect(selectedState.auth).toEqual(auth);
  expect(selectedState.user).toEqual(user);
});

it('persistedAppStateSelector will ignore blacklisted values', () => {
  const auth = {
    someAuthField: 'authField',
    loginRequestState: 'should not be stored!'
  };

  const state = {
    auth,
  };

  const selectedState = persistedAppStateSelector(state);
  expect(selectedState.auth.someAuthField).toEqual(auth.someAuthField);
  expect(selectedState.auth.loginRequestState).toBeUndefined();

});
