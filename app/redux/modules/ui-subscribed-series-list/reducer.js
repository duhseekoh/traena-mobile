// @flow
import _ from 'lodash';
import {
  REFRESH_SERIES_REQUEST_STARTED,
  REFRESH_SERIES_REQUEST_FAILED,
  LOAD_MORE_SERIES_REQUEST_STARTED,
  LOAD_MORE_SERIES_REQUEST_FAILED,
  REFRESHED_SERIES,
  LOADED_MORE_SERIES,
} from './actions';
import { REQUEST_STATUSES, type RequestStatus } from 'app/redux/constants';

export type ReduxStateSubscribedSeriesList = {|
  seriesIds: number[],
  loadingStatus: ?RequestStatus,
  refreshingStatus: ?RequestStatus,
  currentPage: number,
  canLoadMore: boolean,
|};

const ACTION_HANDLERS = {
  [REFRESHED_SERIES]: (state, action): ReduxStateSubscribedSeriesList => {
    const { seriesPage } = action.payload;
    const { ids, totalPages, number } = seriesPage;
    return {
      ...state,
      seriesIds: ids,
      canLoadMore: number < totalPages - 1,
      currentPage: number,
      loadingStatus: REQUEST_STATUSES.COMPLETE,
      refreshingStatus: REQUEST_STATUSES.COMPLETE,
    };
  },
  [LOADED_MORE_SERIES]: (state, action): ReduxStateSubscribedSeriesList => {
    const { seriesPage } = action.payload;
    const { ids, totalPages, number } = seriesPage;
    const mergedSeriesIds = _.uniq([...state.seriesIds, ...ids]);
    return {
      ...state,
      seriesIds: mergedSeriesIds,
      canLoadMore: number < totalPages - 1,
      currentPage: number,
      loadingStatus: REQUEST_STATUSES.COMPLETE,
      refreshingStatus: REQUEST_STATUSES.COMPLETE,
    };
  },
  [REFRESH_SERIES_REQUEST_STARTED]: (state): ReduxStateSubscribedSeriesList => {
    return {
      ...state,
      refreshingStatus: REQUEST_STATUSES.IN_PROGRESS,
    };
  },
  [REFRESH_SERIES_REQUEST_FAILED]: (state): ReduxStateSubscribedSeriesList => {
    return {
      ...state,
      refreshingStatus: REQUEST_STATUSES.ERROR,
    };
  },
  [LOAD_MORE_SERIES_REQUEST_STARTED]: (
    state,
  ): ReduxStateSubscribedSeriesList => {
    return {
      ...state,
      loadingStatus: REQUEST_STATUSES.IN_PROGRESS,
    };
  },
  [LOAD_MORE_SERIES_REQUEST_FAILED]: (
    state,
  ): ReduxStateSubscribedSeriesList => {
    return {
      ...state,
      loadingStatus: REQUEST_STATUSES.ERROR,
    };
  },
};

const initialState: ReduxStateSubscribedSeriesList = {
  seriesIds: [],
  loadingStatus: null,
  refreshingStatus: null,
  currentPage: 0,
  canLoadMore: false,
};

const subscribedSeriesListReducer = (
  state: ReduxStateSubscribedSeriesList = initialState,
  action: { type: string, payload: any },
) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default subscribedSeriesListReducer;
