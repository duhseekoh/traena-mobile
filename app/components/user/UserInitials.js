// @flow
import React from 'react';
import { TRAText } from 'app/components';
import { getInitials } from 'app/redux/modules/user/util';
import type { User } from 'app/redux/modules/user/types';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  user: User,
  style?: ViewStyleProp,
  size?: 'tiny' | 'small' | 'medium' | 'large';
};

const UserInitials = ({ style, user }: Props) => {
  const initials = getInitials(user);

  return (
    <TRAText style={style}>{initials}</TRAText>
  )
}

export default UserInitials;
