// @flow
import store from 'app/redux/store';
import { authHeadersSelector } from 'app/redux/modules/auth/selectors';

const getAuthHeaders = () => {
  return authHeadersSelector(store.getState());
}

export {
  getAuthHeaders,
};
