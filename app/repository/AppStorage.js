// @flow
import { AsyncStorage } from 'react-native';
import type { AppState } from 'app/redux/modules/app-state/types';

const appStateKey = '@appState';

class AppStorage {
  static async loadState() {
    try {
      const reduxState = await AsyncStorage.getItem(appStateKey);
      if(reduxState) {
        console.log('We have a saved state');
        console.log(reduxState);
        return JSON.parse(reduxState);
      } else {
        return undefined;
      }
    } catch (error) {
      console.log(error);
      console.log('Error retrieving appState');
      return undefined;
    }
  }

  static async saveState(stateToStore: AppState) {
    try {
      const stringifiedState = JSON.stringify(stateToStore);
      console.log(stringifiedState);
      return await AsyncStorage.setItem(appStateKey, stringifiedState);
    } catch (error) {
      console.log(error);
      console.log('Error saving appState');
    }
  }

  static async clearState() {
    try {
      return await AsyncStorage.removeItem(appStateKey);
    } catch(error) {
      console.log(error);
      console.log('Error clearing appState');
    }
  }
}

export default AppStorage;
