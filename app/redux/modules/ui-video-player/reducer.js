// @flow
import {
  SET_ACTIVE_VIDEO,
  CLEAR_ACTIVE_VIDEO,
 } from './actions';

 import type { Action } from 'app/types/Action';

 export type ReduxStateVideo = {
  activeVideoId: ?number,
 };

const ACTION_HANDLERS = {
  [SET_ACTIVE_VIDEO]: (state, action) => {
    return Object.assign({}, state, {
      activeVideoId: action.payload,
    });
  },
  [CLEAR_ACTIVE_VIDEO]: (state) => {
    return Object.assign({}, state, {
      activeVideoId: null,
    });
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  activeVideoId: null,
};

const videoPlayerReducer = (state: ReduxStateVideo = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default videoPlayerReducer;
