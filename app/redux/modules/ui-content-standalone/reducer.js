// @flow
import {
  STANDALONE_FETCH_STARTED,
  STANDALONE_FETCH_SUCCEEDED,
  STANDALONE_FETCH_FAILED,
 } from './actions';
import { REQUEST_STATUSES, type RequestStatus } from 'app/redux/constants';

import type { Action } from 'app/types/Action';

const ACTION_HANDLERS = {
  [STANDALONE_FETCH_STARTED]: (state: ReduxStateUiContentStandalone, action: Action) => {
    const contentId: number = action.payload;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [contentId]: {
          loadingStatus: REQUEST_STATUSES.IN_PROGRESS,
        }
      }
    }
  },
  [STANDALONE_FETCH_SUCCEEDED]: (state: ReduxStateUiContentStandalone, action: Action) => {
    const contentId: number = action.payload;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [contentId]: {
          loadingStatus: REQUEST_STATUSES.COMPLETE,
        }
      }
    }
  },
  [STANDALONE_FETCH_FAILED]: (state: ReduxStateUiContentStandalone, action: Action) => {
    const contentId: number = action.payload;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [contentId]: {
          loadingStatus: REQUEST_STATUSES.ERROR,
        }
      }
    }
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
export type ReduxStateUiContentStandalone = {
  uiSlices: {
    [contentId: number]: {
      loadingStatus: RequestStatus
    }
  }
};

const initialState = {
  uiSlices: { },
};

const contentStandaloneReducer = (state: ReduxStateUiContentStandalone = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default contentStandaloneReducer;
