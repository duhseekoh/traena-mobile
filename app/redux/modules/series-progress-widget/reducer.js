// @flow
import {
  LOAD_SERIES_PROGRESS_WIDGET_REQUEST_STARTED,
  LOAD_SERIES_PROGRESS_WIDGET_REQUEST_FINISHED,
} from './actions';
import type { Action } from 'app/types/Action';

export type ReduxStateSeriesProgressWidget = {
  seriesIds: number[],
  isLoading: boolean,
};

const ACTION_HANDLERS = {
  [LOAD_SERIES_PROGRESS_WIDGET_REQUEST_STARTED]: (state: ReduxStateSeriesProgressWidget) => {
    return {
      ...state,
      isLoading: true,
    };
  },
  [LOAD_SERIES_PROGRESS_WIDGET_REQUEST_FINISHED]: (state: ReduxStateSeriesProgressWidget, { payload }) => {
    const seriesIds = payload;
    return {
      ...state,
      isLoading: false,
      seriesIds,
    };
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  seriesIds: [],
  isLoading: false,
};

const seriesProgressWidget = (state: ReduxStateSeriesProgressWidget = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

export default seriesProgressWidget;
