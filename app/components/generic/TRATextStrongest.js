// @flow
import React from 'react';
import type { TextStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import {
  Text,
  StyleSheet,
} from 'react-native';
import styleVars from 'app/style/variables';

type Props = {
  style?: TextStyleProp,
  children: Node,
};

const TRATextStrongest = (props: Props) => {
  const {style, children} = props;
  return (
    <Text {...props} style={[styles.textStyle, style,]}>{children}</Text>
  );
}

export default TRATextStrongest;

const styles = StyleSheet.create({
  textStyle: {
    fontFamily: styleVars.fontFamily.bold,
    color: styleVars.color.regular,
  }
});
