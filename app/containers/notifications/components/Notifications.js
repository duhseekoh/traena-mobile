// @flow
import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import NotificationsList from './NotificationsList';
import type { Notification as NotificationType } from 'app/redux/modules/notification/types';

type Props = {
  notifications: NotificationType[],
  isLoading?: boolean,
  hasOlder?: boolean,
  onOpenNotification: (notification: NotificationType) => void,
  onGetOlderNotifications: () => void,
  onRefresh: () => void,
};

const Notifications = ({
  notifications,
  isLoading,
  hasOlder,
  onOpenNotification,
  onGetOlderNotifications,
  onRefresh,
}: Props) => {
  return (
    <View style={styles.container}>
      <NotificationsList
        style={styles.list}
        notifications={notifications}
        isLoading={isLoading}
        hasOlder={hasOlder}
        onPressLoadOlder={onGetOlderNotifications}
        onRefresh={onRefresh}
        onOpenNotification={onOpenNotification}
       />
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    flex: 1,
  },
});

export default Notifications;
