import { createSelector } from 'reselect';

const activityResponsesSelector = (state, props) => {
  return state.activityResponse.activityResponses.filter(r => r.activityId === props.activityId);
}

const hasActivityResponseSelector = createSelector(
  activityResponsesSelector,
  (activityResponses) => activityResponses.length > 0
);

const mostRecentActivityResponseSelector = createSelector(
  activityResponsesSelector,
  (activityResponses) => {
    return activityResponses.length > 0 ? activityResponses[0] : null;
  }
)

export {
  activityResponsesSelector,
  hasActivityResponseSelector,
  mostRecentActivityResponseSelector,
};
