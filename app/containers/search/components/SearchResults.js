// @flow
import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  ListView,
  StyleSheet,
} from 'react-native';
import type { SearchResults as SearchResultsType, Suggestion } from 'app/redux/modules/search/types';
import { TRATextStrong } from 'app/components';
import SearchResultItem from './SearchResultItem';
import SearchSuggestions from './SearchSuggestions';
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {|
  results: SearchResultsType,
  onSuggestionSelected: (suggestion: string) => void,
  onTagSelected: (tag: string) => void,
  onSearchResultSelected: () => void,
  recentSearches: Suggestion[],
  commonSearches: Suggestion[],
  contentTags: Suggestion[],
  style?: ViewStyleProp,
  isLoading?: boolean,
  queryHasNoResults?: boolean,
  displayNoMoreResults?: boolean,
  onPressLoadMore: () => void,
  hasQueryText?: boolean,
  canLoadMore?: boolean,
|};

type State = {
  dataSource: any,
};

export default class SearchResults extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(props.results),
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    // TODO - check if an update is necessary
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(nextProps.results)
    });
  }

  render() {
    const { style, isLoading } = this.props;
    const listViewStyles = [isLoading && styles.listViewLoading];

    return (
      <View style={[style, styles.container]}>

        <ListView
          style={listViewStyles}
          keyboardShouldPersistTaps={'never'}
          scrollingEnabled={true}
          enableEmptySections={true}
          dataSource={this.state.dataSource}
          renderRow={this._renderRow}
          renderHeader={this._renderHeader}
          renderFooter={this._renderFooter}
          onEndReached={this._loadMoreSearchResults}
          // needs to be greater than 0 or android wont trigger onEndReached
          onEndReachedThreshold={0.1}
          renderSeparator={(sectionID, rowID) => <View style={styles.listItemSeparator} key={sectionID+'_'+rowID} />}
        />

      </View>
    );
  }

  _loadMoreSearchResults = () => {
    if(this.props.hasQueryText && this.props.canLoadMore && !this.props.isLoading) {
      this.props.onPressLoadMore();
    }
  }

  _renderHeader = () => {
    const hasResults = !!this.state.dataSource.getRowCount();

    return (
      <View style={styles.headerContainer}>
        {!hasResults && this._renderNoResultsView()}
      </View>
    )
  }

  _renderFooter = () => {
    const { isLoading, displayNoMoreResults } = this.props;
    return (
      <View>
        { isLoading && (
          <ActivityIndicator
            size="small"
            color={styleVars.color.traenaPrimaryGreen}
          />
        )}
        { displayNoMoreResults && <TRATextStrong style={styles.loadingText}>no more results</TRATextStrong>}
      </View>
    )
  }

  _renderRow = (result /*,sectionID, rowID , highlightRow*/) => {
    const { onSearchResultSelected } = this.props;

    return (
      <SearchResultItem
        style={styles.searchResultItem}
        result={result}
        onPress={onSearchResultSelected} />
    )
  }

  _renderNoResultsView = () => {
    const {
      queryHasNoResults,
    } = this.props;

    // Searched, but no results came back
    if(queryHasNoResults) {
      return (
        <View>
          <View style={styles.suggestionsContainer}>
            <TRATextStrong>
              No results found. Search by typing full words or names.
              You can also try one of the suggestions below.
            </TRATextStrong>
          </View>
          {this._renderSearchSuggestions()}
        </View>
      );
    }

    // Not loading and the query is empty.
    return this._renderSearchSuggestions();
  }

  _renderSearchSuggestions = () => {
    const {
      recentSearches, commonSearches,
      onSuggestionSelected, contentTags, onTagSelected
    } = this.props;
    return (
      <View style={styles.suggestionsContainer}>
        { recentSearches && recentSearches.length > 0 &&
          <SearchSuggestions style={styles.searchSuggestions}
            title={'Recent'}
            suggestions={recentSearches}
            onSuggestionSelected={onSuggestionSelected}
          />
        }
        { commonSearches && commonSearches.length > 0 &&
          /**
          * TODO: Since these aren't actually `commmon` searches, we may want to
          * change naming of common -> suggested throughout app/api if they stay as a permanent feature
          */
          <SearchSuggestions style={styles.searchSuggestions}
            title={'Suggested'}
            suggestions={commonSearches}
            onSuggestionSelected={onSuggestionSelected}
          />
        }
        { contentTags && contentTags.length > 0 &&
          <SearchSuggestions style={styles.searchSuggestions}
            title={'Tags'}
            suggestions={contentTags}
            onSuggestionSelected={onTagSelected}
          />
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: styleVars.color.white,
  },
  headerContainer: {
    marginVertical: 5,
    marginHorizontal: styleVars.padding.edgeOfScreen
  },
  listViewLoading: {
    opacity: .5,
  },
  listItemSeparator: {
    marginVertical: 15/2,
  },
  suggestionsContainer: {
    marginTop: 10,
    marginHorizontal: 25
  },
  searchSuggestions: {
    marginBottom: 30,
  },
  loadingText: {
    textAlign: 'center',
    paddingVertical: 10,
  },
  searchResultItem: {
    marginHorizontal: styleVars.padding.edgeOfScreen,
  }
});
