// @flow
import React, { Component } from 'react';
import type {
  NavigationStackScreenOptions,
  NavigationNavigatorProps,
} from 'react-navigation';
import ProfileContainer from 'app/containers/profile/ProfileContainer';
import styleVars from 'app/style/variables';

type NavigationStateParams = *;
type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;
type Props = NavigationProps;

class ProfileScreen extends Component<Props> {
  static navigationOptions: (*) => NavigationStackScreenOptions = ({
    navigationOptions,
  }) => {
    return {
      headerStyle: {
        ...navigationOptions.headerStyle,
        backgroundColor: styleVars.color.traenaPrimaryGreen,
        ...styleVars.shadow.none,
      },
    };
  };

  render() {
    return <ProfileContainer />;
  }
}

export default ProfileScreen;
