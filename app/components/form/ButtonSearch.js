// @flow
import React from 'react';
import { StyleSheet } from 'react-native';
import ButtonStyled, { type Props } from './ButtonStyled';
import { FeatherIcon } from 'app/components';
import styleVars from 'app/style/variables';

const ButtonSearch = (props: Props) => (
  <ButtonStyled
    style={styles.container}
    icon={<FeatherIcon name="search" fontSize={styleVars.iconSize.large} />}
    {...props}
  />
);

export default ButtonSearch;

const styles = StyleSheet.create({
  container: {
    // could be moved to a wrapper around this that could be reused for all headerRight buttons
    paddingRight: styleVars.padding.edgeOfScreen,
  },
});
