// flow-typed signature: 07a252ca6f0ccb4cbdcf9408ee57c409
// flow-typed version: <<STUB>>/react-native-parsed-text_v^0.0.18/flow_v0.61.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'react-native-parsed-text'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'react-native-parsed-text' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'react-native-parsed-text/src/lib/TextExtraction' {
  declare module.exports: any;
}

declare module 'react-native-parsed-text/src/ParsedText' {
  declare module.exports: any;
}

// Filename aliases
declare module 'react-native-parsed-text/src/lib/TextExtraction.js' {
  declare module.exports: $Exports<'react-native-parsed-text/src/lib/TextExtraction'>;
}
declare module 'react-native-parsed-text/src/ParsedText.js' {
  declare module.exports: $Exports<'react-native-parsed-text/src/ParsedText'>;
}
