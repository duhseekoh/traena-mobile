// @flow
import React from 'react';
import {
  TouchableOpacity,
} from 'react-native';
import { MoreLessText } from 'app/components';

type Props = {
  onPress: () => *,
};

const ListFooterMore = ({onPress = () => {}}: Props) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <MoreLessText text={'More...'} />
    </TouchableOpacity>
  )
}

export default ListFooterMore;
