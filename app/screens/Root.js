// @flow
import React, { Component } from 'react';
import {
  AppState,
  View,
  StatusBar,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import Orientation from 'react-native-orientation';
import { withNetworkConnectivity } from 'react-native-offline';
import { Toast, Connectivity, ToastHolder, HandleUniversalLinks, KeyboardAvoidingViewPassthrough } from 'app/components';
import ModalDisplayer from './ModalDisplayer';
import Routes from './Routes';
import LoadingScreen from './LoadingScreen';
import LoginScreen from './LoginScreen';
import { isLoggedInSelector, isJwtValidSelector } from 'app/redux/modules/auth/selectors';
import { modalTypeSelector, modalPropsSelector } from 'app/redux/modules/modal/selectors';

import {
  loadAppState, saveAppState, makeAppToForegroundRequests, appActivated, appDeactivated, makeInitialNetworkRequests,
} from 'app/redux/modules/app-state/actions';
import { refreshJwt } from 'app/redux/modules/auth/actions';
import { navigateUniversalLink } from 'app/redux/modules/navigation/actions';
import type { ModalType } from 'app/types/Modal';
import styleVars from 'app/style/variables';

type Props = {
  appActivated: () => void,
  appDeactivated: () => void,
  isLoggedIn: boolean,
  hasValidJwt: boolean,
  modalType?: ModalType,
  modalProps?: any,
  saveAppState: () => void,
  loadAppState: () => Promise<*>,
  refreshJwt: () => void,
  makeAppToForegroundRequests: () => void,
  navigateUniversalLink: (url: string) => void,
  makeInitialNetworkRequests: () => void,
};

type State = {
  isAppStateInitialized: boolean,
};
/**
 * Responsible for app initialization. Setting up state handling, timers, etc...
 * Also displays either the login view or the main app view. No other display
 * should happen here.
 */
class Root extends Component<Props, State> {

  constructor(props) {
    super(props);
    this.state = {
      // not storing in redux, because app state initialization clears the redux
      // state and loads a saved one
      isAppStateInitialized: false,
    };

    // Load up the app state which was saved prior to termination
    this.props.loadAppState().then(() => {
      this.setState({
        isAppStateInitialized: true
      });
      if(this.props.isLoggedIn) {
        this.props.makeInitialNetworkRequests();
      }
    });
  }

  componentWillUpdate(nextProps) {
    if(!this.props.isLoggedIn && nextProps.isLoggedIn) {
      this.props.makeInitialNetworkRequests();
    }
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (currentAppState) => {
    if(currentAppState === 'inactive' || currentAppState === 'background') {
      this.props.saveAppState();
      this.props.appDeactivated();
    } else if(currentAppState === 'active') {
      this.props.appActivated();
      if(this.props.isLoggedIn) {
        this.props.makeAppToForegroundRequests();
      }
    }
  }

  render() {
    const { isAppStateInitialized } = this.state;
    const { modalType, modalProps, isLoggedIn, hasValidJwt } = this.props;
    const isTranslucentStatusBar = Platform.OS === 'ios';

    if(isAppStateInitialized) {
      const shouldDisplayLoginScreen = !isLoggedIn;
      const shouldDisplayLoadingScreen = isLoggedIn && !hasValidJwt;
      const shouldDisplayHome = isLoggedIn && hasValidJwt;
      return (
        <KeyboardAvoidingViewPassthrough behavior={'padding'} style={{flex: 1}}>

          <StatusBar
            backgroundColor={
              isTranslucentStatusBar ?
              styleVars.color.translucentBGgreen :
              styleVars.color.white
            }
            translucent={isTranslucentStatusBar}
            barStyle='dark-content'
          />
          {shouldDisplayLoginScreen && <LoginScreen />}
          {shouldDisplayLoadingScreen && <LoadingScreen />}
          {shouldDisplayHome &&
            <View style={{flex: 1}}>
              <Routes />
              <HandleUniversalLinks />
            </View>}
          <ModalDisplayer modalType={modalType} modalProps={modalProps} />
          <ToastHolder>
            <Connectivity />
            <Toast />
          </ToastHolder>

        </KeyboardAvoidingViewPassthrough>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: isLoggedInSelector(state),
    hasValidJwt: isJwtValidSelector(state),
    modalType: modalTypeSelector(state),
    modalProps: modalPropsSelector(state),
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveAppState: () => {
      return dispatch(saveAppState());
    },
    loadAppState: () => {
      return dispatch(loadAppState());
    },
    refreshJwt: () => {
      return dispatch(refreshJwt());
    },
    makeInitialNetworkRequests: () => {
      return dispatch(makeInitialNetworkRequests());
    },
    makeAppToForegroundRequests: () => {
      return dispatch(makeAppToForegroundRequests());
    },
    appActivated: () => {
      return dispatch(appActivated());
    },
    appDeactivated: () => {
      return dispatch(appDeactivated());
    },
    navigateUniversalLink: (url: string) => {
      return dispatch(navigateUniversalLink(url));
    }
  };
};

const ConnectedRoot = connect(
  mapStateToProps,
  mapDispatchToProps
)(Root);

export default withNetworkConnectivity({
  withRedux: true,
})(ConnectedRoot);
