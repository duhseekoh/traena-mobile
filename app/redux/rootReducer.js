import { reducer as network } from 'react-native-offline';
import { combineReducers } from 'redux';
import activity from './modules/activity/reducer';
import activityResponse from './modules/activityResponse/reducer';
import activityUI from './modules/ui-activity/reducer';
import auth from './modules/auth/reducer';
import bookmark from './modules/bookmark/reducer';
import bookmarkUI from './modules/ui-bookmark/reducer';
import uiBookmarksWidget from './modules/ui-bookmarks-widget/reducer';
import comment from './modules/comment/reducer';
import content from './modules/content/reducer';
import contentStandalone from './modules/ui-content-standalone/reducer';
import feed from './modules/feed/reducer';
import whatsNew from './modules/whats-new/reducer';
import modal from './modules/modal/reducer';
import notification from './modules/notification/reducer';
import search from './modules/search/reducer';
import series from './modules/series/reducer';
import stats from './modules/stats/reducer';
import traenaTask from './modules/traena-task/reducer';
import user from './modules/user/reducer';
import userSearch from './modules/user-search/reducer';
import videoPlayer from './modules/ui-video-player/reducer';
import toast from './modules/toast/reducer';
import tagStandalone from './modules/tag-standalone/reducer';
import channelStandalone from './modules/ui-channel-standalone/reducer';
import seriesStandalone from './modules/ui-series-standalone/reducer';
import channel from './modules/channel/reducer';
import uiChannelList from './modules/ui-channel-list/reducer';
import trendingTags from './modules/trending-tags/reducer';
import subscribedSeriesList from './modules/ui-subscribed-series-list/reducer';
import uiSeriesContentPosition from './modules/ui-series-content-position/reducer';
import seriesProgressWidget from './modules/series-progress-widget/reducer';
// Add other reducers here. Then add them to the combineReducers obj below

import { LOGGED_OUT } from 'app/redux/modules/auth/actions';
import { APP_STATE_LOADED } from 'app/redux/modules/app-state/actions';

const appReducer = combineReducers({
  feed,
  whatsNew,
  activity,
  activityResponse,
  activityUI,
  auth,
  bookmark,
  bookmarkUI,
  comment,
  content,
  contentStandalone,
  modal,
  notification,
  search,
  seriesStandalone,
  series,
  subscribedSeriesList,
  stats,
  traenaTask,
  user,
  userSearch,
  videoPlayer,
  toast,
  network,
  tagStandalone,
  channelStandalone,
  uiBookmarksWidget,
  channel,
  uiChannelList,
  trendingTags,
  uiSeriesContentPosition,
  seriesProgressWidget,
});

const rootReducer = (state, action) => {
  if(action.type === LOGGED_OUT) {
    // makes it so all reducers end up using their initial state
    state = undefined;
  }

  if(action.type === APP_STATE_LOADED) {
    const storedState = action.payload;
    // whatever app state that was stored loaded up
    state = storedState;
  }

  return appReducer(state, action);
};

export default rootReducer;
