// @flow
import React from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Animated,
} from 'react-native';
import ImageAutoSize from './ImageAutoSize';
import type { ImageContentItem, Image } from 'app/redux/modules/content/types';
import GenericContentItem from '../GenericContentItem';
import styleVars from 'app/style/variables';

type Props = {
  contentItem: ImageContentItem,
  onLikePressed: (doesUserLike: boolean) => void,
  onCommentPressed: () => void,
  onBookmarkPressed: () => void,
  onCompletePressed: () => void,
  onPressImage: (imageIndex: number) => void,
  isDetailView?: boolean,
};

type State = {
  galleryWidth: ?number,
};

class ImageContentType extends React.Component<Props, State> {
  scrollX = new Animated.Value(0);

  constructor(props: Props) {
    super(props);

    this.state = {
      // Initial value of null means the multi-images won't attempt to render
      // until we get a galleryWidth set from the scrollview layout lifecycle
      galleryWidth: null,
    };
  }

  /**
   * Capture the scroll view's width so we can set the pages
   * of the scroll view to take up the full width of the scroll view
   */
  onScrollViewLayout = ({nativeEvent}: *) => {
    // Don't update if width is the same
    if (nativeEvent.layout.width === this.state.galleryWidth) {
      return;
    }

    this.setState({
      galleryWidth: nativeEvent.layout.width,
    });
  }

  /**
   * Render the dots below the images. The unique dot represents the active image.
   */
  renderImagePager(count: number) {
    if(!this.state.galleryWidth) {
      return null;
    }

    const position = Animated.divide(this.scrollX, this.state.galleryWidth);

    let pages = [];
    for(let i = 0; i < count; i++) {
      const opacity = position.interpolate({
        inputRange: [i - 1, i, i + 1],
        outputRange: [0.3, 1, 0.3],
        extrapolate: 'clamp'
      });
     pages.push(
       <Animated.View
         key={i}
         style={{ opacity, height: 6, width: 6, backgroundColor: styleVars.color.darkGrey4, margin: 4, borderRadius: 3 }}
       />);
    }
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
        {pages}
      </View>);
  }

  renderImages(images: Image[]) {
    const isMultiImage = images.length > 1;
    const imageContainerStyle = this.props.isDetailView ? styles.imageContainerDetail : styles.imageContainer;

    return(
      <View style={imageContainerStyle}>
        <ScrollView
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { x: this.scrollX } } }]
          )}
          contentContainerStyle={styles.imageScrollerContainer}
          style={styles.imageScroller}
          showsHorizontalScrollIndicator={false}
          horizontal
          bounces={isMultiImage}
          onLayout={this.onScrollViewLayout}
          pagingEnabled>
          { // display the image gallery once the scrollview has determined page width
            this.state.galleryWidth && images.map((image, i) => {
            const uri = `${image.baseCDNPath}${image.variations.medium.filename}`;
            // Each View represents a page. Images are contents of that page.
            return (
              <View style={[styles.page, { width: this.state.galleryWidth }]} key={uri}>
                <ImageAutoSize
                  uri={uri}
                  onPressImage={() => this.props.onPressImage(i)}
                />
              </View>
            )
          })}
        </ScrollView>
        {isMultiImage && this.renderImagePager(images.length)}
      </View>)
  }

  render() {
    const { images } = this.props.contentItem;
    return (
      <GenericContentItem {...this.props}>
        { images && this.renderImages(images) }
      </GenericContentItem>
    );
  }
}

export default ImageContentType;

const contentPadding = 8;
const styles = StyleSheet.create({
  imageScrollerContainer: {
    backgroundColor: styleVars.color.lightGrey,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageScroller: {
    flex: 1,
  },
  imageContainer: {
    marginHorizontal: -contentPadding,
    marginBottom: 5,
    flex: 1,
  },
  imageContainerDetail: {
    marginBottom: 5,
    flex: 1,
  },
  page: {
    flex: 1,
  },
});
