/**
* Action dispatched by action creators in Redux.
* Actions are usually dispatched after an interaction taken by the User or triggered by a server response
* @param {type} string describes the action dispatched.
* @param {payload} any data that will be integrated into Redux state after this action is intercepted by a reducer
*/
export type Action = { // eslint-disable-line
  type: string,
  payload?: any,
  meta?: any,
}
