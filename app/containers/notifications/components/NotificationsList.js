// @flow
import React, { Component } from 'react';
import {
  ActivityIndicator,
  ListView,
  RefreshControl,
  View,
} from 'react-native';
import {
  ButtonSubtle,
  TRATextStrong,
} from 'app/components';
import Notification from './Notification';
import styleVars from 'app/style/variables';
import type { Notification as NotificationType } from 'app/redux/modules/notification/types';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style?: ViewStyleProp,
  notifications: NotificationType[],
  isLoading?: boolean,
  hasOlder?: boolean,
  onOpenNotification: (notification: NotificationType) => void,
  onPressLoadOlder: () => void,
  onRefresh: () => void,
};

type State = {
  dataSource: any,
};

class NotificationsList extends Component<Props, State> {

  constructor(props: Props) {
    const { notifications } = props;
    super(props);
    const ds = new ListView.DataSource({
      rowHasChanged: (notificationOne, notificationTwo) => notificationOne.id !== notificationTwo.id
    });
    this.state = {
      dataSource: ds.cloneWithRows(notifications),
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(nextProps.notifications)
    });
  }

  render() {
    const { style } = this.props;

    return (
      <ListView
        refreshControl={this._renderRefreshControl()}
        style={[styles.listView, style]}
        scrollingEnabled={true}
        enableEmptySections={true}
        dataSource={this.state.dataSource}
        renderRow={this._renderRow}
        renderHeader={this._renderHeader}
        renderFooter={this._renderFooter}
        renderSeparator={(sectionID, rowID) => <View style={styles.listItemSeparator} key={sectionID+'_'+rowID} />}
      />
    );
  }

  _renderRefreshControl = () => {
    const { isLoading, onRefresh } = this.props;
    return (
      <RefreshControl
         refreshing={!!isLoading}
         onRefresh={onRefresh}
       />
    );
  }

  /**
   * Display a NO NOTIFICATIONS message as long as there aren't any and it's not loading
   */
  _renderHeader = () => {
    const { isLoading } = this.props;
    const hasNotifications = !!this.state.dataSource.getRowCount();

    if(!hasNotifications && !isLoading) {
      return (
        <View style={styles.noNotificationsContainer}>
          <TRATextStrong style={styles.noNotificationsText}>You have not received any notifications yet.</TRATextStrong>
        </View>
      )
    }
  }

  /**
   * Display loading text if loading
   * Display a load older button if there are older items we can make a request for
   */
  _renderFooter = () => {
    const { isLoading, hasOlder, onPressLoadOlder } = this.props;
    const hasNotifications = !!this.state.dataSource.getRowCount();
    const displayOlderButton = !isLoading && hasOlder;

    if(hasNotifications) {
      return(
        <View>
          { isLoading && (
            <ActivityIndicator
              size="small"
              color={styleVars.color.traenaPrimaryGreen}
            />
          )}
        { displayOlderButton &&
          <ButtonSubtle style={styles.loadOlderButton} title={'older...'} onPress={onPressLoadOlder} />
        }
        </View>
      )
    }
  }

  _renderRow = (notification) => {
    const { onOpenNotification } = this.props;

    return (
      <Notification
        key={notification.id}
        notification={notification}
        style={styles.notification}
        onOpenNotification={onOpenNotification} />
    );
  }
}

export default NotificationsList;

const styles = {
  container: {

  },
  notification: {
    paddingHorizontal: styleVars.padding.edgeOfScreen,
    paddingVertical: 15,
  },
  listView: {

  },
  listItemSeparator: {
    borderBottomWidth: 1,
    borderColor: styleVars.color.lightGrey3,
  },
  noNotificationsContainer: {

  },
  noNotificationsText: {
    marginVertical: 15,
    textAlign: 'center',
    color: styleVars.color.mediumGrey,
  },
  loadingText: {
    textAlign: 'center',
    paddingVertical: 10,
  },
  loadOlderButton: {
    paddingVertical: 10,
  }
};
