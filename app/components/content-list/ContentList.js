// @flow
import React, { Component } from 'react';
import {
  ActivityIndicator,
  Animated,
  FlatList,
  RefreshControl,
  StyleSheet,
  View,
} from 'react-native';
import {
  TRATextStrong,
} from 'app/components';
import ContentContainer from 'app/containers/content/ContentContainer';
import type { ContentItem } from 'app/redux/modules/content/types';
import styleVars from 'app/style/variables';

type Props = {
  loadNextPage: () => void,
  refreshContent: () => void,
  canLoadMore: boolean,
  isLoadingMore: boolean,
  isRefreshing: boolean,
  displayNoMoreResults: boolean,
  contentItems: ContentItem[],
  // TODO: `listProp` should have a type of FlatListProps; fix the import of this type
  // something like `import type { Props as FlatListProps } from 'react-native/Libraries/Lists/FlatList';`, maybe
  listProps?: any,
  refreshIconOffset: number,
  header?: React$Node,
  hasError?: boolean,
};

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
class ContentList extends Component<Props> {
  props: Props;
  render() {
    const { refreshContent, isRefreshing = false, listProps, contentItems, refreshIconOffset } = this.props;
    return (
      <View style={{flex: 1}}>
        <AnimatedFlatList
          horizontal={false}
          scrollEnabled={true}
          data={contentItems}
          keyExtractor={this._keyExtractor}
          // setting to false fixes a bug where the feed is blank until scroll
          // details: https://github.com/facebook/react-native/issues/13316
          removeClippedSubviews={false}
          // needs to be greater than 0 or android wont trigger onEndReached
          onEndReachedThreshold={0.1}
          scrollEventThrottle={1}
          // disable virtualization because this unmounts views
          // that are off screen causing glitchy jumps
          disableVirtualization={true}
          refreshControl={
            <RefreshControl
              // android only && only if there is a top banner
              progressViewOffset={refreshIconOffset}
              refreshing={isRefreshing}
              onRefresh={refreshContent}
            />
          }
          ListHeaderComponent={this._renderHeader}
          ListFooterComponent={this._renderFooter}
          renderItem={this._renderItem}
          style={styles.list}
          onEndReached={this._loadMoreItems}
          {...listProps}
        />
      </View>
    );
  }


  _renderItem = ({item}) => {
    return (
    <View style={styles.itemHolder}>
      <ContentContainer
        contentItem={item}
        isDetailView={false}
      />
    </View>
  )}

  _renderHeader = () => {
    const { header } = this.props;
    if(header) {
      return (
        <View>
          {header}
        </View>
      );
    }
    return null;
  }

  _renderFooter = () => {
    const { isLoadingMore, displayNoMoreResults, hasError } = this.props;
    return (
      <View>
        { hasError && <TRATextStrong style={styles.endOfList}>error loading posts</TRATextStrong> }
        { !hasError && isLoadingMore && (
          <ActivityIndicator
            size="small"
            color={styleVars.color.traenaPrimaryGreen}
            style={{paddingVertical: 50}}
          />
        )}
        { !hasError && displayNoMoreResults && <TRATextStrong style={styles.endOfList}>no more results</TRATextStrong> }
      </View>
    )
  }

  _loadMoreItems = () => {
    if(!this.props.isRefreshing && !this.props.isLoadingMore && this.props.canLoadMore) {
      this.props.loadNextPage();
    }
  }

  _keyExtractor = (item): string => new String(item.id).toString();

}

export default ContentList;

const styles = StyleSheet.create({
  itemHolder: {
    marginVertical: 5
  },
  list: {
    flex: 1,
    backgroundColor: styleVars.color.white,
  },
  endOfList: {
    textAlign: 'center',
    paddingVertical: 50,
    color: styleVars.color.mediumGrey,
  }
});
