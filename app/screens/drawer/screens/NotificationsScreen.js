// @flow
import React, { Component } from 'react';
import type {
  NavigationNavigatorProps,
  NavigationStackScreenOptions,
} from 'react-navigation';
import { SafeAreaView } from 'react-native';
import styleVars from 'app/style/variables';

import NotificationsContainer from 'app/containers/notifications/NotificationsContainer';

type NavigationStateParams = {
  params: {
    title: string,
  },
} & *;

type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;
type Props = NavigationProps;

class NotificationsScreen extends Component<Props> {
  static navigationOptions: (*) => NavigationStackScreenOptions = () => {
    return {
      headerTitle: 'Notifications',
    };
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: styleVars.color.white }}>
        <NotificationsContainer />
      </SafeAreaView>
    );
  }
}

export default NotificationsScreen;
