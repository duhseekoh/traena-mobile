// @flow
import React from 'react';
import logger from 'app/logging/logger';
import _ from 'lodash';
import {
  ActivityIndicator,
  StyleSheet,
  Image,
  View,
  TouchableOpacity
} from 'react-native';
import { TRATextStrong } from 'app/components';

import styleVars from 'app/style/variables';

// Clip the height of any images outside these bounds
const MAX_RATIO = 2.0; // flat images e.g.  2.0 == 375x187.5
const MIN_RATIO = 0.83; // tall images e.g. .83 == 450

type Props = {
  uri: string,
  onPressImage: () => void,
};

type State = {
  uri: string,
  isLoading: boolean,
  hasError: boolean,
  imageRatio: number,
};

/**
 * Given an image uri, this component will present a loading message
 * while the image is fetched followed by displaying a container at the full
 * width of its parent, with the image contained inside at its correct aspect ratio.
 */
class ImageAutoSize extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    const uri = props.uri;
    this.state = {
      uri,
      isLoading: true,
      hasError: false,
      imageRatio: 0,
      width: 0,
      height: 0,
    };
  }

  componentDidMount() {
    this._calculateImageSize();
  }

  _calculateImageSize() {
    Image.getSize(
      this.state.uri,
      (width = 1, height = 1) => {
        // get the image ratio and clip it if desired
        // clipping is done by a combo of restricting the view aspect ratio
        // and using image resizeMode of cover
        const imageRatio = _.clamp(width / height, MIN_RATIO, MAX_RATIO);
        this.setState({
          imageRatio,
          isLoading: false,
        });
      },
      () => {
        logger.logBreadcrumb('Error in Image.getSize()');
        this.setState({
          hasError: true,
          isLoading: false,
        });
      },
    );
  }

  render() {
    let pageContent;
    if (!this.state.isLoading && this.state.hasError) {
      pageContent = (
        <View style={styles.imageOverlay}>
          <TRATextStrong style={styles.overlayText}>
            Error loading image.
          </TRATextStrong>
        </View>
      );
    } else if (this.state.isLoading) {
      pageContent = (
        <View style={styles.imageOverlay}>
          <ActivityIndicator
            size="small"
            color={styleVars.color.traenaPrimaryGreen}
          />
        </View>
      );
    } else {
      pageContent = (
        <Image
          style={[{ aspectRatio: this.state.imageRatio }, styles.image]}
          source={{ uri: this.state.uri }}
        />
      );
    }
    return (
      <TouchableOpacity
        style={styles.touchable}
        onPress={this.props.onPressImage}
        delayPressIn={100} // give a parent scrollview time to cancel the touch
        activeOpacity={.8}
      >
        {/* Need this wrapper view otherwise a parent scrollview won't recognize scrollable
        content correctly */}
        <View style={styles.imageWrapper}>
          {pageContent}
        </View>
      </TouchableOpacity>
    );
  }
}

export default ImageAutoSize;

const styles = StyleSheet.create({
  touchable: {
    flex: 1,
  },
  imageWrapper: {
    flex: 1,
    justifyContent: 'center',
  },
  image: {
    resizeMode: 'cover',
    backgroundColor: styleVars.color.lightGrey,
  },
  imageOverlay: {
    backgroundColor: styleVars.color.lightGrey,
    width: '100%',
    height: 250,
    justifyContent: 'center',
    alignItems: 'center',
  },
  overlayText: {
    fontSize: styleVars.fontSize.large,
    color: styleVars.color.white,
  },
});
