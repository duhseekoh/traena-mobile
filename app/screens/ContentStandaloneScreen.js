// @flow
import React, { Component } from 'react';
import type {
  NavigationNavigatorProps,
  NavigationStackScreenOptions,
} from 'react-navigation';
import { SafeAreaView, View } from 'react-native';
import ContentStandaloneContainer from 'app/containers/content/ContentStandaloneContainer';
import { BookmarkStandaloneButton } from 'app/components';
import styleVars from 'app/style/variables';

type NavigationStateParams = {
  params: {
    contentId: number,
  },
} & *;

type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

class ContentStandaloneScreen extends Component<NavigationProps> {
  /**
   * Adding this static method to the component allows us to add
   * buttons ( in this case the bookmark button ) to the header
   */
  static navigationOptions = ({
    navigation,
    navigationOptions,
  }: *): NavigationStackScreenOptions => {
    const { params } = navigation.state;
    return {
      headerStyle: {
        ...navigationOptions.headerStyle,
        ...styleVars.shadow.none,
      },
      headerRight: (
        <View style={{ marginRight: styleVars.padding.edgeOfScreen }}>
          <BookmarkStandaloneButton {...params} />
        </View>
      ),
    };
  };

  render() {
    const { params } = this.props.navigation.state;
    const { contentId } = params;
    return (
      <SafeAreaView
        style={{ flex: 1, backgroundColor: styleVars.color.white }}
      >
        <ContentStandaloneContainer contentId={contentId} />
      </SafeAreaView>
    );
  }
}

export default ContentStandaloneScreen;
