// @flow
import type { User } from 'app/redux/modules/user/types';

type AppState = {
  auth: {
    accessToken: string,
    idToken: string,
    isLoggedIn: boolean,
  },
  user: User,
};

export type { AppState };
