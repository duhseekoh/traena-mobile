// @flow
import { getSubscribedSeries, getAllSeriesProgress } from 'app/redux/modules/series/actions';
import { currentSeriesPageSelector } from './selectors';
import type { IndexedPage } from '../../../types/Pagination';
import type { Series } from '../series/types';
import type { Dispatch, GetState } from 'app/redux/constants';

export const REFRESH_SERIES_REQUEST_STARTED = 'UI_SUBSCRIBED_SERIES_LIST/REFRESH_SERIES_REQUEST_STARTED';
const startedRefreshSeriesRequest = () => {
  return {
    type: REFRESH_SERIES_REQUEST_STARTED,
    payload: null,
  };
};

export const REFRESH_SERIES_REQUEST_FAILED = 'UI_SUBSCRIBED_SERIES_LIST/REFRESH_SERIES_REQUEST_FAILED';
const failedRefreshSeriesRequest = () => {
  return {
    type: REFRESH_SERIES_REQUEST_FAILED,
    payload: null,
  };
};

export const LOAD_MORE_SERIES_REQUEST_STARTED = 'UI_SUBSCRIBED_SERIES_LIST/LOAD_MORE_SERIES_REQUEST_STARTED';
const startedLoadMoreSeriesRequest = () => {
  return {
    type: LOAD_MORE_SERIES_REQUEST_STARTED,
    payload:  null,
  };
};

export const LOAD_MORE_SERIES_REQUEST_FAILED = 'UI_SUBSCRIBED_SERIES_LIST/LOAD_MORE_SERIES_REQUEST_FAILED';
const failedLoadMoreSeriesRequest = () => {
  return {
    type: LOAD_MORE_SERIES_REQUEST_FAILED,
    payload: null,
  };
};

export const LOADED_MORE_SERIES = 'UI_SUBSCRIBED_SERIES_LIST/LOADED_MORE_SERIES';
function loadedMoreSeries(seriesPage: IndexedPage<number, Series>) {
  return {
    type: LOADED_MORE_SERIES,
    payload: { seriesPage }
  };
}

export const REFRESHED_SERIES = 'UI_SUBSCRIBED_SERIES_LIST/REFRESHED_SERIES';
function refreshedSeries(seriesPage: IndexedPage<number, Series>) {
  return {
    type: REFRESHED_SERIES,
    payload: { seriesPage }
  };
}

export const refreshSeriesList = () => {
  return async (dispatch: Dispatch) => {
    dispatch(startedRefreshSeriesRequest());
    try {
      const getSeriesOptions = { page: 0, sort: 'createdTimestamp:desc' };
      const result: IndexedPage<number, Series> = await dispatch(getSubscribedSeries(getSeriesOptions));
      await dispatch(getAllSeriesProgress());
      return dispatch(refreshedSeries(result));
    } catch(error) {
      dispatch(failedRefreshSeriesRequest());
    }
  };
}

export const loadMoreSeries = () => {
  return async (dispatch: Dispatch, getState: GetState) => {
    dispatch(startedLoadMoreSeriesRequest());
    const page = currentSeriesPageSelector(getState()) + 1;
    try {
      const getSeriesOptions = { page, sort: 'createdTimestamp:desc' };
      const result: IndexedPage<number, Series> = await dispatch(getSubscribedSeries(getSeriesOptions));
      return dispatch(loadedMoreSeries(result));
    } catch(error) {
      dispatch(failedLoadMoreSeriesRequest());
    }
  };
};
