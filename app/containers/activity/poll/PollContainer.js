// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  StyleSheet,
  ScrollView,
  Animated,
} from 'react-native';
import {
  TRAText,
  TRATextStrong,
  IONIcon,
} from 'app/components';
import ButtonFinish from './components/ButtonFinish';
import ButtonPoll from './components/ButtonPoll';
import { mostRecentActivityResponseSelector, hasActivityResponseSelector } from 'app/redux/modules/activityResponse/selectors';
import { currentActivitySelector, currentActivityResultsSelector, contentIdForActivitySelector } from 'app/redux/modules/activity/selectors';
import { getActivityResults } from 'app/redux/modules/activity/actions';
import { getActivityResponses, createActivityResponse } from 'app/redux/modules/activityResponse/actions';
import styleVars from 'app/style/variables';
import type { PollActivity, ActivityResults } from 'app/redux/modules/activity/types';
import type { PollResponse, ResponseBody } from 'app/redux/modules/activityResponse/types';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import type { Dispatch } from 'app/redux/constants';

type OwnProps = {
  style?: ViewStyleProp,
  exitActivity: () => any,
  activityId: number,
};

/*
* isAnswered: true if the user has an activityResponse for this activity
* results: all the aggregate scores for each poll choice
* response: the response made by this user
*/
type ConnectedProps = {
  isAnswered: boolean,
  results: ActivityResults,
  response: PollResponse,
  contentId: number,
  poll: PollActivity,
  getActivityResults: (contentId: number, activityId: number) => any,
  getActivityResponses: (activityId: number) => any,
  createActivityResponse: (activityId: number, body: ResponseBody) => any,
};

type Props = OwnProps & ConnectedProps;

type State = {
  scrollWidth: number,
};

class PollContainer extends Component<Props, State> {
  scrollRef: ScrollView;
  fadeInValue: Animated.Value;

  constructor(props: Props) {
    super(props);
    this.state = {
      scrollWidth: 0,
    };
  }

  componentWillMount() {
    const { getActivityResults, getActivityResponses, activityId, contentId, isAnswered } = this.props;

    this.fadeInValue = new Animated.Value(0);
    getActivityResponses(this.props.activityId);
    getActivityResults(contentId, activityId);
    if(isAnswered) {
      Animated.timing(this.fadeInValue, {
        toValue: 1,
        duration: 650,
      }).start();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { getActivityResults, activityId, contentId, response } = this.props;
    if(!response && nextProps.response) {
      getActivityResults(contentId, activityId);
      Animated.timing(this.fadeInValue, {
        toValue: 1,
        duration: 650,
      }).start();
    }
  }

  _onLayout = ({nativeEvent}: *) => {
    if (nativeEvent.layout.width === this.state.scrollWidth) {
      return;
    }

    this.setState({
      scrollWidth: nativeEvent.layout.width,
    });
  }

  _createActivityResponse = (activityId, selectedChoiceId) => {
     this.props.createActivityResponse(activityId, { selectedChoiceId });
  }

  _choicePressed = (activityId, choiceId) => {
    this._createActivityResponse(activityId, choiceId);
  }

  render() {
    const { poll, results, activityId, isAnswered, response } = this.props;
    const selectedChoiceId = response ? response.body.selectedChoiceId : null;
    const questionFontSize = isAnswered ? styleVars.fontSize.medlar : styleVars.fontSize.large;
    const content = (<View style={{flex: 1}}>
      {/* Vertical scrolling for long questions */}
        <View style={styles.scrollContainer}>
          <ScrollView
            style={[styles.pollCard]}
            bounce={false}
            showsVerticalScrollIndicator={false}>
            <View style={styles.titleBox}>
              <IONIcon name='ios-stats' fontSize={styleVars.fontSize.medlar} color={styleVars.color.traenaOne} />
              {isAnswered && results && <TRATextStrong style={styles.responsesText}>{results.count} responses</TRATextStrong>}
              <TRAText style={[{fontSize: questionFontSize}, styles.title]}>{poll.body.title}</TRAText>
            </View>
            {poll.body.choices.map(choice => {
              const percent = isAnswered && results && results.choiceResults[choice.id] ? results.choiceResults[choice.id].percent : 0;
              return (<View style={styles.pollButton} key={choice.id}>
                <ButtonPoll
                  text={choice.choiceText}
                  onPress={() => this._choicePressed(activityId, choice.id)}
                  isAnswered={isAnswered}
                  isSelected={selectedChoiceId === choice.id}
                  percent={percent} />
                </View>);
              })
            }
          </ScrollView>
        </View>
        {isAnswered &&
          <Animated.View
            style={[styles.bottomSeat, {opacity: this.fadeInValue}]}>
            <ButtonFinish
              text={'Finished!'}
              onPress={() => this.props.exitActivity()}
              focused={false}
              disabled={false}
              loading={false} />
          </Animated.View>
        }
        </View>);

    if(isAnswered) {
      return (
        <View style={styles.container}>
          <Animated.Image
            style={[styles.heroImage, {opacity: this.fadeInValue}]}
            source={require('../../../images/poll-complete.png')} />
            {content}
        </View>);

    }

    return (
      <View style={styles.container}>
        {content}
      </View>);
  }
}

const mapStateToProps = (state, props: OwnProps) => {
  return {
    isAnswered: hasActivityResponseSelector(state, props),
    poll: currentActivitySelector(state, props),
    results: currentActivityResultsSelector(state, props),
    response: mostRecentActivityResponseSelector(state, props),
    contentId: contentIdForActivitySelector(state, props)
  }
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    getActivityResults: (contentId: number, activityId: number) =>
      dispatch(getActivityResults(contentId, activityId)),
    getActivityResponses: (activityId: number) =>
      dispatch(getActivityResponses(activityId)),
    createActivityResponse: (activityId: number, body: ResponseBody) =>
      dispatch(createActivityResponse(activityId, body)),
  };
};

const ConnectedPollContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(PollContainer);

export default ConnectedPollContainer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: styleVars.color.traenaOne,
    paddingHorizontal: 22,
    paddingTop: 70,
  },
  scrollContainer: {
    marginBottom: 50
  },
  pollCard: {
    paddingVertical: 15,
    paddingHorizontal: 12,
    backgroundColor: styleVars.color.white,
    borderRadius: 6,
    marginBottom: 25,
  },
  title: {
    marginTop: 12,
    marginBottom: 18,
  },
  titleBox: {
    flex: 1,
    alignItems: 'center'
  },
  responsesText: {
    fontSize: styleVars.fontSize.small,
    color: styleVars.color.mediumGrey,
  },
  heroImage: {
    resizeMode: 'contain',
    position: 'absolute',
    flex: 1,
  },
  pollButton: {
    marginBottom: 12,
  },
  bottomSeat: {
    position: 'absolute',
    bottom: 12,
    left: 0,
    right: 0,
  }
});
