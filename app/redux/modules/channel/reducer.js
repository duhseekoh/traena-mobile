// @flow
import {
  CHANNEL_LIST_FETCH_SUCCEEDED,
} from './actions';
import type { Action } from 'app/types/Action';
import type { Channel } from './types';

export type ReduxStateChannel = {
  channels: {[channelId: number]: Channel},
};

const ACTION_HANDLERS = {
  [CHANNEL_LIST_FETCH_SUCCEEDED]: (state: ReduxStateChannel, action: Action) => {
    const channels: {[channelId: number]: Channel} = action.payload;
    return {
      ...state,
      channels: {
        ...state.channels,
        ...channels,
      },
    }
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  channels: {},
};

const channelReducer = (state: ReduxStateChannel = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default channelReducer;
