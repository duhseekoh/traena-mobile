// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import type {
  NavigationNavigatorProps,
  NavigationEventSubscription,
} from 'react-navigation';
import config from 'app/config';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  RefreshControl,
  SafeAreaView,
} from 'react-native';
import {
  ButtonPrimary,
  TRAText,
} from 'app/components';
import type { User } from 'app/redux/modules/user/types';
import { logout } from 'app/redux/modules/auth/actions';
import { fetchUser } from 'app/redux/modules/user/actions';
import { fetchUserStats } from 'app/redux/modules/stats/actions';
import { goToFeedbackPage } from 'app/redux/modules/navigation/actions';
import {
  isLoadingSelector,
  cumulativeChartDataSelector,
  daysActiveSelector,
  activityStreakSelector,
  currentNumberCompletedTasksSelector,
  previousNumberCompletedTasksSelector,
  currentNumberCommentsSelector,
  previousNumberCommentsSelector,
} from 'app/redux/modules/stats/selectors';
import { userSelector } from 'app/redux/modules/user/selectors';
import UserSummary from './components/UserSummary';
import AnalyticsPanel from './components/AnalyticsPanel';
import styleVars from 'app/style/variables';
import logger from 'app/logging/logger';
import type { Dispatch } from 'app/redux/constants';
type Props = {
  activityStreak: number,
  daysActive: number,
  chartData: {
    date: Date,
    value: number,
  }[],
  currentNumberCompletedTasks: number,
  isLoading?: boolean,
  previousNumberCompletedTasks: number,
  currentNumberComments: number,
  previousNumberComments: number,
  user: User,
  onLogout: () => void,
  refreshUser: () => void,
  refreshStats: () => void,
  isSelected?: boolean,
  goToFeedbackPage: () => any,
} & NavigationNavigatorProps<*, *>;

class ProfileContainer extends Component<Props> {
  _navigationListener: NavigationEventSubscription;

  constructor(props: Props) {
    super(props);
  }

  componentDidMount() {
    this._navigationListener = this.props.navigation.addListener(
      'didFocus',
      () => {
        logger.logBreadcrumb('Profile screen displayed');
        this.refreshData();
      }
    );
  }

  componentWillUnmount() {
    this._navigationListener.remove();
  }

  refreshData = () => {
    this.props.refreshUser();
    this.props.refreshStats();
  }

  render() {
    const { user } = this.props;
    return (
      <SafeAreaView
        style={styles.profileScreenContainer}
        refreshControl={
          <RefreshControl
            refreshing={!!this.props.isLoading}
            onRefresh={this.refreshData}
          />
        }
      >
        <View style={styles.contents}>
          <UserSummary
            style={styles.avatarAndNameContainer}
            user={user}
          />

          <View style={styles.analyticsPanelsContainer}>
            <AnalyticsPanel
              style={[styles.statPanel]}
              title="Days Active"
              icon="calendar"
              value={this.props.daysActive}
              streak={this.props.activityStreak}
            />
            <AnalyticsPanel
              style={[styles.statPanel]}
              title="Posts Completed"
              icon="check-circle"
              value={this.props.currentNumberCompletedTasks}
              delta={this.props.currentNumberCompletedTasks
                - this.props.previousNumberCompletedTasks}
            />
          </View>

          <View style={styles.feedbackLinkContainer}>
            <TRAText style={styles.feedbackBlurb}>Do you have questions or comments?</TRAText>
            <TouchableOpacity
              onPress={() => this.props.goToFeedbackPage()}
            >
              <TRAText style={styles.feedbackLink}>Contact Us</TRAText>
            </TouchableOpacity>
          </View>

          <View style={styles.buttonsHolder}>
            <ButtonPrimary
              style={styles.logoutButton}
              title={'Log Out'}
              onPress={this.props.onLogout} />
          </View>
        </View>

        <View style={styles.environmentDetails}>
          { config && config.displayEnvironmentDetails &&
            <View>
              <TRAText style={styles.environmentDetailsText}>{config.apiBaseUrl}</TRAText>
              <TRAText style={styles.environmentDetailsText}>{config.auth0.domain}</TRAText>
            </View>
          }
          <TRAText style={styles.environmentDetailsText}>Version: {config.packageJson.version}</TRAText>
        </View>

      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  profileScreenContainer: {
    flex: 1,
    backgroundColor: styleVars.color.white,
  },
  contents: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarAndNameContainer: {
    flex: 1,
    flexGrow: 1.5, // small screens, this area needs more real estate
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: styleVars.color.traenaPrimaryGreen,
    paddingHorizontal: 15,
    paddingBottom: 40,
    shadowColor: styleVars.color.darkGrey,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.7,
    shadowRadius: 2,
    marginBottom: 15,
  },
  analyticsPanelsContainer: {
    borderBottomWidth: 1,
    borderBottomColor: styleVars.color.lightGrey,
    marginBottom: 15,
    paddingBottom: 15,
    paddingHorizontal: 0,
    width: '100%',
  },
  statPanel: {
    alignSelf: 'stretch',
    marginTop: 10,
    marginHorizontal: 10,
  },
  buttonsHolder: {
    flex: 1,
    marginHorizontal: 50,
    marginTop: 20,
    paddingBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'flex-end',
  },
  logoutButton: {
  },
  environmentDetails: {},
  feedbackLinkContainer: {
  },
  feedbackBlurb: {
    textAlign: 'center',
    marginBottom: 5,
  },
  feedbackLink: {
    fontFamily: styleVars.fontFamily.bold,
    color: styleVars.color.traenaOne,
    textDecorationLine: 'underline',
    textAlign: 'center',
  },
  environmentDetailsText: {
    textAlign: 'center',
    fontSize: 9,
    color: styleVars.color.mediumGrey,
  }
});

function mapStateToProps(state) {
  return {
    activityStreak: activityStreakSelector(state),
    daysActive: daysActiveSelector(state),
    chartData: cumulativeChartDataSelector(state),
    currentNumberCompletedTasks: currentNumberCompletedTasksSelector(state),
    isLoading: isLoadingSelector(state),
    previousNumberCompletedTasks: previousNumberCompletedTasksSelector(state),
    currentNumberComments: currentNumberCommentsSelector(state),
    previousNumberComments: previousNumberCommentsSelector(state),
    user: userSelector(state),
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onLogout: function() {
      return dispatch(logout());
    },
    refreshUser: function() {
      return dispatch(fetchUser());
    },
    refreshStats: function() {
      return dispatch(fetchUserStats());
    },
    goToFeedbackPage: () => dispatch(goToFeedbackPage()),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(ProfileContainer));
