// @flow
import React from 'react';
import {
  View,
  StyleSheet
} from 'react-native';
import styleVars from 'app/style/variables';

const ListItemSeparator = () => {
  return (
    <View style={styles.separator}/>
  )
}

const styles = StyleSheet.create({
  separator: {
    height: 8,
    backgroundColor: styleVars.color.white,
  }
});

export default ListItemSeparator;
