// @flow
import React from 'react';
import {
  StyleSheet,
} from 'react-native';
import {
  FeatherIcon,
} from 'app/components';
import styleVars from 'app/style/variables';
import ButtonStyled, { type Props as ButtonStyledProps } from 'app/components/form/ButtonStyled';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style?: ViewStyleProp,
  backgroundColor: string,
  title: string,
  onPress: () => any,
};

const FeedButton = (props: Props & ButtonStyledProps) => {
  const bgColor = { backgroundColor: props.backgroundColor };
  return (
    <ButtonStyled
      defaultButtonStyle={[bgColor, styles.button]}
      text={props.title}
      defaultTextStyle={styles.buttonTitle}
      icon={<FeatherIcon name="chevron-right" style={styles.buttonIcon} />}
      onPress={props.onPress}
      {...props}
    />

  );
}

const styles = StyleSheet.create({
  button: {
    padding: 15,
    borderRadius: 6,
    ...styleVars.shadow.shadowThree,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonTitle: {
    color: styleVars.color.white,
    fontFamily: styleVars.fontFamily.bold,
    fontSize: styleVars.fontSize.medlar,
  },
  buttonIcon: {
    color: styleVars.color.white,
    fontSize: styleVars.iconSize.large,
  }
});

export default FeedButton;
