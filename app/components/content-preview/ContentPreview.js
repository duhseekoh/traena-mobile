// @flow
import React from 'react';
import {
  TouchableOpacity,
  View,
} from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import DailyActionPreview from './DailyActionPreview';
import TrainingVideoPreview from './TrainingVideoPreview';
import ImageContentItemPreview  from './ImageContentItemPreview';
import type { ContentItem } from 'app/redux/modules/content/types';

type Props = {
  onPress: (contentId: number) => void,
  contentItem: ContentItem,
  style?: ViewStyleProp,
};

const ContentPreview = (props: Props) => {
  const { contentItem, onPress, style, ...rest } = props;
  return (
    <TouchableOpacity
      style={style}
      onPress={() => onPress(contentItem.id)}>
      <View pointerEvents={'none'}>
        { (contentItem.type === 'TrainingVideo') && <TrainingVideoPreview contentItem={contentItem} {...rest} /> }
        { (contentItem.type === 'DailyAction') && <DailyActionPreview contentItem={contentItem} {...rest} /> }
        { (contentItem.type === 'Image') && <ImageContentItemPreview contentItem={contentItem} {...rest} /> }
      </View>
    </TouchableOpacity>
  );
}

export default ContentPreview;
