// @flow
import React, { Component } from 'react';
import type {
  NavigationNavigatorProps,
  NavigationStackScreenOptions,
} from 'react-navigation';
import { connect } from 'react-redux';
import {
  SafeAreaView,
} from 'react-native';
import {
  ContentList,
} from 'app/components';
import { contentSelector, isLoadingMoreSelector, isRefreshingSelector, canLoadMoreSelector, displayNoMoreResultsSelector } from 'app/redux/modules/tag-standalone/selectors';
import { refreshTagContent, loadMoreTagContent } from 'app/redux/modules/tag-standalone/actions';
import type { ContentItem } from 'app/redux/modules/content/types';

type NavigationStateParams = {
  params: {
    title: string,
    tag: string,
  },
}

type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

type Props = {
  content: ContentItem[],
  refreshTagContent: (tag: string) => void,
  loadMoreTagContent: (tag: string) => void,
  isLoadingMore: boolean,
  isRefreshing: boolean,
  canLoadMore: boolean,
  displayNoMoreResults: boolean,
  tag: string,
} & NavigationProps;

class TagStandaloneScreen extends Component<Props> {
  props: Props;

  /**
  * Adding this static method to the component allows us to use
  * dynamic titles.  For more info https://reactnavigation.org/docs/headers.html#using-params-in-the-title
  */
  static navigationOptions = (
    { navigation }: NavigationProps,
  ): NavigationStackScreenOptions => {
    const { params } = navigation.state;
    return {
      headerTitle: params ? params.title : 'CHANNEL',
    }
  };

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const { refreshTagContent } = this.props;
    const { tag } = params;
    refreshTagContent(tag);
  }

  render() {
    const { params } = this.props.navigation.state;
    const { content, refreshTagContent, isRefreshing = false, loadMoreTagContent, isLoadingMore, canLoadMore, displayNoMoreResults } = this.props;
    const { tag } = params;

    return (
      <SafeAreaView style={{flex: 1}}>
        <ContentList
          refreshContent={() => refreshTagContent(tag)}
          isRefreshing={isRefreshing}
          contentItems={content}
          loadNextPage={() => loadMoreTagContent(tag)}
          isLoadingMore={isLoadingMore}
          canLoadMore={canLoadMore}
          displayNoMoreResults={displayNoMoreResults}
          refreshIconOffset={0} />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { params } = props.navigation.state;
  return {
    content: contentSelector(state, params),
    isRefreshing: isRefreshingSelector(state, params),
    isLoadingMore: isLoadingMoreSelector(state, params),
    canLoadMore: canLoadMoreSelector(state, params),
    displayNoMoreResults: displayNoMoreResultsSelector(state, params),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    refreshTagContent: (tag) => {
      return dispatch(refreshTagContent(tag));
    },
    loadMoreTagContent: (tag) => {
      return dispatch(loadMoreTagContent(tag));
    },
  };
};

export { TagStandaloneScreen as TagStandaloneScreenCmp };
const ConnectedTagStandaloneScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(TagStandaloneScreen);
export default ConnectedTagStandaloneScreen;
