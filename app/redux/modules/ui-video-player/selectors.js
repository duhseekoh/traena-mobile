const STATE_KEY = 'videoPlayer';
const stateFromRoot = (state) => state[STATE_KEY];

export const activeVideoIdSelector = (state) => {
  return stateFromRoot(state).activeVideoId;
}
