// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import {
  goToSeriesStandalone,
  goToContentStandalone,
} from 'app/redux/modules/navigation/actions';
import { TRAText, TRATextStrong, ProgressBar } from 'app/components';
import SeriesContentSlider from './components/SeriesContentSlider';
import type { Series, SeriesProgress } from 'app/redux/modules/series/types';
import type { ContentItem } from 'app/redux/modules/content/types';
import type { Dispatch } from 'app/redux/constants';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import styleVars from 'app/style/variables';
import { seriesProgressSelector } from 'app/redux/modules/series/selectors'

type OwnProps = {
  series: Series,
  style?: ViewStyleProp,
  contentItems: ContentItem[],
};

type ConnectedProps = {
  goToSeriesStandalone: (
    id: number,
    title: string,
    description: string,
  ) => void,
  goToContentStandalone: () => void,
  seriesProgress: SeriesProgress,
};

type Props = OwnProps & ConnectedProps;

class SeriesPreviewContainer extends Component<Props> {
  props: Props;

  render() {
    const {
      series,
      goToSeriesStandalone,
      goToContentStandalone,
      contentItems,
      seriesProgress,
    } = this.props;
    const { id, title, description } = series;
    const { completedContentCount, contentCount } = seriesProgress;
    const percent = (completedContentCount / contentCount) * 100;

    return (
      <View style={{ paddingVertical: 15, flex: 1 }}>
        <TouchableOpacity
          delayPressIn={100}
          activeOpacity={0.8}
          onPress={() => goToSeriesStandalone(id, title, description)}
        >
          <View style={{ marginHorizontal: styleVars.padding.edgeOfScreen }}>
            <TRATextStrong style={{ fontSize: styleVars.fontSize.large }}>
              {series.title}
            </TRATextStrong>
            {series.description && (
              <TRAText
                style={{ marginTop: 4, color: styleVars.color.lightGrey2 }}
              >
                {series.description}
              </TRAText>
            )}
            <View style={styles.progressBarHolder}>
              <TRATextStrong>
                {completedContentCount}/{contentCount} Posts Completed
              </TRATextStrong>
              <ProgressBar percent={percent} style={styles.progressBar} />
            </View>
          </View>
        </TouchableOpacity>


        {contentItems &&
          contentItems.length > 0 && (
            <View style={{ marginTop: 20 }}>
              <SeriesContentSlider
                contentItems={contentItems}
                goToContentStandalone={goToContentStandalone}
                goToSeriesStandalone={() =>
                  goToSeriesStandalone(id, title, description)
                }
              />
            </View>
          )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  progressBarHolder: {
    flex: 1,
    marginTop: 10,
  },
  progressBar: {
    marginTop: 5,
  },
});

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    goToSeriesStandalone: (id: number, title: string, description: string) => {
      return dispatch(goToSeriesStandalone(id, title, description));
    },
    goToContentStandalone: (contentId: number) => {
      return dispatch(goToContentStandalone(contentId));
    },
  };
};

const mapStateToProps = (state, props) => {
  return {
    seriesProgress: seriesProgressSelector(state, { id: props.series.id }),
  }
};

const ConnectedSeriesPreviewContainer = connect(mapStateToProps, mapDispatchToProps)(
  SeriesPreviewContainer,
);

export default ConnectedSeriesPreviewContainer;
