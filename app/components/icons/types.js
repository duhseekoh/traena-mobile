// @flow
import styleVars from 'app/style/variables';

type Color = $Values<typeof styleVars.color>;

type Size = $Values<typeof styleVars.iconSize>;

export type {
  Color,
  Size,
}
