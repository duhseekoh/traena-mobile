// @flow
import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  ListView,
  StyleSheet,
} from 'react-native';
import type { SearchResults } from 'app/redux/modules/user-search/types';
import {
  TRATextStrong,
} from 'app/components';
import UserSearchResultItem from './UserSearchResultItem';
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  isLoading?: boolean,
  isQueryEmpty?: boolean,
  results: SearchResults,
  onSearchResultSelected: (result: string) => void,
  style?: ViewStyleProp,
};

type State = {
  dataSource: any,
};

export default class UserSearchResults extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(props.results),
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    // TODO - check if an update is necessary
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(nextProps.results)
    });
  }

  render() {
    const { style, isLoading } = this.props;
    const listViewStyles = [styles.listView, isLoading && styles.listViewLoading];

    return (
      <View style={[style, styles.container]}>
        <ListView
          style={listViewStyles}
          keyboardShouldPersistTaps={'handled'}
          scrollingEnabled={true}
          enableEmptySections={true}
          dataSource={this.state.dataSource}
          renderRow={this._renderRow}
          renderHeader={this._renderHeader}
          renderSeparator={(sectionID, rowID) => <View style={styles.listItemSeparator} key={sectionID+'_'+rowID} />}
        />
      </View>
    );
  }

  _renderHeader = () => {
    const hasResults = !!this.state.dataSource.getRowCount();

    return (
      <View style={styles.headerContainer}>
        {!hasResults && this._renderNoResultsView()}
      </View>
    )
  }

  _renderRow = (result /*,sectionID, rowID , highlightRow*/) => {
    const { onSearchResultSelected } = this.props;

    return (
      <UserSearchResultItem
        result={result}
        style={styles.searchResultItem}
        onPress={() => onSearchResultSelected(result)} />
    )
  }

  _renderNoResultsView = () => {
    const { isQueryEmpty, isLoading } = this.props;

    let noResults;
    if(isLoading) {
      noResults = (
        <ActivityIndicator
          size="small"
          color={styleVars.color.traenaPrimaryGreen}
        />
      )
    } else if(isQueryEmpty) {
      noResults = <TRATextStrong>Start searching for people in your organization by their first and last name.</TRATextStrong>
    } else {
      noResults = <TRATextStrong>No results found</TRATextStrong>;
    }

    return (
      <View style={styles.suggestionsContainer}>
        {noResults}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: styleVars.color.lightGrey4,
  },
  headerContainer: {
    marginVertical: 5,
    marginHorizontal: styleVars.padding.edgeOfScreen
  },
  listView: {
  },
  listViewLoading: {
    opacity: .5
  },
  listItemSeparator: {
    borderBottomWidth: 1,
    borderColor: styleVars.color.lightGrey4,
    marginVertical: 5,
  },
  suggestionsContainer: {
    marginVertical: 10,
    marginHorizontal: 25
  },
  searchResultItem: {
    marginHorizontal: styleVars.padding.edgeOfScreen,
  },
});
