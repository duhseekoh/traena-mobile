// @flow
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  TRATextStrongest,
  TRATextStrong,
 } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  onPressClose: () => any,
  title: string,
  description: string,
};

class InfoModal extends Component<Props> {
  props: Props;
  render() {
    const { title, description } = this.props;
    return (
      <View style={{flex: 1}}>
        <TouchableWithoutFeedback
          onPress={this.props.onPressClose}>
          <View style={styles.exitBox} />
        </TouchableWithoutFeedback>
        <View style={styles.infoBox}>
          <TRATextStrongest style={styles.title}>{title}</TRATextStrongest>
          <TRATextStrong style={styles.description}>{description}</TRATextStrong>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  // takes up any space that infoBox does not
  exitBox: {
    alignSelf: 'stretch',
    backgroundColor: styleVars.color.white,
    opacity: styleVars.opacity.modalOpacity,
    flex: 1,
  },
  // grows to height of title and description
  infoBox: {
    backgroundColor: styleVars.color.traenaOne,
    padding: 20,
    paddingBottom: 40,
  },
  title: {
    color: styleVars.color.white,
    fontSize: styleVars.fontSize.xlarge,
    marginBottom: 10,
    textShadowOffset: { width: 0, height: 1 },
    textShadowColor: styleVars.color.textShadow,
    textShadowRadius: 2,
  },
  description: {
    color: styleVars.color.darkGrey,
    fontSize: styleVars.fontSize.large,
  }
});

export default InfoModal;
