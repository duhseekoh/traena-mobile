// @flow
import { getWhatsNewTpl } from './util/endpoints';

function retrieveWhatsNew() {
  return fetch(getWhatsNewTpl());
}

export default {
  retrieveWhatsNew,
}
