// @flow
import type { Toast } from 'app/types/Toast';
import type { Dispatch } from 'app/redux/constants';

export const TOAST_CLEAR = 'TOAST_CLEAR';
export const TOAST_CREATE = 'TOAST_CREATE';


function clearToast() {
  return {
    type: TOAST_CLEAR,
  };
}

function showToast(toast: Toast) {
  return {
    type: TOAST_CREATE,
    payload: toast,
  };
}

export function createToast(toast: Toast) {
  return (dispatch: Dispatch) => {
    dispatch(showToast(toast));
    setTimeout(() => {
      dispatch(clearToast());
    }, 1500)
  }
}
