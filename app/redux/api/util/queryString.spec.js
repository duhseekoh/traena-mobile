import 'react-native';
import { getQueryString } from './queryString';

describe('getQueryString', () => {
  it('should output a querystring for string and text key value pairs', () => {
    const qp = {
      test1: 'great',
      test2: 3,
      test3: 'ok',
    };
    expect(getQueryString(qp)).toEqual('?test1=great&test2=3&test3=ok');
  });

  it('should return an empty string for an empty object', () => {
    const qp = {};
    expect(getQueryString(qp)).toEqual('');
  });

  it('should throw when no param provided', () => {
    expect(() => getQueryString()).toThrow();
  });

  it('should throw when no param provided', () => {
    expect(() => getQueryString()).toThrow();
  });

  it('should not include params with null or undefined values', () => {
    const qp = {
      test1: null,
      test2: 3,
      test3: undefined,
      test4: 'cool',
    };
    expect(getQueryString(qp)).toEqual('?test2=3&test4=cool');
  });

  it('should encode each param value', () => {
    const qp = {
      test1: '#awesome',
      test2: ' ha',
      test3: 'ok',
    };
    expect(getQueryString(qp)).toEqual('?test1=%23awesome&test2=%20ha&test3=ok');
  });

});
