// @flow
export type Video = {
  previewImageURI: string,
  videoURI: string,
};
