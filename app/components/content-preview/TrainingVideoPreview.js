// @flow
import React from 'react';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import {
  FeatherIcon,
} from 'app/components';
import GenericContentPreview from './GenericContentPreview';
import type { TrainingContentItem } from 'app/redux/modules/content/types';
import styleVars from 'app/style/variables';

type Props = {
  contentItem: TrainingContentItem,
  style?: ViewStyleProp,
};

const TrainingVideoPreview = (props: Props) => {
  const typeIcon = <FeatherIcon name='video' fontSize={styleVars.iconSize.small} color={styleVars.color.traenaOne} />

  return (
    <GenericContentPreview
      {...props}
      typeIcon={typeIcon}
    />
  );
}

 export default TrainingVideoPreview;
