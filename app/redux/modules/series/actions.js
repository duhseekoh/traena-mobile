// @flow
import api from 'app/redux/api';
import type { Dispatch } from 'app/redux/constants';
import { contentsFetchSucceeded } from 'app/redux/modules/content/actions';
import type { Series, SeriesProgress } from 'app/redux/modules/series/types';
import type { IndexedPage } from 'app/types/Pagination';

const SERIES_FETCH_STARTED = 'SERIES_FETCH_STARTED';
function seriesFetchStarted(seriesIds: number[]) {
  return {
    type: SERIES_FETCH_STARTED,
    payload: seriesIds,
  };
}

const SERIES_FETCH_SUCCEEDED = 'SERIES_FETCH_SUCCEEDED';
function seriesFetchSucceeded(series: Series[]) {
  return {
    type: SERIES_FETCH_SUCCEEDED,
    payload: series,
  };
}

const SERIES_FETCH_FAILED = 'SERIES_FETCH_FAILED';
function seriesFetchFailed(seriesIds: number[], error: string | Error) {
  return {
    type: SERIES_FETCH_FAILED,
    payload: {
      seriesIds,
      error,
    },
  };
}

const SERIES_FETCH_PROGRESS_SUCCEEDED = 'SERIES_FETCH_PROGRESS_SUCCEEDED';
function seriesFetchProgressSucceeded(seriesProgress: SeriesProgress) {
  return {
    type: SERIES_FETCH_PROGRESS_SUCCEEDED,
    payload: seriesProgress,
  };
}

const getSeriesById = (seriesId: number) => {
  return (dispatch: Dispatch) => {
    dispatch(seriesFetchStarted([seriesId]))
    return api.series.retrieveSeriesPreview(seriesId)
      .then(results => {
        if(results) {
          dispatch(seriesFetchSucceeded([results.series]));
          dispatch(contentsFetchSucceeded(results.contentPreviews));
        }
      })
      .catch((error) => {
        dispatch(seriesFetchFailed([seriesId], error));
        throw error;
      });
  }
};

/**
 * Fetchs a page of series a user is subscribed to.
 * Extracts content preview items then stores the series and content.
 *
 * @TODO: Allow sort by in-progress, so we can
 * rely on the first page of results to populate our feed widget
 * @returns
 */
const getSubscribedSeries = (
  options: { page?: number, size?: number, sort?: string, filterProgressStatus?: string },
) => {
  const { page, size, sort, filterProgressStatus } = options;
  return async (dispatch: Dispatch): Promise<IndexedPage<number, Series>> => {
    const result = await api.series.retrieveSubscribedSeries(page, size, sort, filterProgressStatus);
    dispatch(seriesFetchSucceeded(result.series));
    dispatch(contentsFetchSucceeded(result.contentPreviews));
    return result.page;
  };
};

/**
 * Get the current users progress for a given series.
 */
const getSeriesProgress = (seriesId: number) => {
  return async (dispatch: Dispatch) => {
    const seriesProgress = await api.series.retrieveSeriesProgress(seriesId);
    dispatch(seriesFetchProgressSucceeded(seriesProgress));
  }
};

/**
* Get the current user's progress for all series to which they are subscribed
*/
const getAllSeriesProgress = () => {
  return async (dispatch: Dispatch) => {
    const seriesProgress = await api.series.retrieveSeriesProgress();
    dispatch(seriesFetchProgressSucceeded(seriesProgress));
  }
};

const getSeriesForChannel = (channelId: number, pageNumber?: number = 0) => {
  return async (dispatch: Dispatch): Promise<IndexedPage<number, Series>> => {
    const result = await api.series.retrieveSeriesForChannel(channelId, pageNumber);
    dispatch(seriesFetchSucceeded(result.series));
    dispatch(contentsFetchSucceeded(result.contentPreviews));
    return result.page;
  };
};

export {
  SERIES_FETCH_SUCCEEDED,
  SERIES_FETCH_PROGRESS_SUCCEEDED,
  seriesFetchFailed,
  seriesFetchSucceeded,
  getSeriesById,
  getSeriesForChannel,
  getSeriesProgress,
  getAllSeriesProgress,
  getSubscribedSeries,
}
