import { createSelector } from 'reselect';

const STATE_KEY = 'trendingTags';
const stateFromRoot = (state) => state[STATE_KEY];

export const isLoadingSelector = (state) => stateFromRoot(state).isLoading;

export const trendingTagsSelector = createSelector([
  stateFromRoot,
], (trendingTagsState) => {
  return trendingTagsState.tags;
});
