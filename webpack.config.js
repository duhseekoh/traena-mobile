import path from 'path';

export default {
  resolve: {
    modules: [
      path.resolve(__dirname, './'), //eslint-disable-line
      path.resolve('./node_modules')
    ]
  }
};
