// @flow
export type IndexedPage<I, T: ?Object> = {
  ids: Array<I>,
  totalPages: number,
  first: boolean,
  last: boolean,
  number: number,
  size: number,
  index: {[_: I]: T},
  numberOfPages: number,
  totalElements: number,
};

export type ArrayPage<T: ?Object> = {
  totalPages: number,
  first: boolean,
  last: boolean,
  number: number,
  size: number,
  content: T[],
  numberOfPages: number,
  totalElements: number,
};
