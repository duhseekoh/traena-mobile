import { createSelector } from 'reselect';
import { contentItemsSelectorFactory } from 'app/redux/modules/content/selectors';
import { REQUEST_STATUSES } from 'app/redux/constants';

const STATE_KEY = 'bookmarkUI';
const stateFromRoot = (state) => state[STATE_KEY];

const uiBookmarkContentIdsSelector = createSelector([
  stateFromRoot
], (bookmarkState) => {
  return bookmarkState.uiBookmarkContentIds;
});

const bookmarkSelector = contentItemsSelectorFactory(uiBookmarkContentIdsSelector);


const isLoadingMoreSelector = (state) => stateFromRoot(state).loadingStatus === REQUEST_STATUSES.IN_PROGRESS;

const isRefreshingSelector = (state) => stateFromRoot(state).refreshingStatus === REQUEST_STATUSES.IN_PROGRESS;

const canLoadMoreSelector = (state) => stateFromRoot(state).canLoadMore;
const currentPageSelector = (state) => stateFromRoot(state).currentPage;

const hasErrorSelector = (state) =>
  stateFromRoot(state).loadingStatus === REQUEST_STATUSES.ERROR
  || stateFromRoot(state).refreshingStatus === REQUEST_STATUSES.ERROR;

/**
 * Are we at the bottom of the results list? Can't be loading, cant be
 refreshing, and make sure that we have hit the end of paging.
 */
const displayNoMoreResultsSelector = createSelector([
  isLoadingMoreSelector,
  isRefreshingSelector,
  canLoadMoreSelector,
], (isLoadingMore, isRefreshing, canLoadMore) => {
  return !isLoadingMore && !isRefreshing && !canLoadMore;
});

export {
  STATE_KEY,
  bookmarkSelector,
  isLoadingMoreSelector,
  isRefreshingSelector,
  canLoadMoreSelector,
  currentPageSelector,
  hasErrorSelector,
  displayNoMoreResultsSelector,
};
