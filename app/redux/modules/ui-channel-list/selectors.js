// @flow
import * as fromChannel from '../channel/selectors';
import { REQUEST_STATUSES } from 'app/redux/constants';
import type { ReduxState } from 'app/redux/constants';
 const STATE_KEY = 'uiChannelList';
const stateFromRoot = (state: ReduxState) => state[STATE_KEY];
const channelIdsSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  return reducerState.channelIds;
};

const selectChannelList = (state: ReduxState) => {
  const channelIds = channelIdsSelector(state);
  return fromChannel.selectChannels(state, { channelIds });
};

const isLoadingMoreChannelListSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  return reducerState.loadingStatus === REQUEST_STATUSES.IN_PROGRESS;
};
const isRefreshingChannelListSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  return reducerState.refreshingStatus === REQUEST_STATUSES.IN_PROGRESS;
};
const hasErrorChannelListSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  return (
    reducerState.loadingStatus === REQUEST_STATUSES.ERROR ||
    reducerState.refreshingStatus === REQUEST_STATUSES.ERROR
  );
};
const canLoadMoreChannelListSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  return reducerState.canLoadMore;
};
const currentChannelListPageSelector = (state: ReduxState) => {
 const reducerState = stateFromRoot(state);
 return reducerState.currentPage;
};
const displayNoMoreChannelListResultsSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  return (
    !reducerState.isLoadingMore &&
    !reducerState.isRefreshing &&
    !reducerState.canLoadMore
  );
};
export {
  STATE_KEY,
  isLoadingMoreChannelListSelector,
  isRefreshingChannelListSelector,
  hasErrorChannelListSelector,
  currentChannelListPageSelector,
  canLoadMoreChannelListSelector,
  displayNoMoreChannelListResultsSelector,
  selectChannelList,
};
