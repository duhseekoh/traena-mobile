// @flow
import React from "react";
import { Animated, View, StyleSheet, LayoutAnimation } from "react-native";
import type { ViewStyleProp } from "react-native/Libraries/StyleSheet/StyleSheet";
import styleVars from "app/style/variables";

type Props = {
  style?: ViewStyleProp,
  percent: number,
  barColor?: string,
  barBackgroundColor?: string,
};

/**
 * A simple progress bar. Given a percentage, will display a filled in color
 * on top of a neutral background color.
 */
class ProgressBar extends React.Component<Props> {

  componentDidUpdate(prevProps: Props) {
    // animate the progress bar
    if (prevProps.percent !== this.props.percent) {
      LayoutAnimation.linear();
    }
  }

  static defaultProps = {
    percent: 0,
    barColor: styleVars.color.traenaPrimaryGreen,
    barBackgroundColor: styleVars.color.lightGrey6,
  }

  render() {
    const { percent, style, barColor, barBackgroundColor } = this.props;
    return (
      <View style={[styles.container, style, {
        backgroundColor: barBackgroundColor,
      }]}>
        <Animated.View
          style={[
            styles.progress,
            {
              width: `${percent}%`,
              backgroundColor: barColor,
            }
          ]}
        />
      </View>
    );
  }
}

export default ProgressBar;

const styles = StyleSheet.create({
  container: {
    height: 5,
    borderRadius: 5,
  },
  progress: {
    height: '100%',
    borderRadius: 5,
  }
});
