// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import { TRATextStrong, FeatherIcon, FeedStatusCard, TRAText } from 'app/components';
import { isLoadingSelector, selectBookmarks } from 'app/redux/modules/ui-bookmarks-widget/selectors';
import * as bookmarksWidgetActions from 'app/redux/modules/ui-bookmarks-widget/actions';
import { goToContentStandalone, goToBookmarks } from 'app/redux/modules/navigation/actions';
import HorizontalSlider from 'app/components/horizontal-slider/HorizontalSlider';
import styleVars from 'app/style/variables';
import type { ContentItem } from 'app/redux/modules/content/types';
import ContentCard from 'app/components/content-card/ContentCard';

type Props = {
  content: ContentItem[],
  initialize: () => any,
  goToContentStandalone: (contentId: number) => any,
  goToBookmarks: () => any,
};

class BookmarksWidgetContainer extends Component<Props> {
  componentDidMount() {
    this.props.initialize();
  }

  render() {
    const noNewContent = !this.props.isLoading &&
      !this.props.content.length;
    const displayLoading = this.props.isLoading &&
      !this.props.content.length;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <TRATextStrong style={styles.title}>
            <FeatherIcon
              fontSize={styleVars.iconSize.medlar}
              color={styleVars.color.traenaPrimaryGreen}
              name={'bookmark'}
            /> Bookmarks
          </TRATextStrong>
          {!noNewContent &&
            <TouchableOpacity style={styles.viewAllButton} onPress={this.props.goToBookmarks}>
              <TRAText style={styles.viewAllButtonText}>
                VIEW ALL
              </TRAText>
              <FeatherIcon name="arrow-right" style={styles.viewAllButtonIcon} />
            </TouchableOpacity>
          }
        </View>
        <View style={{width: '100%'}}>
          {displayLoading ?
            <View style={{
              flex: 1,
              paddingHorizontal: styleVars.padding.edgeOfScreen,
            }}>
              <Image
                style={{
                  flex: 1,
                  aspectRatio: 1.6,
                  width: 'auto',
                  height: 'auto',
                  borderRadius: 8,
                  resizeMode: 'cover',
                }}
                source={require('app/images/post-ghost.png')}
              />
            </View> :
            <HorizontalSlider
              data={this.props.content}
              renderItem={(item: ContentItem) => <ContentCard
                key={item.id}
                contentItem={item}
                onPress={this.props.goToContentStandalone}
              />}
            />
          }
          {noNewContent &&
            <View style={{ flex: 1, paddingHorizontal: styleVars.padding.edgeOfScreen }}>
              <FeedStatusCard
                imageSource={require('app/images/emptybookmark.png')}
                title={'Save for later.'}
                message={'Add posts to your bookmark list by \n tapping the icon.'}
              />
            </View>
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
  },
  header: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
  },
  title: {
    fontSize: styleVars.fontSize.large,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  viewAllButton: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginRight: styleVars.padding.edgeOfScreen,
  },
  viewAllButtonText: {
    color: styleVars.color.forestGreen,
    fontFamily: styleVars.fontFamily.medium,
    marginRight: 10,
  },
  viewAllButtonIcon: {
    fontSize: styleVars.iconSize.large,
    marginTop: -3,
  },
});

const mapStateToProps = (state) => ({
  content: selectBookmarks(state),
  isLoading: isLoadingSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  initialize: () => dispatch(bookmarksWidgetActions.initialize()),
  goToContentStandalone: (contentId: number) => dispatch(goToContentStandalone(contentId)),
  goToBookmarks: () => dispatch(goToBookmarks()),
})

export default connect(mapStateToProps, mapDispatchToProps)(BookmarksWidgetContainer);
