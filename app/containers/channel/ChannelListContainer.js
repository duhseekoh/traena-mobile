// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import type {
  NavigationNavigatorProps,
  NavigationEventSubscription,
} from 'react-navigation';
import { View } from 'react-native';
import {
  isLoadingMoreChannelListSelector,
  isRefreshingChannelListSelector,
  canLoadMoreChannelListSelector,
  displayNoMoreChannelListResultsSelector,
  selectChannelList,
} from 'app/redux/modules/ui-channel-list/selectors';
import { refreshChannelList, loadMoreChannelList } from 'app/redux/modules/ui-channel-list/actions';
import type { Channel } from 'app/redux/modules/channel/types';
import logger from 'app/logging/logger';
import type { Dispatch, ReduxState } from 'app/redux/constants';
import { ChannelList } from 'app/components';
 type Props = {
  isRefreshingChannelList: boolean,
  isLoadingMoreChannelList: boolean,
  canLoadMoreChannelList: boolean,
  displayNoMoreChannelListResults: boolean,
  channels: {[channelId: number]: Channel}[],
  refreshChannelList: () => any,
  loadMoreChannelList: () => any,
} & NavigationNavigatorProps<*, *>;
 /**
 * Displays all channels a user is subscribed to.
 */
class ChannelListContainer extends Component<Props> {
  _navigationListener: NavigationEventSubscription;
  componentDidMount() {
    this._navigationListener = this.props.navigation.addListener(
      'didFocus',
      () => {
        logger.logBreadcrumb('Channel List displayed');
        this.props.refreshChannelList();
      },
    );
  }
   componentWillUnmount() {
    this._navigationListener.remove();
  }
   render() {
    const {
      isRefreshingChannelList,
      isLoadingMoreChannelList,
      canLoadMoreChannelList,
      displayNoMoreChannelListResults,
      channels,
      refreshChannelList,
      loadMoreChannelList,
    } = this.props;
     return (
      <View style={{ flex: 1 }}>
        <ChannelList
          refreshChannelList={() => refreshChannelList()}
          isRefreshing={isRefreshingChannelList}
          channels={channels}
          loadNextPage={() => loadMoreChannelList()}
          isLoadingMore={isLoadingMoreChannelList}
          canLoadMore={canLoadMoreChannelList}
          displayNoMoreResults={displayNoMoreChannelListResults}
        />
      </View>
    );
  }
}
 const mapStateToProps = (state: ReduxState) => {
  return {
    channels: selectChannelList(state),
    isRefreshingChannelList: isRefreshingChannelListSelector(state),
    isLoadingMoreChannelList: isLoadingMoreChannelListSelector(state),
    canLoadMoreChannelList: canLoadMoreChannelListSelector(state),
    displayNoMoreChannelListResults: displayNoMoreChannelListResultsSelector(state),
  };
};
 const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    refreshChannelList: () => {
      return dispatch(refreshChannelList());
    },
    loadMoreChannelList: () => {
      return dispatch(loadMoreChannelList());
    },
  };
};
 export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withNavigation(ChannelListContainer));
