// @flow
import React from 'react';
import { View } from 'react-native';

type Props = {
  size?: number;
}
/**
* a spacer used to keep uniform distance between elements on our feed.
*/
const FeedSpacer = ({ size }: Props) => (
  <View style={{ marginVertical: size }} />
);

FeedSpacer.defaultProps = {
  size: 5,
};

export default FeedSpacer;
