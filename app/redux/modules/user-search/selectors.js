const STATE_KEY = 'userSearch';
const stateFromRoot = (state) => state[STATE_KEY];

const searchResultsSelector = (state) => stateFromRoot(state).results;
const queryTextSelector = (state) => stateFromRoot(state).queryText;
const isLoadingSelector = (state) => stateFromRoot(state).isLoading;

export {
  STATE_KEY,
  queryTextSelector,
  searchResultsSelector,
  isLoadingSelector,
};
