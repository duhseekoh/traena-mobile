// @flow
import fetchIntercept from 'fetch-intercept';
import { getAuthHeaders } from './headers';
import { httpStatusCodeChecker } from './errorHandler';
import logger from 'app/logging/logger';
import appConfig from 'app/config';

function getJsonConfig(config) {
  config.headers = Object.assign({}, {
    'Content-Type': 'application/json',
  }, config.headers);
  if (config.body) config.body = JSON.stringify(config.body);
  return config;
}

export default () => fetchIntercept.register({
  request(url, config = {}) {
    // Bypass if the request is going to the network
    // Which might mean it's being used by a different lib
    if(!RegExp(appConfig.apiBaseUrl).test(url)) {
      return [url, config];
    }
    // Take whatever headers set on fetch, and apply them on top of auth headers
    config.headers = Object.assign({}, getAuthHeaders(), config.headers);
    return [url, getJsonConfig(config)];
  },
  response(response) {
    if(!RegExp(appConfig.apiBaseUrl).test(response.url)) {
      //If this wasn't hitting our API, return the original response
      return response;
    }
    if (!response.ok) {
      logger.log(response);
      httpStatusCodeChecker(response);
    }
    const type = response.headers.get('content-type');
    if (/application\/json/.test(type)) {
      return response.json();
    }
    return response.text();
  },
});
