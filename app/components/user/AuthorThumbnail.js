// @flow
import React from 'react';
import {
  StyleSheet,
  Image,
  View
} from 'react-native';
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  imageURI: string,
  size?: 'small' | 'medium' | 'large';
  style?: ViewStyleProp,
};

const AuthorThumbnail = ({imageURI, style, size = 'medium'}: Props) => {
  let width = 70, height = 70, borderRadius = 35;
  if(size === 'small') {
    width = 40;
    height = 40;
    borderRadius = 20;
  } else if(size === 'large') {
    width = 120;
    height = 120;
    borderRadius = 60;
  }

  return (
    <View style={[
      styles.roundedContainer, style,
      {width: width, height: height}
    ]}>
      <Image style={[
        styles.image, { borderRadius }
      ]}
        source={{uri: imageURI}}
      />
    </View>
  )
}

export default AuthorThumbnail;

const styles = StyleSheet.create({
  roundedContainer: {
    borderWidth: 2,
    borderColor: styleVars.color.white,
    overflow: 'hidden'
  },
  image: {
    resizeMode: 'cover',
    width: null,
    height: null,
    flex: 1
  }
});
