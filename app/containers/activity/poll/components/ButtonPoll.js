// @flow
import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
} from 'react-native';
import {
  TRATextStrong,
} from 'app/components';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import styleVars from 'app/style/variables';
import ProgressBar from '../../components/ProgressBar';

type Props = {
  text: string,
  style?: ViewStyleProp,
  onPress: () => *,
  isAnswered: boolean,
  isSelected: boolean,
  percent: number,
};


const ButtonPoll = (props: Props) => {
  const { text, percent, isAnswered, onPress, isSelected } = props;
  const boxStyle = isAnswered && isSelected ? styles.boxStyleSelected : styles.boxStyle;
  const fillStyle = isAnswered && isSelected ? styles.fillStyleSelected : styles.fillStyle;
  return  (
    <TouchableOpacity onPress={onPress} disabled={isAnswered}>
      <ProgressBar
        progress={percent}
        boxStyle={boxStyle}
        fillStyle={fillStyle}>
        <View style={styles.lableStyle}>
          <TRATextStrong>{text}</TRATextStrong>
          {isAnswered && <TRATextStrong>{Math.round(percent)}%</TRATextStrong>}
        </View>
      </ProgressBar>
      <View style={styles.clips} />
    </TouchableOpacity>);
}

export default ButtonPoll;

const styles = StyleSheet.create({
  clips: {
    position: 'absolute',
    borderRadius: 30,
    borderWidth: 10,
    borderColor: styleVars.color.white,
    top: -10,
    bottom: -10,
    left: -10,
    right: -10,
  },
  boxStyleSelected: {
    height: 50,
    borderRadius: 25,
    borderColor: styleVars.color.traenaOne,
    borderWidth: 2,
    backgroundColor: styleVars.color.transparent,
    overflow: 'hidden'
  },
  fillStyleSelected: {
    height: 50,
    backgroundColor: styleVars.color.traenaOne,
  },
  boxStyle: {
    height: 50,
    borderRadius: 25,
    borderColor: styleVars.color.mediumGrey2,
    borderWidth: 2,
    backgroundColor: styleVars.color.transparent,
    overflow: 'hidden'
  },
  fillStyle: {
    height: 50,
    backgroundColor: styleVars.color.mediumGrey2,
  },
  lableStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 50,
    alignItems: 'center',
    paddingHorizontal: 12
  },
});
