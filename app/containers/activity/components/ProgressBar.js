// @flow
import React, { Component } from 'react';
import {
  View,
  Animated,
  StyleSheet,
} from 'react-native';
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  progress: number,
  children?: React$Node,
  fillStyle?: ViewStyleProp,
  boxStyle?: ViewStyleProp,
  containerStyle?: ViewStyleProp,
};

class ProgressBar extends Component<Props> {
  props: Props;
  animation: Animated.Value;
  widthInterpolated: *;

  componentWillMount() {
    const startProgress = this.props.progress ? this.props.progress : 0;
    this.animation = new Animated.Value(startProgress);
    this.widthInterpolated = this.animation.interpolate({
      inputRange: [0, 100],
      outputRange: ['0%', '100%'],
      extrapolate: "clamp",
    });
  }

  componentDidUpdate(prevProps: Props) {
    if(this.props.progress !== prevProps.progress) {
      Animated.timing(this.animation, {
        toValue: this.props.progress,
        duration: 800,
      }).start();
    }
  }

  render() {
    return (
      <View style={{height: 50, backgroundColor: styleVars.color.transparent}}>
        <View style={[styles.container, this.props.containerStyle]}>
          <View style={[styles.barBox, this.props.boxStyle]}>
            <Animated.View style={[styles.barFill, this.props.fillStyle, {width: this.widthInterpolated }]}></Animated.View>
          </View>
          {this.props.children && <View style={styles.barLabel}>{this.props.children}</View>}
        </View>
      </View>
    );
  }
}

export default ProgressBar;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  barBox: {
    position: 'absolute',
    flex: 1,
    height: 12,
    width: '100%',
    backgroundColor: styleVars.color.lightGrey,
  },
  barLabel: {
    position: 'absolute',
    flex: 1,
    width: '100%',
    backgroundColor: styleVars.color.transparent,
  },
  barFill: {
    position: 'absolute',
    flex: 1,
    height: 12,
    backgroundColor: styleVars.color.traenaPrimaryGreen,
  }
});
