// @flow
import { getSelfAnalyticsTpl } from './util/endpoints';
import moment from 'moment';

export function fetchStats() {
  const currentOffset = moment().format('Z');
  const url = getSelfAnalyticsTpl({offset: currentOffset});
  return fetch(url);
}
