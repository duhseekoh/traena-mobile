// @flow
import { SET_CURRENT_QUESTION, SET_CORRECT_FIRST_TRY, RESET_QUESTION_SET, RESET_ACTIVITY } from './actions';
import type { Action } from 'app/types/Action';
import type { QuestionSetUIData } from './types';
import questionSetReducer from './questionSetReducer';

export type ReduxStateActivityUI = QuestionSetUIData | null;

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = null;

const activityUIReducer = (state: ReduxStateActivityUI = initialState, action: Action) => {
  switch(action.type) {
    case RESET_ACTIVITY:
      return initialState;
    // set of actions for Question Sets
    case SET_CURRENT_QUESTION:
    case SET_CORRECT_FIRST_TRY:
    case RESET_QUESTION_SET:
      return state ? questionSetReducer(state, action) : questionSetReducer(undefined, action);
    default:
      return state;
  }
};

export default activityUIReducer;
