// @flow
import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import type { TrainingContentItem } from 'app/redux/modules/content/types';
import GenericContentItem from '../GenericContentItem';
import {
  VideoPlayer,
} from 'app/components';

type Props = {
  contentItem: TrainingContentItem,
  shouldTruncateText?: boolean,
  onLikePressed: (updatedDoesLike: boolean) => void,
  onCommentPressed: () => void,
  onBookmarkPressed: () => void,
  onCompletePressed: () => void,
  isDetailView?: boolean,
};

function TrainingItem(props: Props) {
  const { video = {}, id } = props.contentItem;
  const { previewImageURI, videoURI } = video;
  const playerStyle = props.isDetailView ? styles.videoPlayerDetail : styles.videoPlayer;
  return (
    <GenericContentItem {...props}>
      <View>
        <VideoPlayer
          eventMeta={{contentId: id}}
          style={playerStyle}
          videoURI={videoURI} previewImageURI={previewImageURI} />
        </View>
    </GenericContentItem>

  );
}

export default TrainingItem;

const contentPadding = 8;
const styles = StyleSheet.create({
  videoPlayer: {
    marginHorizontal: -contentPadding,
    marginBottom: 5,
    position: 'relative'
  },
  videoPlayerDetail: {
    marginBottom: 5,
    position: 'relative'
  },
});
