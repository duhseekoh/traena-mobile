// @flow
import React from 'react';
import { StyleSheet, View } from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  children: Node,
  style?: ViewStyleProp,
};

/**
 * Displays horizontally aligned icons.
 */
const IconBar = ({ children, style }: Props) => {
  const elems = (
    <View style={[styles.iconBar, style]}>
      {
        React.Children.map(children, (child, idx) => {
          // check that child exists. it may just be a false value,
          // if consumer is using `isCondition && <element>` conditionals
          return child && <View key={idx} style={styles.iconHolder}>{child}</View>
        })
      }
    </View>
  )
  return elems;
}

export default IconBar;

const styles = StyleSheet.create({
  iconBar: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  iconHolder: {
    marginLeft: 5,
  }
});
