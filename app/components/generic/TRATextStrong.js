// @flow
import React from 'react';
import type { TextStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import {
  Text,
  StyleSheet,
} from 'react-native';
import styleVars from 'app/style/variables';

type Props = {
  style?: TextStyleProp,
  children: Node,
};

const TRATextStrong = (props: Props) => {
  const {style, children} = props;
  return (
    <Text {...props} style={[styles.textStyle, style]}>{children}</Text>
  );
}

export default TRATextStrong;

const styles = StyleSheet.create({
  textStyle: {
    color: styleVars.color.regular,
    fontFamily: styleVars.fontFamily.medium,
    fontSize: styleVars.fontSize.regular,
  }
});
