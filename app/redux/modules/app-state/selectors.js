import { createSelector } from 'reselect';
import _ from 'lodash';

/**
 * Chooses what parts of the redux state get persisted.
 *  When we find a good case to persist data, lets do it. Persisting everything
 *  can have unintended consequences like...  persisting the state of something
 *  like isLoading and the user quits the app in the middle of that. Or building
 *  up a bunch of historical state in storage modules like task and content.
 */
const persistedAppStateSelector = createSelector([
  state => state
], (appState) => {
  const { auth, user } = appState;

  // Blacklist some properties from being saved
  const persistAuth = _.omit(auth, ['loginRequestState']);

  return {
    auth: persistAuth,
    user,
  }
});

const isConnectedSelector = state => state.network.isConnected;

export {
  persistedAppStateSelector,
  isConnectedSelector,
}
