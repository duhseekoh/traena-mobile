// flow-typed signature: 302c03dc7eff32bb2c6bd7422bb8557f
// flow-typed version: <<STUB>>/bugsnag-react-native_v^2.2.3/flow_v0.61.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'bugsnag-react-native'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'bugsnag-react-native' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'bugsnag-react-native/lib/Bugsnag' {
  declare module.exports: any;
}

// Filename aliases
declare module 'bugsnag-react-native/index' {
  declare module.exports: $Exports<'bugsnag-react-native'>;
}
declare module 'bugsnag-react-native/index.js' {
  declare module.exports: $Exports<'bugsnag-react-native'>;
}
declare module 'bugsnag-react-native/lib/Bugsnag.js' {
  declare module.exports: $Exports<'bugsnag-react-native/lib/Bugsnag'>;
}
