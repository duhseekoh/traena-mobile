export type Toast = { // eslint-disable-line
  title: string,
  message: string,
};
