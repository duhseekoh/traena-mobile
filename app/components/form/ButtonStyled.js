// @flow
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { TRATextStrong } from 'app/components';

export type Props = {
  text: string,
  icon?: React$Node,
  style?: ViewStyleProp,
  onPress: () => any,
  disabled: boolean,
  loading: boolean,
  focused: boolean,
  defaultTextStyle: ViewStyleProp,
  defaultButtonStyle: ViewStyleProp,
  focusedTextStyle: ViewStyleProp,
  focusedButtonStyle: ViewStyleProp,
  disabledTextStyle: ViewStyleProp,
  disabledButtonStyle: ViewStyleProp,
  loadingTextStyle: ViewStyleProp,
  loadingButtonStyle: ViewStyleProp,
};

const ButtonStyled = (props: Props) => {
  const buttonOnPress =
    props.disabled || props.loading ? () => {} : props.onPress;
  const buttonWrapperStyle = [props.defaultButtonStyle];
  const buttonTextStyle = [props.defaultTextStyle];

  if (props.loading) {
    buttonWrapperStyle.push(props.loadingButtonStyle);
    buttonTextStyle.push(props.loadingTextStyle);
  }

  if (props.focused) {
    buttonWrapperStyle.push(props.focusedButtonStyle);
    buttonTextStyle.push(props.focusedTextStyle);
  } else if (props.disabled) {
    buttonWrapperStyle.push(props.disabledButtonStyle);
    buttonTextStyle.push(props.disabledTextStyle);
  }

  return (
    <TouchableOpacity style={props.style} onPress={buttonOnPress}>
      <View style={buttonWrapperStyle}>
        {props.text && (
          <TRATextStrong style={buttonTextStyle}>{props.text}</TRATextStrong>
        )}
        {props.icon}
      </View>
    </TouchableOpacity>
  );
};

export default ButtonStyled;
