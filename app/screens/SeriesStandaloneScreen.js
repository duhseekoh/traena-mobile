// @flow
import React, { Component } from "react";
import { connect } from "react-redux";
import type {
  NavigationNavigatorProps,
  NavigationStackScreenOptions,
  NavigationEventSubscription,
} from "react-navigation";
import { SafeAreaView, StyleSheet, View } from "react-native";
import {
  ContentList,
  ProgressBar,
  TRATextStrong,
  TRAText
} from "app/components";
import {
  contentForSeriesSelector,
  isLoadingMoreSelector,
  isRefreshingSelector,
  canLoadMoreSelector,
  displayNoMoreResultsSelector
} from "app/redux/modules/ui-series-standalone/selectors";
import { seriesProgressSelector } from "app/redux/modules/series/selectors";
import {
  refreshSeriesContent,
  loadMoreSeriesContent
} from "app/redux/modules/ui-series-standalone/actions";
import type { ContentItem } from "app/redux/modules/content/types";
import type { SeriesProgress } from "app/redux/modules/series/types";
import styleVars from "app/style/variables";
import type { ReduxState } from 'app/redux/constants';

type NavigationStateParams = {
  params: {|
    id: number,
    title: string,
    description: string
  |}
};

type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

type Props = {
  content: ContentItem[],
  seriesProgress: ?SeriesProgress,
  loadMoreSeriesContent: (seriesId: number) => void,
  refreshSeriesContent: (seriesId: number) => void,
  isLoadingMore: boolean,
  isRefreshing: boolean,
  canLoadMore: boolean,
  displayNoMoreResults: boolean
} & NavigationProps;

class SeriesScreen extends Component<Props> {
  props: Props;
  _navigationListener: NavigationEventSubscription;
  /**
   * Adding this static method to the component allows us to use
   * dynamic titles.  For more info https://reactnavigation.org/docs/headers.html#using-params-in-the-title
   */
  static navigationOptions = ({
    navigation
  }: NavigationProps): NavigationStackScreenOptions => {
    const { params } = navigation.state;
    return {
      title: params ? params.title : "SERIES",
    };
  };

  /**
   * Whenever this screen is displayed, refresh the content and progress indicator
   */
  componentDidMount() {
    this._navigationListener = this.props.navigation.addListener(
      'didFocus',
      () => {
        this.refreshSeriesContent();
      }
    );
  }

  componentWillUnmount() {
    this._navigationListener.remove();
  }

  refreshSeriesContent() {
    const { refreshSeriesContent } = this.props;
    const { params } = this.props.navigation.state;
    const { id } = params;

    refreshSeriesContent(id);
  }

  render() {
    const {
      content,
      refreshSeriesContent,
      isRefreshing = false,
      loadMoreSeriesContent,
      isLoadingMore,
      canLoadMore,
      displayNoMoreResults
    } = this.props;
    const { params } = this.props.navigation.state;
    const { id } = params;

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ContentList
          refreshContent={() => refreshSeriesContent(id)}
          isRefreshing={isRefreshing}
          contentItems={content}
          loadNextPage={() => loadMoreSeriesContent(id)}
          header={this.renderHeader()}
          isLoadingMore={isLoadingMore}
          canLoadMore={canLoadMore}
          displayNoMoreResults={displayNoMoreResults}
          refreshIconOffset={0}
        />
      </SafeAreaView>
    );
  }

  renderHeader = () => {
    const { params } = this.props.navigation.state;
    const { title, description } = params;
    return (
      <View style={{ padding: 20, backgroundColor: styleVars.color.white }}>
        <TRATextStrong
          style={{ fontSize: styleVars.fontSize.large, paddingBottom: 5 }}
        >
          {title}
        </TRATextStrong>
        <TRAText>{description}</TRAText>
        {this.renderProgressBar()}
      </View>
    );
  };

  renderProgressBar = () => {
    const { seriesProgress } = this.props;

    if (!seriesProgress) {
      return null;
    }

    const { completedContentCount, contentCount } = seriesProgress;
    const percent = (completedContentCount / contentCount) * 100;

    return (
      <View style={styles.progressBarHolder}>
        <TRATextStrong>
          {completedContentCount}/{contentCount} Posts Completed
        </TRATextStrong>
        <ProgressBar percent={percent} style={styles.progressBar} />
      </View>
    );
  };
}

const mapStateToProps = (state: ReduxState, props: Props) => {
  const { params } = props.navigation.state;
  const selectorProps = { id: params.id };
  return {
    content: contentForSeriesSelector(state, selectorProps),
    isRefreshing: isRefreshingSelector(state, selectorProps),
    isLoadingMore: isLoadingMoreSelector(state, selectorProps),
    canLoadMore: canLoadMoreSelector(state, selectorProps),
    displayNoMoreResults: displayNoMoreResultsSelector(state, selectorProps),
    seriesProgress: seriesProgressSelector(state, selectorProps),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    refreshSeriesContent: seriesId => {
      return dispatch(refreshSeriesContent(seriesId));
    },
    loadMoreSeriesContent: seriesId => {
      return dispatch(loadMoreSeriesContent(seriesId));
    }
  };
};

export { SeriesScreen };
const ConnectedSeriesScreen = connect(mapStateToProps, mapDispatchToProps)(
  SeriesScreen
);
export default ConnectedSeriesScreen;

const styles = StyleSheet.create({
  progressBarHolder: {
    flex: 1,
    marginTop: 25,
  },
  progressBar: {
    marginTop: 5,
  },
});
