import envConfig from '../env-config';
import packageJson from '../package.json';

export default Object.freeze({
  // prefix to all of our traena endpoints
  apiBaseUrl: envConfig.API_BASE_URL,
  // aka environment name. used only by bugsnag currently.
  stageName: envConfig.STAGE_NAME,
  // bugsnag configuration
  bugsnag: {
    apiKey: envConfig.BUGSNAG_API_KEY,
  },
  // auth0 configuration, used for auth0 lock and auth0 rest client
  auth0: {
    clientId: envConfig.AUTH0_CLIENT_ID,
    domain: envConfig.AUTH0_DOMAIN,
    audience: envConfig.AUTH0_AUDIENCE,
    userClaimNamespace: envConfig.AUTH0_USER_CLAIM_NAMESPACE,
    dbConnectionName: envConfig.AUTH0_DB_CONNECTION_NAME,
    scope: 'openid email groups details roles permissions offline_access'
  },
  fcm: {
    senderId: envConfig.FCM_SENDER_ID,
  },
  codepush: {
    // unique based on ios vs android and environment deploying to
    deploymentKey: envConfig.CODEPUSH_DEPLOYMENT_KEY,
  },
  feedbackUri: envConfig.FEEDBACK_URI,
  displayEnvironmentDetails: envConfig.DISPLAY_ENVIRONMENT_DETAILS === 'true' ? true : false,
  packageJson,
});
