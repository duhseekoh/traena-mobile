// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import DailyAction from './components/daily-action/DailyAction';
import TrainingItem from './components/training/TrainingItem';
import ImageContentType from './components/image/ImageContentType';
import { likeContent, unlikeContent } from 'app/redux/modules/content/actions';
import { addCompletion } from 'app/redux/modules/traena-task/actions';
import { addBookmark, deleteBookmark } from 'app/redux/modules/bookmark/actions';
import { goToCommentsStandalone, goToContentStandalone, goToImageFullscreen, goToTagStandalone } from 'app/redux/modules/navigation/actions';
import type { ContentItem } from 'app/redux/modules/content/types';
import type { Dispatch } from 'app/redux/constants';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  contentItem: ContentItem,
  style?: ViewStyleProp,
  addBookmark: (contentId: number) => void,
  deleteBookmark: (contentId: number) => void,
  addCompletion: (contentId: number) => void,
  updateDoesLike: (contentId: number, updatedDoesLike: boolean) => void,
  goToComments: (contentId: number) => void,
  goToContentStandalone: (id: number) => void,
  goToTagStandalone: (tag: string) => void,
  shouldClipImage?: boolean,
  isDetailView?: boolean,
  goToImageFullscreen?: (contentItem: ContentItem, initialImageIndex: number) => void,
};

class ContentContainer extends Component<Props> {
  props: Props;
  /**
   * Only a few props can change over the life of a content item. We don't want
   * to re-render and check virtual dom equivalency if just the list containing
   * these items was modified in some way, but not the props we care about.
   */
  shouldComponentUpdate(nextProps: Props) {
    const { isBookmarked, isCompleted, social, id } = this.props.contentItem;
    const { likes, comments } = social;
    const { isBookmarked:nextIsBookmarked, isCompleted:nextIsCompleted, social:nextSocial, id:nextId } = nextProps.contentItem;
    const { likes:nextLikes, comments:nextComments } = nextSocial;

    return ( id !== nextId
      || likes.count !== nextLikes.count
      || comments.count !== nextComments.count
      || likes.doesUserLike !== nextLikes.doesUserLike
      || isBookmarked !== nextIsBookmarked
      || isCompleted !== nextIsCompleted
    );
  }

  render() {
    const { contentItem, style, goToContentStandalone, isDetailView } = this.props;
    const { id } = contentItem;
    let content;
    if(contentItem.type === 'TrainingVideo') {
      content = <TrainingItem
        contentItem={contentItem}
        onLikePressed={this._onLikePressed}
        onCommentPressed={this._onCommentPressed}
        onBookmarkPressed={this._onBookmarkPressed}
        onCompletePressed={this._onCompletePressed}
        isDetailView={isDetailView}
      />
    } else if(contentItem.type === 'DailyAction') {
      content = <DailyAction
        contentItem={contentItem}
        onLikePressed={this._onLikePressed}
        onCommentPressed={this._onCommentPressed}
        onBookmarkPressed={this._onBookmarkPressed}
        onCompletePressed={this._onCompletePressed}
        isDetailView={isDetailView}
      />
    } else if(contentItem.type === 'Image') {
      content = <ImageContentType
        onPressImage={this._onImagePressed}
        contentItem={contentItem}
        onLikePressed={this._onLikePressed}
        onCommentPressed={this._onCommentPressed}
        onBookmarkPressed={this._onBookmarkPressed}
        onCompletePressed={this._onCompletePressed}
        isDetailView={isDetailView}
      />
    }

    return (<TouchableWithoutFeedback style={style}
            onPress={() => goToContentStandalone(id)}>
            <View>
              {content}
            </View>
          </TouchableWithoutFeedback>)
  }

  _onLikePressed = (updatedDoesLike: boolean) => {
    const contentId = this.props.contentItem.id;
    this.props.updateDoesLike(contentId, updatedDoesLike);
  }

  _onCompletePressed = () => {
    const contentId = this.props.contentItem.id;
    this.props.addCompletion(contentId);
  }

  _onCommentPressed = () => {
    const contentId = this.props.contentItem.id;
    this.props.goToComments(contentId);
  }

  _onBookmarkPressed = () => {
    const { contentItem, addBookmark, deleteBookmark } = this.props;
    const { id, isBookmarked } = contentItem;
    if (isBookmarked) {
      deleteBookmark(id);
    } else {
      addBookmark(id);
    }
  }

  _onImagePressed = (imageIndex) => {
    const { isDetailView, contentItem, goToImageFullscreen, goToContentStandalone } = this.props;
    if (isDetailView) {
      goToImageFullscreen && goToImageFullscreen(contentItem, imageIndex);
    } else {
      goToContentStandalone && goToContentStandalone(contentItem.id);
    }
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    updateDoesLike: (contentId: number, updatedDoesLike: boolean) => {
      if(updatedDoesLike) {
        return dispatch(likeContent(contentId));
      } else {
        return dispatch(unlikeContent(contentId));
      }
    },
    addBookmark: (contentId: number) => {
      return dispatch(addBookmark(contentId));
    },
    deleteBookmark: (contentId: number) => {
      return dispatch(deleteBookmark(contentId));
    },
    addCompletion: (contentId) => {
      dispatch(addCompletion(contentId));
    },
    goToComments: (contentId: number) => {
      return dispatch(goToCommentsStandalone({contentId}));
    },
    goToContentStandalone: (contentId: number) => {
      return dispatch(goToContentStandalone(contentId));
    },
    goToImageFullscreen: (contentItem, initialImageIndex) => {
      return dispatch(goToImageFullscreen(contentItem, initialImageIndex));
    },
    goToTagStandalone: (tag: string) => {
      return dispatch(goToTagStandalone(tag));
    },
  };
};

const ConnectedContentContainer = connect(
  null,
  mapDispatchToProps
)(ContentContainer);

export default ConnectedContentContainer;
