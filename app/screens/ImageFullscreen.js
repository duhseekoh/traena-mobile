// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import type {
  NavigationNavigatorProps,
  NavigationStackScreenOptions,
} from 'react-navigation';
import {
  ActivityIndicator,
  StyleSheet,
  View,
  StatusBar,
  TouchableOpacity,
  InteractionManager,
} from 'react-native';
import { FeatherIcon } from 'app/components';
import styleVars from 'app/style/variables';
import type { ImageContentItem } from 'app/redux/modules/content/types';
import Gallery from 'react-native-image-gallery';
import Orientation from 'react-native-orientation';
import { exitImageFullscreen } from 'app/redux/modules/navigation/actions';

type NavigationStateParams = {
  params: {
    contentItem: ImageContentItem,
    initialImageIndex: number,
  },
};

type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

type Props = {
  exitImageFullscreen: () => void,
} & NavigationProps;

type State = {
  isReady: boolean,
};

class ImageFullscreen extends Component<Props, State> {
  static navigationOptions = (): NavigationStackScreenOptions => {
    return {
      header: null,
    };
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      isReady: false,
    }
  }

  componentDidMount() {
    Orientation.unlockAllOrientations();
    // the gallery won't display unless we trigger render from
    // runAfterInteractions. Setting state to know when to render.
    // https://github.com/archriss/react-native-image-gallery/issues/35
    InteractionManager.runAfterInteractions(() => {
      this.setState({isReady: true});
    });
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
  }

  _onPressExitImageFullscreen = () => {
    this.props.exitImageFullscreen();
  }

  render() {
    const { params } = this.props.navigation.state;

    if(this.state.isReady && params.contentItem) {
      return this._renderGallery(params.contentItem, params.initialImageIndex);
    }

    return (
      <View style={{flex: 1}}>
        <StatusBar
          backgroundColor={styleVars.color.lightGrey4}
          hidden />
          <View style={{flex: 1, justifyContent: 'center'}}>
            <View style={styles.loadingOverlay}>
              <ActivityIndicator
                size="small"
                color={styleVars.color.traenaPrimaryGreen}
              />
            </View>
          </View>
          <View style={{position: 'absolute', top: 10, right: 10}}>
            <TouchableOpacity   onPress={this._onPressExitImageFullscreen}
              hitSlop={{top: 20, left: 20, right: 20, bottom: 20}}>
                <FeatherIcon name='x' style={styles.exitIcon} fontSize={styleVars.iconSize.large} color={styleVars.color.black} />
            </TouchableOpacity>
          </View>
        </View>
    );
  }

  _renderGallery = (contentItem: ImageContentItem, initialImageIndex: number) => {
    let images = contentItem.images.map(i => {
      return { source: { uri: `${i.baseCDNPath}${i.variations.large.filename}`}};
    });

    return (
      <View style={{flex: 1}}>
        <StatusBar hidden />
        <Gallery
          initialPage={initialImageIndex}
          resizeMode={'contain'}
          images={images}
          style={{ flex: 1, backgroundColor: styleVars.color.lightGrey4 }}
        />
        <View style={{position: 'absolute', top: 10, right: 10}}>
          <TouchableOpacity onPress={this._onPressExitImageFullscreen}
            hitSlop={{top: 20, left: 20, right: 20, bottom: 20}}>
              <FeatherIcon name='x' style={styles.exitIcon} fontSize={styleVars.iconSize.large} color={styleVars.color.black} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    exitImageFullscreen: () => {
      return dispatch(exitImageFullscreen());
    },
  };
}

export default connect(null, mapDispatchToProps)(ImageFullscreen);

const styles = StyleSheet.create({
  exitIcon: {
    backgroundColor: styleVars.color.transparent,
  },
  loadingOverlay: {
    backgroundColor: styleVars.color.lightGrey,
    width: '100%',
    height: 250,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
