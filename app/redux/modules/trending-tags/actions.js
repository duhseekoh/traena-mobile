// @flow
import api from 'app/redux/api';
import type { ArrayPage } from '../../../types/Pagination';
import type { TagAggregation } from './types';

export const LOAD_TRENDING_TAGS_REQUEST_STARTED = 'LOAD_TRENDING_TAGS_REQUEST_STARTED';
const startedLoadTrendingTagsRequest = () => {
  return {
    type: LOAD_TRENDING_TAGS_REQUEST_STARTED,
  };
};

export const LOAD_TRENDING_TAGS_REQUEST_FINISHED = 'LOAD_TRENDING_TAGS_REQUEST_FINISHED';
const finishedLoadTrendingTagsRequest = () => {
  return {
    type: LOAD_TRENDING_TAGS_REQUEST_FINISHED,
  };
};

export const LOAD_TRENDING_TAGS = 'LOAD_TRENDING_TAGS';
function loadTrendingTags(tags: TagAggregation[]) {
  return {
    type: LOAD_TRENDING_TAGS,
    payload: tags
  };
}

export function initialize() {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(startedLoadTrendingTagsRequest());
      const results: ArrayPage<TagAggregation> = await api.search.retrieveContentTags({
        size: 5,
        daysAgo: 30,
      });
      dispatch(loadTrendingTags(results.content));
      dispatch(finishedLoadTrendingTagsRequest());
    } catch (err) {
      console.log('trendingTagsActions.initialize error: ', err);
      dispatch(finishedLoadTrendingTagsRequest());
    }
  }
}
