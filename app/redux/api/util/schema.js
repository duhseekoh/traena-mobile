// @flow
import { schema } from 'normalizr';

export const contentEntity = new schema.Entity('contents');

export const actionEntity = new schema.Entity('actions', {}, {
  idAttribute: (value, parent) => `${parent.id}_${value.id}`,
});

const contentPreviewEntitiy = new schema.Entity('contentPreview', undefined, {
  idAttribute: v => v.id,
});

export const seriesEntity = new schema.Entity('series', { contentPreview: new schema.Array(contentPreviewEntitiy) }, {
  idAttribute: v => v.id,
});

export const seriesEntityList = new schema.Array({contentPreview: new schema.Array(contentPreviewEntitiy)});
