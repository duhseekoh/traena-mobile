// @flow
export type TagAggregation = {
  key: string,
  doc_count: number,
};
