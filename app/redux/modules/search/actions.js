// @flow
import api from 'app/redux/api';
import { contentsFetchSucceeded } from 'app/redux/modules/content/actions';
import { queryTextSelector, currentPageSelector } from './selectors';
import type { IndexedPage } from '../../../types/Pagination';
import type { ContentItem } from '../content/types';

import { PAGE_SIZE } from './constants';
import type { Dispatch, GetState } from 'app/redux/constants';

const RETRIEVED_SEARCH_RESULTS = 'RETRIEVED_SEARCH_RESULTS';
const retrievedSearchResults = (searchResults: IndexedPage<*, ContentItem>) => {
  return {
    type: RETRIEVED_SEARCH_RESULTS,
    payload: searchResults
  };
};

const UI_SEARCH_TEXT_UPDATED = 'UI_SEARCH_TEXT_UPDATED';
const uiSearchTextUpdated = (queryText: string) => {
  return {
    type: UI_SEARCH_TEXT_UPDATED,
    payload: queryText
  }
};

const NEW_SEARCH_REQUEST_STARTED = 'NEW_SEARCH_REQUEST_STARTED';
const startedNewSearchRequest = () => {
  return {
    type: NEW_SEARCH_REQUEST_STARTED,
  };
};

const MORE_SEARCH_REQUEST_STARTED = 'MORE_SEARCH_REQUEST_STARTED';
const startedMoreSearchRequest = () => {
  return {
    type: MORE_SEARCH_REQUEST_STARTED,
  };
};

const SEARCH_REQUEST_FINISHED = 'SEARCH_REQUEST_FINISHED';
const finishedSearchRequest = () => {
  return {
    type: SEARCH_REQUEST_FINISHED,
  };
};

const CLEARED_SEARCH = 'CLEARED_SEARCH';
const clearSearch = () => {
  return {
    type: CLEARED_SEARCH,
  };
};

const ADDED_RECENT_SEARCH = 'ADDED_RECENT_SEARCH';
const addRecentSearch = (searchText: string) => {
  return {
    type: ADDED_RECENT_SEARCH,
    payload: searchText
  }
};

const RETRIEVED_RECOMMENDED_SEARCHES = 'RETRIEVED_RECOMMENDED_SEARCHES';
const retrievedRecommendSearches = (recommendedSearches: string[]) => {
  return {
    type: RETRIEVED_RECOMMENDED_SEARCHES,
    payload: recommendedSearches,
  }
};

const RETRIEVED_CONTENT_TAGS = 'RETRIEVED_CONTENT_TAGS';
const retrievedContentTags = (contentTags: Array<{ doc_count: number; key: string; }>) => {
  return {
    type: RETRIEVED_CONTENT_TAGS,
    payload: contentTags,
  }
};

/**
 * Gets the first page of search results. This is called whenever the user searches
 * from the search bar.
 */
const getSearchResults = () => {
  return (dispatch: Dispatch, getState: GetState) => {
    const searchText = queryTextSelector(getState());
    if(!searchText || searchText.trim().length <= 0) {
      return;
    }
    dispatch(startedNewSearchRequest());
    return api.search.retrieveSearchResults(searchText, 0, PAGE_SIZE)
      .then((results: IndexedPage<*, ContentItem>) => {
        dispatch(contentsFetchSucceeded(results.index));
        return dispatch(retrievedSearchResults(results));
      })
      .finally(() => {
        dispatch(finishedSearchRequest());
      });
  };
};

/**
 * Gets subsequent search result pages. It's called from pressing the load more button
 * that is displayed at the bottom of the search results. It automatically increases
 * the page count.
 */
const getMoreSearchResults = () => {
  return (dispatch: Dispatch, getState: GetState) => {
    const searchText = queryTextSelector(getState());
    dispatch(startedMoreSearchRequest());
    const pageNumber = currentPageSelector(getState())+1;
    return api.search.retrieveSearchResults(searchText, pageNumber, PAGE_SIZE)
      .then((results: IndexedPage<*, ContentItem>) => {
        dispatch(contentsFetchSucceeded(results.index));
        return dispatch(retrievedSearchResults(results));
      })
      .finally(() => {
        dispatch(finishedSearchRequest());
      });
  };
};

/**
 * Get the recommended search terms that we can suggest that the user tap to fill in the search box.
 * Named as recommended searches, because it may encompass more than just common searches in
 * the next iteration.
 */
const getRecommendedSearches = () => {
  return (dispatch: Dispatch) => {
    // No need to dispatch loading status. Recommended terms will just display when they arrive.
    // If it fails, nbd. Will attempt again on application relaunch.
    return api.search.retrieveCommonSearches()
      .then((commonSearches: string[]) => {
        return dispatch(retrievedRecommendSearches(commonSearches));
      });
  };
};

/**
 * Get the tags used on content within the org sorted by their frequency, to be used as
 * more recommended search terms for the user
 */
const getContentTags = () => {
  return async (dispatch: Dispatch) => {
    const contentTags = await api.search.retrieveContentTags({
      size: 200,
    });
    return dispatch(retrievedContentTags(contentTags.content));
  };
};

export {
  CLEARED_SEARCH,
  RETRIEVED_SEARCH_RESULTS,
  UI_SEARCH_TEXT_UPDATED,
  ADDED_RECENT_SEARCH,
  NEW_SEARCH_REQUEST_STARTED,
  MORE_SEARCH_REQUEST_STARTED,
  RETRIEVED_RECOMMENDED_SEARCHES,
  RETRIEVED_CONTENT_TAGS,
  SEARCH_REQUEST_FINISHED,
  clearSearch,
  getMoreSearchResults,
  getRecommendedSearches,
  getSearchResults,
  getContentTags,
  addRecentSearch,
  uiSearchTextUpdated,
};
