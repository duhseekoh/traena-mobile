// @flow
import {
  createActivityResponseTpl,
  getActivityResponsesTpl,
} from './util/endpoints';
import type { ResponseBody } from 'app/redux/modules/activityResponse/types';

const createActivityResponse = (activityId: number, body: ResponseBody) => {
  return fetch(createActivityResponseTpl(), {
    method: 'POST',
    body: {
      body,
      activityId,
    },
  });
};

const getActivityResponses = (activityId: number) => {
  return fetch(getActivityResponsesTpl({ activityId }));
};

export default {
  createActivityResponse,
  getActivityResponses
}
