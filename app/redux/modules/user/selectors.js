// @flow
import type { ReduxState } from 'app/redux/constants';
import { createSelector } from 'reselect';
import { auth0UserSelector } from '../auth/selectors';

/**
 * Gets 'self'. If the user hasn't been retrieved from self endpoint, then fallback
 * to minimal user data available in auth0 user from idToken.
 */
const userSelector = createSelector(
  [
    (state: ReduxState) => state.user.user,
    auth0UserSelector,
  ],
  (user, auth0User) => {
    if (user) {
      return user;
    } else {
      // fallback to auth0User
      return auth0User ? {
        email: auth0User.email,
        firstName: auth0User.details.firstName,
        lastName: auth0User.details.lastName,
        organizationId: auth0User.details.organizationId,
        id: auth0User.details.traenaId,
      } : {};
    }
  }
);

const selectOrganizationId = (state: ReduxState) => {
  const user = userSelector(state);
  if (user.organization) {
    return user.organization.id;
  }
  return user.organizationId;
};

export { userSelector, selectOrganizationId };
