import { createSelector } from 'reselect';
import moment from 'moment';

const activitySelector = state => state.stats.activity;
const commentsSelector = state => state.stats.comments;
const completedTasksSelector = state => state.stats.completedTasks;
export const activityStreakSelector = state => state.stats.activityStreak;
export const isLoadingSelector = state => state.stats.isLoading;

const startOfCurrentPeriod = moment().startOf('day').subtract(7, 'days');
const startOfPreviousPeriod = moment().startOf('day').subtract(14, 'days');

const currentPeriodFilter = ({ date }) => moment(date).isSameOrAfter(startOfCurrentPeriod);
const previousPeriodFilter = ({ date }) => {
  const mDate = moment(date);
  return mDate.isSameOrAfter(startOfPreviousPeriod) &&
    mDate.isBefore(startOfCurrentPeriod);
};

export const daysActiveSelector = createSelector(
  [activitySelector],
  activity => {
    const currentActivities = activity.filter(currentPeriodFilter);
    return currentActivities.length;
  }
);

export const previousDaysActiveSelector = createSelector(
  [activitySelector],
  activity => {
    const prevActivities = activity.filter(previousPeriodFilter);
    return prevActivities.length;
  }
);

export const currentNumberCompletedTasksSelector = createSelector(
  [completedTasksSelector],
  completedTasks => completedTasks.filter(currentPeriodFilter)
    .reduce((acc, item) => acc + item.count, 0)
);

export const previousNumberCompletedTasksSelector = createSelector(
  [completedTasksSelector],
  completedTasks => completedTasks.filter(previousPeriodFilter)
    .reduce((acc, item) => acc + item.count, 0)
);

export const currentNumberCommentsSelector = createSelector(
  [commentsSelector],
  comments => comments.filter(currentPeriodFilter)
    .reduce((acc, item) => acc + item.count, 0)
);

export const previousNumberCommentsSelector = createSelector(
  [commentsSelector],
  comments => comments.filter(previousPeriodFilter)
    .reduce((acc, item) => acc + item.count, 0)
);

const convertListToMap = (list) =>
  list.reduce((acc, item) => {
    acc[item.date] = item;
    return acc;
  }, {})

const completedTasksMapSelector = createSelector(
  [completedTasksSelector],
  completedTasks => convertListToMap(completedTasks)
);

const commentsMapSelector = createSelector(
  [commentsSelector],
  comments => convertListToMap(comments)
);

const activityMapSelector = createSelector(
  [activitySelector],
  activities => convertListToMap(activities)
);

export const cumulativeChartDataSelector = createSelector(
  [completedTasksMapSelector, commentsMapSelector, activityMapSelector],
  (completedTasksByDate, commentsByDate, activitiesByDate) => {
    // For the past 7 days, create an array of dates and
    // calculate a value from 0-1 that is whether or not a user has:
    // 1. Opened the app
    // 2. Commented on something
    // 3. Completed a post
    // If they've completed all 3 the value should be 1 for that day
    // If they've completed 2 it should be 0.6666667, etc.

    const data = [0,1,2,3,4,5,6].map(i => {
      const mDate = moment().subtract(i, 'days');
      const dateString = mDate.format('YYYY-MM-DD');
      const result = (!!completedTasksByDate[dateString] * 1
      + !!commentsByDate[dateString] * 1
      + !!activitiesByDate[dateString] * 1) / 3;
      return {
        date: mDate.toDate(),
        value: result,
      };
    });
    return data.sort((a, b) => a.date - b.date);
});
