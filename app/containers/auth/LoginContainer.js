// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'app/redux/constants';
import {
  ImageBackground,
  Linking,
  TextInput,
  View,
  StyleSheet,
} from 'react-native';
import {
  ButtonPrimary,
  ButtonSubtle,
  TRAText,
  TraenaIcon,
} from 'app/components';
import { login } from 'app/redux/modules/auth/actions';
import {
  forgotPasswordUrlSelector,
  loginErrorMessageSelector,
  isLoginInProgressSelector
} from 'app/redux/modules/auth/selectors';
import styleVars from 'app/style/variables';

type Props = {
  onSubmitLoginForm: (username: string, password: string) => Promise<*>,
  isLoginInProgress?: boolean,
  loginErrorMessage?: string,
  forgotPasswordUrl: string,
};

type State = {
  loginInputValue: string,
  passwordInputValue: string,
  formErrorMessage: ?string
};

class Login extends React.Component<Props, State> {
  passwordInput: ?TextInput;

  constructor(props: Props) {
    super(props);
    this.state = {
      loginInputValue: '', //'brian@traena.io',
      passwordInputValue: '', //'test123',
      formErrorMessage: null
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    this._setFormError(nextProps.loginErrorMessage);
  }

  render() {
    const { isLoginInProgress } = this.props;

    return (
      <View style={styles.containerView}>
        <View style={styles.heroContainer}>
          <ImageBackground style={styles.heroImage} resizeMode='cover' source={require('../../images/login-gradient.png')}>
            <TraenaIcon name={'traena-text'} style={styles.heroIcon} />
          </ImageBackground>
        </View>
        <View style={styles.loginFormWrapper}>
          <TextInput
            editable={!isLoginInProgress}
            style={styles.textInput}
            placeholder={'EMAIL'}
            placeholderTextColor={styleVars.color.lightGrey3}
            keyboardType={'email-address'}
            returnKeyType={'next'}
            autoCorrect={false}
            autoCapitalize={'none'}
            autoFocus={true}
            underlineColorAndroid='transparent'
            value={this.state.loginInputValue}
            onChange={(event) => {
              this.setState({loginInputValue: event.nativeEvent.text.trim()});
            }}
            onSubmitEditing={() => this.passwordInput && this.passwordInput.focus()}
          />
          <TextInput
            ref={ ref => this.passwordInput = ref}
            editable={!isLoginInProgress}
            style={styles.textInput}
            placeholder={'PASSWORD'}
            placeholderTextColor={styleVars.color.lightGrey3}
            secureTextEntry={true}
            returnKeyType={'go'}
            underlineColorAndroid='transparent'
            value={this.state.passwordInputValue}
            onChange={(event) => {
              this.setState({passwordInputValue: event.nativeEvent.text});
            }}
            onSubmitEditing={() => {
              this._submitLoginForm();
            }}
          />
          {
            this.state.formErrorMessage &&
            <TRAText style={styles.formError}>{this.state.formErrorMessage}</TRAText>
          }
          {this._renderLoginButton()}

          <ButtonSubtle
            onPress={this._onPressForgotPassword}
            style={styles.helperLink}
            title={'Forgot password?'} />
        </View>
      </View>
    );
  }

  _renderLoginButton = () => {
    const { isLoginInProgress } = this.props;

    let buttonText = 'LOGIN',
      buttonDisabled = false;

    if(isLoginInProgress) {
      buttonText = 'LOGGING IN...';
      buttonDisabled = true;
    }

    return (
      <ButtonPrimary
        style={styles.loginButton}
        title={buttonText}
        disabled={buttonDisabled}
        onPress={() => this._submitLoginForm()}
      />
    )
  }

  _setFormError = (errorMessage) => {
    this.setState({
      formErrorMessage: errorMessage
    });
  }

  _clearFormError = () => {
    this._setFormError(null);
  }

  _submitLoginForm = () => {
    this._clearFormError();
    if(!this.state.loginInputValue || !this.state.passwordInputValue) {
      this._setFormError('Please enter your username and password.');
    } else {
      this.props.onSubmitLoginForm(this.state.loginInputValue, this.state.passwordInputValue);
    }
  }

  _onPressForgotPassword = () => {
    Linking.openURL(this.props.forgotPasswordUrl);
  }
}

function mapStateToProps(state) {
  return {
    isLoginInProgress: isLoginInProgressSelector(state),
    loginErrorMessage: loginErrorMessageSelector(state),
    forgotPasswordUrl: forgotPasswordUrlSelector(),
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onSubmitLoginForm: function(username, password) {
      return dispatch(login(username, password));
    }
  };
}

export { Login }; // for testing
const ConnectedLoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

export default ConnectedLoginContainer;

const styles = StyleSheet.create({
  containerView: {
    flex: 1,
  },
  loginFormWrapper: {
    flex: 1,
    marginTop: 50,
    // justifyContent: 'center',
    paddingHorizontal: 50
  },
  formError: {
    color: styleVars.color.formError,
  },
  textInput: {
    height: 40,
    borderWidth: 1,
    borderColor: styleVars.color.lightGrey3,
    color: styleVars.color.black,
    paddingHorizontal: 10,
    fontSize: styleVars.fontSize.medium,
    marginBottom: 10,
  },
  loginButton: {
    marginTop: 10,
    marginBottom: 40
  },
  helperLink: {
    marginBottom: 10
  },
  heroContainer: {
    // justifyContent: 'flex-start',
  },
  heroImage: {
    width: null,
    height: null,
    paddingVertical: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  heroIcon: {
    backgroundColor: styleVars.color.transparent,
    fontSize: 100,
    color: styleVars.color.white
  }
});
