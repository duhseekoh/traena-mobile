import logger from 'app/logging/logger';

//TODO: We should have these as codes and throw the original response status and message
const errors = {
  // Not currently in use
  CALLED_WHILE_OFFLINE: 'CALLED_WHILE_OFFLINE',
  // for bad login creds, expired JWT, invalid JWT, tampered JWT
  BAD_CREDENTIALS: 'BAD_CREDENTIALS',
  UNKNOWN: 'UNKNOWN'
};

const httpStatusCodeChecker = (response) => {
  if(response.status >= 200 && response.status <= 299) {
    return response;
  } else if(response.status === 401) {
    throw errors.BAD_CREDENTIALS;
  } else {
    throw errors.UNKNOWN;
  }
};

const fetchCatch = (apiError) => {
  logger.logError(new Error(apiError));
  if(errors[apiError]) {
    throw errors[apiError]; // something earlier in the chain defined the error. probably httpStatusCodeChecker
  } else {
    throw errors.UNKNOWN;
  }
};

export {
  errors,
  fetchCatch,
  httpStatusCodeChecker
}
