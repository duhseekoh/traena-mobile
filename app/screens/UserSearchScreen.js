// @flow
import React, { Component } from 'react';
import type {
  NavigationNavigatorProps,
  NavigationStackScreenOptions,
} from 'react-navigation';
import UserSearchContainer from 'app/containers/user-search/UserSearchContainer';

type NavigationStateParams = {
  params: {
    onSearchResultSelected: () => void,
  },
} & *;

type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

class UserSearchScreen extends Component<NavigationProps> {
  static navigationOptions: (*) => NavigationStackScreenOptions = () => ({
    headerTitle: 'FIND USER',
  });

  render() {
    const { params } = this.props.navigation.state;
    const { onSearchResultSelected } = params;
    return (
      <UserSearchContainer onSearchResultSelected={onSearchResultSelected} />
    );
  }
}

export default UserSearchScreen;
