// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import type {
  NavigationNavigatorProps,
  NavigationStackScreenOptions,
} from 'react-navigation';
import {
  View,
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import {
  ContentList,
  ButtonSegmented,
  InfoStandaloneButton,
} from 'app/components';
import SeriesPreviewList from 'app/components/series-preview-list/SeriesPreviewList';
import {
  contentSelector,
  isLoadingMoreContentSelector,
  isRefreshingContentSelector,
  // hasErrorContentSelector,
  canLoadMoreContentSelector,
  displayNoMoreContentResultsSelector,
  isLoadingMoreSeriesSelector,
  isRefreshingSeriesSelector,
  // hasErrorSeriesSelector,
  canLoadMoreSeriesSelector,
  seriesListWithContentSelector,
  displayNoMoreSeriesResultsSelector } from 'app/redux/modules/ui-channel-standalone/selectors';
import { refreshChannelContent, loadMoreChannelContent, refreshSeriesList, loadMoreSeries } from 'app/redux/modules/ui-channel-standalone/actions';
import type { ContentItem } from 'app/redux/modules/content/types';
import type { Series } from 'app/redux/modules/series/types';
import type { ReduxState } from 'app/redux/constants';
import styleVars from 'app/style/variables';

const TAB_IDS = {
  POSTS: 'POSTS',
  SERIES: 'SERIES',
};

type NavigationStateParams = {
  params: {|
    title: string,
    description: string,
    channelId: number,
  |},
}

type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

type Props = {
  content: ContentItem[],
  refreshChannelContent: (channelId: number) => any,
  loadMoreChannelContent: (channelId: number) => any,
  refreshSeriesList: (channelId: number) => any,
  loadMoreSeries: (channelId: number) => any,
  isLoadingMoreContent: boolean,
  isRefreshingContent: boolean,
  canLoadMoreContent: boolean,
  displayNoMoreContentResults: boolean,
  isLoadingMoreSeries: boolean,
  isRefreshingSeries: boolean,
  canLoadMoreSeries: boolean,
  displayNoMoreSeriesResults: boolean,
  seriesListWithContent: {series: Series, previewContent: ContentItem[]}[],
} & NavigationProps;

type State = {
  selectedTab: string,
}

class ChannelScreen extends Component<Props, State> {

  /**
  * Adding this static method to the component allows us to use
  * dynamic titles.  For more info https://reactnavigation.org/docs/headers.html#using-params-in-the-title
  */
  static navigationOptions = (
    { navigation }: NavigationProps
  ): NavigationStackScreenOptions => {
    const { params } = navigation.state;
    return {
      headerTitle: params ? params.title : 'CHANNEL',
      headerRight: (
        <InfoStandaloneButton {...params} />
      ),
    }
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      selectedTab: TAB_IDS.POSTS,
    }
  }

  componentWillMount() {
    const { refreshChannelContent, refreshSeriesList } = this.props;
    const { params } = this.props.navigation.state;
    const { channelId } = params;
    refreshChannelContent(channelId);
    refreshSeriesList(channelId);
  }

  render() {
    const { selectedTab } = this.state;
    const { content, refreshChannelContent, isRefreshingContent = false, isRefreshingSeries = false, loadMoreChannelContent, isLoadingMoreContent, canLoadMoreContent, displayNoMoreContentResults,
      isLoadingMoreSeries, canLoadMoreSeries, displayNoMoreSeriesResults, seriesListWithContent, refreshSeriesList, loadMoreSeries } = this.props;
    const { params } = this.props.navigation.state;
    const { channelId } = params;
    const isPostsTabSelected = selectedTab === TAB_IDS.POSTS;
    const isSeriesTabSelected = selectedTab === TAB_IDS.SERIES;

    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.tabBar}>
          <ButtonSegmented
            isSelected={isPostsTabSelected}
            title='ALL POSTS'
            onPress={() => this.setState({selectedTab: TAB_IDS.POSTS})} />
          <ButtonSegmented
            isSelected={isSeriesTabSelected}
            title='SERIES'
            onPress={() => this.setState({selectedTab: TAB_IDS.SERIES})} />
        </View>
        { selectedTab === TAB_IDS.POSTS &&
          <View style={{flex: 1}}>
            <ContentList
              refreshContent={() => refreshChannelContent(channelId)}
              isRefreshing={isRefreshingContent}
              contentItems={content}
              loadNextPage={() => loadMoreChannelContent(channelId)}
              isLoadingMore={isLoadingMoreContent}
              canLoadMore={canLoadMoreContent}
              displayNoMoreResults={displayNoMoreContentResults}
              refreshIconOffset={0} />
          </View>
        }
        { selectedTab === TAB_IDS.SERIES &&
          <View style={{flex: 1}}>
            <SeriesPreviewList
              refreshSeriesList={() => refreshSeriesList(channelId)}
              isRefreshing={isRefreshingSeries}
              seriesListWithContent={seriesListWithContent}
              loadNextPage={() => loadMoreSeries(channelId)}
              isLoadingMore={isLoadingMoreSeries}
              canLoadMore={canLoadMoreSeries}
              displayNoMoreResults={displayNoMoreSeriesResults}
              refreshIconOffset={0} />
          </View>
        }
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state: ReduxState, props: Props) => {
  const { params } = props.navigation.state;
  const selectorProps = { channelId: params.channelId };
  return {
    content: contentSelector(state, selectorProps),
    seriesListWithContent: seriesListWithContentSelector(state, selectorProps),
    isRefreshingContent: isRefreshingContentSelector(state, selectorProps),
    isLoadingMoreContent: isLoadingMoreContentSelector(state, selectorProps),
    canLoadMoreContent: canLoadMoreContentSelector(state, selectorProps),
    displayNoMoreContentResults: displayNoMoreContentResultsSelector(state, selectorProps),
    isRefreshingSeries: isRefreshingSeriesSelector(state, selectorProps),
    isLoadingMoreSeries: isLoadingMoreSeriesSelector(state, selectorProps),
    canLoadMoreSeries: canLoadMoreSeriesSelector(state, selectorProps),
    displayNoMoreSeriesResults: displayNoMoreSeriesResultsSelector(state, selectorProps),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    refreshChannelContent: (channelId) => {
      return dispatch(refreshChannelContent(channelId));
    },
    loadMoreChannelContent: (channelId) => {
      return dispatch(loadMoreChannelContent(channelId));
    },
    refreshSeriesList: (channelId) => {
      return dispatch(refreshSeriesList(channelId));
    },
    loadMoreSeries: (channelId) => {
      return dispatch(loadMoreSeries(channelId));
    },
  };
};

export { ChannelScreen };
const ConnectedChannelScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(ChannelScreen);
export default ConnectedChannelScreen;

const styles = StyleSheet.create({
  tabBar: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: styleVars.color.lightGrey3,
    backgroundColor: styleVars.color.white,
  },
});
