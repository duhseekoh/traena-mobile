// @flow
import api from 'app/redux/api';
import { contentsFetchSucceeded } from 'app/redux/modules/content/actions';

export const LOAD_BOOKMARKS_WIDGET_REQUEST_STARTED = 'LOAD_BOOKMARKS_WIDGET_REQUEST_STARTED';
const startedLoadBookmarksWidgetRequest = () => {
  return {
    type: LOAD_BOOKMARKS_WIDGET_REQUEST_STARTED,
  };
};

export const LOAD_BOOKMARKS_WIDGET_REQUEST_FINISHED = 'LOAD_BOOKMARKS_WIDGET_REQUEST_FINISHED';
const finishedLoadBookmarksWidgetRequest = (contentIds: number[]) => {
  return {
    type: LOAD_BOOKMARKS_WIDGET_REQUEST_FINISHED,
    payload: contentIds,
  };
};

export function initialize() {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(startedLoadBookmarksWidgetRequest());
      const results = await api.bookmark.retrieveBookmarkContent();
      dispatch(contentsFetchSucceeded(results.index));
      dispatch(finishedLoadBookmarksWidgetRequest(results.ids));
    } catch (err) {
      console.log('uiBookmarksWidget.initialize error: ', err);
      dispatch(finishedLoadBookmarksWidgetRequest([]));
    }
  }
}
