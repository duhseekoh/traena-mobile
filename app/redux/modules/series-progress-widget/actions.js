// @flow
import { getSubscribedSeries, getAllSeriesProgress } from 'app/redux/modules/series/actions';

export const LOAD_SERIES_PROGRESS_WIDGET_REQUEST_STARTED = 'LOAD_SERIES_PROGRESS_WIDGET_REQUEST_STARTED';
const startedLoadSeriesProgressWidgetRequest = () => {
  return {
    type: LOAD_SERIES_PROGRESS_WIDGET_REQUEST_STARTED,
  };
};

export const LOAD_SERIES_PROGRESS_WIDGET_REQUEST_FINISHED = 'LOAD_SERIES_PROGRESS_WIDGET_REQUEST_FINISHED';
const finishedLoadSeriesProgressWidgetRequest = (seriesIds?: number[]) => {
  return {
    type: LOAD_SERIES_PROGRESS_WIDGET_REQUEST_FINISHED,
    payload: seriesIds || [],
  };
};

export function initialize() {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(startedLoadSeriesProgressWidgetRequest());
      const getSeriesOptions = {
        page: 0,
        size: 7,
        sort: 'latestProgressTimestamp:desc,createdTimestamp:desc',
        filterProgressStatus: 'IN_PROGRESS,NOT_STARTED',
      };
      const initMethods = [
        dispatch(getSubscribedSeries(getSeriesOptions)),
        dispatch(getAllSeriesProgress()),
      ];
      const result = await Promise.all(initMethods);
      const seriesIds = result[0] ? result[0].ids : [];
      dispatch(finishedLoadSeriesProgressWidgetRequest(seriesIds));
    } catch (err) {
      dispatch(finishedLoadSeriesProgressWidgetRequest());
    }
  }
}
