// @flow

type QuestionSetUIData = {
  correctFirstTry: boolean[],
  currentQuestionIndex: number,
};

// type ActivityUIData = generic things all uis need;

export type { QuestionSetUIData } ;
