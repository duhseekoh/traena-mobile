// @flow
import _ from 'lodash';
import {
  RETRIEVED_BOOKMARK_IDS,
  ADDED_BOOKMARK,
  REMOVED_BOOKMARK,
} from './actions';
import type { Action } from 'app/types/Action';

export type ReduxStateBookmark = {
  bookmarkContentIds: number[],
};

const ACTION_HANDLERS = {
  [ADDED_BOOKMARK]: (state: ReduxStateBookmark, action: Action) => {
    const id = action.payload;
    const bookmarkIds = _.uniq([...state.bookmarkContentIds, id]);
    return {
      ...state,
      bookmarkContentIds: bookmarkIds,
    };
  },
  [REMOVED_BOOKMARK]: (state: ReduxStateBookmark, action: Action) => {
    const id = action.payload;
    const bookmarkIds = [...state.bookmarkContentIds];
    _.remove(bookmarkIds, bId => bId === id);
    return {
      ...state,
      bookmarkContentIds: bookmarkIds,
    };
  },
  [RETRIEVED_BOOKMARK_IDS]: (state: ReduxStateBookmark, action: Action) => {
    return {
      ...state,
      bookmarkContentIds: action.payload,
    };
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  bookmarkContentIds: [],
};

const bookmarkReducer = (state: ReduxStateBookmark = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

export default bookmarkReducer;
