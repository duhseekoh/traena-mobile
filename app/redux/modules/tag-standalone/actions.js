// @flow
import api from 'app/redux/api';
import { contentsFetchSucceeded } from 'app/redux/modules/content/actions';
import { currentPageSelector } from './selectors';
import type { IndexedPage } from 'app/types/Pagination';
import type { ContentItem } from 'app/redux/modules/content/types';
import type { Dispatch, GetState } from 'app/redux/constants';

export const REFRESH_TAG_CONTENT_REQUEST_STARTED = 'REFRESH_TAG_CONTENT_REQUEST_STARTED';
const startedRefreshTagContentRequest = (tag: string) => {
  return {
    type: REFRESH_TAG_CONTENT_REQUEST_STARTED,
    payload: tag,
  };
};

export const REFRESH_TAG_CONTENT_REQUEST_FAILED = 'REFRESH_TAG_CONTENT_REQUEST_FAILED';
const failedRefreshTagContentRequest = (tag: string) => {
  return {
    type: REFRESH_TAG_CONTENT_REQUEST_FAILED,
    payload: tag,
  };
};

export const LOAD_MORE_TAG_CONTENT_REQUEST_STARTED = 'LOAD_MORE_TAG_CONTENT_REQUEST_STARTED';
const startedLoadMoreTagContentRequest = (tag: string) => {
  return {
    type: LOAD_MORE_TAG_CONTENT_REQUEST_STARTED,
    payload: tag,
  };
};

export const LOAD_MORE_TAG_CONTENT_REQUEST_FAILED = 'LOAD_MORE_TAG_CONTENT_REQUEST_FAILED';
const failedLoadMoreTagContentRequest = (tag: string) => {
  return {
    type: LOAD_MORE_TAG_CONTENT_REQUEST_FAILED,
    payload: tag,
  };
};

export const LOADED_MORE_TAG_CONTENT = 'LOADED_MORE_TAG_CONTENT';
function loadedMoreTagContent(content: IndexedPage<*, ContentItem>, tag: string) {
  return {
    type: LOADED_MORE_TAG_CONTENT,
    payload: { ...content, tag }
  };
}

export const REFRESHED_TAG_CONTENT = 'REFRESHED_TAG_CONTENT';
function refreshedTagContent(content: IndexedPage<*, ContentItem>, tag: string) {
  return {
    type: REFRESHED_TAG_CONTENT,
    payload: { ...content, tag }
  };
}

export const refreshTagContent = (tag: string) => {
  return async (dispatch: Dispatch) => {
    dispatch(startedRefreshTagContentRequest(tag));
    try {
      const results: IndexedPage<*, ContentItem> = await api.search.retrieveTagContent(tag);
      dispatch(contentsFetchSucceeded(results.index));
      return dispatch(refreshedTagContent(results, tag));
    } catch(error) {
      dispatch(failedRefreshTagContentRequest(tag));
      console.log(error); // fetchCatch will notify bugsnag, so just extra dev log here
    }
  };
}

export const loadMoreTagContent = (tag: string) => {
  return async (dispatch: Dispatch, getState: GetState) => {
    dispatch(startedLoadMoreTagContentRequest(tag));
    const page = currentPageSelector(getState(), { tag }) + 1;
    try {
      const results: IndexedPage<*, ContentItem> = await api.search.retrieveTagContent(tag, page);
      dispatch(contentsFetchSucceeded(results.index));
      return dispatch(loadedMoreTagContent(results, tag));
    } catch(error) {
      console.log(error); // fetchCatch will notify bugsnag, so just extra dev log here
      dispatch(failedLoadMoreTagContentRequest(tag));
    }
  };
};
