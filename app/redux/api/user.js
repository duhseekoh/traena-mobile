// @flow
import { getSelf, getChannelsTpl } from './util/endpoints';

export function fetchSelf() {
  return fetch(getSelf);
}

const retrieveChannels = (page?: number = 0, size: number = 100) => {
  return fetch(getChannelsTpl({ page, size }));
};

export default {
  retrieveChannels,
}
