// @flow
import React from 'react';
import { connect } from 'react-redux';
import {
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import ActionSheet from 'react-native-actionsheet';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import moment from 'moment';
import {
  TimeAgo,
  TRAText,
  TRATextStrong,
  UserAvatar,
  UserFullName,
  FeatherIcon,
} from 'app/components';
import styleVars from 'app/style/variables';
import { userSelector } from 'app/redux/modules/user/selectors';
import { deleteComment } from 'app/redux/modules/comment/actions';
import type { Comment } from 'app/redux/modules/comment/types';
import type { User } from 'app/redux/modules/user/types';

type OwnProps = {
  style: ViewStyleProp,
  comment: Comment,
};

type Props = OwnProps & ConnectedProps;

class CommentCmp extends React.Component<Props> {
  actionSheetRef: ActionSheet;
  props: Props; // when no constructor, this satisfies eslint

  _menuButtonPressed = () => {
    this.actionSheetRef.show();
  }

  _actionSheetButtonPressed = (buttonIndex) => {
    const { comment, deleteComment } = this.props;
    if (buttonIndex === 0) {
      deleteComment(comment.contentId, comment.id);
    }
  }

  render() {
    const { style, comment, user } = this.props;
    const { author, taggedUsers, createdTimestamp } = comment;
    const createdAtMoment = moment(createdTimestamp);
    const hasTaggedUsers = taggedUsers && taggedUsers.length > 0;
    const ownComment = user.id === author.id;
    return (
      <View style={[styles.container, style]}>
        <View>
          <UserAvatar size={'small'} user={author} />
        </View>
        <View style={styles.right}>
          <View style={styles.nameWrapper}>
            <TRATextStrong>
              <UserFullName style={styles.name} user={author} />
            </TRATextStrong>
            <TimeAgo style={styles.time} time={createdAtMoment} />
            {ownComment &&
              <TouchableOpacity onPress={this._menuButtonPressed} style={styles.menuButton}
                hitSlop={{top: 20, left: 20, right: 20, bottom: 20}}>
                <FeatherIcon name='more-horizontal' />
              </TouchableOpacity>
            }
          </View>

          <TRAText style={styles.bodyText}>{comment.text}</TRAText>
          { hasTaggedUsers &&
            <View style={styles.taggedUsersContainer}>
              {taggedUsers && taggedUsers.map((user) => { // must please flow with &&
                return (
                  <View key={user.id} style={styles.taggedUserBubble}>
                    <UserFullName style={styles.taggedUserText} user={user} />
                  </View>
                )
              })}
            </View>
          }
        </View>
        <ActionSheet
          ref={o => this.actionSheetRef = o}
          title={'Delete this comment'}
          message={comment.text.substring(0, 140)}
          options={['Delete', 'Cancel']}
          cancelButtonIndex={1}
          destructiveButtonIndex={0}
          onPress={this._actionSheetButtonPressed}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  right: {
    flex: 1,
    marginTop: -3,
    paddingLeft: 5,
  },
  nameWrapper: {
    flexDirection: 'row',
  },
  name: {
    fontSize: styleVars.fontSize.medium,
    fontFamily: styleVars.fontFamily.medium,
  },
  time: {
    marginLeft: 8,
  },
  menuButton: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  bodyText: {
    color: styleVars.color.darkGrey,
    fontSize: styleVars.fontSize.medium,
    marginTop: 5,
  },
  taggedUsersContainer: {
    marginTop: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  taggedUserBubble: {
    paddingHorizontal: 3,
    paddingVertical: 2,
    marginHorizontal: 2,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: styleVars.color.lightGrey,
    backgroundColor: styleVars.color.white,
    marginBottom: 5,
  },
  taggedUserText: {
    color: styleVars.color.mediumGrey,
    fontSize: styleVars.fontSize.small,
    textAlign: 'left'
  },
});

type ConnectedProps = {
  user: User,
  deleteComment: (contentId: number, commentId: number) => Promise<*>,
};

const mapStateToProps = (state) => ({
  user: userSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  deleteComment: (contentId: number, commentId: number) => dispatch(deleteComment(contentId, commentId))
});

export default connect(mapStateToProps, mapDispatchToProps)(CommentCmp);
