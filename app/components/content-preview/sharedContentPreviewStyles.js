import {
  StyleSheet
} from 'react-native';
import styleVars from 'app/style/variables';

const styles = StyleSheet.create({
  itemContainer: {
    borderWidth: 1,
    borderColor: styleVars.color.lightGrey3,
    backgroundColor: styleVars.color.white
  },
  content: {
    minHeight: 115,
    padding: 10
  },
  titleHolder: {
    // flex: 1,
    marginBottom: 10,
  },
  titleText: {
    fontSize: styleVars.fontSize.large,
    color: styleVars.color.black
  },
  callToAction: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    backgroundColor: styleVars.color.darkSlateGrey,
    marginLeft: 10,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  callToActionIcon: {
    fontSize: styleVars.iconSize.large,
    color: styleVars.color.white,
  },
  topRight: {
    position: 'absolute',
    top: 10,
    right: 8,
    flexDirection: 'row'
  },
  statusBarIcon: {
    marginLeft: 5
  },
});

export default styles;
