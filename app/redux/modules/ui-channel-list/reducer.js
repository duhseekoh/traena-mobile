// @flow
import _ from 'lodash';
import {
  REFRESH_CHANNEL_LIST_REQUEST_STARTED,
  REFRESH_CHANNEL_LIST_REQUEST_FAILED,
  LOAD_MORE_CHANNEL_LIST_REQUEST_STARTED,
  LOAD_MORE_CHANNEL_LIST_REQUEST_FAILED,
  REFRESHED_CHANNEL_LIST,
  LOADED_MORE_CHANNEL_LIST,
} from './actions';
import { REQUEST_STATUSES, type RequestStatus } from 'app/redux/constants';

export type ReduxStateChannelList = {
  channelIds: number[],
  loadingStatus: ?RequestStatus,
  refreshingStatus: ?RequestStatus,
  currentPage: number,
  canLoadMore: boolean,
};

const ACTION_HANDLERS = {
  [REFRESHED_CHANNEL_LIST]: (state, action): ReduxStateChannelList => {
    const { channelPage } = action.payload;
    const { ids, totalPages, number } = channelPage;
    return {
      ...state,
      channelIds: ids,
      canLoadMore: number < totalPages - 1,
      currentPage: number,
      loadingStatus: REQUEST_STATUSES.COMPLETE,
      refreshingStatus: REQUEST_STATUSES.COMPLETE,
    };
  },
  [LOADED_MORE_CHANNEL_LIST]: (state, action): ReduxStateChannelList => {
    const { channelPage } = action.payload;
    const { ids, totalPages, number } = channelPage;
    const mergedChannelIds = _.uniq([...state.channelIds, ...ids]);
    return {
      ...state,
      channelIds: mergedChannelIds,
      canLoadMore: number < totalPages - 1,
      currentPage: number,
      loadingStatus: REQUEST_STATUSES.COMPLETE,
      refreshingStatus: REQUEST_STATUSES.COMPLETE,
    };
  },
  [REFRESH_CHANNEL_LIST_REQUEST_STARTED]: (state): ReduxStateChannelList => {
    return {
      ...state,
      refreshingStatus: REQUEST_STATUSES.IN_PROGRESS,
    };
  },
  [REFRESH_CHANNEL_LIST_REQUEST_FAILED]: (state): ReduxStateChannelList => {
    return {
      ...state,
      refreshingStatus: REQUEST_STATUSES.ERROR,
    };
  },
  [LOAD_MORE_CHANNEL_LIST_REQUEST_STARTED]: (
    state,
  ): ReduxStateChannelList => {
    return {
      ...state,
      loadingStatus: REQUEST_STATUSES.IN_PROGRESS,
    };
  },
  [LOAD_MORE_CHANNEL_LIST_REQUEST_FAILED]: (
    state,
  ): ReduxStateChannelList => {
    return {
      ...state,
      loadingStatus: REQUEST_STATUSES.ERROR,
    };
  },
};

const initialState: ReduxStateChannelList = {
  channelIds: [],
  loadingStatus: null,
  refreshingStatus: null,
  currentPage: 0,
  canLoadMore: false,
};

const channelListReducer = (
  state: ReduxStateChannelList = initialState,
  action: { type: string, payload: any },
) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default channelListReducer;
