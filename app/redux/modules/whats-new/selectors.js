
import { createSelector } from 'reselect';
import { contentItemsSelectorFactory } from 'app/redux/modules/content/selectors';

const STATE_KEY = 'whatsNew';
const stateFromRoot = (state) => state[STATE_KEY];

export const isLoadingSelector = (state) => stateFromRoot(state).isLoading;

export const whatsNewContentIdsSelector = createSelector([
  stateFromRoot
], (whatsNewState) => {
  return whatsNewState.contentIds;
});

export const whatsNewSelector = contentItemsSelectorFactory(whatsNewContentIdsSelector);
