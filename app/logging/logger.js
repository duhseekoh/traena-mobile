// @flow
import { Client, Configuration } from 'bugsnag-react-native'; // eslint-disable-line import/named
import config from 'app/config';

class Logger {
  bugsnagClient: Client;

  constructor({ bugsnagClient }: { bugsnagClient: Client}) {
    this.bugsnagClient = bugsnagClient;
  }

  setupUser(id: number, email: string) {
    this.bugsnagClient.setUser(id + '', email, email);
  }

  // type must be: error, log, navigation, process, request, state, user, manual (default)
  logBreadcrumb(message: string, metadata: Object = {}, type: string = 'manual') {
    if (__DEV__) {
      console.log(`crumb: ${type} - ${message}`, metadata);
    }
    this.bugsnagClient.leaveBreadcrumb(message, {...metadata}); //Bugsnag mutates the metadata being passed in
  }

  log(message: string) {
    if (__DEV__) {
      console.log(message);
    }
  }

  logError(error: Error) {
    if (__DEV__) {
      console.log('--LOGGING BUGSNAG ERROR--', error);
    }
    this.bugsnagClient.notify(error);
  }
}

const bugsnagConfig = __DEV__ ? new Configuration('dummy-key')
  : new Configuration(config.bugsnag.apiKey);
bugsnagConfig.releaseStage = config.stageName;
bugsnagConfig.codeBundleId = config.packageJson.version;
const bugsnagClient = new Client(bugsnagConfig);
const logger = new Logger({ bugsnagClient });

export default logger;
