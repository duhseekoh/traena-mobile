// @flow

type TraenaTask = {
  id: number,
  userId: number,
  contentId: number,
  completedTimestamp: ?string,
  createdTimestamp: string,
  modifiedTimestamp: string,
  status: string,
  rating: ?number,
};

export type {
  TraenaTask,
};
