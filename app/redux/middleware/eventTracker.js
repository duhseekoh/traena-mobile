//@flow
import logger from 'app/logging/logger';
import type { Event } from './eventFactory';

/**
 * Pulls the event metadata off of a redux action and calls the supplied track
 * method with it.
 */
function eventTrackerMiddleware(
  track: Event => any, // supply method that makes remote call to send event to tracking service
) {
  return () => (next: Function) => (action: Object) => {
    const result = next(action);
    if (!action || !action.meta || !action.meta.event) {
      return result;
    }
    const event: Event = action.meta.event;

    logger.logBreadcrumb('Tracking event: ', event);
    track(event); //Side effect- this will actually track the event
    return result;
  };
}
export default eventTrackerMiddleware;
