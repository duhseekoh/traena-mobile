// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { TRATextStrong, TRAText, FeatherIcon } from 'app/components';
import { initialize } from 'app/redux/modules/ui-series-content-position/actions';
import {
  isLoadingSelector,
  selectNextSeriesContentId,
  selectPrevSeriesContentId,
  selectSeriesContentPosition,
  selectSeriesLength,
} from 'app/redux/modules/ui-series-content-position/selectors';
import { goToContentStandalone } from 'app/redux/modules/navigation/actions';
import styleVars from 'app/style/variables';
import type { ContentItem } from 'app/redux/modules/content/types';
import type { Series } from 'app/redux/modules/series/types';

type OwnProps = {
  series: Series,
  content: ContentItem,
};

type ConnectedProps = {
  initialize: (seriesId: number) => any,
  goToContentStandalone: (contentId: number) => any,
  nextSeriesContentId: number,
  prevSeriesContentId: number,
  seriesLength: number,
  contentPosition: number,
  isLoading: boolean,
};

type Props = OwnProps & ConnectedProps;

class SeriesContentPositionContainer extends Component<Props> {
  componentDidMount() {
    this.props.initialize(this.props.series.id);
  }

  render() {
    const {
      isLoading,
      content,
      contentPosition,
      nextSeriesContentId,
      prevSeriesContentId,
      seriesLength,
      series,
      goToContentStandalone,
    } = this.props;

    if (isLoading) {
      return (
        <View style={styles.loaderContainer}>
          <ActivityIndicator
            size="small"
            color={styleVars.color.traenaPrimaryGreen}
          />
        </View>
      )
    }


    if (!content) {
      return false;
    }

    return (
      <View style={styles.container}>
        <View style={styles.arrowHolder}>
          { prevSeriesContentId &&
            <TouchableOpacity
              onPress={() => goToContentStandalone(prevSeriesContentId)}>
              <FeatherIcon
                style={styles.prevButtonIcon}
                name="arrow-left" fontSize={styleVars.iconSize.xlarge} />
            </TouchableOpacity>
          }
        </View>
        <View style={styles.seriesTitleWrapper}>
          <TRATextStrong style={styles.title} numberOfLines={1} ellipsizeMode={'tail'}>
            {series.title}
          </TRATextStrong>
          <TRAText>
            {contentPosition} / {seriesLength}
          </TRAText>
        </View>
        <View style={styles.arrowHolder}>
          { nextSeriesContentId &&
            <TouchableOpacity
              onPress={() => goToContentStandalone(nextSeriesContentId)}>
              <FeatherIcon
                style={styles.nextButtonIcon}
                name="arrow-right" fontSize={styleVars.iconSize.xlarge} />
            </TouchableOpacity>
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: styleVars.padding.edgeOfScreen,
    backgroundColor: styleVars.color.white,
    ...styleVars.shadow.shadowFour,
    paddingVertical: 10,
  },
  loaderContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: styleVars.padding.edgeOfScreen,
    backgroundColor: styleVars.color.white,
    ...styleVars.shadow.shadowFour,
    paddingVertical: 10,
  },
  seriesTitleWrapper: {
    flexDirection: 'column',
    alignItems: 'center',
    flex: 4,
  },
  title: {
    fontSize: styleVars.fontSize.medlar,
    maxWidth: 230,
  },
  arrowHolder: {
    flex: 1,
  },
  prevButtonIcon: {
    textAlign: 'left',
  },
  nextButtonIcon: {
    textAlign: 'right',
  },
});

const mapStateToProps = (state, props) => {
  const ids = {
    contentId: props.content.id,
    seriesId: props.series.id,
  };
  return {
    isLoading: isLoadingSelector(state),
    nextSeriesContentId: selectNextSeriesContentId(state, ids),
    prevSeriesContentId: selectPrevSeriesContentId(state, ids),
    contentPosition: selectSeriesContentPosition(state, ids),
    seriesLength: selectSeriesLength(state, { seriesId: props.series.id }),
  }
};

const mapDispatchToProps = (dispatch, props) => ({
  initialize: () => dispatch(initialize(props.series.id)),
  goToContentStandalone: (contentId: number) => dispatch(goToContentStandalone(contentId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SeriesContentPositionContainer);
