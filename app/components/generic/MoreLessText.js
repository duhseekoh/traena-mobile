// @flow
import React from 'react';
import {
  StyleSheet
} from 'react-native';
import styleVars from 'app/style/variables';
import { TRAText } from 'app/components';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style: ViewStyleProp,
  text: string,
};

/**
 * Styled text which is mainly meant for being the 'More...' and 'Less'
 * buttons that live at the end of expandable or navigable content.
 * @param {object} style
 * @param {string} text  display
 */
const MoreLessText = ({style, text}: Props) => {
  return (
    <TRAText style={[style, styles.text]}>{text}</TRAText>
  )
}

const styles = StyleSheet.create({
  text: {
    textAlign: 'center',
    color: styleVars.color.lightGrey2,
    marginTop: 10,
    marginBottom: 10
  }
});

export default MoreLessText;
