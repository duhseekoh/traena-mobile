// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import {
  TRAText,
  FeatherIcon,
} from 'app/components';
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type StreakProps = {
  streak: number,
};

function Streak(props: StreakProps) {
  return (
    <View style={styles.detailContainer}>
      <TRAText style={[styles.subline, styles.streak]}>{props.streak} day streak!</TRAText>
    </View>
  );
}

type AnalyticsProps = {
  style?: ViewStyleProp,
  title?: string,
  icon?: string,
  value?: number,
  delta?: number,
  streak?: number,
};

function AnalyticsPanel(props: AnalyticsProps) {
  return (
    <View style={[props.style, styles.container]}>
      <View style={styles.iconContainer}>
        <FeatherIcon
          name={props.icon}
          style={styles.icon}
          fontSize={styleVars.fontSize.xlarge}
        />
      </View>
      <View style={styles.titleContainer}>
        <TRAText style={styles.title}>{props.title}</TRAText>
        { props.streak ? <Streak {...props} /> : null }
      </View>
      <TRAText style={styles.number}>{props.value}</TRAText>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 60,
  },
  iconContainer: {
    height: '100%',
    flexBasis: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    color: styleVars.color.traenaPrimaryGreen,
  },
  titleContainer: {
    height: 40,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: styleVars.fontSize.medlar,
    fontFamily: styleVars.fontFamily.medium,
    marginBottom: 3,
  },
  number: {
    fontSize: styleVars.fontSize.xlarger,
    fontFamily: styleVars.fontFamily.bold,
    color: styleVars.color.black,
    marginLeft: 'auto',
  },
  detailContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingRight: 15,
  },
  subline: {
    fontSize: styleVars.fontSize.small,
  },
  streak: {
    color: styleVars.color.mediumGrey,
  },
});

export default AnalyticsPanel;
