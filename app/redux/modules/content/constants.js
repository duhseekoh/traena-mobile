// @flow
export const CONTENT_TYPES = {
  TRAINING_VIDEO: 'TrainingVideo',
  DAILY_ACTION: 'DailyAction',
  IMAGE: 'Image',
}
