// @flow
import React, { Component } from 'react';
import {
  FlatList,
  RefreshControl,
  StyleSheet,
  View,
  ActivityIndicator,
} from 'react-native';
import {
  TRATextStrong,
  ChannelPreview,
} from 'app/components';
import type { Channel } from 'app/redux/modules/channel/types';
import styleVars from 'app/style/variables';

type Props = {
  loadNextPage: () => void,
  refreshChannelList: () => void,
  canLoadMore: boolean,
  isLoadingMore: boolean,
  isRefreshing: boolean,
  displayNoMoreResults: boolean,
  // TODO: `listProp` should have a type of FlatListProps; fix the import of this type
  // something like `import type { Props as FlatListProps } from 'react-native/Libraries/Lists/FlatList';`, maybe
  listProps?: any,
  channels: {[channelId: number]: Channel}[],
};

class ChannelList extends Component<Props> {
  props: Props;
  render() {
    const { refreshChannelList, isRefreshing = false, listProps, channels } = this.props;
    return (
      <View style={{flex: 1}}>
        <FlatList
          horizontal={false}
          scrollEnabled={true}
          data={channels}
          keyExtractor={this._keyExtractor}
          removeClippedSubviews={true}
          // needs to be greater than 0 or android wont trigger onEndReached
          onEndReachedThreshold={0.1}
          scrollEventThrottle={1}
          // disable virtualization because this unmounts views
          // that are off screen causing glitchy jumps
          disableVirtualization={true}
          refreshControl={
            <RefreshControl
              // android only && only if there is a top banner
              refreshing={isRefreshing}
              onRefresh={refreshChannelList}
            />
          }
          ListFooterComponent={this._renderFooter}
          renderItem={this._renderItem}
          style={styles.list}
          onEndReached={this._loadMoreItems}
          {...listProps}
        />
      </View>
    );
  }

  _renderItem = ({item}: {item: Channel}) => {
    return (
      <View style={styles.itemHolder}>
        <ChannelPreview
          channel={item}
        />
      </View>
  )}

  _renderFooter = () => {
    const { isLoadingMore, displayNoMoreResults } = this.props;
    return (
      <View>
        { isLoadingMore &&
          <ActivityIndicator
            size="small"
            color={styleVars.color.traenaPrimaryGreen}
            style={{paddingVertical: 50}}
          />
        }
        { displayNoMoreResults && <TRATextStrong style={styles.endOfList}>no more results</TRATextStrong> }
      </View>
    )
  }

  _loadMoreItems = () => {
    if(!this.props.isRefreshing && !this.props.isLoadingMore && this.props.canLoadMore) {
      this.props.loadNextPage();
    }
  }

  _keyExtractor = (item) => Number(item.id).toString();

}

export default ChannelList;

const styles = StyleSheet.create({
  itemHolder: {
  },
  list: {
    flex: 1,
    backgroundColor: styleVars.color.white,
  },
  endOfList: {
    textAlign: 'center',
    paddingVertical: 50,
    color: styleVars.color.mediumGrey,
  }
});
