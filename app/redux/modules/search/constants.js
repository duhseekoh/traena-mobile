const PAGE_SIZE = 10; // number of results in a page that we request from ES

export {
  PAGE_SIZE,
}
