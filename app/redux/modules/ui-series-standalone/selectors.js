// @flow
import { createSelector } from 'reselect';
import { contentItemsSelectorFactory } from 'app/redux/modules/content/selectors';
import { REQUEST_STATUSES } from 'app/redux/constants';
import type { ReduxState } from 'app/redux/constants';

const STATE_KEY = 'seriesStandalone';
const stateFromRoot = (state: ReduxState) => state[STATE_KEY];

const contentIdsSelector = createSelector([
  stateFromRoot,
  (_, props) => props.id,
], (state, seriesId) => {
  const uiSlices = state.uiSlices;
  return (uiSlices && uiSlices[seriesId]) ? uiSlices[seriesId].contentIds : [];
});

const contentForSeriesSelector = contentItemsSelectorFactory(contentIdsSelector);

const isLoadingMoreSelector = (state: ReduxState, { id }: { id: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[id];
  return uiSlice && uiSlice.loadingStatus === REQUEST_STATUSES.IN_PROGRESS;
}

const isRefreshingSelector = (state: ReduxState, { id }: { id: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[id];
  return uiSlice && uiSlice.refreshingStatus === REQUEST_STATUSES.IN_PROGRESS;
}

const hasErrorSelector = (state: ReduxState, { id }: { id: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[id];
  return uiSlice && (
    uiSlice.loadingStatus === REQUEST_STATUSES.ERROR || uiSlice.refreshingStatus === REQUEST_STATUSES.ERROR);
}

const canLoadMoreSelector = (state: ReduxState, { id }: { id: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[id];
  return uiSlice && uiSlice.canLoadMore;
}

const currentPageSelector = (state: ReduxState, { id }: { id: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[id];
  return uiSlice && uiSlice ? uiSlice.currentPage : 0;
}

/**
 * Are we at the bottom of the results list? Can't be loading, cant be
 refreshing, and make sure that we have hit the end of paging.
 */
const displayNoMoreResultsSelector = createSelector([
  isLoadingMoreSelector,
  isRefreshingSelector,
  canLoadMoreSelector,
], (isLoadingMore, isRefreshing, canLoadMore) => {
  return !isLoadingMore && !isRefreshing && !canLoadMore;
});

export {
  STATE_KEY,
  contentForSeriesSelector,
  isLoadingMoreSelector,
  isRefreshingSelector,
  hasErrorSelector,
  canLoadMoreSelector,
  currentPageSelector,
  displayNoMoreResultsSelector,
};
