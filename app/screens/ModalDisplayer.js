// @flow
import React from 'react';
import {
  Modal,
  View,
} from 'react-native';
import type { ModalType } from 'app/types/Modal';
import { InfoModal } from 'app/components';
import { MODAL_TYPES } from 'app/redux/modules/modal/constants';
type Props = {
  modalType?: ModalType,
  modalProps: any,
};

const ModalDisplayer = ({modalType, modalProps = {}}: Props) => {
  if(!modalType) {
    return null;
  }

  return (
    <Modal
      animationType={'fade'}
      transparent={true}
      visible={true}
      onRequestClose={() => {console.log('Modal has been closed.')}}
      >
     <View style={{flex: 1}}>
        {_renderModalContents(modalType, modalProps)}
     </View>
   </Modal>
  )
}

const _renderModalContents = (modalType: ModalType, modalProps: any) => { // eslint-disable-line no-unused-vars
  let contents = null;
  switch(modalType) {
    case MODAL_TYPES.INFO_MODAL:
      return <InfoModal {...modalProps} />;
    default:
      break;
  }

  return contents;
};

export default ModalDisplayer;
