// @flow

// QuestionSet actions
export const SET_CORRECT_FIRST_TRY = 'SET_CORRECT_FIRST_TRY';
export const RESET_QUESTION_SET = 'RESET_QUESTION_SET';
export const SET_CURRENT_QUESTION = 'SET_CURRENT_QUESTION';

// General actions
export const RESET_ACTIVITY = 'RESET_ACTIVITY';

const setCurrentQuestion = (questionIndex: number) => {
  return {
    type: SET_CURRENT_QUESTION,
    payload: questionIndex
  }
}

const setCorrectFirstTry = (questionIndex: number) => {
  return {
    type: SET_CORRECT_FIRST_TRY,
    payload: questionIndex
  }
}

const resetQuestionSet = (questionsLen: number) => {
  return {
    type: RESET_QUESTION_SET,
    payload: questionsLen
  }
}

const resetActivity = () => {
  return {
    type: RESET_ACTIVITY,
  }
}

export {
  setCurrentQuestion,
  setCorrectFirstTry,
  resetQuestionSet,
  resetActivity,
}
