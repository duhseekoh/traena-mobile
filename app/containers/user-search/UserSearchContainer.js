// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View
} from 'react-native';
import {
  SearchBar,
} from 'app/components';
import UserSearchResults from './components/UserSearchResults';
import { getSearchResults } from 'app/redux/modules/user-search/actions';
import {
  isLoadingSelector,
  queryTextSelector,
  searchResultsSelector
} from 'app/redux/modules/user-search/selectors';
import type { SearchResults } from 'app/redux/modules/user-search/types';

type Props = {
  onSearchQueryTyping: (result: string) => void,
  onSearchResultSelected: (result: string) => void, // supplied externally
  searchResults: SearchResults,
  searchQuery?: string,
  isLoading?: boolean,
};

class UserSearchContainer extends Component<Props> {

  render() {
    const { searchResults, searchQuery,
      onSearchResultSelected, isLoading } = this.props;

    return (
      <View style={{flex: 1}}>
        {this._renderSearchBar()}
        <UserSearchResults
          results={searchResults}
          onSearchResultSelected={onSearchResultSelected}
          isQueryEmpty={!searchQuery}
          isLoading={isLoading}
        />
      </View>
    );
  }

  _renderSearchBar = () => {
    const {
      searchQuery,
      onSearchQueryTyping
    } = this.props;

    return (
      <View>
        <SearchBar
          onSearchQueryTyping={onSearchQueryTyping}
          searchQuery={searchQuery}
          onClearSearch={this._onClearSearch}
        />
      </View>
    )
  }

  _onClearSearch = () => {
    this.props.onSearchQueryTyping('');
  }
}

const mapStateToProps = (state) => {
  return {
    searchResults: searchResultsSelector(state),
    searchQuery: queryTextSelector(state),
    isLoading: isLoadingSelector(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSearchQueryTyping: function(searchQuery) {
      return dispatch(getSearchResults(searchQuery));
    },
  };
};

const ConnectedUserSearchContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserSearchContainer);

export default ConnectedUserSearchContainer;
