import { contentItemsSelectorFactory } from 'app/redux/modules/content/selectors';

const STATE_KEY = 'uiBookmarksWidget';
const stateFromRoot = (state) => state[STATE_KEY];

export const isLoadingSelector = (state) => stateFromRoot(state).isLoading;

const selectBookmarkIds = (state) => stateFromRoot(state).contentIds;

export const selectBookmarks = contentItemsSelectorFactory(selectBookmarkIds);
