// @flow
import { selectSeriesById } from '../series/selectors';
import type { ReduxState } from 'app/redux/constants';
const STATE_KEY = 'seriesProgressWidget';
const stateFromRoot = (state: ReduxState) => state[STATE_KEY];

export const isLoadingSelector = (state: ReduxState) => stateFromRoot(state).isLoading;

export const seriesSelector = (state: ReduxState) => {
  const seriesIds = stateFromRoot(state).seriesIds;
  return seriesIds
    .map(seriesId => selectSeriesById(state, { seriesId }));
};
