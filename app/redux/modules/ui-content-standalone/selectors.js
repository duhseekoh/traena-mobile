// @flow
import _ from 'lodash';
import { createSelector } from 'reselect';
import { contentItemsSelectorFactory } from 'app/redux/modules/content/selectors';
import * as fromSeries from 'app/redux/modules/series/selectors';
import type { SeriesWithPreview } from 'app/redux/modules/series/types';
import { completeContentIdsSelector } from 'app/redux/modules/traena-task/selectors';
import { CONTENT_ITEM_STATUSES } from 'app/redux/modules/ui-content-standalone/constants';
import { REQUEST_STATUSES, type ReduxState } from 'app/redux/constants';
import type { ReduxStateUiContentStandalone } from './reducer';
import type { ContentItem } from 'app/redux/modules/content/types';

const STATE_KEY = 'contentStandalone';
const stateFromRoot = (state): ReduxStateUiContentStandalone => state[STATE_KEY];

// Does a particular ui instance have an error loading
const isLoadingSelector = (state: ReduxState, { contentId }: { contentId: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[contentId];
  return uiSlice && uiSlice.loadingStatus === REQUEST_STATUSES.IN_PROGRESS;
}
// Does a particular ui instance have an error loading
const hasErrorSelector = (state: ReduxState, { contentId }: { contentId: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[contentId];
  return uiSlice && uiSlice.loadingStatus === REQUEST_STATUSES.ERROR;
}
// All content ids we are keeping track of in standalone views
const contentIdsSelector = (state: ReduxState): number[] => Object.keys(stateFromRoot(state).uiSlices)
  .map(contentId => parseInt(contentId, 10)); // object keys are always strings, convert here
// All content items we are keeping track of in standalone views, keyed by id
const standaloneContentItemsSelector = contentItemsSelectorFactory(contentIdsSelector);
const standaloneContentItemsMapSelector = createSelector([
  standaloneContentItemsSelector,
], (contentItems): {[number]: ?ContentItem} => _.keyBy(contentItems, 'id'));
// The content id we are looking for in a particular ui instance
const currentContentIdSelector = (state, { contentId }): number => contentId;
// The content item we are looking for in a particular ui instance
const publishedContentSelector = createSelector([
  standaloneContentItemsMapSelector,
  currentContentIdSelector,
], (contentItems: {[number]: ?ContentItem}, contentId) => {
  const contentItem = contentItems[contentId];
  if (!contentItem) {
    return null;
  }
  const status = _.get(contentItem, 'raw.contentItem.status', null);
  return status === CONTENT_ITEM_STATUSES.PUBLISHED ? contentItem : null;
});

const isCompletedSelector = createSelector([
    currentContentIdSelector, completeContentIdsSelector
  ], (currentContentId, completeContentIds) => completeContentIds.includes(currentContentId));

/**
 * Gets the series associated with the specificed contentId, then grabs that series
 * and the content previews for it.
 */
const seriesWithContentSelector = (state: ReduxState, { contentId }: { contentId: number }): ?SeriesWithPreview => {
  const contentItem = publishedContentSelector(state, { contentId });
  if (contentItem && contentItem.series && contentItem.series.id) {
    return fromSeries.seriesWithContentSelector(state, { seriesId: contentItem.series.id });
  }

  return null;
}

export {
  STATE_KEY,
  isCompletedSelector,
  isLoadingSelector,
  hasErrorSelector,
  publishedContentSelector,
  seriesWithContentSelector,
};
