const completeContentIdsSelector = (state) => state.traenaTask.completeContentIds || [];

export {
  completeContentIdsSelector
};
