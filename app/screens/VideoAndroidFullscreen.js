// @flow
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
} from 'react-native';
import type {
  NavigationNavigatorProps,
  NavigationStackScreenOptions,
} from 'react-navigation';
import Orientation from 'react-native-orientation';
import {
  VideoPlayer,
} from 'app/components';


type NavigationStateParams = {
  params: {
    videoURI: string,
    previewImageURI: string,
  },
} & *;

type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

type Props = {
  videoURI: string,
  previewImageURI: string,
} & NavigationProps;

class VideoAndroidFullscreen extends Component<Props> {
  static navigationOptions: (*) => NavigationStackScreenOptions = () => {
    return {
      header: null,
    };
  };

  componentDidMount() {
    Orientation.unlockAllOrientations();
  }

  componentWillUnmount() {
    // locking here in case back button used
    Orientation.lockToPortrait();
  }

  render() {
    const { params } = this.props.navigation.state;
    const { videoURI, previewImageURI } = params;

    return (
      <View style={{flex: 1}}>
        <StatusBar hidden />
        <VideoPlayer
          style={styles.videoPlayer}
          videoURI={videoURI} previewImageURI={previewImageURI} isAndroidFullscreen={true} />
      </View>
    );
  }
}

export default VideoAndroidFullscreen;

const styles = StyleSheet.create({
  videoPlayer: {
  }
});
