import 'react-native';
import channelStandaloneReducer from './reducer';
import { REFRESHED_CHANNEL_CONTENT } from './actions';

it('action REFRESHED_CHANNEL_CONTENT sets the channel associated content', () => {
  const awesomeFeed = ['some-content-id', 'some-content-id-2'];
  const awesomeChannel = 'awesome';
  const state = channelStandaloneReducer(undefined, {type: REFRESHED_CHANNEL_CONTENT, payload: { ids: awesomeFeed, channelId: awesomeChannel }});
  expect(state.uiSlices[awesomeChannel].content.contentIds).toEqual(awesomeFeed);
});
