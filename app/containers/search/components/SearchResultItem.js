// @flow
import React, { Component } from 'react';
import { ContentPreview } from 'app/components';
import type { SearchResults } from 'app/redux/modules/search/types';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style?: ViewStyleProp,
  result: SearchResults,
  onPress: () => void,
};

class SearchResultItem extends Component<Props> {
  render() {
    const { result, onPress, style } = this.props;

    return (
      <ContentPreview
        contentItem={result}
        onPress={onPress}
        style={style}
      />
    )
  }
}

export default SearchResultItem;
