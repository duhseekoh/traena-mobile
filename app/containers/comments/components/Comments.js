// @flow
import React from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet,
} from 'react-native';
import {
  ButtonSubtle,
} from 'app/components';
import CommentsList from './CommentsList';
import NewComment from './NewComment';
import styleVars from 'app/style/variables';
import type { Comment } from 'app/redux/modules/comment/types';
import type { User } from 'app/redux/modules/user/types';

type Props = {
  comments: Comment[],
  isLoadingRecent?: boolean,
  isLoadingOlder?: boolean,
  isPosting?: boolean,
  hasOlderComments?: boolean,
  newCommentText?: string,
  newCommentTaggedUsers?: User[],
  onPressLoadOlderComments: () => Promise<*>,
  onPressPostComment: () => void,
  onPressTagUser: () => void,
  onPressRemoveUser: (userToRemove: User) => void,
  onTypingNewComment: (text: string) => void,
  onRefresh: () => Promise<*>,
};

const Comments = ({
  comments,
  isLoadingRecent,
  isLoadingOlder,
  isPosting,
  hasOlderComments,
  newCommentText,
  newCommentTaggedUsers,
  onPressLoadOlderComments,
  onPressPostComment,
  onPressTagUser,
  onPressRemoveUser,
  onTypingNewComment,
  onRefresh,
}: Props) => {
  const displayOlderButton = !isLoadingOlder && hasOlderComments;
  return (
    <View style={styles.container}>
      <CommentsList
        header={
          <View>
            { isLoadingOlder && (
              <ActivityIndicator
                size="small"
                color={styleVars.color.traenaPrimaryGreen}
              />
            )}
          { displayOlderButton &&
            <ButtonSubtle style={styles.loadOlderButton} title={'older...'} onPress={onPressLoadOlderComments} />
          }
          </View>
        }
        style={styles.commentsList}
        comments={comments}
        isLoadingRecent={isLoadingRecent}
        onRefresh={onRefresh}
       />

      <NewComment
        style={styles.newComment}
        text={newCommentText}
        taggedUsers={newCommentTaggedUsers}
        onPressPost={onPressPostComment}
        onPressTagUser={onPressTagUser}
        onPressRemoveUser={onPressRemoveUser}
        onTyping={onTypingNewComment}
        disabled={isPosting}
        />
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: styleVars.color.lightGrey4,
  },
  loadOlderButton: {
    paddingVertical: 10,
  },
  commentsList: {
    flex: 1,
  },
  newComment: {
    flex: 0,
    marginTop: 2,
  },
});

export default Comments;
