// @flow
import React, { Component } from 'react';
import {
  ListView,
  View,
  RefreshControl,
} from 'react-native';
import {
  TRATextStrong,
} from 'app/components';
import Comment from './Comment';
import InvertibleScrollView from 'react-native-invertible-scroll-view'; //eslint-disable-line import/default
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import type { Comment as CommentType } from 'app/redux/modules/comment/types';

type Props = {
  style?: ViewStyleProp,
  comments: CommentType[],
  header?: React$Element<*>,
  isLoadingRecent?: boolean,
  onRefresh: () => Promise<*>,
};

type State = {
  dataSource: any,
};

class CommentsList extends Component<Props, State> {

  constructor(props: Props) {
    const { comments } = props;
    super(props);
    const ds = new ListView.DataSource({
      rowHasChanged: (commentOne, commentTwo) => commentOne.id !== commentTwo.id
    });
    this.state = {
      dataSource: ds.cloneWithRows(comments),
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(nextProps.comments)
    });
  }

  render() {
    const { style, isLoadingRecent, onRefresh } = this.props;

    return (
      <View style={[styles.container, style]}>
        <ListView
          refreshControl={
            <RefreshControl
              // android only
              progressViewOffset={100}
              refreshing={!!isLoadingRecent}
              onRefresh={onRefresh}
            />
          }
          style={styles.listView}
          renderScrollComponent={props => <InvertibleScrollView {...props} inverted />}
          scrollingEnabled={true}
          enableEmptySections={true}
          dataSource={this.state.dataSource}
          renderRow={this._renderRow}
          renderFooter={this._renderFooter}
          renderSeparator={(sectionID, rowID) => <View style={styles.listItemSeparator} key={sectionID+'_'+rowID} />}
        />
      </View>
    );
  }

  _renderFooter = () => {
    const hasComments = !!this.state.dataSource.getRowCount();
    if(hasComments) {
      return this.props.header;
    }

    return (
      <View style={styles.noCommentsContainer}>
        <TRATextStrong style={styles.noCommentsText}>Be the first to comment.</TRATextStrong>
      </View>
    )
  }

  _renderRow = (comment) => {
    return (
      <Comment key={comment.id} comment={comment} style={styles.comment} />
    )
  }
}

export default CommentsList;

const styles = {
  container: {

  },
  comment: {
    paddingHorizontal: 5
  },
  listView: {

  },
  listItemSeparator: {
    borderBottomWidth: 1,
    borderColor: styleVars.color.lightGrey3,
    marginVertical: 10,
  },
  noCommentsContainer: {

  },
  noCommentsText: {
    marginVertical: 15,
    textAlign: 'center',
    color: styleVars.color.mediumGrey,
  },
};
