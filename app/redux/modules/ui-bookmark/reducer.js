// @flow
import _ from 'lodash';
import {
  LOADED_MORE_BOOKMARKS, REFRESHED_BOOKMARKS, REFRESH_BOOKMARKS_REQUEST_STARTED, LOAD_MORE_BOOKMARKS_REQUEST_STARTED, REFRESH_BOOKMARKS_REQUEST_FAILED, LOAD_MORE_BOOKMARKS_REQUEST_FAILED
} from './actions';
import {
  ADDED_BOOKMARK,
  REMOVED_BOOKMARK,
} from '../bookmark/actions';
import { REQUEST_STATUSES, type RequestStatus } from 'app/redux/constants';
import type { Action } from 'app/types/Action';

export type ReduxStateBookmarkUI = {
  uiBookmarkContentIds: number[],
  loadingStatus: RequestStatus,
  refreshingStatus: RequestStatus,
  canLoadMore: boolean,
  currentPage: number,
};

const ACTION_HANDLERS = {
  [ADDED_BOOKMARK]: (state: ReduxStateBookmarkUI, action: Action) => {
    const id = action.payload;
    const bookmarkIds = _.uniq([...state.uiBookmarkContentIds, id]);
    return {
      ...state,
      uiBookmarkContentIds: bookmarkIds,
    };
  },
  [REMOVED_BOOKMARK]: (state: ReduxStateBookmarkUI, action: Action) => {
    const id = action.payload;
    const bookmarkIds = [...state.uiBookmarkContentIds];
    _.remove(bookmarkIds, bId => bId === id);
    return {
      ...state,
      uiBookmarkContentIds: bookmarkIds,
    };
  },
  [REFRESHED_BOOKMARKS]: (state: ReduxStateBookmarkUI, action: Action) => {
    const { ids, last, number } = action.payload;
    return {
      ...state,
      uiBookmarkContentIds: ids,
      canLoadMore: !last,
      currentPage: number,
      refreshingStatus: REQUEST_STATUSES.COMPLETE,
    };
  },
  [LOADED_MORE_BOOKMARKS]: (state: ReduxStateBookmarkUI, action: Action) => {
    const { ids, last, number } = action.payload;
    const mergedContentIds = _.uniq([...state.uiBookmarkContentIds, ...ids]);
    return {
      ...state,
      uiBookmarkContentIds: mergedContentIds,
      canLoadMore: !last,
      currentPage: number,
      loadingStatus: REQUEST_STATUSES.COMPLETE,
    };
  },
  [REFRESH_BOOKMARKS_REQUEST_STARTED]: (state: ReduxStateBookmarkUI) => {
    return {
      ...state,
      refreshingStatus: REQUEST_STATUSES.IN_PROGRESS,
      loadingStatus: REQUEST_STATUSES.COMPLETE,
    };
  },
  [REFRESH_BOOKMARKS_REQUEST_FAILED]: (state: ReduxStateBookmarkUI) => {
    return {
      ...state,
      refreshingStatus: REQUEST_STATUSES.ERROR,
    };
  },
  [LOAD_MORE_BOOKMARKS_REQUEST_STARTED]: (state: ReduxStateBookmarkUI) => {
    return {
      ...state,
      loadingStatus: REQUEST_STATUSES.IN_PROGRESS,
      refreshingStatus: REQUEST_STATUSES.COMPLETE,
    };
  },
  [LOAD_MORE_BOOKMARKS_REQUEST_FAILED]: (state: ReduxStateBookmarkUI) => {
    return {
      ...state,
      loadingStatus: REQUEST_STATUSES.ERROR,
    };
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  uiBookmarkContentIds: [],
  canLoadMore: false,
  currentPage: 0,
  loadingStatus: REQUEST_STATUSES.COMPLETE,
  refreshingStatus: REQUEST_STATUSES.COMPLETE,
};

const bookmarkUIReducer = (state: ReduxStateBookmarkUI = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

export default bookmarkUIReducer;
