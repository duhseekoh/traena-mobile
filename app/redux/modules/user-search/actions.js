// @flow
import api from 'app/redux/api';
import type { Dispatch } from 'app/redux/constants';
import type { ContentItem } from '../content/types';

const RETRIEVED_USER_SEARCH_RESULTS = 'RETRIEVED_USER_SEARCH_RESULTS';
const retrievedSearchResults = (results: ContentItem[]) => {
  return {
    type: RETRIEVED_USER_SEARCH_RESULTS,
    payload: { results }
  };
};

const UI_USER_SEARCH_TEXT_UPDATED = 'UI_USER_SEARCH_TEXT_UPDATED';
const uiSearchTextUpdated = (queryText: string) => {
  return {
    type: UI_USER_SEARCH_TEXT_UPDATED,
    payload: queryText
  }
};

const USER_SEARCH_REQUEST_STARTED = 'USER_SEARCH_REQUEST_STARTED';
const startedUserSearchRequest = () => {
  return {
    type: USER_SEARCH_REQUEST_STARTED,
  };
};

const USER_SEARCH_REQUEST_FINISHED = 'USER_SEARCH_REQUEST_FINISHED';
const finishedUserSearchRequest = () => {
  return {
    type: USER_SEARCH_REQUEST_FINISHED,
  };
};

const getSearchResults = (searchText: string) => {
  return (dispatch: Dispatch) => {
    dispatch(uiSearchTextUpdated(searchText));
    if(searchText.length === 0) {
      return dispatch(retrievedSearchResults([]));
    }

    dispatch(startedUserSearchRequest());
    return api.search.retrieveUserSearchResults(searchText)
      .then((results: ContentItem[]) => {
        dispatch(finishedUserSearchRequest());
        return dispatch(retrievedSearchResults(results));
      })
      .catch(() => {
        dispatch(finishedUserSearchRequest());
        alert('Network Error'); // TODO - come up with something better
      });
  };
};

export {
  RETRIEVED_USER_SEARCH_RESULTS,
  UI_USER_SEARCH_TEXT_UPDATED,
  USER_SEARCH_REQUEST_STARTED,
  USER_SEARCH_REQUEST_FINISHED,
  getSearchResults,
}
