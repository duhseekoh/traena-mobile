// @flow
import type { User } from 'app/redux/modules/user/types';
import type { Comment } from 'app/redux/modules/comment/types';

type Notification = {
  id: number,
  createdTimestamp: string,
  commentId?: number,
  contentId?: number,
  from: User,
  comment: Comment,
  notificationType: string,
  content: {
    body: {
      title: string,
      text: string,
      type: string,
    },
  },
};

export type {
  Notification,
};
