// @flow
import {
  SERIES_FETCH_SUCCEEDED,
  SERIES_FETCH_PROGRESS_SUCCEEDED,
} from './actions';
import type { Action } from 'app/types/Action';
import type { Series, SeriesProgress } from './types';

export type ReduxStateSeries = {
  series: {[seriesId: number]: Series},
  seriesProgress: {[seriesId: number]: SeriesProgress},
};

const ACTION_HANDLERS = {
  [SERIES_FETCH_SUCCEEDED]: (state: ReduxStateSeries, action: Action) => {
    const newSeries: Series[] = action.payload;
    const seriesObj = newSeries.reduce((map, series) => ({
      ...map,
      [series.id]: series,
    }), {});
    return {
      ...state,
      series: {
        ...state.series,
        ...seriesObj,
      },
    }
  },
  [SERIES_FETCH_PROGRESS_SUCCEEDED]: (state: ReduxStateSeries, action: Action) => {
    const seriesProgress: SeriesProgress[] = action.payload;
    const seriesProgressObj = seriesProgress.reduce((map, seriesProgress) => ({
      ...map,
      [seriesProgress.seriesId]: seriesProgress,
    }), {});
    return {
      ...state,
      seriesProgress: {
        ...state.seriesProgress,
        ...seriesProgressObj,
      },
    }
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  series: {},
  seriesProgress: {},
};

const seriesReducer = (state: ReduxStateSeries = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default seriesReducer;
