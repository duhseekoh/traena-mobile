import 'react-native';
import React from 'react';
import { Login } from './LoginContainer';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer.create(
    <Login
      onSubmitLoginForm={() => {}}
      forgotPasswordUrl={'http://cool.com'}
      isLoginInProgress={false} />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly when login is in progress', () => {
  const tree = renderer.create(
    <Login
      onSubmitLoginForm={() => {}}
      forgotPasswordUrl={'http://cool.com'}
      isLoginInProgress={true} />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});
