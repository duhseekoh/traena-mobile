// @flow
import { userSelector } from './selectors';
import logger from 'app/logging/logger';
import * as userApi from 'app/redux/api/user';
import type { User } from './types';
import type { Dispatch, GetState } from 'app/redux/constants';

export const USER_RETRIEVED = 'USER_RETRIEVED';
const userRetrieved = (user: User) => {
  logger.logBreadcrumb('User retrieved', { user });
  if (user) {
    logger.setupUser(user.id, user.email);
  }
  return {
    type: USER_RETRIEVED,
    payload: user
  };
}

/**
 * Called when app state is loaded. Use as hook for any special user setup.
 * Current use case: Setup the user to be identified in crash log reporting.
 */
const userStateLoaded = () => {
  return (dispatch: Dispatch, getState: GetState) => {
    const user: Object = userSelector(getState());
    if (user) {
      logger.setupUser(user.id, user.email);
    }
  };
};

export {
  userRetrieved,
  userStateLoaded,
}

export function fetchUser() {
  return (dispatch: Dispatch) => {
    userApi.fetchSelf().then((user) => {
      dispatch(userRetrieved(user));
    });
  }
}
