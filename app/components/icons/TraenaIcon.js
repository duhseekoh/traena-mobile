// @flow
import React from 'react';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from 'app/fonts/traena-icon-icomoon-config.json';
import type { Color, Size } from './types';
import type { TextStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import styleVars from 'app/style/variables';

type Props = {
  style?: TextStyleProp,
  name: 'stopwatch' | 'newsfeed' | 'traena-text' | 't' | 't-thin' | 'heart-filled' | 'checklist' | 't-open' | 'data' | 't-closed' | 'bookmark-filled' | 'bolt',
  fontSize: Size,
  color: Color,
};

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const TraenaIcon = ({ fontSize, color, style, name }: Props) => {

  return (
    <Icon
      style={[
        { fontSize, color },
        style,
        {backgroundColor: styleVars.color.transparent}
      ]}
      name={name}
    />
  );

};

export default TraenaIcon;
