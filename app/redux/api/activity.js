// @flow
import {
  getActivitesByContentIdTpl,
  getActivityResults,
} from './util/endpoints';

const retrieveActivitiesByContentId = (contentId: number) => {
  return fetch(getActivitesByContentIdTpl({ contentId }));
};

const retrieveActivitiesResults = (contentId: number, activityId: number) => {
  return fetch(getActivityResults({ contentId, activityId }));
};

export default {
  retrieveActivitiesByContentId,
  retrieveActivitiesResults,
}
