//@flow
type User = {
  id: number,
  firstName: string,
  lastName: string,
  profileImageURI?: string,
  position: string,
  tags: string[],
  email: string,
  organizationId?: number,
  organization: ?{
    name: string,
    id: number,
  },
};

export type { User } ;
