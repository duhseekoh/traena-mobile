// @flow
import * as statsApi from 'app/redux/api/stats';
import type { Dispatch } from 'app/redux/constants';
import type { Stats } from 'app/redux/modules/stats/types';

export const STATS_RECEIVED = 'STATS_RECEIVED';
export function statsReceived(stats: Stats) {
  return {
    type: STATS_RECEIVED,
    payload: stats,
  }
}

export const STATS_LOADING = 'STATS_LOADING';
export function toggleStatsFetchLoading(val: boolean) {
  return {
    type: STATS_LOADING,
    payload: val,
  }
}

export function fetchUserStats() {
  return (dispatch: Dispatch) => {
    dispatch(toggleStatsFetchLoading(true));
    statsApi.fetchStats().then((stats) => {
      dispatch(statsReceived(stats));
      dispatch(toggleStatsFetchLoading(false));
    });
  }
}
