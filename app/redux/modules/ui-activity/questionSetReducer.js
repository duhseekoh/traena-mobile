// @flow
import { SET_CURRENT_QUESTION, SET_CORRECT_FIRST_TRY, RESET_QUESTION_SET } from './actions';
import type { Action } from 'app/types/Action';
import type { QuestionSetUIData } from './types';

const ACTION_HANDLERS = {
  [SET_CURRENT_QUESTION]: (state: QuestionSetUIData, action: Action) => {
    return {
      ...state,
      currentQuestionIndex: action.payload,
    };
  },
  [SET_CORRECT_FIRST_TRY]: (state: QuestionSetUIData, action: Action) => {
    const correctArray = [...state.correctFirstTry];
    correctArray[action.payload] = true;

    return {
      ...state,
      correctFirstTry: correctArray,
    };
  },
  [RESET_QUESTION_SET]: (state: QuestionSetUIData, action: Action) => {
    let correctArray = [];
    for(let i = 0; i < action.payload; i+=1) {
      correctArray.push(false);
    }
    return {
      ...state,
      currentQuestionIndex: 0,
      correctFirstTry: correctArray,
    };
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  correctFirstTry: [],
  currentQuestionIndex: 0,
};

const questionSetReducer = (state: QuestionSetUIData = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default questionSetReducer;
