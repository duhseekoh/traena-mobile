// @flow
import React from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  View,
} from 'react-native';

type Props = {
  children: Node,
};

/**
 * For Android, we are setting the windowSoftInputMode in AndroidManifest.xml
 * which controls keyboard avoiding. So we never want to use the RN KeyboardAvoidingView
 * in Android, only iOS.
 */
const KeyboardAvoidingViewPassthrough = (props: Props) => {
  if(Platform.OS === 'ios') {
    return (
      <KeyboardAvoidingView {...props}>
        {
          React.Children.map(props.children, (child) => child)
        }
      </KeyboardAvoidingView>
    )
  } else {
    return (
      <View {...props}>
        {
          React.Children.map(props.children, (child) => child)
        }
      </View>
    )
  }
}

export default KeyboardAvoidingViewPassthrough;
