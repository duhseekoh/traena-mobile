// @flow
import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style: ViewStyleProp,
  cardColor: string,
  onPress: any => any,
  children: Node,
  imageBackground: string,
  imageOpacity: number,
}

/**
* TODO: Replace ContentCard's View elements with this FeedCard
* This needs to support ImageBackgrounds
*/
const FeedCard = ({
  style,
  cardColor,
  onPress,
  children,
  imageBackground,
  imageOpacity,
}: Props) => {
  return (
    <View
      style={[styles.container, style]}

    >
      <TouchableOpacity
        style={styles.touchable}
        onPress={onPress}
        delayPressIn={100}
        activeOpacity={0.8}
      >
        <ImageBackground
          source={imageBackground}
          resizeMode='cover'
          opacity={imageOpacity}
          style={[
            styles.card,
            { backgroundColor: cardColor },
          ]}
        >
          <View style={styles.cardContent}>
            {children}
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    width: '100%',
    aspectRatio: 1.6,
    borderRadius: 8,
    marginVertical: 5,
    ...styleVars.shadow.shadowThree,
  },
  touchable: {
    borderRadius: 8,
    overflow: 'hidden',
  },
  card: {
    width: '100%',
    height: '100%',
  },
  cardContent: {
    padding: 15,
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
});

export default FeedCard;
