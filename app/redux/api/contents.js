// @flow
import {
  getContentItemByIdTpl,
  getContentItemsByIdsTpl,
  postLikeOnContentTpl,
  deleteLikeOnContentTpl,
  getUserLikesTpl
} from './util/endpoints';

const retrieveContentItemById = (contentId: number) => {
  return fetch(getContentItemByIdTpl({
    contentId,
  }));
};

const retrieveContentItemsByIds = (contentIds: number[]) => {
  return fetch(getContentItemsByIdsTpl({
    contentIds
  }));
};

const likeContent = (contentId: number) => {
  return fetch(postLikeOnContentTpl({contentId}), {
    method: 'POST',
  });
};

const unlikeContent = (contentId: number) => {
  return fetch(deleteLikeOnContentTpl({contentId}), {
    method: 'DELETE',
  });
};

const retrieveUserLikes = () => {
  return fetch(getUserLikesTpl());
}

export default {
  retrieveContentItemById,
  retrieveContentItemsByIds,
  retrieveUserLikes,
  likeContent,
  unlikeContent,
}
