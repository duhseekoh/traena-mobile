// @flow
import React from 'react';
import { ART } from 'react-native';
import AnimShape from './AnimShape';
const { Path} = ART;

type Props = {
  radius: number,
  x: number,
  y: number,
}

function Circle(props: Props) {
  const { radius, x, y, ...rest } = props;
  const path = Path().moveTo(x, y-radius)
        .arc(0, radius * 2, radius)
        .arc(0, radius * -2, radius)
        .close();
  return (<AnimShape {...rest} d={path} />);

}

export default Circle;
