// @flow
import React from 'react';
import _ from 'lodash';
import {
  View,
  StyleSheet,
} from 'react-native';
import moment from 'moment';
import {
  TimeAgo,
  TRAText,
  TRATextStrong,
  UserFullName,
} from 'app/components';
import styleVars from 'app/style/variables';
import type { Notification as NotificationType } from 'app/redux/modules/notification/types';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style: ViewStyleProp,
  notification: NotificationType,
  onOpenNotification: (notification: NotificationType) => void,
};

const NOTIFICATION_TYPES = {
  COMMENT_ON_POST: 'COMMENT_ON_POST',
  LIKE_ON_POST: 'LIKE_ON_POST',
  TAG_IN_COMMENT: 'TAG_IN_COMMENT',
};

const Notification = ({style, notification, onOpenNotification}: Props) => {
  const { from: fromUser, comment, createdTimestamp, notificationType, content } = notification;
  const createdAtMoment = moment(createdTimestamp);
  const truncatedTitle = _.truncate(content.body.title.trim(), { length: 30 });

  let truncatedComment;
  if(comment) {
    truncatedComment = _.truncate(comment.text, { length: 70 });
  }

  let notificationContent;
  switch (notificationType) {
    case NOTIFICATION_TYPES.TAG_IN_COMMENT:
      notificationContent = <TRAText>tagged you in a comment on <TRATextStrong> &quot;{truncatedTitle}&quot;</TRATextStrong> : <TRATextStrong> &quot;{truncatedComment}&quot;</TRATextStrong></TRAText>;
      break;
    case NOTIFICATION_TYPES.LIKE_ON_POST:
      notificationContent = <TRAText>likes your post <TRATextStrong> &quot;{truncatedTitle}&quot;</TRATextStrong></TRAText>;
      break;
    case NOTIFICATION_TYPES.COMMENT_ON_POST:
      notificationContent =
        <TRAText>
          commented<TRATextStrong> &quot;{truncatedComment}&quot; </TRATextStrong>
          on your post<TRATextStrong> &quot;{truncatedTitle}&quot; </TRATextStrong>
        </TRAText>;
      break;
    default:
      notificationContent = "";
      break;
  }
  return (
    <View style={[styles.container, style]}>
      <View style={styles.right}>
        <TRAText style={styles.bodyText}>
          <UserFullName style={styles.name} user={fromUser} />{' '}
          {notificationContent}
        </TRAText>
        <TRATextStrong style={styles.linkButtonText} onPress={() => onOpenNotification(notification)}>
          view content
        </TRATextStrong>
        <View>
          <TimeAgo style={styles.time} time={createdAtMoment} />
        </View>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  right: {
    flex: 1,
    marginTop: -7,
  },
  name: {
    fontFamily: styleVars.fontFamily.medium,
    color: styleVars.color.black,
  },
  time: {
    textAlign: 'left',
  },
  bodyText: {
    color: styleVars.color.darkGrey,
    fontSize: styleVars.fontSize.medium,
    marginTop: 5,
  },
  linkButtonText: {
    color: styleVars.color.traenaOne,
    textDecorationLine: 'underline',
  },
});

export default Notification;
