// @flow
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'remote-redux-devtools';
import thunk from 'redux-thunk';
import reducers from './rootReducer';
import eventTrackerMiddleware from './middleware/eventTracker';
import type { Event } from './middleware/eventFactory';
import { enqueueEvent } from './api/events';
import initInterceptor from './api/util/httpInterceptor';

initInterceptor();

const configureStore = (initialState = {}) => {
  // sends analytic events to api
  const eventTracker = eventTrackerMiddleware((event: Event) => enqueueEvent(event));
  // converts react-native-router-flux screen events to analytics events
  const middleware = [thunk, eventTracker];

  const store = createStore(
    reducers,
    initialState,
    composeWithDevTools(applyMiddleware(...middleware)),
  );

  return store;
};

export default configureStore();
