import * as RNKeychain from 'react-native-keychain';

class Keychain {

  static async setRefreshToken(refreshToken) {
    return await RNKeychain.setGenericPassword('refreshToken', refreshToken);
  }

  static async getRefreshToken() {
    const { password: refreshToken } = await RNKeychain.getGenericPassword();
    return refreshToken;
  }

  static async clearRefreshToken() {
    return await RNKeychain.resetGenericPassword()
  }

}

export default Keychain;
