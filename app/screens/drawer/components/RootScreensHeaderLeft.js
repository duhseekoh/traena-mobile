// @flow
import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { ButtonHamburger } from 'app/components';
import * as fromNotifications from 'app/redux/modules/notification/selectors';

type OwnProps = {
  style?: ViewStyleProp,
  onPressHamburger: () => any,
};

type ConnectedProps = {
  hasUnacknowledged: boolean,
};

type Props = OwnProps & ConnectedProps;

/**
 * Screens that are accessible from within the drawer get this left header.
 * Allows one to open the drawer.
 */
const RootScreensHeaderLeft = (props: Props) => {
  return (
    <View style={props.style}>
      <ButtonHamburger
        onPress={props.onPressHamburger}
        showIndicator={props.hasUnacknowledged}
      />
    </View>
  );
};

const mapStateToProps = state => {
  return {
    hasUnacknowledged: fromNotifications.hasUnacknowledgedSelector(state),
  };
};

export default connect(mapStateToProps, null)(RootScreensHeaderLeft);
