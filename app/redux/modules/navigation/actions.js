// @flow
import _ from 'lodash';
import { Linking } from 'react-native';
import type { NavigationAction } from 'react-navigation';
import {
  uiSearchTextUpdated,
  getSearchResults,
} from 'app/redux/modules/search/actions';
import { deactivateVideoPlayer } from 'app/redux/modules/ui-video-player/actions';
import { resetActivity } from 'app/redux/modules/ui-activity/actions';
import type { Video } from 'app/types/Video';
import URL from 'url-parse';
import type { Dispatch, GetState } from 'app/redux/constants';
import type { ContentItem } from 'app/redux/modules/content/types';
import type { Channel } from 'app/redux/modules/channel/types';
import type { Activity } from 'app/redux/modules/activity/types';
import NavigationService from 'app/redux/modules/navigation/NavigationService';
import { createScreenChangedEvent } from 'app/redux/middleware/eventFactory';
import { ROUTES } from './constants';
import { FEEDBACK_URI } from 'app/redux/constants';
import { userSelector } from 'app/redux/modules/user/selectors';
import { getQueryString } from 'app/redux/api/util/queryString';

export const navigationService = new NavigationService();

const goToCommentsStandalone = (params: Object) => {
  return () => {
    navigationService.navigate(ROUTES.COMMENTS, params);
  };
};

const goToUserSearch = (params: Object) => {
  return () => {
    navigationService.navigate(ROUTES.USER_SEARCH, params);
  };
};

const goToSearch = (searchText: string) => {
  return (dispatch: Dispatch) => {
    dispatch(uiSearchTextUpdated(searchText));
    dispatch(getSearchResults());
    navigationService.navigate(ROUTES.SEARCH);
  };
};

/**
 * Navigates to the detailed view of a content item. The content item and the traena
 * tasks are retrieved at this time.
 * @param  {number} contentId]
 */
const goToContentStandalone = (contentId: number) => {
  return () => {
    navigationService.navigate(
      ROUTES.CONTENT_STANDALONE,
      { contentId },
      `content-${contentId}`,
    );
  };
};

const goToTagStandalone = (tag: string) => {
  return () => {
    const title = `#${tag}`;
    navigationService.navigate(ROUTES.TAG_STANDALONE, { tag, title });
  };
};

const goToChannelStandalone = (channel: Channel) => {
  return () => {
    navigationService.navigate(
      ROUTES.CHANNEL_STANDALONE,
      {
        channelId: channel.id,
        title: channel.name,
        description: channel.description,
      },
      `channel-${channel.id}`,
    );
  };
};

const goToSeriesStandalone = (
  id: number,
  title: string,
  description: string,
) => {
  return () => {
    navigationService.navigate(
      ROUTES.SERIES_STANDALONE,
      { id, title, description },
      `series-${id}`,
    );
  };
};

const goToAndroidFullscreenVideo = (video: Video) => {
  return () => {
    navigationService.navigate(ROUTES.VIDEO_ANDROID_FULLSCREEN, { ...video });
  };
};

const goToImageFullscreen = (
  contentItem: ContentItem,
  initialImageIndex: number,
) => {
  return () => {
    navigationService.navigate(ROUTES.IMAGE_FULLSCREEN, {
      contentItem,
      initialImageIndex,
    });
  };
};

const goToActivity = (activity: Activity) => {
  return (dispatch: Dispatch) => {
    dispatch(resetActivity());
    navigationService.navigate(ROUTES.ACTIVITY, { activity });
  };
};

const goToBookmarks = () => {
  return () => {
    navigationService.navigate(ROUTES.BOOKMARKS);
  };
};

const goToHome = () => {
  return () => {
    navigationService.navigate(ROUTES.HOME);
  };
};

const goToNotifications = () => {
  return () => {
    navigationService.navigate(ROUTES.NOTIFICATIONS);
  };
};

const goToProfile = () => {
  return () => {
    navigationService.navigate(ROUTES.PROFILE);
  };
};

const goToChannelList = () => {
  return () => {
    navigationService.navigate(ROUTES.CHANNEL_LIST);
  };
};

const goToChannelListDrawer = () => {
  return () => {
    navigationService.navigate(ROUTES.CHANNEL_LIST_DRAWER);
  };
};

const goToSeriesList = () => {
  return () => {
    navigationService.navigate(ROUTES.SERIES_LIST);
  };
};

const goToSeriesListDrawer = () => {
  return () => {
    navigationService.navigate(ROUTES.SERIES_LIST_DRAWER);
  };
};

const exitImageFullscreen = () => {
  return () => {
    navigationService.pop();
  };
};

const exitAndroidFullscreenVideo = () => {
  return () => {
    navigationService.pop();
  };
};

const exitActivity = () => {
  return () => {
    navigationService.pop();
  };
};

const popScene = () => {
  return () => {
    navigationService.pop();
  };
};

/**
 * Ran for every navigation change.
 */
const initializeSceneOnNavigate = () => {
  return (dispatch: Dispatch) => {
    // always stop a video from playing when navigating.
    dispatch(deactivateVideoPlayer());
  };
};

/**
 * Universal links we define to be handled by the app
 * are parsed and directed to the appropriate page
 * currently linking to standalone view for content.
 * If the link can't be handled by this app, open the url with Linking
 * @param  {string} url - url to open
 */
const navigateUniversalLink = (url: string) => {
  return (dispatch: Dispatch) => {
    const HANDLED_UNIVERSAL_LINKS = {
      CONTENT: {
        regex: /^\/content\/([0-9]+)\/view\/?$/,
        handleLinking: (path, regex) =>
          dispatch(goToContentStandalone(parseInt(regex.exec(path)[1], 10))),
      },
      USER: {
        regex: /^\/user\/([0-9]+)\/view\/?$/, // example regex
        handleLinking: (path, regex) => console.log(regex.exec(path)[1]),
      },
    };

    const parsableURL = new URL(url);

    const host = parsableURL.hostname;
    const path = parsableURL.pathname;

    // If the link can be handled within Traena, handle it
    if (host.includes('traenaapp.io')) {
      for (let key in HANDLED_UNIVERSAL_LINKS) {
        const link = HANDLED_UNIVERSAL_LINKS[key];
        const re = link.regex;
        if (re.test(path)) {
          link.handleLinking(path, re);
          return;
        }
      }
    }

    // Can't handle within Traena, resort to OS handling opening of link
    // RN linking needs an http in front of a link. Parsed text sees 'www.something' as a link
    if (_.startsWith(url, 'www')) {
      Linking.openURL(`http://${url}`);
    } else {
      Linking.openURL(url);
    }
  };
};

/**
 * Track a react-navigation navigation action.
 * Creates a redux action that formulates the necessary structure to track
 * a screen event.
 * Once dispatched the event tracker middleware picks up the meta data and sends
 * it out.
 */
export const trackNavigationEvent = (action: NavigationAction) => {
  return (dispatch: Dispatch) => {
    if (!action.routeName) {
      return;
    }

    const decoratedAction = {
      // dummy type just to get it to in redux to pass throug middleware
      type: 'TRACKING_EVENT',
      payload: null,
      meta: {
        event: createScreenChangedEvent({
          screenName: action.routeName,
          screenProperties: {
            ...action.params,
          },
        }),
      },
    };
    dispatch(decoratedAction);
  };
};

const FEEDBACK_LINK_VISITED = 'FEEDBACK_LINK_VISITED';
const goToFeedbackPage = () => {
  return async (dispatch: Dispatch, getState: GetState) => {
    const user = userSelector(getState());
    const qs = getQueryString({
      prefill_Email: user.email || '',
      prefill_FirstName: user.firstName || '',
      prefill_LastName: user.lastName || '',
      prefill_OrganizationName: user.organization ? user.organization.name : '',
    });
    Linking.openURL(`${FEEDBACK_URI}${qs}`);
    dispatch({
      type: FEEDBACK_LINK_VISITED,
      payload: user,
    });
  };
};

export {
  goToBookmarks,
  goToHome,
  goToNotifications,
  goToProfile,
  goToCommentsStandalone,
  goToContentStandalone,
  goToTagStandalone,
  goToChannelStandalone,
  goToSearch,
  goToSeriesStandalone,
  goToSeriesList,
  goToSeriesListDrawer,
  goToUserSearch,
  goToAndroidFullscreenVideo,
  goToImageFullscreen,
  goToActivity,
  popScene,
  exitAndroidFullscreenVideo,
  exitImageFullscreen,
  exitActivity,
  initializeSceneOnNavigate,
  navigateUniversalLink,
  goToFeedbackPage,
  goToChannelList,
  goToChannelListDrawer,
};
