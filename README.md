# Traena Mobile App
## Setup and Prerequisites

1. Install the required dependencies

| Name                            | Link                                                                                                                                 |
|:--------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------|
| React Native                    | [React Native](http://facebook.github.io/react-native/docs/getting-started.html#content)                                             |
| iOS Dependencies                | [Xcode](https://facebook.github.io/react-native/docs/getting-started.html#xcode)                                                     |
| Android Dependencies (See Note) | [Android Development Environment](https://facebook.github.io/react-native/docs/getting-started.html#android-development-environment) |
| React Native Dependencies       | [Node & Watchman](https://facebook.github.io/react-native/docs/getting-started.html#node-watchman)                                   |

**Note: I chose to install the `android-sdk` via Homebrew (mentioned in link)**

2. Run `yarn install`
3. Run the `setup-local-env-vars.sh` script from Dom. It's not checked in to git.
4. Populate the environment variables in the `env-config.js` file that's created.
5. Run `npm run createEnvConfig`

## Running the App
### iOS
1. Ensure your current working directory is `traena-app`
1. Run `react-native run-ios`

### Android
1. Open the `traena-app/android` directory in Android Studio
  * **Note: Android Studio should detect a gradle project. Do not ignore the notifications, take action on the gradle popups.**
2. From the **Tools** menu, select **Android** -> **AVD Manager**
3. Select **Create Virtual Device...**
  * **Note: Even if a device is already present, you may still need to create one, the existing device might be using the wrong API**
4. Select the **Nexus 5X** and press **Next**
5. Select the **x86 Images** tab and ensure the the following option is selected:

| Release Name | API Level | ABI    | Target                    |
|:-------------|:----------|:-------|:--------------------------|
| Marshmallow  | 23        | x86_64 | Android 6.0 (Google APIs) |

6. Press **Next**, name the device (or leave name as is) and press **Finish**
7. Press the green play button in the **Actions** column to start the emulator
  * **Note: This is required to start the app!**
8. After the emulator finishes loading, from the **Run** menu, select **Run app**
  * **Note: If you get this error: `Could not get BatchedBridge, make sure your bundle is packaged properly`, run `react-native run-android` from within `/traena-app`**

### Android Issues/General Notes
  * Ensure that you have an Android emulator already running before attempting to start the app
  * `run-android` currently fails to initially launch the app. You must open the app after its installed. The failure to launch is related to this [issue](https://github.com/facebook/react-native/issues/5546) and [pull request](https://github.com/facebook/react-native/pull/12820).
  * In the meantime either open the app after failed launch, or run the project from **Android Studio**.
  * If you are running the app against the local API (`http://localhost:3001`), you need to make a change to `traena-app/env-config.js`. The Android emulator needs to have your laptops IP address in place of `localhost`. e.g. `http://192.168.100.104:3001`
    * **Note: You can get your local IP by running this bash command: `ipconfig getifaddr en0`**

## Debugging
1. Press `⌘-D` inside the iOS simulator to open the hidden menu
1. Select **Start Remote JS Developing** to open the remote debugger in a Chrome window
1. Visit [http://remotedev.io/local/](http://remotedev.io/local/) to access the Redux devtools
1. Install [React devtools](https://github.com/facebook/react-devtools/tree/master/packages/react-devtools) (`npm i -g react-devtools`) for React debugging

## Troubleshooting

1. Network request errors?
  * In Android, make sure in `env-config.js` you have changed `http://localhost:3001` to `http://{your-local-network-ip-address}:3001` e.g. `http://192.168.1.104:3001`

# React Native Version
The project is setup to run using react-native **0.43.3**. This was the April 2017 release by the react-native team. We should attempt and upgrade at least every couple of months.
