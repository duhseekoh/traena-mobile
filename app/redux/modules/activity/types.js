// @flow

type Choice = {
  id: string,
  choiceText: string,
  correct: boolean,
};

type PollChoice = {
  id: string,
  choiceText: string,
}

type MultipleChoiceQuestion = {
  id: string,
  questionText: string,
  choices: Choice[],
  summary?: string,
};

// would also include additional question types
type Question = MultipleChoiceQuestion;

type QuestionSet = {
  questions: Question[],
  order: string[]
};

type Poll = {
  title: string,
  choices: PollChoice[],
}

export const ACTIVITY_TYPES = {
  QUESTION_SET: 'QuestionSet',
  POLL: 'Poll',
};

export type ActivityType = $Values<typeof ACTIVITY_TYPES>;

type Activity = {
  id: number,
  contentId: number,
  version: number,
  type: ActivityType,
  body: QuestionSet | Poll,
  order: number,
  deleted: boolean,
};

type PollActivity = {
  id: number,
  contentId: number,
  version: number,
  type: ActivityType,
  body: Poll,
  order: number,
  deleted: boolean,
};

type ChoiceResult = {
  count: number,
  id: string,
  percent: number,
};

type ActivityResults = {
  activity: Activity,
  count: number,
  choiceResults: {[choiceId: string]: ChoiceResult}
};

export type { Activity, Question, Poll, PollActivity, ActivityResults } ;
