// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  goToBookmarks,
  goToCommentsStandalone,
  // goToChannels,
  goToChannelStandalone,
  goToContentStandalone,
  goToHome,
  goToNotifications,
  goToProfile,
  goToSearch,
  goToTagStandalone,
  // goToSeries,
  goToSeriesStandalone,
  goToChannelList,
  goToChannelListDrawer,
  goToSeriesList,
  goToSeriesListDrawer,
} from 'app/redux/modules/navigation/actions';
import type { Dispatch } from 'app/redux/constants';
import type { Channel } from 'app/redux/modules/channel/types';

export type WithNavigationProps = {
  goToBookmarks: () => any,
  goToHome: () => any,
  goToNotifications: () => any,
  goToProfile: () => any,
  goToSearch: Function,
  goToTagStandalone: Function,
  goToChannelList: Function,
  goToChannelListDrawer: Function,
  goToCommentsStandalone: Function,
  goToContentStandalone: Function,
  goToChannelStandalone: Function,
  goToSeriesStandalone: Function,
  goToSeriesList: Function,
  goToSeriesListDrawer: Function,
};

function withNavigation(WrappedComponent: any) {
  const wrapped = class extends Component<WithNavigationProps> {
    displayName: 'WithNavigationWrapper';

    render() {
      return (
        <WrappedComponent
          {...this.props}
        />
      );
    }
  };

  return connect(null, (dispatch: Dispatch) => ({
    goToBookmarks: () => dispatch(goToBookmarks()),
    goToSearch: (searchText: string) => dispatch(goToSearch(searchText)),
    goToTagStandalone: (tag: string) => dispatch(goToTagStandalone(tag)),
    goToCommentsStandalone: (props: Object) =>
      dispatch(goToCommentsStandalone(props)),
    goToContentStandalone: (contentId: number) =>
      dispatch(goToContentStandalone(contentId)),
    goToChannelStandalone: (channel: Channel) =>
      dispatch(goToChannelStandalone(channel)),
    goToNotifications: () =>
      dispatch(goToNotifications()),
    goToProfile: () =>
      dispatch(goToProfile()),
    goToSeriesStandalone: (id: number, title: string, description: string) =>
      dispatch(goToSeriesStandalone(id, title, description)),
    goToHome: () => dispatch(goToHome()),
    goToChannelList: () => dispatch(goToChannelList()),
    goToChannelListDrawer: () => dispatch(goToChannelListDrawer()),
    goToSeriesList: () => dispatch(goToSeriesList()),
    goToSeriesListDrawer: () => dispatch(goToSeriesListDrawer()),
  }))(wrapped);
}

export default withNavigation;
