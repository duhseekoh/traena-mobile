// @flow
import React, { Component } from 'react';
import {
  View,
  TouchableWithoutFeedback,
  StyleSheet,
  Animated,
  Image,
  LayoutAnimation,
  ScrollView,
} from 'react-native';
import {
  TRATextStrong,
  TRAText,
  FeatherIcon,
} from 'app/components';
import ButtonQuiz from './ButtonQuiz';
import ButtonNext from './ButtonNext';
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import type { Question } from 'app/redux/modules/activity/types';

type Props = {
  style?: ViewStyleProp,
  currentQuestionIndex: number,
  question: Question,
  questionIndex: number,
  totalQuestions: number,
  setCurrentQuestion: (quesitonIndex: number) => void,
  setCorrectFirstTry: (questionIndex: number) => void,
  scrollToNext: () => void,
};

type State = {
    selectedChoices: string[],
    cardFlipped: boolean,
    selectedChoiceId?: string,
    answeredCorrectly: boolean,
    correctChoiceId: ?string,
    incorrectChoiceIds: string[],
    canFlip: boolean,
    firstTry: boolean,
    flipTimeout?: TimeoutID,
};

class MultipleChoiceQuestion extends Component<Props, State> {
  backInterpolate: Animated.Interpolation;
  frontInterpolate: Animated.Interpolation;
  animatedValue: Animated.Value;
  fadeInValue: Animated.Value;
  fadeOutValue: Animated.Value;
  backOpacity: Animated.Interpolation;
  frontOpacity: Animated.Interpolation;

  constructor(props: Props) {
    super(props);
    this.state = {
        selectedChoices: [],
        cardFlipped: false,
        correctChoiceId: this._getCorrectChoiceId(),
        incorrectChoiceIds: this._getIncorrectChoiceIds(),
        answeredCorrectly: false,
        canFlip: false,
        firstTry: true,
    };
  }

  componentWillMount() {
    this.animatedValue = new Animated.Value(0);
    this.fadeInValue = new Animated.Value(0);
    this.fadeOutValue = new Animated.Value(1);
    this.frontInterpolate = this.animatedValue.interpolate({
      inputRange: [0,180],
      outputRange: ['0deg', '180deg']
    });
    this.backInterpolate = this.animatedValue.interpolate({
      inputRange: [0,180],
      outputRange: ['160deg', '360deg']
    });

    // android doesn't support backfaceVisibility. instead we
    // are giving the face that shouldn't be seen 0 opacity.
    this.backOpacity = this.animatedValue.interpolate({ inputRange: [89, 90], outputRange: [0, 1] })
    this.frontOpacity = this.animatedValue.interpolate({ inputRange: [89, 90], outputRange: [1, 0] })
  }

  _getCorrectChoiceId = () => {
    const correctChoice = this.props.question.choices.find(c => c.correct);
    return correctChoice ? correctChoice.id : null;
  }

  _getIncorrectChoiceIds = () => {
    return this.props.question.choices
      .filter(c => c.correct)
      .map(c => c.id);
  }

  _validateChoice = (questionIndex, choiceId) => {
    LayoutAnimation.linear();
    const answeredCorrectly = this.state.correctChoiceId === choiceId;
    const selectedChoices = answeredCorrectly ?
      [...this.state.incorrectChoiceIds]
      : [...this.state.selectedChoices, choiceId];

    if(this.state.firstTry && answeredCorrectly) {
      this.props.setCorrectFirstTry(questionIndex);
    }

    if(this.state.flipTimeout) {
      clearTimeout(this.state.flipTimeout);
    }

    if(answeredCorrectly) {
      requestAnimationFrame(() => {
        Animated.sequence([
          Animated.timing(this.fadeOutValue, {
            toValue: 0,
            duration: 650,
          }),
          Animated.timing(this.fadeInValue, {
            toValue: 1,
            duration: 650,
          }),
        ]).start();
      });

      if(this.props.currentQuestionIndex + 1 < this.props.totalQuestions) {
        const newIndex = this.props.currentQuestionIndex + 1;
        this.props.setCurrentQuestion(newIndex);
      } else {
        this.props.setCurrentQuestion(this.props.totalQuestions);
      }
    } else {
      const newTimeout = setTimeout(() => {
        this._showCardFront();
      }, 1600);

      this.setState({
        flipTimeout: newTimeout
      });
    }

    this.setState({
      selectedChoices,
      answeredCorrectly,
      selectedChoiceId: choiceId,
      canFlip: true,
      firstTry: this.state.firstTry && answeredCorrectly,
    });

    this._showCardBack();
  }

  _showCardBack = () => {
    Animated.timing(this.animatedValue, {
      toValue: 180,
      duration: 650,
    }).start();
    this.setState({
      cardFlipped: true,
    });
  }

  _showCardFront = () => {
    Animated.timing(this.animatedValue, {
      toValue: 0,
      duration: 650,
    }).start();
    this.setState({
      cardFlipped: false,
    });
  }

  _flipCard = () => {
    if(!this.state.canFlip) {
      return;
    }

    if(this.state.cardFlipped) {
      this._showCardFront();
    } else {
      this._showCardBack();
    }
  }

  _renderCorrectBack(summary) {
    if(summary) {
      return (<View>
        <View style={{flexDirection: 'row'}}>
          <FeatherIcon style={[styles.correctIcon, { marginLeft: 0 }]} name='check-circle' />
          <TRATextStrong style={styles.correctText}>You got it right!</TRATextStrong>
        </View>
        <TRAText style={styles.cardText}>{summary}</TRAText>
      </View>);
    } else {
      return (<View style={{flex: 1}}>
           <TRATextStrong style={styles.correctTextAlt}>You got it right!</TRATextStrong>
           <View style={styles.imageContainer}>
             <Image style={[styles.summaryImage]} source={require('app/images/activity-correct.png')} />
           </View>
         </View>);
    }
  }

  _renderIncorrectBack() {
    return (<View style={{flex: 1}}>
         <TRATextStrong style={[styles.incorrectText]}>Whoops! Thats not it.</TRATextStrong>
         <View style={styles.imageContainer}>
           <Image style={[styles.summaryImage]} source={require('app/images/activity-incorrect.png')} />
         </View>
       </View>);
  }


  render() {
    const { question, questionIndex } = this.props;
    const selectedChoice = this.state.selectedChoiceId && question.choices.find(c => c.id === this.state.selectedChoiceId);

    const frontAnimatedStyle = {
      transform: [
        { rotateY: this.frontInterpolate }
      ],
      opacity: this.frontOpacity,
    };
    const backAnimatedStyle = {
      transform: [
        { rotateY: this.backInterpolate }
      ],
      opacity: this.backOpacity,
    };
    const questionBoxStyle = this.state.answeredCorrectly ? {justifyContent: 'space-between'} : {};
    // only show question text when the front of the card is showing and still going through
    // incorrect answers. otherwise, a long question makes it so the back of the card is really tall
    const displayQuestionText = !this.state.cardFlipped || !this.state.answeredCorrectly;
    return (
      <View style={styles.container}>
        {/* Vertical scrolling for long questions */}
        <ScrollView
          style={styles.questionScroll}
          bounce={false}
          showsVerticalScrollIndicator={false}
          >
          <View style={styles.questionContainer}>
            <TouchableWithoutFeedback onPress={() => this._flipCard()}>
              <View>
                <Animated.View style={[frontAnimatedStyle, styles.questionCard]}>
                  <TRATextStrong>Question {questionIndex + 1}</TRATextStrong>
                  { displayQuestionText &&
                    <TRAText style={styles.cardText}>{question.questionText}</TRAText>
                  }
                </Animated.View>
                <Animated.View style={[backAnimatedStyle, styles.questionCard, styles.questionCardBack]}>
                  {selectedChoice && this.state.answeredCorrectly && this._renderCorrectBack(question.summary)}
                  {selectedChoice && !this.state.answeredCorrectly && this._renderIncorrectBack()}
                </Animated.View>
              </View>
            </TouchableWithoutFeedback>
            <View style={[{flex: 1}, questionBoxStyle]}>
            {
              // After the user gets the correct answer, this shows up. Shows the correct answer on its own.
              this.state.answeredCorrectly && selectedChoice &&
              <Animated.View style={[styles.buttonQuizHolder, { opacity: this.fadeInValue }]}>
                <ButtonQuiz
                  text={selectedChoice.choiceText}
                  onPress={() => {}}
                  focused={true}
                  disabled={true}
                  loading={false} />
              </Animated.View>
            }
            {
              // Display all of the buttons until the user finds the correct choice
              !this.state.answeredCorrectly && question.choices.map((choice) => {
                  return (
                    <Animated.View
                      style={[styles.buttonQuizHolder, { opacity: this.fadeOutValue }]}
                      key={choice.id}>
                      <ButtonQuiz
                        text={choice.choiceText}
                        onPress={() => this._validateChoice(questionIndex, choice.id)}
                        focused={this.state.answeredCorrectly && choice.correct}
                        disabled={(this.state.answeredCorrectly && !choice.correct) || this.state.selectedChoices.includes(choice.id)}
                        loading={false} />
                    </Animated.View>);
              })
            }
            </View>
          </View>
        </ScrollView>
        {/* Sits fixed on top of the vertical scrollview at the bottom of the screen */}
        {this.state.answeredCorrectly &&
          <Animated.View
            style={[styles.bottomSeat, {opacity: this.fadeInValue}]}>
            <ButtonNext
              text={'Next'}
              icon={<FeatherIcon
                style={styles.nextIcon}
                name={'arrow-right'}
                color={styleVars.color.traenaOne} />}
              onPress={this.props.scrollToNext}
              focused={false}
              disabled={false}
              loading={false} />
          </Animated.View>
        }
      </View>
    )
  }
}

export default MultipleChoiceQuestion;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
  },
  questionScroll: {
    flex: 1,
  },
  questionContainer: {
    flex: 1,
    alignSelf: 'stretch',
  },
  questionCard: {
    marginHorizontal: 20,
    marginVertical: 15,
    paddingVertical: 20,
    paddingHorizontal: 28,
    minHeight: 210,
    borderRadius: 4,
    borderColor: styleVars.color.lightGrey,
    borderWidth: 1,
    backfaceVisibility: 'hidden',
  },
  imageContainer: {
    flex: 1,
    marginHorizontal: -28,
  },
  summaryImage: {
    resizeMode: 'contain',
    width: null,
    flex: 1,
    height: null,
  },
  questionCardBack: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  nextIcon: {
    position: 'absolute',
    right: 21,
    paddingVertical: 17,
  },
  correctIcon: {
    fontSize: styleVars.fontSize.xlarge,
    color: styleVars.color.traenaPrimaryGreen,
    marginHorizontal: 10,
  },
  correctText: {
    color: styleVars.color.traenaPrimaryGreen,
    lineHeight: styleVars.fontSize.xlarge,
  },
  correctTextAlt: {
    color: styleVars.color.traenaPrimaryGreen,
    fontSize: styleVars.fontSize.large,
    textAlign: 'center',
  },
  incorrectText: {
    fontSize: styleVars.fontSize.large,
    textAlign: 'center',
  },
  cardText: {
    marginTop: 15,
    fontSize: styleVars.fontSize.large,
  },
  buttonQuizHolder: {
    paddingHorizontal: 20,
    paddingTop: 12,
  },
  bottomSeat: {
    position: 'absolute',
    bottom: 12,
    left: 20,
    right: 20,
  }
});
