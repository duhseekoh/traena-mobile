// @flow
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import styleVars from 'app/style/variables';
import type { Channel } from 'app/redux/modules/channel/types';
import type { DailyActionContentItem, TrainingContentItem, ImageContentItem } from 'app/redux/modules/content/types';
import {
  ButtonBookmark,
  TRAText,
  TRATextStrong,
  LikesCounter,
  CommentsCounter,
  SocialTags,
  TRATextParsed,
  FeatherIcon,
} from 'app/components';
import ContentMeta from './ContentMeta';
import withNavigation from '../../hoc/withNavigation';

type Props = {
  contentItem: DailyActionContentItem |
    TrainingContentItem |
    ImageContentItem,
  onLikePressed: (updatedDoesLike: boolean) => void,
  onCommentPressed: () => void,
  onBookmarkPressed: () => void,
  onCompletePressed: () => void,
  goToChannelStandalone: (channel: Channel) => void,
  goToSeriesStandalone: (id: number, title: string, description: string) => void,
  goToSearch: (search: string) => void,
  children: React$Node,
  isDetailView: boolean,
};

type State = {
  markedComplete: boolean,
}

class GenericContentItem extends Component<Props, State> {
  state: State;
  constructor(props: Props) {
    super(props);
    // content item is marked as complete when navigating away
    // from this view. only dispatch action if it is marked as
    // complete when leaving. cant be un-completed after leaving.
    this.state = {
      markedComplete: false,
    };
  }

  componentWillUnmount() {
    if(this.state.markedComplete) {
      this.props.onCompletePressed();
    }
  }

  _renderDetail() {
    const { onCommentPressed, children, goToSearch, goToChannelStandalone, goToSeriesStandalone } = this.props;
    const { title, social, author, text, activityCount, isCompleted, isSeries, channel, series } = this.props.contentItem;
    const { likes, comments, tags } = social;
    const { markedComplete } = this.state;
    const onPressAuthor = goToSearch ? goToSearch.bind(null, `${author.firstName} ${author.lastName}`) : () => {};
    const onPressOrg = goToSearch ? goToSearch.bind(null, author.organization.name) : () => {};
    const onPressTitle = goToChannelStandalone ? goToChannelStandalone.bind(null, channel) : () => {};
    let onPressSeries;
    if(series) {
      onPressSeries = goToSeriesStandalone ? goToSeriesStandalone.bind(null, series.id, series.title, series.description) : () => {};
    }
    return (
      <View style={styles.detailItemContainer}>
        <View>
          {children}
        </View>
        <View style={styles.detailContentMeta}>
          <ContentMeta
            title={channel.name}
            onPressTitle={onPressTitle}
            isCompleted={isCompleted}
            isSeries={isSeries}
            hasActivity={activityCount > 0}/>
        </View>
        <View style={styles.detailContent}>
          {series && <TouchableWithoutFeedback onPress={onPressSeries}>
            <TRATextStrong style={styles.seriesTitle}>
              {series.title.toUpperCase()}
            </TRATextStrong>
          </TouchableWithoutFeedback>}
          <TRATextStrong style={styles.detailTitle}>
            {title}
          </TRATextStrong>
          <View style={styles.userTextHolder}>
            <TRAText onPress={onPressAuthor} numberOfLines={1} ellipsizeMode={'tail'} style={styles.authorName}>
              by {author.firstName} {author.lastName}
            </TRAText>
            <TRAText onPress={onPressOrg} numberOfLines={1} ellipsizeMode={'tail'} style={styles.organizationName}>
              ,{' '}{author.organization.name}
            </TRAText>
          </View>
          <TRATextParsed style={styles.bodyText}>
            {text}
          </TRATextParsed>

          <SocialTags isDetailView={true} tags={tags} style={styles.socialTagsBelowContent} />

          <View style={styles.socialBar}>
            <View style={styles.socialBarLeft}>
              <LikesCounter count={likes.count} isActive={likes.doesUserLike} style={styles.socialCounter}
                onPress={this._onSocialBarLikesPressed}
              />
              <CommentsCounter
                count={comments.count}
                style={styles.socialCounter}
                onPress={onCommentPressed}
              />
            </View>
            <View style={styles.socialBarRight}>
              {isCompleted &&
                <View style={styles.completeText}>
                  <FeatherIcon
                    name='check'
                    fontSize={styleVars.iconSize.small}
                    color={styleVars.color.traenaOne}
                  />
                  <TRATextStrong>Completed</TRATextStrong>
                </View>
              }
              {!isCompleted &&
                <TouchableOpacity
                  onPress={() => this.setState({ markedComplete: !markedComplete })}
                >
                  {!markedComplete &&
                    <View style={styles.completeText}>
                      <FeatherIcon
                        name='check'
                        fontSize={styleVars.iconSize.small}
                        color={styleVars.color.mediumGrey}
                      />
                      <TRATextStrong>Mark Post Complete</TRATextStrong>
                    </View>}
                  {markedComplete &&
                    <View style={styles.completeText}>
                      <FeatherIcon
                        name='check'
                        fontSize={styleVars.iconSize.small}
                        color={styleVars.color.traenaOne}
                      />
                      <TRATextStrong>Completed</TRATextStrong>
                    </View>}
                </TouchableOpacity>
              }
            </View>
          </View>
        </View>
      </View>
    );
  }

  _renderCard() {
    const { goToSearch, goToChannelStandalone, onBookmarkPressed, onCommentPressed, goToSeriesStandalone, children } = this.props;
    const { title, social, author, isBookmarked, isCompleted, isSeries, activityCount, text, channel, series } = this.props.contentItem;
    const { likes, comments, tags } = social;
    const onPressAuthor = goToSearch ? goToSearch.bind(null, `${author.firstName} ${author.lastName}`) : () => {};
    const onPressTitle = goToChannelStandalone ? goToChannelStandalone.bind(null, channel) : () => {};
    let onPressSeries;
    if(series) {
      onPressSeries = goToSeriesStandalone ? goToSeriesStandalone.bind(null, series.id, series.title, series.description) : () => {};
    }
    return (
      <View style={styles.itemContainer}>
        <View style={styles.contentMetaContainer}>
          <ContentMeta
            title={channel.name}
            onPressTitle={onPressTitle}
            isCompleted={isCompleted}
            isSeries={isSeries}
            hasActivity={activityCount > 0}/>
        </View>

        <View>
          {children}
        </View>
        <View style={styles.content}>
          {series && <TouchableWithoutFeedback onPress={onPressSeries}>
            <TRATextStrong style={styles.seriesTitle} numberOfLines={1} ellipsizeMode={'tail'}>
              {series.title.toUpperCase()}
            </TRATextStrong>
          </TouchableWithoutFeedback>}
          <TRATextStrong style={styles.title} numberOfLines={2} ellipsizeMode={'tail'}>
            {title}
          </TRATextStrong>
          <View style={styles.userTextHolder}>
            <TRAText onPress={onPressAuthor} numberOfLines={1} ellipsizeMode={'tail'}>
              by {author.firstName} {author.lastName}
            </TRAText>
          </View>
          <TRATextParsed style={styles.bodyText} numberOfLines={3} ellipsizeMode={'tail'}>
            {text}
          </TRATextParsed>

          <SocialTags isDetailView={false} tags={tags} style={styles.socialTagsBelowContent} />

          <View style={styles.socialBar}>
            <View style={styles.socialBarLeft}>
              <LikesCounter count={likes.count} isActive={likes.doesUserLike} style={styles.socialCounter}
                onPress={this._onSocialBarLikesPressed}
              />
              <CommentsCounter count={comments.count} style={styles.socialCounter}
                onPress={onCommentPressed}
              />
            </View>

            <View style={styles.socialBarRight}>
              <ButtonBookmark
                onPress={onBookmarkPressed}
                isBookmarked={isBookmarked} />
            </View>
          </View>
        </View>
      </View>
    );
  }
  render() {
    let content;
    if(this.props.isDetailView) {
      content = this._renderDetail();
    }
    else {
      content = this._renderCard();
    }
    return (
      <View>
        {content}
      </View>
    );
  }

  _onSocialBarLikesPressed = () => {
    this.props.onLikePressed(!this.props.contentItem.social.likes.doesUserLike);
  }
}

export default withNavigation(GenericContentItem);

const contentMargin = 10,
  contentPadding = 8
const styles = StyleSheet.create({
  detailItemContainer: {
    backgroundColor: styleVars.color.white,
    paddingBottom: 40,
  },
  itemContainer: {
    backgroundColor: styleVars.color.white,
    padding: contentPadding,
    marginHorizontal: contentMargin,
    marginVertical: 2,
    overflow: 'visible',
    borderRadius: 4,
    shadowColor: styleVars.color.mediumGrey2,
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 5,
    elevation: 2,
  },
  content: {
    paddingHorizontal: 8,
    marginTop: 5,
  },
  detailContent: {
    paddingHorizontal: 18,
    marginTop: 5,
  },
  contentMetaContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 8,
    borderBottomWidth: 1,
    borderBottomColor: styleVars.color.lightGrey,
    marginHorizontal: -8,
    paddingVertical: 10,
  },
  detailContentMeta: {
    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 20
  },
  title: {
    fontSize: styleVars.fontSize.large,
  },
  seriesTitle: {
    fontSize: styleVars.fontSize.small,
    color: styleVars.color.mediumGrey,
    fontFamily: styleVars.fontFamily.bold,
    letterSpacing: 1.8,
  },
  detailTitle: {
    fontSize: styleVars.fontSize.xlarge,
  },
  socialTagsBelowContent: {
    marginTop: 5,
  },
  socialBar: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
  },
  socialBarLeft: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  socialBarRight: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  socialCounter: {
    marginRight: 10,
    minWidth: 40,
  },
  bodyText: {
    marginTop: 22,
  },
  completeText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  userTextHolder: {
    flexDirection: 'row',
    display: 'flex',
    justifyContent: 'flex-start',
    width: '100%',
  },
  authorName: {
    maxWidth: '75%',
  },
  organizationName: {
    flexGrow: 1,
    flexBasis: '25%',
  },
});
