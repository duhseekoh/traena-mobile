import moment from 'moment';
import jwtSimple from 'jwt-simple';

describe('auth selectors', () => {
  beforeEach(() => {
    jest.resetModules();
  });

  describe('authHeadersSelector', () => {
    let authHeadersSelector;
    beforeEach(() => {
      authHeadersSelector = require('./selectors').authHeadersSelector;
    });

    it('will return an object with an Authorization entry', () => {
      const state = {
        auth: {
          accessToken: 'sample-jwt',
        },
      };
      const authHeaders = authHeadersSelector(state);
      expect(authHeaders['Authorization']).toEqual('Bearer sample-jwt');
    });

    it('authHeadersSelector will return an empty object when no jwt is found', () => {
      const state = {
        auth: {},
      };
      const authHeaders = authHeadersSelector(state);
      expect(authHeaders['Authorization']).toBeUndefined();
      expect(Object.keys(authHeaders).length).toEqual(0);
    });
  });

  describe('forgotPasswordUrlSelector', () => {
    it('will return a useable password url when all config is supplied', () => {
      jest.doMock('app/config', () => ({
        auth0: {
          clientId: '123',
          domain: 'dummyauth0.com',
          dbConnectionName: 'dummyuserdb',
        },
      }));
      const forgotPasswordUrlSelector = require('./selectors').forgotPasswordUrlSelector;
      const url = forgotPasswordUrlSelector();
      expect(url).toEqual('https://dummyauth0.com/login?client=123&connection=dummyuserdb&popup=false&forgot_password_only_view=1');
    });
  });

  describe('isLoginInProgressSelector', () => {
    const isLoginInProgressSelector = require('./selectors').isLoginInProgressSelector;

    it('will return true when request is in progress', () => {
      const state = {
        auth: {
          loginRequestState: 'IN_PROGRESS',
        },
      };
      const result = isLoginInProgressSelector(state);
      expect(result).toEqual(true);
    });

    it('will return true when request is anything but in progress', () => {
      const state = {
        auth: {
          loginRequestState: 'SUCCESS',
        },
      };
      const result = isLoginInProgressSelector(state);
      expect(result).toEqual(false);

      state.auth.loginRequestState = 'UNKNOWN_FAILURE';
      const result2 = isLoginInProgressSelector(state);
      expect(result2).toEqual(false);

      state.auth.loginRequestState = undefined;
      const result3 = isLoginInProgressSelector(state);
      expect(result3).toEqual(false);
    });
  });

  describe('loginErrorMessageSelector', () => {
    const loginErrorMessageSelector = require('./selectors').loginErrorMessageSelector;

    it('will return an error message for bad credentials', () => {
      const state = {
        auth: {
          loginRequestState: 'BAD_CREDENTIALS_FAILURE',
        },
      };
      const message = loginErrorMessageSelector(state);
      expect(message).toEqual('You entered an invalid username or password. Please try again.');
    });

    it('will return an error message for an unknown failure', () => {
      const state = {
        auth: {
          loginRequestState: 'UNKNOWN_FAILURE',
        },
      };
      const message = loginErrorMessageSelector(state);
      expect(message).toEqual('A network or system error has occurred. Make sure you are connected to the internet.');
    });

    it('will return no message for any non-failure state', () => {
      const state = {
        auth: {
          loginRequestState: 'SUCCESS',
        },
      };
      const message = loginErrorMessageSelector(state);
      expect(message).toBeUndefined();

      state.auth.loginRequestState = 'IN_PROGRESS';
      const message2 = loginErrorMessageSelector(state);
      expect(message2).toBeUndefined();

      state.auth.loginRequestState = 'anything else';
      const message3 = loginErrorMessageSelector(state);
      expect(message3).toBeUndefined();
    });
  });

  function generateJwtWithExpiration(expDate: moment$Moment) {
    const expMilliseconds: number = expDate.unix();
    return jwtSimple.encode({ exp: expMilliseconds }, 'anysecret');
  }

  describe('isJwtValidSelector', () => {
    const isJwtValidSelector = require('./selectors').isJwtValidSelector;

    it('will return false for jwts with an expiration of right now', () => {
      const jwt = generateJwtWithExpiration(moment());
      const state = {
        auth: {
          accessToken: jwt,
        },
      };
      const result = isJwtValidSelector(state);
      expect(result).toEqual(false);
    });

    it('will return true for jwts with expiration times in the future', () => {
      const jwt = generateJwtWithExpiration(moment().add(1, 'days'));
      const state = {
        auth: {
          accessToken: jwt,
        },
      };
      const result = isJwtValidSelector(state);
      expect(result).toEqual(true);
    });

    it('will return true for jwts with expiration times in the future', () => {
      const jwt = generateJwtWithExpiration(moment().add(5, 'seconds'));
      const state = {
        auth: {
          accessToken: jwt,
        },
      };
      const result = isJwtValidSelector(state);
      expect(result).toEqual(true);
    });

    it('will return false for corrupt jwts', () => {
      const jwt = '32321.321321.321321';
      const state = {
        auth: {
          accessToken: jwt,
        },
      };
      const result = isJwtValidSelector(state);
      expect(result).toEqual(false);
    });

    it('will return false for no jwt', () => {
      const state = {
        auth: {},
      };
      const result = isJwtValidSelector(state);
      expect(result).toEqual(false);
    });

  });
});
