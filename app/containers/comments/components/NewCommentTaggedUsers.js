// @flow
import React from 'react';
import {
  TouchableOpacity,
  View,
  StyleSheet,
} from 'react-native';
import {
  UserFullName,
  TRAText,
  FeatherIcon,
} from 'app/components';
import styleVars from 'app/style/variables';
import type { User } from 'app/redux/modules/user/types';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style?: ViewStyleProp,
  taggedUsers: User[],
  onPressRemoveUser: (userToRemove: User) => void,
};

const NewCommentTaggedUsers = ({ taggedUsers, onPressRemoveUser, style }: Props) => {

  return (
    <View style={[styles.container, style]}>
      {
        taggedUsers.map((user) => {
          return (
            <TouchableOpacity key={user.id} style={styles.userButton}
              onPress={() => onPressRemoveUser(user)}
              >
              <FeatherIcon style={styles.removeIcon} name='x' />
              <TRAText style={styles.nameText}>
                {' '}<UserFullName user={user} />
              </TRAText>
            </TouchableOpacity>
          );
        })
      }
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
  },
  userButton: {
    flexDirection: 'row',
    marginHorizontal: 5,
    marginVertical: 2,
    padding: 3,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: styleVars.color.mediumGrey,
    backgroundColor: styleVars.color.white,
  },
  nameText: {
    fontSize: styleVars.fontSize.small,
    color: styleVars.color.traenaOne,
  },
  removeIcon: {
    color: styleVars.color.traenaOne,
  },
});

export default NewCommentTaggedUsers;
