import { createSelector } from 'reselect';

const STATE_KEY = 'bookmark';
const stateFromRoot = (state) => state[STATE_KEY];

const bookmarkContentIdsSelector = createSelector([
  stateFromRoot
], (bookmarkState) => {
  return bookmarkState.bookmarkContentIds;
});

export {
  STATE_KEY,
  bookmarkContentIdsSelector,
};
