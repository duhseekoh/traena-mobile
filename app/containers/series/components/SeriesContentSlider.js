// @flow
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
} from 'react-native';
import {
  FeatherIcon,
  TRATextStrong
} from 'app/components';
import ContentPreview from 'app/components/content-preview/ContentPreview';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import styleVars from 'app/style/variables';
import type { ContentItem } from 'app/redux/modules/content/types';

type Props = {
  contentItems: ContentItem[],
  style?: ViewStyleProp,
  goToContentStandalone: (contentId: number) => void,
  goToSeriesStandalone: (id: number, title: string, description: string) => void,
};

type State = {
  galleryWidth: ?number,
};

class SeriesContentSlider extends Component<Props, State> {
  scrollX = new Animated.Value(0);

  constructor(props: Props) {
    super(props);

    this.state = {
      galleryWidth: null,
    };
  }

  onScrollViewLayout = ({nativeEvent}: *) => {
    // Don't update if width is the same
    if (nativeEvent.layout.width === this.state.galleryWidth) {
      return;
    }

    this.setState({
      galleryWidth: nativeEvent.layout.width,
    });
  }

  renderPager(count: number) {
    if(!this.state.galleryWidth) {
      return <View></View>;
    }

    const position = Animated.divide(this.scrollX, this.state.galleryWidth);

    let pages = [];
    for(let i = 0; i < count; i++) {
      const opacity = position.interpolate({
        inputRange: [i - 1, i, i + 1],
        outputRange: [0.3, 1, 0.3],
        extrapolate: 'clamp'
      });
     pages.push(
       <Animated.View
         key={i}
         style={{ opacity, height: 6, width: 6, backgroundColor: styleVars.color.darkGrey4, margin: 4, borderRadius: 3 }}
       />);
    }
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
        {pages}
      </View>);
  }

  render() {
    const { contentItems, goToContentStandalone, goToSeriesStandalone } = this.props;
    return (
      <View style={{flex: 1}}>
        { contentItems && contentItems.length > 0 &&
          <View style={{flex: 1}}>
            <ScrollView
              onScroll={Animated.event(
                [{ nativeEvent: { contentOffset: { x: this.scrollX } } }]
              )}
              contentContainerStyle={styles.slideContainer}
              style={styles.slider}
              showsHorizontalScrollIndicator={false}
              horizontal
              bounces={true}
              scrollEventThrottle={16}
              onLayout={this.onScrollViewLayout}
              pagingEnabled>
              { // display the preview cards once the scrollview has determined page width
                this.state.galleryWidth && contentItems.map((contentItem) => {
                  return (
                    <View key={contentItem.id} style={{flex: 1, width: this.state.galleryWidth, paddingHorizontal: 10, backgroundColor: styleVars.color.white}}>
                      <ContentPreview
                        contentItem={contentItem}
                        onPress={goToContentStandalone}
                        />
                      </View>)
                })
              }
              {this.state.galleryWidth && <TouchableOpacity
                onPress={goToSeriesStandalone}>
                <View style={[{ width: this.state.galleryWidth }, styles.moreSeriesCardWrapper]}>
                  <View style={styles.moreSeriesCard}>
                    <FeatherIcon style={styles.moreSeriesCardIcon} name={'copy'} fontSize={styleVars.iconSize.xlarge}/>
                    <TRATextStrong style={styles.moreSeriesCardText} >All Posts in Series</TRATextStrong>
                  </View>
                </View>
              </TouchableOpacity>}
            </ScrollView>
            {this.renderPager(contentItems.length + 1)}
          </View>
        }
      </View>
    )
  }
}

export default SeriesContentSlider;

const contentPadding = 8;
const styles = StyleSheet.create({
  slideContainer: {
    backgroundColor: styleVars.color.lightGrey,
    justifyContent: 'center',
    alignItems: 'center',
  },
  slider: {
    flex: 1,
  },
  moreSeriesCardWrapper: {
    flex: 1,
    paddingHorizontal: 10,
    backgroundColor: styleVars.color.white
  },
  moreSeriesCard: {
    flex: 1,
    backgroundColor: styleVars.color.traenaOne,
    padding: contentPadding,
    marginVertical: 2,
    borderRadius: 4,
    shadowColor: styleVars.color.mediumGrey2,
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 5,
    elevation: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  moreSeriesCardIcon: {
    color: styleVars.color.white,
    opacity: .5,
  },
  moreSeriesCardText: {
    color: styleVars.color.white,
    marginTop: 15,
    fontSize: styleVars.fontSize.large,
  }
});
