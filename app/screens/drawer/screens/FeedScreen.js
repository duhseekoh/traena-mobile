// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  RefreshControl,
  ScrollView,
  View,
  StyleSheet,
} from 'react-native';
import type {
  NavigationNavigatorProps,
  NavigationStackScreenOptions,
} from 'react-navigation';
import {
  ButtonSearch,
  TraenaIcon,
  FeedButton,
  FeedSpacer,
} from 'app/components';
import {
  isRefreshingSelector,
} from 'app/redux/modules/feed/selectors';
import { userSelector } from 'app/redux/modules/user/selectors';
import { refreshFeed } from 'app/redux/modules/feed/actions';
import { goToSeriesList, goToChannelList } from 'app/redux/modules/navigation/actions';
import type { ContentItem } from 'app/redux/modules/content/types';
import type { User } from 'app/redux/modules/user/types';
import { ROUTES } from 'app/redux/modules/navigation/constants';
import styleVars from 'app/style/variables';
import WhatsNewContainer from 'app/containers/whats-new/WhatsNewContainer';
import BookmarksWidgetContainer from 'app/containers/bookmarks-widget/BookmarksWidgetContainer';
import TrendingTagsContainer from 'app/containers/trending-tags/TrendingTagsContainer';
import SeriesProgressWidgetContainer from 'app/containers/series-progress-widget/SeriesProgressWidgetContainer';

type NavigationStateParams = *;
type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

type Props = {|
  feed: ContentItem[],
  user: User,
  refreshFeed: () => void,
  loadMoreFeed: () => void,
  isLoadingMore: boolean,
  isRefreshing: boolean,
  canLoadMore: boolean,
  displayNoMoreResults: boolean,
|} & NavigationProps &
  *;

class FeedScreen extends Component<Props> {
  static navigationOptions = ({
    navigation,
  }: *): NavigationStackScreenOptions => {
    return {
      headerTitle: (
        <TraenaIcon
          name="traena-text"
          color={styleVars.color.traenaPrimaryGreen}
          fontSize={styleVars.iconSize.xxlarge}
          // to get it centered on android
          style={{ width: '100%', textAlign: 'center' }}
        />
      ),
      headerRight: (
        <ButtonSearch onPress={() => navigation.navigate(ROUTES.SEARCH)} />
      ),
    };
  };

  constructor(props: Props) {
    super(props);
  }

  renderFeedWidgets() {
    return (
      <View style={styles.feedWidgetsWrapper}>
        <WhatsNewContainer />
        <FeedSpacer size={13} />
        <FeedButton
          style={styles.feedButtonStyle}
          backgroundColor={styleVars.color.forestGreen}
          title="Browse Channels"
          onPress={this.props.goToChannelList}
        />
        <FeedSpacer />
        <SeriesProgressWidgetContainer />
        <FeedSpacer size={13} />
        <FeedButton
          style={styles.feedButtonStyle}
          backgroundColor={styleVars.color.forestGreen}
          title="Browse Series"
          onPress={this.props.goToSeriesList}
        />
        <FeedSpacer />
        <TrendingTagsContainer />
        <FeedSpacer />
        <BookmarksWidgetContainer />
      </View>
    );
  }

  render() {
    const {
      refreshFeed,
      isRefreshing = false,
    } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={styles.scrollView}
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={refreshFeed}
            />
          }
          >
          {this.renderFeedWidgets()}
        </ScrollView>

      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: userSelector(state),
    isRefreshing: isRefreshingSelector(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    refreshFeed: () => {
      return dispatch(refreshFeed());
    },
    goToSeriesList: () => {
      return dispatch(goToSeriesList());
    },
    goToChannelList: () => {
      return dispatch(goToChannelList());
    },
  };
};

export { FeedScreen as FeedScreenCmp };
const ConnectedFeedScreen = connect(mapStateToProps, mapDispatchToProps)(
  FeedScreen,
);

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: styleVars.color.white,
  },
  feedWidgetsWrapper: {
    marginBottom: 40,
  },
  feedButtonStyle: {
    marginTop: 15,
    marginBottom: 15,
    marginHorizontal: styleVars.padding.edgeOfScreen,
  },
});

export default ConnectedFeedScreen;
