// @flow
import React, { Component } from 'react';
import {
  SafeAreaView,
} from 'react-native';
import type {
  NavigationNavigatorProps,
  NavigationStackScreenOptions,
} from 'react-navigation';
import CommentsContainer from 'app/containers/comments/CommentsContainer';
import styleVars from 'app/style/variables';

type NavigationStateParams = {
  params: {
    contentId: number,
  },
} & *;

type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

class CommentsScreen extends Component<NavigationProps> {
  static navigationOptions: (*) => NavigationStackScreenOptions = () => ({
    headerTitle: 'COMMENTS',
  })

  render() {
    const { params } = this.props.navigation.state;
    const { contentId } = params;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: styleVars.color.lightGrey5}}>
        <CommentsContainer contentId={contentId} />
      </SafeAreaView>
    );
  }

}

export default CommentsScreen;
