// @flow
import { MODAL_TYPES } from './constants';
import type { ModalProps } from './types';
import type { Dispatch } from 'app/redux/constants';

const SHOW_MODAL = 'SHOW_MODAL';
const showModal = (modalType, modalProps) => {
  return {
    type: SHOW_MODAL,
    payload: {modalType, modalProps}
  }
};

const showInfoModal = (modalProps: ?ModalProps) => {
  return (dispatch: Dispatch) => {
    const defaultModalProps = {
      onPressClose: () => {
        dispatch(hideModal());
      }
    };
    const mergedModalProps = {...defaultModalProps, ...modalProps};
    return dispatch(showModal(MODAL_TYPES.INFO_MODAL, mergedModalProps));
  };
};

const HIDE_MODAL = 'HIDE_MODAL';
const hideModal = () => {
  return {
    type: HIDE_MODAL
  }
};

export {
  showInfoModal,
  SHOW_MODAL,
  hideModal,
  HIDE_MODAL
}
