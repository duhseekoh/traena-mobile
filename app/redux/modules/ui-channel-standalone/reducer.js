// @flow
import _ from 'lodash';
import {
  REFRESH_CHANNEL_CONTENT_REQUEST_STARTED,
  REFRESH_CHANNEL_CONTENT_REQUEST_FAILED,
  LOAD_MORE_CHANNEL_CONTENT_REQUEST_STARTED,
  LOAD_MORE_CHANNEL_CONTENT_REQUEST_FAILED,
  REFRESH_SERIES_REQUEST_STARTED,
  REFRESH_SERIES_REQUEST_FAILED,
  LOAD_MORE_SERIES_REQUEST_STARTED,
  LOAD_MORE_SERIES_REQUEST_FAILED,
  LOADED_MORE_CHANNEL_CONTENT,
  REFRESHED_CHANNEL_CONTENT,
  REFRESHED_SERIES,
  LOADED_MORE_SERIES,
} from './actions';
import { REQUEST_STATUSES, type RequestStatus } from 'app/redux/constants';

export type ReduxStateChannelStandalone = {
  uiSlices: {
    [channelId: number]: {
      content: {
        loadingStatus: RequestStatus,
        refreshingStatus: RequestStatus,
        contentIds: number[],
        currentPage: number,
        canLoadMore: boolean,
      },
      series: {
        seriesIds: number[],
        loadingStatus: RequestStatus,
        refreshingStatus: RequestStatus,
        currentPage: number,
        canLoadMore: boolean,
      }
    }
  }
};

const ACTION_HANDLERS = {
  [REFRESHED_SERIES]: (state, action): ReduxStateChannelStandalone => {
    const { seriesPage, channelId } = action.payload;
    const { ids, totalPages, number } = seriesPage;
    const series = !!state.uiSlices[channelId] && !!state.uiSlices[channelId].series ? {...state.uiSlices[channelId].series} : {};
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [channelId]: {
          ...state.uiSlices[channelId],
          series: {
            ...series,
            seriesIds: ids,
            canLoadMore: number < totalPages - 1,
            currentPage: number,
            refreshingStatus: REQUEST_STATUSES.COMPLETE,
          }
        }
      }
    }
  },
  [LOADED_MORE_SERIES]: (state, action) => {
    const { seriesPage, channelId } = action.payload;
    const { ids, totalPages, number } = seriesPage;
    const series = !!state.uiSlices[channelId] && !!state.uiSlices[channelId].series ? {...state.uiSlices[channelId].series} : {};
    const mergedSeriesIds = _.uniq([...state.uiSlices[channelId].series.seriesIds, ...ids]);
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [channelId]: {
          ...state.uiSlices[channelId],
          series: {
            ...series,
            seriesIds: mergedSeriesIds,
            canLoadMore: number < totalPages - 1,
            currentPage: number,
            loadingStatus: REQUEST_STATUSES.COMPLETE,
          }
        }
      }
    }
  },
  [REFRESHED_CHANNEL_CONTENT]: (state, action) => {
    const { ids, totalPages, number, channelId } = action.payload;
    const content = !!state.uiSlices[channelId] && !!state.uiSlices[channelId].content ? {...state.uiSlices[channelId].content} : {};
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [channelId]: {
          ...state.uiSlices[channelId],
          content: {
            ...content,
            contentIds: ids,
            canLoadMore: number < totalPages - 1,
            currentPage: number,
            refreshingStatus: REQUEST_STATUSES.COMPLETE,
          }
        }
      }
    }
  },
  [LOADED_MORE_CHANNEL_CONTENT]: (state, action) => {
    const { ids, totalPages, number, channelId } = action.payload;
    const content = !!state.uiSlices[channelId] && !!state.uiSlices[channelId].content ? {...state.uiSlices[channelId].content} : {};
    const mergedContentIds = _.uniq([...state.uiSlices[channelId].content.contentIds, ...ids]);
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [channelId]: {
          ...state.uiSlices[channelId],
          content: {
            ...content,
            contentIds: mergedContentIds,
            canLoadMore: number < totalPages - 1,
            currentPage: number,
            loadingStatus: REQUEST_STATUSES.COMPLETE,
          }
        }
      }
    }
  },
  [REFRESH_CHANNEL_CONTENT_REQUEST_STARTED]: (state, action) => {
    const channelId = action.payload;
    const content = !!state.uiSlices[channelId] && !!state.uiSlices[channelId].content ? {...state.uiSlices[channelId].content} : {};
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [channelId]: {
          ...state.uiSlices[channelId],
          content: {
            ...content,
            refreshingStatus: REQUEST_STATUSES.IN_PROGRESS,
          },
        }
      }
    }
  },
  [REFRESH_CHANNEL_CONTENT_REQUEST_FAILED]: (state, action) => {
    const channelId = action.payload;
    const content = !!state.uiSlices[channelId] && !!state.uiSlices[channelId].content ? {...state.uiSlices[channelId].content} : {};
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [channelId]: {
          ...state.uiSlices[channelId],
          content: {
            ...content,
            refreshingStatus: REQUEST_STATUSES.ERROR,
          }
        }
      }
    };
  },
  [LOAD_MORE_CHANNEL_CONTENT_REQUEST_STARTED]: (state, action) => {
    const channelId = action.payload;
    const content = !!state.uiSlices[channelId] && !!state.uiSlices[channelId].content ? {...state.uiSlices[channelId].content} : {};
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [channelId]: {
          ...state.uiSlices[channelId],
          content: {
            ...content,
            loadingStatus: REQUEST_STATUSES.IN_PROGRESS,
          }
        }
      }
    };
  },
  [LOAD_MORE_CHANNEL_CONTENT_REQUEST_FAILED]: (state, action) => {
    const channelId = action.payload;
    const content = !!state.uiSlices[channelId] && !!state.uiSlices[channelId].content ? {...state.uiSlices[channelId].content} : {};
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [channelId]: {
          ...state.uiSlices[channelId],
          content: {
            ...content,
            loadingStatus: REQUEST_STATUSES.ERROR,
          }
        }
      }
    };
  },
  [REFRESH_SERIES_REQUEST_STARTED]: (state, action) => {
    const channelId = action.payload;
    const series = !!state.uiSlices[channelId] && !!state.uiSlices[channelId].series ? {...state.uiSlices[channelId].series} : {};
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [channelId]: {
          ...state.uiSlices[channelId],
          series: {
            ...series,
            refreshingStatus: REQUEST_STATUSES.IN_PROGRESS,
          },
        }
      }
    }
  },
  [REFRESH_SERIES_REQUEST_FAILED]: (state, action) => {
    const channelId = action.payload;
    const series = !!state.uiSlices[channelId] && !!state.uiSlices[channelId].series ? {...state.uiSlices[channelId].series} : {};
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [channelId]: {
          ...state.uiSlices[channelId],
          series: {
            ...series,
            refreshingStatus: REQUEST_STATUSES.ERROR,
          }
        }
      }
    };
  },
  [LOAD_MORE_SERIES_REQUEST_STARTED]: (state, action) => {
    const channelId = action.payload;
    const series = !!state.uiSlices[channelId] && !!state.uiSlices[channelId].series ? {...state.uiSlices[channelId].series} : {};
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [channelId]: {
          ...state.uiSlices[channelId],
          series: {
            ...series,
            loadingStatus: REQUEST_STATUSES.IN_PROGRESS,
          }
        }
      }
    };
  },
  [LOAD_MORE_SERIES_REQUEST_FAILED]: (state, action) => {
    const channelId = action.payload;
    const series = !!state.uiSlices[channelId] && !!state.uiSlices[channelId].series ? {...state.uiSlices[channelId].series} : {};
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [channelId]: {
          ...state.uiSlices[channelId],
          series: {
            ...series,
            loadingStatus: REQUEST_STATUSES.ERROR,
          }
        }
      }
    };
  },
};

const initialState: ReduxStateChannelStandalone = {
  uiSlices: {}
};

const channelStandaloneReducer = (state: ReduxStateChannelStandalone = initialState, action: {type: string, payload: any}) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

export default channelStandaloneReducer;
