// @flow
import * as fromSeries from 'app/redux/modules/series/selectors';
import { REQUEST_STATUSES } from 'app/redux/constants';
import type { ReduxState } from 'app/redux/constants';
import type { ReduxStateSubscribedSeriesList } from './reducer';

const STATE_KEY = 'subscribedSeriesList';
const stateFromRoot = (state: ReduxState): ReduxStateSubscribedSeriesList => state[STATE_KEY];

const seriesIdsSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  return reducerState.seriesIds;
};

const seriesListWithContentSelector = (state: ReduxState) => {
  const seriesIds = seriesIdsSelector(state);
  const seriesListWithContent = fromSeries.seriesListWithContentSelectorv2(state, {
    seriesIds,
  });
  return seriesListWithContent;
};

const isLoadingMoreSeriesSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  return reducerState.loadingStatus === REQUEST_STATUSES.IN_PROGRESS;
};

const isRefreshingSeriesSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  return reducerState.refreshingStatus === REQUEST_STATUSES.IN_PROGRESS;
};

const hasErrorSeriesSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  return (
    reducerState.loadingStatus === REQUEST_STATUSES.ERROR ||
    reducerState.refreshingStatus === REQUEST_STATUSES.ERROR
  );
};

const canLoadMoreSeriesSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  return reducerState.canLoadMore;
};

const currentSeriesPageSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  return reducerState.currentPage;
};

const displayNoMoreSeriesResultsSelector = (state: ReduxState) => {
  const reducerState = stateFromRoot(state);
  const { canLoadMore } = reducerState;
  return (
    !isLoadingMoreSeriesSelector(state) &&
    !isRefreshingSeriesSelector(state) &&
    !canLoadMore
  );
};

export {
  STATE_KEY,
  isLoadingMoreSeriesSelector,
  isRefreshingSeriesSelector,
  hasErrorSeriesSelector,
  canLoadMoreSeriesSelector,
  currentSeriesPageSelector,
  displayNoMoreSeriesResultsSelector,
  seriesListWithContentSelector,
};
