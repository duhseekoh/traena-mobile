// @flow
import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import { FeatherIcon, TRAText } from 'app/components';
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style?: ViewStyleProp,
  count?: number,
  onPress: () => *,
};

const CommentsCounter = ({style, count = 0, onPress}: Props) => {
  return (
    <View style={style}>
      <TouchableOpacity style={styles.button} onPress={onPress}
        hitSlop={{top: 10, left: 10, right: 10, bottom: 10}}>
        <FeatherIcon name={'message-square'} style={styles.icon} />
        <TRAText style={styles.text}>{count}</TRAText>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
  },
  icon: {
    fontSize: styleVars.iconSize.large,
    marginRight: 5,
    marginTop: 1,
    color: styleVars.color.darkGrey,
  },
  text: {
    paddingTop: 3,
    color: styleVars.color.darkGrey
  }
});

export default CommentsCounter;
