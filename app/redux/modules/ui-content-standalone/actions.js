// @flow
import { getContentItem } from 'app/redux/modules/content/actions';
import { createContentViewedEvent } from 'app/redux/middleware/eventFactory';

export const STANDALONE_FETCH_STARTED = 'STANDALONE_FETCH_STARTED';
const standaloneFetchStarted = (contentId: number) => {
  return {
    type: STANDALONE_FETCH_STARTED,
    payload: contentId
  };
};

export const STANDALONE_FETCH_SUCCEEDED = 'STANDALONE_FETCH_SUCCEEDED';
const standaloneFetchSucceeded = (contentId: number) => {
  return {
    type: STANDALONE_FETCH_SUCCEEDED,
    payload: contentId,
    meta: {
      event: createContentViewedEvent({contentId}),
    }
  };
};

export const STANDALONE_FETCH_FAILED = 'STANDALONE_FETCH_FAILED';
const standaloneFetchFailed = (contentId: number) => {
  return {
    type: STANDALONE_FETCH_FAILED,
    payload: contentId,
  };
};

/**
 * Get a single piece of content for the standalone content screen. The content is stored
 * in the content module, not here. The only state stored in this module is the combined request
 * statuses of getting the content and user tasks associated to it
 */
export const getStandaloneContentItem = (contentId: number): * => {
  return (dispatch: any) => {
    dispatch(standaloneFetchStarted(contentId));
    return dispatch(getContentItem(contentId))
      .then(() => dispatch(standaloneFetchSucceeded(contentId)))
      .catch(() => dispatch(standaloneFetchFailed(contentId)));
  };
};
