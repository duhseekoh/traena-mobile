// @flow
import {
  LOAD_WHATS_NEW,
  LOAD_WHATS_NEW_REQUEST_STARTED,
  LOAD_WHATS_NEW_REQUEST_FINISHED,
} from './actions';
import type { Action } from 'app/types/Action';

export type ReduxStateWhatsNew = {
  contentIds: number[],
  isLoading: boolean,
};

const ACTION_HANDLERS = {
  [LOAD_WHATS_NEW]: (state: ReduxStateWhatsNew, action: Action) => {
    const { ids } = action.payload;
    return {
      ...state,
      contentIds: ids,
    };
  },
  [LOAD_WHATS_NEW_REQUEST_STARTED]: (state: ReduxStateWhatsNew) => {
    return {
      ...state,
      isLoading: true,
    };
  },
  [LOAD_WHATS_NEW_REQUEST_FINISHED]: (state: ReduxStateWhatsNew) => {
    return {
      ...state,
      isLoading: false,
    };
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  contentIds: [],
  isLoading: false,
};

const whatsNewReducer = (state: ReduxStateWhatsNew = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

export default whatsNewReducer;
