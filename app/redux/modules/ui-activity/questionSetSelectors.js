import { createSelector } from 'reselect';
import { currentActivitySelector } from '../activity/selectors';

const STATE_KEY = 'activityUI';

const totalQuestionsSelector = createSelector([
  currentActivitySelector
], (activity) => {
  return (activity && activity.body.questions) ? activity.body.questions.length : 0;
});

const questionsSelector = createSelector([
  currentActivitySelector,
], (activity) => {
  return activity ? activity.body.questions : [];
});

const correctFirstTrySelector = (state) => state[STATE_KEY] ? state[STATE_KEY].correctFirstTry : [];

const currentQuestionIndexSelector = (state) => {
  return state[STATE_KEY] ? state[STATE_KEY].currentQuestionIndex : null;
}

const activityProgressSelector = createSelector([
  currentActivitySelector,
  currentQuestionIndexSelector
], (activity, currentQuestionIndex) => {
  if(!currentQuestionIndex || !activity || !activity.body.questions) {
    return 0;
  }
  const totalQuestions = activity.body.questions.length;
  return (currentQuestionIndex / totalQuestions) * 100;
});

const showSummarySelector = createSelector([
  activityProgressSelector,
], (progress) => {
  return progress === 100;
});

const currentQuestionSelector = createSelector([
  questionsSelector,
  currentQuestionIndexSelector,
], (questions, currentQuestionIndex) => {
  return (questions && currentQuestionIndex) ? questions[currentQuestionIndex] : null;
});

const choicesSelector = createSelector([
  currentQuestionSelector,
], (question) => {
  return question ? question.choices : [];
});

const correctChoiceIdSelector = createSelector([
  choicesSelector,
], (choices) => {
  return (choices && choices.length > 0) ? choices.find(c => c.correct).id : null;
});

const incorrectChoiceIdsSelector = createSelector([
  choicesSelector,
], (choices) => {
  return  choices ? choices.filter(c => !c.correct).map(c => c.id) : [];
});

export {
  STATE_KEY,
  currentQuestionSelector,
  correctFirstTrySelector,
  currentQuestionIndexSelector,
  choicesSelector,
  correctChoiceIdSelector,
  incorrectChoiceIdsSelector,
  questionsSelector,
  activityProgressSelector,
  showSummarySelector,
  totalQuestionsSelector,
};
