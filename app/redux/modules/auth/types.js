// @flow
type Auth0UserClaim = {
  email: string,
  details: {
    traenaId: number,
    organizationId: number,
    firstName: string,
    lastName: string,
    subscribedChannelIds: number[],
  },
  groups: string[],
  roles: string[],
  permissionins: string[],
};

type Auth0DecodedIdToken = {
  [auth0UserClaimNamespace: string]: Auth0UserClaim,
  exp: number,
  iat: number,
};

export type {
  Auth0DecodedIdToken,
  Auth0UserClaim,
}
