// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, Image } from 'react-native';
import {
  TRATextStrong,
  FeatherIcon,
  FeedStatusCard,
  ContentCard,
} from 'app/components';
import {
  whatsNewSelector,
  isLoadingSelector,
} from 'app/redux/modules/whats-new/selectors';
import * as whatsNewActions from 'app/redux/modules/whats-new/actions';
import { goToContentStandalone } from 'app/redux/modules/navigation/actions';
import HorizontalSlider from 'app/components/horizontal-slider/HorizontalSlider';
import styleVars from 'app/style/variables';
import type { ContentItem } from 'app/redux/modules/content/types';

type Props = {
  content: ContentItem[],
  initialize: () => any,
  goToContentStandalone: (contentId: number) => any,
};

class WhatsNewContainer extends Component<Props> {
  componentDidMount() {
    this.props.initialize();
  }

  render() {
    const noNewContent = !this.props.isLoading && !this.props.content.length;
    const displayLoading = this.props.isLoading && !this.props.content.length;
    return (
      <View style={styles.container}>
        <TRATextStrong style={styles.title}>
          <FeatherIcon
            fontSize={styleVars.iconSize.medlar}
            color={styleVars.color.traenaPrimaryGreen}
            name={'activity'}
          />{' '}
          What{"'"}s New
        </TRATextStrong>
        {/* TODO all three of the current feed groups has a similar loading,
          content, and no content view. Build a wrapper component that provides
          placement or a set of components to put into each widget that bring
          the styles with them.
           */}
        <View style={{width: '100%'}}>
          {displayLoading ? (
            <View style={{
              flex: 1,
              paddingHorizontal: styleVars.padding.edgeOfScreen,
            }}>
              <Image
                style={{
                  flex: 1,
                  aspectRatio: 1.6,
                  width: 'auto',
                  height: 'auto',
                  borderRadius: 8,
                  resizeMode: 'cover'
                }}
                source={require('app/images/post-ghost.png')}
              />
            </View>
          ) : (
            <HorizontalSlider
              data={this.props.content}
              renderItem={(item: ContentItem) => (
                <ContentCard
                  key={item.id}
                  contentItem={item}
                  onPress={this.props.goToContentStandalone}
                />
              )}
            />
          )}
          {noNewContent && (
            <View style={{ flex: 1, paddingHorizontal: styleVars.padding.edgeOfScreen }}>
              <FeedStatusCard
                imageSource={require('app/images/caughtup.png')}
                title={'All caught up!'}
                message={'Browse channels below to view more content.'}
              />
            </View>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
  },
  title: {
    fontSize: styleVars.fontSize.large,
    paddingHorizontal: 10,
    marginTop: 15,
    marginBottom: 10,
  },
});

const mapStateToProps = state => ({
  content: whatsNewSelector(state),
  isLoading: isLoadingSelector(state),
});

const mapDispatchToProps = dispatch => ({
  initialize: () => dispatch(whatsNewActions.initialize()),
  goToContentStandalone: (contentId: number) =>
    dispatch(goToContentStandalone(contentId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(WhatsNewContainer);
