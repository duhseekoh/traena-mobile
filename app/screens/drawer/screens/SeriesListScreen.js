// @flow
import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import type {
  NavigationStackScreenOptions,
  NavigationNavigatorProps,
} from 'react-navigation';
import SeriesListContainer from 'app/containers/series/SeriesListContainer';
import styleVars from 'app/style/variables';

type NavigationStateParams = *;

type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

class SeriesListScreen extends Component<NavigationProps> {
  static navigationOptions: (*) => NavigationStackScreenOptions = () => {
    return {
      headerTitle: 'Series',
    };
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: styleVars.color.lightGrey4 }}>
        <SeriesListContainer />
      </SafeAreaView>
    );
  }
}

export default SeriesListScreen;
