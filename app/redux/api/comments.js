// @flow
import { getCommentsByContentIdTpl, postCommentTpl, deleteCommentTpl } from './util/endpoints';
import moment from 'moment';

const retrieveCommentsByContentId = (contentId: number, maxDate?: moment) => {
  const timestamp = maxDate ? maxDate.toISOString() : null;
  return fetch(getCommentsByContentIdTpl({contentId, maxDateTimestamp: timestamp}), {
    method: 'GET',
  });
};

const addComment = (contentId: number, text: string, taggedUserIds: Array<string>) => {
  return fetch(postCommentTpl({contentId}), {
    method: 'POST',
    body: {
      text,
      taggedUserIds,
    },
  });
};

const deleteComment = (contentId: number, commentId: number) => {
  return fetch(deleteCommentTpl({contentId, commentId}), {
    method: 'DELETE',
  });
}

export default {
  addComment,
  retrieveCommentsByContentId,
  deleteComment,
}
