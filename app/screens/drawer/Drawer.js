// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, SafeAreaView, StyleSheet, View } from 'react-native';
import { ROUTES } from 'app/redux/modules/navigation/constants';
import { unacknowledgedCountSelector } from 'app/redux/modules/notification/selectors';
import { TraenaIcon } from 'app/components';
import withNavigation, {
  type WithNavigationProps,
} from 'app/containers/hoc/withNavigation';
import NavButton from './components/NavButton';
import NotificationCount from './components/NotificationCount';
import styleVars from 'app/style/variables';

// react-navigation type defs don't currently have prop type to pass in here,
// but they exist, so using '*' for now.
type Props = * & WithNavigationProps;

/**
 * Contents of the slideout drawer menu.
 *
 * Note that by default <DrawerItems> provided by navigation would be used and
 * would dynamically render menu items, but our layout and need for notification
 * items, we custom display the nav items.
 */
class Drawer extends Component<Props> {
  render() {
    const { activeItemKey } = this.props;
    return (
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <TraenaIcon
            name="t"
            fontSize={styleVars.fontSize.xxlarge}
            color={styleVars.color.traenaPrimaryGreen}
            style={styles.logo}
          />
          <View style={styles.navButtonSection}>
            <NavButton
              style={styles.navButton}
              onPress={() => this.props.goToHome()}
              label="Home"
              focused={activeItemKey === ROUTES.HOME}
              disabled={activeItemKey === ROUTES.HOME}
            />
            <NavButton
              style={styles.navButton}
              onPress={() => this.props.goToChannelListDrawer()}
              label="Channels"
              focused={activeItemKey === ROUTES.CHANNEL_LIST_DRAWER}
              disabled={activeItemKey === ROUTES.CHANNEL_LIST_DRAWER}
            />
            <NavButton
              style={styles.navButton}
              onPress={() => this.props.goToSeriesListDrawer()}
              label="Series"
              focused={activeItemKey === ROUTES.SERIES_LIST_DRAWER}
              disabled={activeItemKey === ROUTES.SERIES_LIST_DRAWER}
            />
          </View>
          <View style={styles.navButtonSection}>
            <NavButton
              style={styles.navButton}
              onPress={() => this.props.goToProfile()}
              label="Profile"
              focused={activeItemKey === ROUTES.PROFILE}
              disabled={activeItemKey === ROUTES.PROFILE}
            />
            <NavButton
              style={styles.navButton}
              onPress={() => this.props.goToNotifications()}
              label="Notifications"
              focused={activeItemKey === ROUTES.NOTIFICATIONS}
              disabled={activeItemKey === ROUTES.NOTIFICATIONS}
              extraContent={
                <NotificationCount count={this.props.notificationCount} />
              }
            />
            <NavButton
              style={styles.navButton}
              onPress={() => this.props.goToBookmarks()}
              label="Bookmarks"
              focused={activeItemKey === ROUTES.BOOKMARKS}
              disabled={activeItemKey === ROUTES.BOOKMARKS}
            />
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state: *) => {
  return {
    notificationCount: unacknowledgedCountSelector(state),
  };
};

export default connect(mapStateToProps)(withNavigation(Drawer));

const styles = StyleSheet.create({
  container: {
    marginTop: styleVars.padding.edgeOfScreen,
  },
  navButtonSection: {
    marginBottom: 40,
  },
  navButton: {
    paddingVertical: 10,
    paddingHorizontal: styleVars.padding.edgeOfScreen,
  },
  logo: {
    marginBottom: 60,
    marginLeft: styleVars.padding.edgeOfScreen,
  },
});
