// @flow
type Organization = {
  id: number,
  name: string,
}

// @flow
export const CHANNEL_SOURCES = {
  INTERNAL: 'internal',
  EXTERNAL: 'external',
  ALL: 'all',
};

export type ChannelSources = $Values<typeof CHANNEL_SOURCES>;

export type Channel = {
  id: number,
  createdTimestamp: string,
  default: boolean,
  description: string,
  modifiedTimestamp: string,
  name: string,
  organizationId: number,
  organization: Organization,
  visibility: ChannelSources,
};
