const getInitials = (user) => {
  if(!user) {
    return {};
  }

  let initials = '';
  if(user.firstName) {
    initials += user.firstName[0];
  }
  if(user.lastName) {
    initials += user.lastName[0];
  }

  return initials;
};

const getFullName = (user) => {
  if(!user) {
    return {};
  }

  let fullName = '';
  if(user.firstName) {
    fullName += user.firstName;
  }
  if(user.lastName) {
    fullName += ` ${user.lastName}`;
  }
  fullName = fullName.trim();

  return fullName;
};

export {
  getInitials,
  getFullName,
};
