// @flow
import moment from 'moment';
import type { Channel } from 'app/redux/modules/channel/types';
import type { Series } from 'app/redux/modules/series/types';

const ParseableDate = (props, propName, componentName) => {
  const date = moment(props[propName]);
  if (!date.isValid()) {
    return new Error(`Invalid prop: ${propName} supplied to ${componentName}.
      Expecting a date or datestring but got: ${props[propName]}`);
  }
}

type Author = {
  id: number,
  firstName: string,
  lastName: string,
  profileImageURI?: string,
  organization: any,
};

type Social = {
  likes: {
    count: number,
    doesUserLike?: boolean
  },
  comments: {
    count: number
  },
  tags?: string[]
};

type Video = {
  videoURI: string,
  previewImageURI: string,
};

type TrainingContentItem = {|
  id: number,
  type: 'TrainingVideo',
  author: Author,
  title: string,
  text: string,
  body: Object,
  tags: string[],
  commentCount: number,
  likeCount: number,
  activityCount: number,
  channel: Channel,
  video: Video,
  social: Social,
  isBookmarked?: boolean,
  isPinned?: boolean,
  isCompleted?: boolean,
  isSeries?: boolean,
  series?: Series,
  publishedTimestamp?: ParseableDate,
|};

type DailyActionContentItem = {|
  id: number,
  type: 'DailyAction',
  author: Author,
  title: string,
  text: string,
  body: Object,
  tags: string[],
  commentCount: number,
  likeCount: number,
  activityCount: number,
  channel: Channel,
  contentImageURI?: string,
  social: Social,
  isPinned?: boolean,
  isBookmarked?: boolean,
  isCompleted?: boolean,
  isSeries?: boolean,
  series?: Series,
  publishedTimestamp?: ParseableDate,
|};

type ImageSize = {
  width: number,
  height: number,
  filename: string,
  format: string,
};

type Image = {
  variations: {
    tiny: ImageSize,
    small: ImageSize,
    medium: ImageSize,
    large: ImageSize,
    original: ImageSize
  },
  baseCDNPath: string,
};

type ImageContentItem = {|
  author: Author,
  id: number,
  body: Object,
  tags: string[],
  commentCount: number,
  likeCount: number,
  activityCount: number,
  images: Image[],
  channel: Channel,
  isCompleted?: boolean,
  isPinned?: boolean,
  isBookmarked?: boolean,
  isSeries?: boolean,
  series?: Series,
  publishedTimestamp?: ParseableDate,
  social: Social,
  title: string,
  text?: string,
  type: 'Image',
|};

type ContentItem = ImageContentItem | TrainingContentItem | DailyActionContentItem;

export type {
  Author,
  Image,
  Video,
  ContentItem,
  TrainingContentItem,
  DailyActionContentItem,
  ImageContentItem,
};
