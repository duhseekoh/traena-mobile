// @flow
import {
  RETRIEVED_USER_SEARCH_RESULTS,
  UI_USER_SEARCH_TEXT_UPDATED,
  USER_SEARCH_REQUEST_STARTED,
  USER_SEARCH_REQUEST_FINISHED,
} from './actions';
import type { Action } from 'app/types/Action';
import type { ContentItem } from 'app/redux/modules/content/types';

export type ReduxStateUserSearch = {
  queryText: string,
  results: ContentItem[],
  isLoading: boolean,
};

const ACTION_HANDLERS = {
  [RETRIEVED_USER_SEARCH_RESULTS]: (state: ReduxStateUserSearch, action: Action) => {
    const { results } = action.payload;
    return Object.assign({}, state, {
      results,
    });
  },
  [UI_USER_SEARCH_TEXT_UPDATED]: (state: ReduxStateUserSearch, action: Action) => {
    const queryText = action.payload;
    return Object.assign({}, state, {
      queryText: queryText
    });
  },
  [USER_SEARCH_REQUEST_STARTED]: (state: ReduxStateUserSearch) => {
    return Object.assign({}, state, {
      isLoading: true
    });
  },
  [USER_SEARCH_REQUEST_FINISHED]: (state: ReduxStateUserSearch) => {
    return Object.assign({}, state, {
      isLoading: false
    });
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  queryText: '',
  results: [],
  isLoading: false,
};

export default function userSearch(state: ReduxStateUserSearch = initialState, action: Action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}
