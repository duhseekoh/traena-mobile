// @flow

import {
  postEventUrl
} from './util/endpoints';
import type { Event } from 'app/redux/middleware/eventFactory';
import { getAuthHeaders } from 'app/redux/api/util/headers';
import logger from 'app/logging/logger';

const FLUSH_TIMEOUT = 10000;
const FLUSH_SIZE = 10;
let queuedEventCalls: { event: Event, headers: Object }[] = [];
let timeoutId = undefined;

/**
 * An event occurred and we want to track it. Throw it in a queue until the queue
 * spills over or hits the time limit, then we flush it.
 */
export function enqueueEvent(event: Event) {
  logger.logBreadcrumb('adding event to queue', event);
  queuedEventCalls.push({
    event,
    headers: getAuthHeaders(), // store auth headers as they existed at the time of insertion
  });

  if (queuedEventCalls.length > FLUSH_SIZE) {
    flushQueue();
  } else if (!timeoutId) {
    timeoutId = setTimeout(flushQueue, FLUSH_TIMEOUT);
  }
}

function flushQueue() {
  logger.logBreadcrumb('flushing queue', { queuedEventCalls });
  queuedEventCalls.forEach(({event, headers}: *) => {
    postEvent(event, headers);
  });
  queuedEventCalls = [];
  clearTimeout(timeoutId);
  timeoutId = undefined;
}

function postEvent(event: Event, headers?: Object) {
  logger.logBreadcrumb('posting event', { event, headers });
  return fetch(postEventUrl, {
    method: 'POST',
    body: event,
    // specify headers here, beause we cannot rely on the auth headers being in
    // the tracker middleware by the time this is sent out. specifically for events
    // triggered when the user is logging out.
    headers: headers || getAuthHeaders(),
  });
}
