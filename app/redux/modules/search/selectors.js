import { createSelector } from 'reselect';
import { contentItemsSelectorFactory } from 'app/redux/modules/content/selectors';

const STATE_KEY = 'search';
const stateFromRoot = (state) => state[STATE_KEY];
const resultContentIdsSelector = (state) => stateFromRoot(state).resultContentIds;

const searchResultsSelector = contentItemsSelectorFactory(resultContentIdsSelector);

const recentSearchesSelector = (state) => stateFromRoot(state).recentSearches.map(search => {
  return {title: search, value: search, key: search}
});
const commonSearchesSelector = (state) => stateFromRoot(state).commonSearches.map(search => {
  return {title: search, value: search, key: search}
});
const contentTagsSelector = (state) => stateFromRoot(state).contentTags.map(tag => {
  return {title: tag.key, value: tag.key, key: tag.key}
});

const canLoadMoreSelector = (state) => stateFromRoot(state).canLoadMore;
const queryTextSelector = (state) => stateFromRoot(state).queryText;
const isLoadingSelector = (state) => stateFromRoot(state).isLoading;
const hasNoResultsSelector = (state) => stateFromRoot(state).queryHasNoResults;
const currentPageSelector = (state) => stateFromRoot(state).currentPage;

/**
 * Are we at the bottom of the results list? Can't be loading, need to already have
 * search results, and make sure that we already have hit the end of paging.
 */
const displayNoMoreResultsSelector = createSelector([
  isLoadingSelector,
  searchResultsSelector,
  canLoadMoreSelector,
], (isLoading, searchResults, canLoadMore) => {
  return !isLoading && searchResults.length > 0 && !canLoadMore;
});

export {
  STATE_KEY,
  commonSearchesSelector,
  isLoadingSelector,
  hasNoResultsSelector,
  queryTextSelector,
  currentPageSelector,
  recentSearchesSelector,
  searchResultsSelector,
  contentTagsSelector,
  displayNoMoreResultsSelector,
  canLoadMoreSelector,
};
