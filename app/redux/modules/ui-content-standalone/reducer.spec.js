import 'react-native';
import contentStandaloneReducer from './reducer';
import {
  STANDALONE_FETCH_STARTED,
  STANDALONE_FETCH_SUCCEEDED,
  STANDALONE_FETCH_FAILED,
 } from './actions';

 import { REQUEST_STATUSES } from 'app/redux/constants';

jest.mock('app/logging/logger');

it('action STANDALONE_FETCH_STARTED updates content status to IN_PROGRESS', () => {
  let mockContentId = 7;
  let loadingStatus = contentStandaloneReducer(undefined, {
    type: STANDALONE_FETCH_STARTED,
    payload: mockContentId
  }).uiSlices[mockContentId].loadingStatus;
  expect(loadingStatus).toEqual(REQUEST_STATUSES.IN_PROGRESS);
});

it('action STANDALONE_FETCH_SUCCEEDED updates content status to COMPLETE', () => {
  let mockContentId = 7;
  let loadingStatus = contentStandaloneReducer(undefined, {
    type: STANDALONE_FETCH_SUCCEEDED,
    payload: mockContentId
  }).uiSlices[mockContentId].loadingStatus;
  expect(loadingStatus).toEqual(REQUEST_STATUSES.COMPLETE);
});

it('action STANDALONE_FETCH_FAILED updates content status to ERROR', () => {
  let mockContentId = 7;
  let loadingStatus = contentStandaloneReducer(undefined, {
    type: STANDALONE_FETCH_FAILED,
    payload: mockContentId
  }).uiSlices[mockContentId].loadingStatus;
  expect(loadingStatus).toEqual(REQUEST_STATUSES.ERROR);
});
