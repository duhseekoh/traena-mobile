// @flow
import React from 'react';
import {
  TouchableOpacity,
  View,
  StyleSheet,
} from 'react-native';
import {
  UserAvatar,
  UserFullName,
} from 'app/components';
import type { SearchResult } from 'app/redux/modules/user-search/types';

type Props = {
  result: SearchResult,
  style: number,
  onPress: () => void,
};

const UserSearchResultItem = ({result, style, onPress}: Props) => {

  return (
    <TouchableOpacity onPress={onPress} style={[styles.container, style]}>
      <UserAvatar user={result} style={styles.avatar} size={'small'} />
      <View style={styles.nameContainer}>
        <UserFullName style={styles.nameText} user={result} />
      </View>
    </TouchableOpacity>
  )

}

export default UserSearchResultItem;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  },
  avatar: {
    flex: 0,
  },
  nameContainer: {
    marginLeft: 5,
    justifyContent: 'center',
  },
  nameText: {

  }
});
