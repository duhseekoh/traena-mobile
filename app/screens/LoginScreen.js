// @flow
import React from 'react';
import {
  View
} from 'react-native';
import LoginContainer from 'app/containers/auth/LoginContainer';

const LoginScreen = () => {
  return (
    <View style={{flex: 1}}>
      {/* <ScreenTitle text={`LOGIN`} /> */}
      <LoginContainer />
    </View>
  );
};

export default LoginScreen;
