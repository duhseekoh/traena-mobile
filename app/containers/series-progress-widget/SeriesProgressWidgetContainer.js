// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, Image } from 'react-native';
import {
  TRATextStrong,
  FeatherIcon,
  SeriesProgressCard,
} from 'app/components';
import {
  isLoadingSelector,
  seriesSelector,
} from 'app/redux/modules/series-progress-widget/selectors';
import * as seriesProgressWidgetActions from 'app/redux/modules/series-progress-widget/actions';
import { goToSeriesStandalone } from 'app/redux/modules/navigation/actions';
import HorizontalSlider from 'app/components/horizontal-slider/HorizontalSlider';
import styleVars from 'app/style/variables';
import type { Series } from 'app/redux/modules/series/types';
import type { ReduxState } from 'app/redux/constants';

type Props = {
  series: Series[],
  initialize: () => any,
  isLoading: boolean,
  goToSeriesStandalone: (seriesId: number) => any,
};

class SeriesProgressWidgetContainer extends Component<Props> {
  componentDidMount() {
    this.props.initialize();
  }

  render() {
    const noSeries = !this.props.isLoading && !this.props.series.length;
    const displayLoading = this.props.isLoading && !this.props.series.length;

    if (noSeries) {
      return null;
    }

    return (
      <View style={styles.container}>
        <TRATextStrong style={styles.title}>
          <FeatherIcon
            fontSize={styleVars.iconSize.medlar}
            color={styleVars.color.traenaPrimaryGreen}
            name={'copy'}
          />{' '}
          Series
        </TRATextStrong>
        <View style={{width: '100%'}}>
          {displayLoading ? (
            <View style={{
              flex: 1,
              paddingHorizontal: styleVars.padding.edgeOfScreen,
            }}>
              <Image
                style={{
                  flex: 1,
                  aspectRatio: 1.6,
                  width: 'auto',
                  height: 'auto',
                  borderRadius: 8,
                  resizeMode: 'cover'
                }}
                source={require('app/images/post-ghost.png')}
              />
            </View>
          ) : (
            <HorizontalSlider
              data={this.props.series}
              renderItem={(item: Series) => (
                <SeriesProgressCard
                  key={item.id}
                  series={item}
                  onPress={this.props.goToSeriesStandalone}
                />
              )}
            />
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
  },
  title: {
    fontSize: styleVars.fontSize.large,
    paddingHorizontal: 10,
    marginTop: 15,
    marginBottom: 10,
  },
});

const mapStateToProps = (state: ReduxState) => ({
  series: seriesSelector(state),
  isLoading: isLoadingSelector(state),
});

const mapDispatchToProps = dispatch => ({
  initialize: () => dispatch(seriesProgressWidgetActions.initialize()),
  goToSeriesStandalone: (seriesId: number, title: string, description: string) =>
    dispatch(goToSeriesStandalone(seriesId, title, description)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SeriesProgressWidgetContainer);
