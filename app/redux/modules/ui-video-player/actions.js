// @flow 
export const SET_ACTIVE_VIDEO = 'SET_ACTIVE_VIDEO';
export const CLEAR_ACTIVE_VIDEO = 'CLEAR_ACTIVE_VIDEO';
import { createVideoProgressedEvent } from 'app/redux/middleware/eventFactory';

export const setActiveVideoPlayer = (videoId: *) => {
  return {
    type: SET_ACTIVE_VIDEO,
    payload: videoId
  };
};

export const deactivateVideoPlayer = () => {
  return {
    type: CLEAR_ACTIVE_VIDEO,
  }
};

/**
 * When the video player reports watch progress, this is called. No state is stored about this, but
 * an event gets sent through redux so we track this through our eventing api.
 * @param percent             percent watched of the video at the point of recording this
 * @param isAndroidFullscreen
 * @param eventMeta           any data related to the video we want to send. currently just contentId.
 */
export const videoMajorProgress = (percent: number, isAndroidFullscreen: boolean, eventMeta?: { contentId?: number }) => {
  const em = eventMeta || {};
  return {
    type: 'ANALYTICS_ONLY_ACTION',
    meta: {
      event: createVideoProgressedEvent({
        percent,
        isAndroidFullscreen,
        ...em,
      })
    }
  }
};
