// @flow
import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { TraenaIcon, TRATextStrong, FeatherIcon } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  style?: ViewStyleProp,
  topicsCovered: number,
  lessonsCompleted: number,
  timePerDay: number,
};

const AnalyticsSummary = ({style, topicsCovered, lessonsCompleted, timePerDay}: Props) => {
  const iconSize = styleVars.iconSize.xxlarge;
  const iconColor = styleVars.color.traenaPrimaryGreenLighter;

  return (
    <View style={[styles.container, style]}>
      <View style={styles.column}>
        <TraenaIcon name='data' style={styles.icon} fontSize={iconSize} color={iconColor} />
        <TRATextStrong style={styles.stat}>{topicsCovered}</TRATextStrong>
        <TRATextStrong style={styles.statDescription}>TOTAL TOPICS COVERED</TRATextStrong>
      </View>
      <View style={styles.column}>
        <FeatherIcon name='check-circle' style={styles.icon} fontSize={iconSize} color={iconColor} />
        <TRATextStrong style={styles.stat}>{lessonsCompleted}</TRATextStrong>
        <TRATextStrong style={styles.statDescription}>LESSONS COMPLETED</TRATextStrong>
      </View>
      <View style={styles.column}>
        <TraenaIcon name='stopwatch' style={styles.icon} fontSize={iconSize} color={iconColor} />
        <TRATextStrong style={styles.stat}>{timePerDay}</TRATextStrong>
        <TRATextStrong style={styles.statDescription}>AVG TIME PER DAY (MIN)</TRATextStrong>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  column: {
    flex: 1,
    marginHorizontal: 10,
  },
  icon: {
    textAlign: 'center',
    marginBottom: 15,
  },
  stat: {
    fontSize: styleVars.fontSize.large,
    color: styleVars.color.traenaOne,
    textAlign: 'center',
    marginBottom: 5,
  },
  statDescription: {
    fontSize: styleVars.fontSize.medium,
    color: styleVars.color.traenaOne,
    textAlign: 'center',
  },
});

export default AnalyticsSummary;
