// @flow
import {
  getBookmarkIdsTpl,
  getBookmarkContentTpl,
  addBookmarkTpl,
  deleteBookmarkTpl } from './util/endpoints';

function retrieveBookmarkContent(pageNumber: number = 0) {
  return fetch(getBookmarkContentTpl({ pageNumber }));
}

function retrieveBookmarkIds(pageNumber: number = 0) {
  return fetch(getBookmarkIdsTpl({ pageNumber }));
}

function addBookmark(contentId: number) {
  return fetch(addBookmarkTpl(), {
    method: 'POST',
    body: {
      contentId,
    },
  });
}

function deleteBookmark(contentId: number) {
  return fetch(deleteBookmarkTpl({ contentId }), {
    method: 'DELETE',
  });
}



export default {
  retrieveBookmarkContent,
  retrieveBookmarkIds,
  addBookmark,
  deleteBookmark,
}
