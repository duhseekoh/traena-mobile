// @flow
import {
  REFRESHED_NOTIFICATIONS,
  RETRIEVED_NOTIFICATIONS,
  GET_NOTIFICATION_REQUEST_STARTED,
  GET_NOTIFICATION_REQUEST_FINISHED,
  DEVICE_TOKEN_RECEIVED,
  PUSH_NOTIFICATIONS_ACKNOWLEDGED,
  UNACKNOWLEDGED_COUNT_RETRIEVED,
 } from './actions';
import type { Notification } from './types';
import type { Action } from 'app/types/Action';

export type ReduxStateNotification = {
  notifications: {[number]: Notification},
  isLoading: boolean,
  hasOlder: boolean,
  deviceToken: ?any,
  unacknowledgedCount: number,
};

const ACTION_HANDLERS = {
  [REFRESHED_NOTIFICATIONS]: (state: ReduxStateNotification, action: Action) => {
    const { index: notifications, last } = action.payload;

    return {
      ...state,
      notifications,
      hasOlder: !last,
    };
  },
  [RETRIEVED_NOTIFICATIONS]: (state: ReduxStateNotification, action: Action) => {
    const { index: notifications, last } = action.payload;
    return {
      ...state,
      notifications: {
        ...state.notifications,
        ...notifications,
      },
      hasOlder: !last,
    };
  },
  [GET_NOTIFICATION_REQUEST_STARTED]: (state: ReduxStateNotification) => {
    return {
      ...state,
      isLoading: true,
    };
  },
  [GET_NOTIFICATION_REQUEST_FINISHED]: (state: ReduxStateNotification) => {
    return {
      ...state,
      isLoading: false,
    };
  },
  [DEVICE_TOKEN_RECEIVED]: (state: ReduxStateNotification, action: Action) => {
    return {
      ...state,
      deviceToken: action.payload,
    };
  },
  [UNACKNOWLEDGED_COUNT_RETRIEVED]: (state: ReduxStateNotification, action: Action) => {
    const count: number = action.payload;
    return {
      ...state,
      unacknowledgedCount: count,
    };
  },
  [PUSH_NOTIFICATIONS_ACKNOWLEDGED]: (state: ReduxStateNotification) => {
    return {
      ...state,
      unacknowledgedCount: 0,
    };
  }
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  /**
   * e.g. {
   *  [notificationId]: {notification object}
   * }
   */
  notifications: {},
  isLoading: false,
  hasOlder: true,
  deviceToken: null,
  unacknowledgedCount: 0,
};

const notificationReducer = (state: ReduxStateNotification = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default notificationReducer;
