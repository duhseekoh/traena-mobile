// @flow
import type { ContentItem } from '../content/types';

type Series = {
  id: number,
  authorId: number,
  channelId: number,
  title: string,
  description: string,
  contentPreview?: number[],
  createdTimestamp: string,
};

// A user's progress for a given series.
type SeriesProgress = {
  seriesId: number,
  completedContentCount: number,
  contentCount: number,
  contentIds: number[],
};

type SeriesWithPreview = {
  series: Series,
  previewContent: ContentItem[],
};

export type { Series, SeriesProgress, SeriesWithPreview };
