// @flow
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { TRATextStrong } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  title: string,
  style?: ViewStyleProp,
  onPress?: () => *,
  isSelected?: boolean,
  textStyle: string,
  isDisabled: boolean,
};

const ButtonSubtle = ({title, style, onPress, isDisabled, textStyle}: Props) => {
  return (
    <TouchableOpacity style={style} onPress={onPress} disabled={isDisabled}>
      <View>
        <TRATextStrong style={[styles.button, textStyle]}>{title}</TRATextStrong>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  button: {
    textAlign: 'center',
    fontSize: 14,
    color: styleVars.color.mediumGrey3,
  }
});

export default ButtonSubtle;
