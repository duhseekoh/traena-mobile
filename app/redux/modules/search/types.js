// @flow
import type { ContentItem } from 'app/redux/modules/content/types';

type SearchResult = ContentItem;

type SearchResults = ContentItem[];

type Suggestion = {
  title: string,
  subtitle?: string,
  key: string | number,
  value: any,
}

export type {
  Suggestion,
  SearchResult,
  SearchResults,
};
