// @flow
import React from 'react';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import Icon from 'react-native-vector-icons/Ionicons';
import GenericContentPreview from './GenericContentPreview';
import type { ImageContentItem } from 'app/redux/modules/content/types';
import styleVars from 'app/style/variables';

type Props = {
  contentItem: ImageContentItem,
  style?: ViewStyleProp,
};

const ImageContentItemPreview = (props: Props) => {
  const typeIcon = <Icon name='ios-image-outline' color={styleVars.color.traenaOne} size=
  {20} style={{fontSize: 20, marginTop: -2}} />

  return (
    <GenericContentPreview
      {...props}
      typeIcon={typeIcon}
    />
  );
}

 export default ImageContentItemPreview;
