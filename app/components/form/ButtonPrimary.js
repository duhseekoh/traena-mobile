// @flow
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { TRATextStrong } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  title: string,
  style?: ViewStyleProp,
  onPress: () => *,
  disabled?: boolean,
};

const ButtonPrimary = ({title, style, onPress, disabled}: Props) => {
  const buttonOnPress = disabled ? () => {} : onPress;
  const buttonWrapperStyle = disabled ? [styles.button, styles.buttonDisabled]
    : [styles.button];

  return (
    <TouchableOpacity style={style} onPress={buttonOnPress}>
      <View style={buttonWrapperStyle}>
        <TRATextStrong style={styles.buttonText}>{title}</TRATextStrong>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  buttonText: {
    textAlign: 'center',
    fontSize: styleVars.fontSize.medium,
    color: styleVars.color.white,
    padding: 16,
  },
  button: {
    backgroundColor: styleVars.color.traenaOne,
    borderRadius: 30,
    overflow: 'hidden',
  },
  buttonDisabled: {
    backgroundColor: styleVars.color.lightGrey,
  }
});

export default ButtonPrimary;
