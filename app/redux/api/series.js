// @flow
import { normalize } from 'normalizr';
import type { SeriesProgress, Series } from 'app/redux/modules/series/types';
import type { IndexedPage } from 'app/types/Pagination';
import { seriesEntity, seriesEntityList } from 'app/redux/api/util/schema';
import {
  getSeriesForChannelTpl,
  getSeriesPreviewTpl,
  getSeriesProgressTpl,
  getContentForSeriesTpl,
  getSubscribedSeriesTpl,
} from './util/endpoints';

const retrieveSeriesForChannel = async (channelId: number, page: number = 0) => {
  const seriesPage = await fetch(getSeriesForChannelTpl({ page, channelId, includePreview: true }));
  const normalizedSeries = normalize(Object.values(seriesPage.index), seriesEntityList);
  return { page: seriesPage, series: normalizedSeries.result, contentPreviews: normalizedSeries.entities.contentPreview };
};

const retrieveSubscribedSeries = async (page?: number = 0, size?: number = 10, sort?: string, filterProgressStatus?: string) => {
  const seriesPage: IndexedPage<number, Series> = await fetch(getSubscribedSeriesTpl({ page, size, sort, includePreview: true, filterProgressStatus }));
  const normalizedSeries = normalize(Object.values(seriesPage.index), seriesEntityList);
  return { page: seriesPage, series: normalizedSeries.result, contentPreviews: normalizedSeries.entities.contentPreview };
};

const retrieveSeriesPreview = async (seriesId: number) => {
  const series = await fetch(getSeriesPreviewTpl({ seriesId, includePreview: true }));
  const normalizedSeries = normalize(series, seriesEntity);
  return { series: normalizedSeries.entities.series[normalizedSeries.result], contentPreviews: normalizedSeries.entities.contentPreview };
};

const retrieveContentForSeries = (seriesId: number, page: number = 0) => {
  return fetch(getContentForSeriesTpl({ seriesId, page }));
};

const retrieveSeriesProgress = async (seriesId?: number): Promise<SeriesProgress> => {
  return fetch(getSeriesProgressTpl({ seriesId }));
};

export default {
  retrieveSeriesForChannel,
  retrieveSubscribedSeries,
  retrieveSeriesPreview,
  retrieveSeriesProgress,
  retrieveContentForSeries,
}
