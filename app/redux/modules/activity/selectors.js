import _ from 'lodash';
import { createSelector } from 'reselect';

const activitiesSelector = (state) => {
  return _.values(state.activity.activitiesById);
}

const activitiesByContentIdSelector = createSelector(
  (_, props) => props,
  activitiesSelector,
  (props, activities) => {
    return activities.filter(a => a.contentId === props.contentId)
  }
)

const currentActivitySelector = (state, props) => {
  const { activityId } = props;
  return state.activity.activitiesById ? state.activity.activitiesById[activityId] : null;
};

const currentActivityResultsSelector = (state, props) => {
  const { activityId } = props;
  return state.activity.activityResults ? state.activity.activityResults[activityId] : null;
};

const contentIdForActivitySelector = (state, props) => {
  const { activityId } = props;
  return state.activity.activitiesById ? state.activity.activitiesById[activityId].contentId : null;
}

export {
  activitiesSelector,
  currentActivitySelector,
  currentActivityResultsSelector,
  contentIdForActivitySelector,
  activitiesByContentIdSelector,
};
