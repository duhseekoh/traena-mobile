// @flow
import type { Author } from 'app/redux/modules/content/types';
import type { User } from 'app/redux/modules/user/types';

type Comment = {
  id: number,
  createdTimestamp: string,
  // when a comment is part of a notification, we don't care about the author, so not required
  author: Author,
  contentId: number,
  text: string,
  taggedUsers?: User[],
};

export type { Comment } ;
