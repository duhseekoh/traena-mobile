// @flow
import api from 'app/redux/api';
import type { Dispatch } from 'app/redux/constants';
import type { ActivityResponse, ResponseBody } from './types';

export const CREATED_ACTIVITY_RESPONSE = 'CREATED_ACTIVITY_RESPONSE';
const activityResponseCreated = (activityResponse: ActivityResponse) => {
  return {
    type: CREATED_ACTIVITY_RESPONSE,
    payload: activityResponse,
  }
}

const createActivityResponse = (activityId: number, body: ResponseBody) => {
  return async (dispatch: Dispatch) => {
    const activiyResponse = await api.activityResponse.createActivityResponse(activityId, body)
    dispatch(activityResponseCreated(activiyResponse));
  };
};

export const RETRIEVED_ACTIVITY_RESPONSE = 'RETRIEVED_ACTIVITY_RESPONSE';
const activityResponseRetrieved = (activityResponse: ActivityResponse) => {
  return {
    type: RETRIEVED_ACTIVITY_RESPONSE,
    payload: activityResponse,
  }
}

const getActivityResponses = (activityId: number) => {
  return async (dispatch: Dispatch) => {
    const activityResponses = await api.activityResponse.getActivityResponses(activityId)
    dispatch(activityResponseRetrieved(activityResponses.content));
  };
};

export {
  createActivityResponse,
  getActivityResponses,
}
