// @flow
import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import type {
  NavigationNavigatorProps,
  NavigationEventSubscription,
  NavigationStackScreenOptions,
} from 'react-navigation';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-native';
import { ContentList } from 'app/components';
import {
  bookmarkSelector,
  isLoadingMoreSelector,
  isRefreshingSelector,
  canLoadMoreSelector,
  displayNoMoreResultsSelector,
  hasErrorSelector,
} from 'app/redux/modules/ui-bookmark/selectors';
import {
  refreshBookmarks,
  loadMoreBookmarks,
} from 'app/redux/modules/ui-bookmark/actions';
import type { ContentItem } from 'app/redux/modules/content/types';
import logger from 'app/logging/logger';
import styleVars from 'app/style/variables';

type Props = {
  bookmarks: ContentItem[],
  refreshBookmarks: () => void,
  loadMoreBookmarks: () => void,
  isLoadingMore: boolean,
  isRefreshing: boolean,
  canLoadMore: boolean,
  displayNoMoreResults: boolean,
  hasError: boolean,
} & NavigationNavigatorProps<*, {}>;

class BookmarkScreen extends Component<Props> {
  props: Props;
  _navigationListener: NavigationEventSubscription;

  static navigationOptions = (): NavigationStackScreenOptions => {
    return {
      headerTitle: 'Bookmarks',
    };
  };

  componentDidMount() {
    this._navigationListener = this.props.navigation.addListener(
      'didFocus',
      () => {
        logger.logBreadcrumb('Bookmarks displayed');
        this.props.refreshBookmarks();
      }
    );
  }

  componentWillUnmount() {
    this._navigationListener.remove();
  }

  render() {
    const {
      hasError,
      bookmarks,
      refreshBookmarks,
      isRefreshing = false,
      loadMoreBookmarks,
      isLoadingMore,
      canLoadMore,
      displayNoMoreResults,
    } = this.props;
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: styleVars.color.white }}>
        <ContentList
          refreshContent={refreshBookmarks}
          isRefreshing={isRefreshing}
          contentItems={bookmarks}
          loadNextPage={loadMoreBookmarks}
          isLoadingMore={isLoadingMore}
          canLoadMore={canLoadMore}
          hasError={hasError}
          displayNoMoreResults={displayNoMoreResults}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    bookmarks: bookmarkSelector(state),
    isRefreshing: isRefreshingSelector(state),
    isLoadingMore: isLoadingMoreSelector(state),
    canLoadMore: canLoadMoreSelector(state),
    displayNoMoreResults: displayNoMoreResultsSelector(state),
    hasError: hasErrorSelector(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    refreshBookmarks: () => {
      return dispatch(refreshBookmarks());
    },
    loadMoreBookmarks: () => {
      return dispatch(loadMoreBookmarks());
    },
  };
};

const ConnectedBookmarkScreen = connect(mapStateToProps, mapDispatchToProps)(
  BookmarkScreen,
);
export default withNavigation(ConnectedBookmarkScreen);
