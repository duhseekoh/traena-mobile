import 'react-native';
import authReducer from './reducer';
import requestStatesEnum from './requestStatesEnum';
import { LOGIN_REQUESTING, TOKENS_RECEIVED, LOGIN_BAD_CREDENTIALS, LOGIN_UNKNOWN_FAILURE } from './actions';

it('action LOGIN_REQUESTING sets the request state to in progress', () => {
  const state = authReducer(undefined, {type: LOGIN_REQUESTING});
  expect(state.loginRequestState).toEqual(requestStatesEnum.IN_PROGRESS);
});

it('action TOKENS_RECEIVED sets the request state to success and sets jwt', () => {
  const state = authReducer(undefined, {
    type: TOKENS_RECEIVED,
    payload: {
      accessToken: 'some-access-token',
      idToken: 'some-id-token',
    },
  });
  expect(state.loginRequestState).toEqual(requestStatesEnum.SUCCESS);
  expect(state.accessToken).toEqual('some-access-token');
  expect(state.idToken).toEqual('some-id-token');
  expect(state.isLoggedIn).toEqual(true);
});

it('action LOGIN_BAD_CREDENTIALS sets the request state to bad credentials and clears jwt', () => {
  const state = authReducer(undefined, {type: LOGIN_BAD_CREDENTIALS});
  expect(state.loginRequestState).toEqual(requestStatesEnum.BAD_CREDENTIALS_FAILURE);
  expect(state.accessToken).toBeNull();
  expect(state.idToken).toBeNull();
  expect(state.isLoggedIn).toEqual(false);
});

it('action LOGIN_UNKNOWN_FAILURE sets the request state to unknown failure and clears jwt', () => {
  const state = authReducer(undefined, {type: LOGIN_UNKNOWN_FAILURE});
  expect(state.loginRequestState).toEqual(requestStatesEnum.UNKNOWN_FAILURE);
  expect(state.accessToken).toBeNull();
  expect(state.idToken).toBeNull();
  expect(state.isLoggedIn).toEqual(false);
});
