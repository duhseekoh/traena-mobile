// @flow
import contentsApi from 'app/redux/api/contents';
import api from 'app/redux/api';
import { contentEntity } from 'app/redux/api/util/schema';
import { normalize } from 'normalizr';
import type { ContentItem } from '../content/types';
import type { Dispatch } from 'app/redux/constants';

const CONTENTS_FETCH_STARTED = 'CONTENTS_FETCH_STARTED';
function contentsFetchStarted(contentIds: number[]) {
  return {
    type: CONTENTS_FETCH_STARTED,
    payload: contentIds,
  };
}

const CONTENTS_FETCH_SUCCEEDED = 'CONTENTS_FETCH_SUCCEEDED';
function contentsFetchSucceeded(contentItemsById: {[number]: ContentItem}) {
  return {
    type: CONTENTS_FETCH_SUCCEEDED,
    payload: contentItemsById,
  };
}

const CONTENTS_FETCH_FAILED = 'CONTENTS_FETCH_FAILED';
function contentsFetchFailed(contentIds: number[], error: string | Error) {
  return {
    type: CONTENTS_FETCH_FAILED,
    payload: {
      contentIds,
      error,
    },
  };
}

/**
 * This is how other modules will add content
 * @param  {[type]} contentItems [description]
 * @return {[type]}              [description]
 */
const denormalizedContentsRetrieved = (contentItems: ContentItem[]) => {
  return (dispatch: Dispatch) => {
    const normalized = normalize(contentItems, [ contentEntity ]);
    return dispatch(contentsFetchSucceeded(normalized.entities.contents));
  }
};

/**
 * When we need to fetch content by ids. This is currently used when a traena
 * list is retrieved, to retrieve the associated content.
 * @param  {[type]} contentIds [description]
 * @return {[type]}            [description]
 */
const getContentItem = (contentId: number) => {
  return (dispatch: Dispatch) => {
    dispatch(contentsFetchStarted([contentId]))
    return contentsApi.retrieveContentItemById(contentId)
      .then(contentItem => {
        if(contentItem) {
          dispatch(contentsFetchSucceeded({
            [contentItem.id]: contentItem,
          }));
        }
      })
      .catch((error) => {
        dispatch(contentsFetchFailed([contentId], error));
        throw error;
      });
  }
};

const LIKES_RETRIEVED = 'LIKES_RETRIEVED';
const likesRetrieved = (likes: number[]) => {
  return {
    type: LIKES_RETRIEVED,
    payload: likes
  }
};

const getUserLikes = () => {
  return (dispatch: any) => {
    return api.contents.retrieveUserLikes()
      .then((likes: number[]) => {
        dispatch(likesRetrieved(likes));
      });
  }
};

const UNLIKED_CONTENT = 'UNLIKED_CONTENT';
const unlikedContent = (contentId: number) => {
  return {
    type: UNLIKED_CONTENT,
    payload: contentId
  }
};

const likeContent = (contentId: number) => {
  return (dispatch: any) => {
    dispatch(likedContent(contentId));

    return api.contents.likeContent(contentId)
      .catch(() => {
        // call failed, no need to report, just reverse it
        dispatch(unlikedContent(contentId));
      });
  };
};

const LIKED_CONTENT = 'LIKED_CONTENT';
const likedContent = (contentId: number) => {
  return {
    type: LIKED_CONTENT,
    payload: contentId
  }
};

const unlikeContent = (contentId: number) => {
  return (dispatch: Dispatch) => {
    dispatch(unlikedContent(contentId));

    return api.contents.unlikeContent(contentId)
      .catch(() => {
        // call failed, no need to report, just reverse it
        dispatch(likedContent(contentId));
      });
  };
};

export {
  CONTENTS_FETCH_FAILED,
  CONTENTS_FETCH_STARTED,
  CONTENTS_FETCH_SUCCEEDED,
  LIKED_CONTENT,
  LIKES_RETRIEVED,
  UNLIKED_CONTENT,
  contentsFetchFailed,
  contentsFetchSucceeded,
  denormalizedContentsRetrieved,
  getContentItem,
  getUserLikes,
  likeContent,
  unlikeContent,
}
