// @flow
import _ from 'lodash';
import config from 'app/config';

const baseUrl = config.apiBaseUrl;

// Traena API
// ------------
// ------------

// ---Content
export const getContentItemByIdTpl = _.template(baseUrl + '/api/v1/contents/<%= contentId %>');
export const getContentItemsByIdsTpl = _.template(baseUrl + '/api/v1/contents/?contentIds=<%= contentIds %>');
export const postLikeOnContentTpl = _.template(baseUrl + '/api/v1/contents/<%= contentId %>/likes');
export const deleteLikeOnContentTpl = _.template(baseUrl + '/api/v1/contents/<%= contentId %>/likes');
export const getCommentsByContentIdTpl = _.template(baseUrl + '/api/v1/contents/<%= contentId %>/comments?indexed=true&maxDateTimestamp=<%= maxDateTimestamp %>');
export const postCommentTpl = _.template(baseUrl + '/api/v1/contents/<%= contentId %>/comments');
export const deleteCommentTpl = _.template(baseUrl + '/api/v1/contents/<%= contentId %>/comments/<%= commentId %>');
export const getUserLikesTpl = _.template(baseUrl + '/api/v1/users/self/likes')

// ---Feed
export const getWhatsNewTpl = _.template(baseUrl + '/api/v1/users/self/feed/whatsnew?indexed=true');

// ---Traenalist
// retrieve set of content ids for incomplete and complete traena list items
export const getCompletionsTpl = _.template(baseUrl + '/api/v1/users/self/tasks/completions');
export const postCompletionTpl = _.template(baseUrl + '/api/v1/users/self/tasks/completions?contentId=<%= contentId %>');

// ---Activity
export const getActivitesByContentIdTpl = _.template(baseUrl + '/api/v1/contents/<%= contentId %>/activities?indexed=true');
export const createActivityResponseTpl = _.template(baseUrl + '/api/v1/users/self/activityresponses')
export const getActivityResponsesTpl = _.template(baseUrl + '/api/v1/users/self/activityresponses?page=0&activityId=<%= activityId %>')
export const getActivityResults = _.template(baseUrl + '/api/v1/contents/<%= contentId %>/activities/<%= activityId %>/results');

// ---Search
export const getCommonSearchesTpl = _.template(baseUrl + '/api/v1/organizations/self/commonsearchterms');
export const getContentTagsTpl = _.template(baseUrl + '/api/v1/users/self/subscribedchannels/contenttags<%= qs %>');
export const getSearchTpl = _.template(baseUrl + '/api/v1/search/contents/<%= searchText %>?indexed=true&page=<%= page %>&size=<%= size %>');
export const getUserSearchTpl = _.template(baseUrl + '/api/v1/search/users/<%= searchText %>');
export const getTagSearchTpl = _.template(baseUrl + '/api/v1/search/contents?tags=<%= tag %>&indexed=true&page=<%= page %>&size=<%= size %>&sort=publishedTimestamp:desc');

// ---Notifications
export const getNotificationsTpl = _.template(baseUrl + '/api/v1/users/self/notifications/?indexed=true&maxDateTimestamp=<%= maxDateTimestamp %>');
export const getUnacknowledgedNotificationsCountTpl = _.template(baseUrl + '/api/v1/users/self/notifications/new/count');
export const putAcknowledgeNotificationsTpl = _.template(baseUrl + '/api/v1/users/self/notifications/new/count');
export const putRegisterNotificationsDeviceTpl = _.template(baseUrl + '/api/v1/users/self/devices/<%= deviceToken %>?platform=<%= platform %>');
export const deleteDeregisterNotificationsDeviceTpl = _.template(baseUrl + '/api/v1/users/self/devices/<%= deviceToken %>');

// ---Events
export const postEventUrl = `${baseUrl}/api/v1/events`;

// ---Users
export const getSelf = `${baseUrl}/api/v1/users/self`;
export const getSelfAnalyticsTpl = _.template(`${baseUrl}/api/v1/users/self/analytics?offset=<%= offset %>`);

// ---Channels
export const getChannelSearchTpl = _.template(baseUrl + '/api/v1/search/contents?channelId=<%= channelId %>&indexed=true&page=<%= page %>&size=<%= size %>&sort=publishedTimestamp:desc');
export const getChannelsTpl = _.template(baseUrl + '/api/v1/users/self/subscribedchannels?page=<%= page %>&size=<%= size %>&indexed=true&sort=name:asc');

// --Series
export const getSeriesForChannelTpl = _.template(baseUrl + '/api/v1/users/self/subscribedseries?indexed=true&page=<%= page %>&includeContentPreview=<%= includePreview %>&channelId=<%=channelId %>&sort=createdTimestamp:desc');
export const getSeriesPreviewTpl = _.template(baseUrl + '/api/v1/series/<%= seriesId %>?includeContentPreview=<%= includePreview %>');
export const getContentForSeriesTpl = _.template(baseUrl + '/api/v1/series/<%= seriesId %>/content?indexed=true&page=<%= page %>');
export const getSeriesProgressTpl = _.template(baseUrl + '/api/v1/users/self/seriesProgress?seriesId=<%= seriesId %>');
export const getSubscribedSeriesTpl = _.template(baseUrl + '/api/v1/users/self/subscribedseries?indexed=true&page=<%= page %>&includeContentPreview=<%= includePreview %>&sort=<%= sort %>&size=<%= size %>&filterProgressStatus=<%= filterProgressStatus %>');

// ---Bookmarks
export const getBookmarkIdsTpl = _.template(baseUrl + '/api/v1/users/self/bookmarks');
export const getBookmarkContentTpl = _.template(baseUrl + '/api/v1/users/self/bookmarks/content?indexed=true&page=<%= pageNumber %>');
export const addBookmarkTpl = _.template(baseUrl + '/api/v1/users/self/bookmarks');
export const deleteBookmarkTpl = _.template(baseUrl + '/api/v1/users/self/bookmarks/<%= contentId %>');
