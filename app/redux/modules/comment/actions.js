// @flow
import commentsApi from 'app/redux/api/comments';
import moment from 'moment';
import * as toastActions from 'app/redux/modules/toast/actions'

export const ADDED_COMMENT = 'ADDED_COMMENT';
const commentAdded = (comment: Object) => {
  return {
    type: ADDED_COMMENT,
    payload: comment
  };
};

export const REFRESHED_COMMENTS = 'REFRESHED_COMMENTS';
const commentsRefreshed = (comments) => {
  return {
    type: REFRESHED_COMMENTS,
    payload: comments
  };
};


export const RETRIEVED_COMMENTS = 'RETRIEVED_COMMENTS';
const commentsRetrieved = (comments) => {
  return {
    type: RETRIEVED_COMMENTS,
    payload: comments
  };
};

export const DELETED_COMMENT = 'DELETED_COMMENT';
const commentDeleted = (commentId: number) => {
  return {
    type: DELETED_COMMENT,
    payload: commentId,
  }
}

/**
 * Fetch comments by content id
 * @param  {[type]} contentIds [description]
 * @return {[type]}            [description]
 */
const getRecentComments = (contentId: number) => {
  return (dispatch: any) => {
    return commentsApi.retrieveCommentsByContentId(contentId)
      .then((comments) => {
        dispatch(commentsRefreshed(comments));
      });
  };
};

/**
 * Fetch comments by content id
 * @param  {[type]} contentIds [description]
 * @return {[type]}            [description]
 */
const getMoreComments = (contentId: number, lastComment: Object): * => {
  return (dispatch: any) => {
    if(!lastComment || !lastComment.createdTimestamp) {
      return Promise.reject(new Error('Must provide a lastComment'));
    }

    const maxDate = moment(lastComment.createdTimestamp);
    return commentsApi.retrieveCommentsByContentId(contentId, maxDate)
      .then((comments) => {
        dispatch(commentsRetrieved(comments));
      });
  }
};


const addComment = (contentId: number, text: string, newCommentTaggedUsers: Array<Object>) => {
  return (dispatch: any) => {
    const userIds = newCommentTaggedUsers.map(user => user.id);

    return commentsApi.addComment(contentId, text, userIds)
      .then((newComment) => {
        return dispatch(commentAdded(newComment));
      });
  }
};

const deleteComment = (contentId: number, commentId: number) => {
  return async (dispatch: any) => {
    try {
      await commentsApi.deleteComment(contentId, commentId);
      dispatch(commentDeleted(commentId));
      return true;
    } catch (err) {
      dispatch(toastActions.createToast({
        title: 'Error.',
        message: 'There was an issue deleting that comment.',
      }));
    }
  }
}

export {
  addComment,
  getRecentComments,
  getMoreComments,
  deleteComment,
}
