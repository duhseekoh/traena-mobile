/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';
import { Provider } from 'react-redux';
import store from './app/redux/store';
import Root from './app/screens/Root';
import logger from './app/logging/logger';
import codePush from 'react-native-code-push';
import config from './app/config';

logger.logBreadcrumb('index.ios.js Created');

class traena extends Component {

  constructor(props) {
    super(props);
    logger.logBreadcrumb('index.ios.js Constructor');
  }

  render() {
    return (
      <Provider store={store}>
        <Root/>
      </Provider>
    );
  }
}

const codePushedTraena = codePush({
  deploymentKey: config.codepush.deploymentKey,
})(traena);
export default codePushedTraena;

AppRegistry.registerComponent('traena', () => codePushedTraena);
