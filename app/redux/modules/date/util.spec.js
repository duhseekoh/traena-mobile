import 'react-native';
import moment from 'moment';
import dateUtil from './util';
import { PARTS_OF_DAY } from 'app/redux/modules/date/constants';

it('getPartOfDay is MORNING for any time before noon', () => {
  const morningMoment = moment('2016-11-30 11:59');
  expect(dateUtil.getPartOfDay(morningMoment)).toEqual(PARTS_OF_DAY.MORNING);
});

it('getPartOfDay is AFTERNOON for any time after noon before 6', () => {
  const afternoonMoment = moment('2016-11-30 13:59');
  expect(dateUtil.getPartOfDay(afternoonMoment)).toEqual(PARTS_OF_DAY.AFTERNOON);
});

it('getPartOfDay is EVENING for any time after 6', () => {
  const eveningMoment = moment('2016-11-30 18:59');
  expect(dateUtil.getPartOfDay(eveningMoment)).toEqual(PARTS_OF_DAY.EVENING);
});

it('isToday is true for today', () => {
  const todayMoment = moment();
  expect(dateUtil.isDateToday(todayMoment)).toBeTruthy();

  const lateTodayMoment = todayMoment.hour(23);
  expect(dateUtil.isDateToday(lateTodayMoment)).toBeTruthy();
});

it('isToday is false for not today', () => {
  const todayMoment = moment();
  const randomInteger = Math.round(Math.random() + 1);
  const someMomentInFuture = moment().add(randomInteger, 'days');
  expect(dateUtil.isDateToday(someMomentInFuture)).toBeFalsy();

  const someMomentInPast = moment().add(-randomInteger, 'days');
  expect(dateUtil.isDateToday(someMomentInPast)).toBeFalsy();
});
