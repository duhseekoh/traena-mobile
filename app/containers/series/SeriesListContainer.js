// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import type {
  NavigationNavigatorProps,
  NavigationEventSubscription,
} from 'react-navigation';
import { View } from 'react-native';
import {
  isLoadingMoreSeriesSelector,
  isRefreshingSeriesSelector,
  canLoadMoreSeriesSelector,
  displayNoMoreSeriesResultsSelector,
  seriesListWithContentSelector,
} from 'app/redux/modules/ui-subscribed-series-list/selectors';
import { refreshSeriesList, loadMoreSeries } from 'app/redux/modules/ui-subscribed-series-list/actions';
import type { ContentItem } from 'app/redux/modules/content/types';
import type { Series } from 'app/redux/modules/series/types';
import logger from 'app/logging/logger';
import type { Dispatch, ReduxState } from 'app/redux/constants';
import { SeriesPreviewList } from 'app/components';

type Props = {
  isRefreshingSeries: boolean,
  isLoadingMoreSeries: boolean,
  canLoadMoreSeries: boolean,
  displayNoMoreSeriesResults: boolean,
  seriesListWithContent: {series: Series, previewContent: ContentItem[]}[],
  refreshSeriesList: () => any,
  loadMoreSeries: () => any,
} & NavigationNavigatorProps<*, *>;

/**
 * Displays all series a user is subscribed to.
 */
class SeriesListContainer extends Component<Props> {
  _navigationListener: NavigationEventSubscription;
  componentDidMount() {
    this._navigationListener = this.props.navigation.addListener(
      'didFocus',
      () => {
        logger.logBreadcrumb('Series List displayed');
        this.props.refreshSeriesList();
      },
    );
  }

  componentWillUnmount() {
    this._navigationListener.remove();
  }

  render() {
    const {
      isRefreshingSeries,
      isLoadingMoreSeries,
      canLoadMoreSeries,
      displayNoMoreSeriesResults,
      seriesListWithContent,
      refreshSeriesList,
      loadMoreSeries,
    } = this.props;

    return (
      <View style={{ flex: 1 }}>
        <SeriesPreviewList
          refreshSeriesList={() => refreshSeriesList()}
          isRefreshing={isRefreshingSeries}
          seriesListWithContent={seriesListWithContent}
          loadNextPage={() => loadMoreSeries()}
          isLoadingMore={isLoadingMoreSeries}
          canLoadMore={canLoadMoreSeries}
          displayNoMoreResults={displayNoMoreSeriesResults}
        />
      </View>
    );
  }
}

const mapStateToProps = (state: ReduxState) => {
  return {
    seriesListWithContent: seriesListWithContentSelector(state),
    isRefreshingSeries: isRefreshingSeriesSelector(state),
    isLoadingMoreSeries: isLoadingMoreSeriesSelector(state),
    canLoadMoreSeries: canLoadMoreSeriesSelector(state),
    displayNoMoreSeriesResults: displayNoMoreSeriesResultsSelector(state),
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    refreshSeriesList: () => {
      return dispatch(refreshSeriesList());
    },
    loadMoreSeries: () => {
      return dispatch(loadMoreSeries());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withNavigation(SeriesListContainer));
