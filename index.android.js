/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  UIManager
} from 'react-native';
import { Provider } from 'react-redux';
import store from './app/redux/store';
import Root from './app/screens/Root';
import codePush from 'react-native-code-push';
import config from './app/config';

class traena extends Component {
  constructor(props) {
    super(props);
    UIManager.setLayoutAnimationEnabledExperimental &&
      UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  render() {
    return (
      <Provider store={store}>
        <Root/>
      </Provider>
    );
  }
}

const codePushedTraena = codePush({
  deploymentKey: config.codepush.deploymentKey,
})(traena);
export default codePushedTraena;

AppRegistry.registerComponent('traena', () => codePushedTraena);
