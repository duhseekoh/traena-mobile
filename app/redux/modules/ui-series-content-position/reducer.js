// @flow
import {
  SERIES_CONTENT_POSITION_LOADING,
} from './actions';
import type { Action } from 'app/types/Action';

export type ReduxStateUiSeriesContentPosition = {
  isLoading: boolean,
};

const ACTION_HANDLERS = {
  [SERIES_CONTENT_POSITION_LOADING]: (state: ReduxStateUiSeriesContentPosition, { payload }) => {
    return {
      ...state,
      isLoading: payload,
    };
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isLoading: false,
};

const uiSeriesContentPositionReducer = (state: ReduxStateUiSeriesContentPosition = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}

export default uiSeriesContentPositionReducer;
