// @flow
import React from 'react';
import { View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { ButtonPrimary, FeatherIcon } from 'app/components';
import NewCommentTaggedUsers from './NewCommentTaggedUsers';
import styleVars from 'app/style/variables';
import type { User } from 'app/redux/modules/user/types';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style?: ViewStyleProp,
  text?: string,
  taggedUsers?: User[],
  onPressPost: () => void,
  onPressTagUser: () => void,
  onPressRemoveUser: (userToRemove: User) => void,
  onTyping: (text: string) => void,
  disabled?: boolean,
};

const NewComment = ({
  style,
  text,
  taggedUsers,
  onPressPost,
  onTyping,
  onPressTagUser,
  onPressRemoveUser,
  disabled,
}: Props) => {
  return (
    <View style={[styles.container, style]}>
      <View style={styles.top}>
        <TextInput
          value={text}
          multiline={true}
          editable={!disabled}
          style={styles.input}
          placeholder={'Add a comment...'}
          underlineColorAndroid="transparent"
          onChange={event => {
            onTyping(event.nativeEvent.text);
          }}
          autoFocus
        />
        <TouchableOpacity onPress={onPressTagUser} style={styles.tagUserButton}>
          <FeatherIcon name={'user-plus'} style={styles.tagUserIcon} />
        </TouchableOpacity>
        <ButtonPrimary
          disabled={disabled || !text}
          style={styles.button}
          title={'POST'}
          onPress={onPressPost}
        />
      </View>
      {taggedUsers &&
        taggedUsers.length > 0 && (
          <View style={styles.bottom}>
            <NewCommentTaggedUsers
              taggedUsers={taggedUsers}
              onPressRemoveUser={onPressRemoveUser}
            />
          </View>
        )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: styleVars.color.lightGrey5,
  },
  top: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bottom: {
    flexDirection: 'row',
  },
  input: {
    flex: 1,
    height: styleVars.textInputHeights.medium,
    fontSize: styleVars.fontSize.medium,
    paddingLeft: 5,
    borderRadius: 5,
    backgroundColor: styleVars.color.white,
  },
  button: {
    flex: 0,
  },
  tagUserButton: {
    flex: 0,
    marginHorizontal: 10,
  },
  tagUserIcon: {
    fontSize: styleVars.iconSize.large,
    color: styleVars.color.traenaOne,
  },
});

export default NewComment;
