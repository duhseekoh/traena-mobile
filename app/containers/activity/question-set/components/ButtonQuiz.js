// @flow
import React from 'react';
import {
  StyleSheet,
} from 'react-native';
import {
  ButtonStyled,
} from 'app/components';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import styleVars from 'app/style/variables';

type Props = {
  text: string,
  style?: ViewStyleProp,
  onPress: () => *,
  disabled: boolean,
  loading: boolean,
  focused: boolean,
};


const ButtonQuiz = (props: Props) =>
  <ButtonStyled
    defaultTextStyle={styles.text}
    defaultButtonStyle={styles.button}
    focusedTextStyle={styles.activeText}
    focusedButtonStyle={styles.activeButton}
    disabledTextStyle={styles.disabledText}
    disabledButtonStyle={styles.disabledButton}
    loadingTextStyle={{}}
    loadingButtonStyle={{}}
    {...props} />

export default ButtonQuiz;

const styles = StyleSheet.create({
  text: {
    textAlign: 'center',
    fontSize: styleVars.fontSize.medium,
    color: styleVars.color.darkSlateGrey,
    padding: 16,
    backgroundColor: styleVars.color.transparent,
  },
  activeText: {
    color: styleVars.color.white,
  },
  disabledText: {
    color: styleVars.color.darkSlateGrey,
  },
  button: {
    borderColor: styleVars.color.lightGrey,
    backgroundColor: styleVars.color.white,
    borderRadius: 30,
    borderWidth: 2,
  },
  activeButton: {
    borderColor: styleVars.color.traenaOne,
    backgroundColor: styleVars.color.traenaOne,
  },
  disabledButton: {
    opacity: styleVars.opacity.disabledButton,
    borderColor: styleVars.color.lightGrey,
  }
});
