// @flow
import React, { Component }from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import { Tag } from 'app/components';
import withNavigation from '../../containers/hoc/withNavigation';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  tags?: string[],
  goToTagStandalone?: Function, // from withNavigation
  style?: ViewStyleProp,
  textStyle?: ViewStyleProp,
  isDetailView: boolean,
};

class SocialTags extends Component<Props> {
  props: Props;
  render() {
    const { style, textStyle, tags, isDetailView } = this.props;
    const containerStyle = isDetailView ? styles.containerDetail : styles.container;
    return (
      <View style={[containerStyle, style]} removeClippedSubviews={!isDetailView}>
        {tags && tags.map((tag, idx) => {
            return this._renderTag(tag, textStyle, idx);
          })
        }
      </View>
    );
  }

  _renderTag = (tag: string, textStyle?: ViewStyleProp, idx: number) => {
    const onPress = this.props.goToTagStandalone ? this.props.goToTagStandalone.bind(null, tag) : () => {};
    return (
      <Tag key={idx} value={tag} onPress={onPress}/>
    );
  }
}

export default withNavigation(SocialTags);

const styles = StyleSheet.create({
  containerDetail: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    alignItems: 'flex-start',
    overflow: 'hidden',
  },
});
