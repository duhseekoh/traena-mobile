// @flow
import jwtDecode from 'jwt-decode';
import moment from 'moment';
import { createSelector } from 'reselect';
import requestStatesEnum from './requestStatesEnum';
import config from 'app/config';
import logger from 'app/logging/logger';
import type { Auth0DecodedIdToken, Auth0UserClaim } from './types';
import type { ReduxState } from 'app/redux/constants';

// Direct reducer state selectors
const idTokenSelector = (state: ReduxState): string => state.auth.idToken;
// this one is used in authHeadersSelector which is accessed directly on the store, which could have an empty state
// probably a better way to type out the ReduxState to account for this, but for now this works.
const accessTokenSelector = (state: ReduxState | Object): string => state.auth && state.auth.accessToken;
const loginRequestStateSelector = (state: ReduxState): string => state.auth.loginRequestState;
const isLoggedInSelector = (state: ReduxState): boolean => state.auth.isLoggedIn;

/**
 * Get the data from idToken
 */
const decodedIdTokenSelector = createSelector(
  [idTokenSelector],
  (idToken): ?Auth0DecodedIdToken => idToken ? jwtDecode(idToken) : null,
);

/**
 * Get the data from accessToken
 */
const decodedAccessTokenSelector = createSelector(
  [accessTokenSelector],
  (accessToken): Object | null => {
    if (!accessToken) {
      return null;
    }
    try {
      return jwtDecode(accessToken);
    } catch (err) {
      logger.logBreadcrumb('could not decode access token', { accessToken });
      logger.logError(err);
      return null;
    }
  },
);

/**
 * Gets all of the user claim info from the id token.
 * e.g. email, details, groups, roles, permissions
 */
const auth0UserSelector = createSelector(
  [decodedIdTokenSelector],
  (idTokenData: ?Auth0DecodedIdToken): ?Auth0UserClaim => idTokenData ? idTokenData[config.auth0.userClaimNamespace] : null,
);

/**
 * Gets the access token and creates an Authorization header for use in api requests
 */
const authHeadersSelector = createSelector(
  [accessTokenSelector],
  (jwt: string): Object => {
    if (!jwt) return {};
    return {
      'Authorization': `Bearer ${jwt}`,
    };
  },
);

/**
 * Checks if the jwt stored from the auth reducer exists and has an expiration
 * in the future.
 * Note: Cannot use reselect memoization here since we need to check the
 *  expiration, as opposed to a data mutation
 * @type {Boolean} - true for valid, false for invalid
 */
const isJwtValidSelector = (state: ReduxState): boolean => {
  const accessTokenData: ?Object = decodedAccessTokenSelector(state);
  if(!accessTokenData) {
    logger.logBreadcrumb('no accessToken to validate');
    return false;
  }

  try {
    const { exp }: { exp: string } = accessTokenData;
    const expMoment: moment = moment.utc(exp, 'X');
    const nowMoment: moment = moment.utc();
    const isNowBeforeExpired: boolean = nowMoment.isBefore(expMoment);
    logger.logBreadcrumb('isJwtValidSelector: checking', {
      exp: expMoment.format(),
      now: nowMoment.format(),
      isNowBeforeExpired,
    });
    return isNowBeforeExpired;
  } catch(error) {
    logger.logBreadcrumb('access token unexpected format', accessTokenData);
    logger.logError(error);
    return false;
  }
};

const isLoginInProgressSelector = createSelector([loginRequestStateSelector],
  (loginRequestState: string): boolean => {
    return loginRequestState === requestStatesEnum.IN_PROGRESS;
  }
);

const loginErrorMessageSelector = createSelector([loginRequestStateSelector],
  (loginRequestState: string): ?string => {
    if(loginRequestState === requestStatesEnum.BAD_CREDENTIALS_FAILURE) {
      return 'You entered an invalid username or password. Please try again.';
    } else if(loginRequestState === requestStatesEnum.UNKNOWN_FAILURE) {
      return 'A network or system error has occurred. Make sure you are connected to the internet.';
    }
  }
);

const forgotPasswordUrlSelector = (): string => {
  const { clientId, domain, dbConnectionName } = config.auth0;
  return `https://${domain}/login?client=${clientId}&connection=${dbConnectionName}&popup=false&forgot_password_only_view=1`
};

export {
  authHeadersSelector,
  forgotPasswordUrlSelector,
  isJwtValidSelector,
  isLoggedInSelector,
  isLoginInProgressSelector,
  auth0UserSelector,
  loginErrorMessageSelector,
};
