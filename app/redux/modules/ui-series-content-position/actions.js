// @flow
import { getSeriesById, getSeriesProgress } from 'app/redux/modules/series/actions';
import { selectSeriesById, seriesProgressSelector } from 'app/redux/modules/series/selectors';
import type { Dispatch, GetState } from 'app/redux/constants';

export const SERIES_CONTENT_POSITION_LOADING = 'SERIES_CONTENT_POSITION_LOADING';
const setLoading = (loading: boolean) => {
  return {
    type: SERIES_CONTENT_POSITION_LOADING,
    payload: loading,
  };
};

export function initialize(seriesId: number) {
  return async (dispatch: Dispatch, getState: GetState) => {
    try {
      dispatch(setLoading(true));
      const initMethods = [];

      if (!seriesProgressSelector(getState(), { id: seriesId })) {
        initMethods.push(dispatch(getSeriesProgress(seriesId)));
      }

      if (!selectSeriesById(getState(), { seriesId })) {
        initMethods.push(dispatch(getSeriesById(seriesId)));
      }

      await Promise.all(initMethods);
      dispatch(setLoading(false));
    } catch (err) {
      console.log('uiSeriesContentPosition.initialize error: ', err);
      dispatch(setLoading(false));
    }
  }
}
