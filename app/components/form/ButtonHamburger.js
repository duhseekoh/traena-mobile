// @flow
import React from 'react';
import { StyleSheet, View } from 'react-native';
import ButtonStyled, { type Props as ButtonStyledProps } from './ButtonStyled';
import { FeatherIcon } from 'app/components';
import styleVars from 'app/style/variables';

type OwnProps = {
  showIndicator?: boolean,
};
export type Props = OwnProps & ButtonStyledProps;

const ButtonHamburger = (props: Props) => (
  <ButtonStyled
    style={styles.container}
    icon={
      <View>
        <FeatherIcon name="menu" fontSize={styleVars.iconSize.large} />
        {props.showIndicator && (
          <View style={styles.indicator} pointerEvents="none" />
        )}
      </View>
    }
    {...props}
  />
);

ButtonHamburger.defaultProps = {
  showIndicator: false,
};

export default ButtonHamburger;

const styles = StyleSheet.create({
  container: {
    // could be moved to a wrapper around this that could be reused for all headerLeft buttons
    paddingLeft: styleVars.padding.edgeOfScreen,
  },
  indicator: {
    position: 'absolute',
    top: 3,
    right: 0,
    width: 8,
    height: 8,
    borderRadius: 8,
    backgroundColor: styleVars.color.traenaPrimaryGreen,
  },
});
