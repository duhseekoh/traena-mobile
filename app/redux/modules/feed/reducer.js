// @flow
import {
  REFRESH_FEED_REQUEST_STARTED, REFRESH_FEED_REQUEST_FINISHED,
} from './actions';
import type { Action } from 'app/types/Action';

export type ReduxStateFeed = {
  isRefreshing: boolean,
};

const ACTION_HANDLERS = {
  [REFRESH_FEED_REQUEST_STARTED]: (state: ReduxStateFeed) => {
    return {
      ...state,
      isRefreshing: true,
    };
  },
  [REFRESH_FEED_REQUEST_FINISHED]: (state: ReduxStateFeed) => {
    return {
      ...state,
      isRefreshing: false,
    };
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isRefreshing: false,
};

const feedReducer = (state: ReduxStateFeed = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

export default feedReducer;
