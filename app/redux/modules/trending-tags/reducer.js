// @flow
import {
  LOAD_TRENDING_TAGS,
  LOAD_TRENDING_TAGS_REQUEST_STARTED,
  LOAD_TRENDING_TAGS_REQUEST_FINISHED,
} from './actions';
import type { Action } from 'app/types/Action';
import type { TagAggregation } from './types';

export type ReduxStateTrendingTags = {
  tags: TagAggregation[],
  isLoading: boolean,
};

const ACTION_HANDLERS = {
  [LOAD_TRENDING_TAGS]: (state: ReduxStateTrendingTags, action: Action) => {
    const tags = action.payload;
    return {
      ...state,
      tags,
    };
  },
  [LOAD_TRENDING_TAGS_REQUEST_STARTED]: (state: ReduxStateTrendingTags) => {
    return {
      ...state,
      isLoading: true,
    };
  },
  [LOAD_TRENDING_TAGS_REQUEST_FINISHED]: (state: ReduxStateTrendingTags) => {
    return {
      ...state,
      isLoading: false,
    };
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  tags: [],
  isLoading: false,
};

const trendingTagsReducer = (state: ReduxStateTrendingTags = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

export default trendingTagsReducer;
