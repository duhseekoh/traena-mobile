// @flow
import {
  LOAD_BOOKMARKS_WIDGET_REQUEST_STARTED,
  LOAD_BOOKMARKS_WIDGET_REQUEST_FINISHED,
} from './actions';
import type { Action } from 'app/types/Action';

export type ReduxStateBookmarksWidget = {
  isLoading: boolean,
  contentIds: number[],
};

const ACTION_HANDLERS = {
  [LOAD_BOOKMARKS_WIDGET_REQUEST_STARTED]: (state: ReduxStateBookmarksWidget) => {
    return {
      ...state,
      isLoading: true,
    };
  },
  [LOAD_BOOKMARKS_WIDGET_REQUEST_FINISHED]: (state: ReduxStateBookmarksWidget, action: Action) => {
    const contentIds = action.payload;
    return {
      ...state,
      isLoading: false,
      contentIds,
    };
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isLoading: false,
  contentIds: [],
};

const bookmarksWidgetReducer = (state: ReduxStateBookmarksWidget = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

export default bookmarksWidgetReducer;
