// @flow
import React from 'react';
import {
  StyleSheet
} from 'react-native';
import { TRAText } from 'app/components';

type Props = {
  children: Node,
};

const SectionHeader = (props: Props) => {
  return (
    <TRAText style={styles.header}>{props.children}</TRAText>
  )
}

const styles = StyleSheet.create({
  header: {
    // LAYOUT
    flex: 1,
    padding: 20,
    // TEXT
    fontSize: 20,
    textAlign: 'center'
  }
});

export default SectionHeader;
