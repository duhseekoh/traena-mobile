// @flow
import { USER_RETRIEVED } from './actions';
import type { User } from './types';
import type { Action } from 'app/types/Action';

const ACTION_HANDLERS = {
  [USER_RETRIEVED]: (state: ReduxStateUser, action: Action) => {
    return {
      ...state,
      user: action.payload,
    }
  }
};

// ------------------------------------
// Reducer
// ------------------------------------
export type ReduxStateUser = {
  user: ?User,
};

const initialState: ReduxStateUser = {
  user: null,
};

export default function user(state: ReduxStateUser = initialState, action: Action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}
