// @flow
import api from 'app/redux/api';
import { contentsFetchSucceeded } from 'app/redux/modules/content/actions';
import type { IndexedPage } from '../../../types/Pagination';
import type { ContentItem } from 'app/redux/modules/content/types';

export const LOAD_WHATS_NEW_REQUEST_STARTED = 'LOAD_WHATS_NEW_REQUEST_STARTED';
const startedLoadWhatsNewRequest = () => {
  return {
    type: LOAD_WHATS_NEW_REQUEST_STARTED,
  };
};

export const LOAD_WHATS_NEW_REQUEST_FINISHED = 'LOAD_WHATS_NEW_REQUEST_FINISHED';
const finishedLoadWhatsNewRequest = () => {
  return {
    type: LOAD_WHATS_NEW_REQUEST_FINISHED,
  };
};

export const LOAD_WHATS_NEW = 'LOAD_WHATS_NEW';
function loadWhatsNew(whatsNew: IndexedPage<number, ContentItem>) {
  return {
    type: LOAD_WHATS_NEW,
    payload: whatsNew
  };
}

export function initialize() {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(startedLoadWhatsNewRequest());
      const results: IndexedPage<number, ContentItem> = await api.feed.retrieveWhatsNew();
      dispatch(contentsFetchSucceeded(results.index));
      dispatch(loadWhatsNew(results));
      dispatch(finishedLoadWhatsNewRequest());
    } catch (err) {
      console.log('whatsNewActions.initialize error: ', err);
      dispatch(finishedLoadWhatsNewRequest());
    }
  }
}
