// @flow
import _ from 'lodash';
import { LIKED_CONTENT, UNLIKED_CONTENT } from './actions';
import { ADDED_COMMENT, RETRIEVED_COMMENTS } from 'app/redux/modules/comment/actions';
import type { Action } from 'app/types/Action';

/**
 * Note that this state is partial and not stored in the state tree.
 * It is meant to be used in other reducers as reducer that
 * encapsulates logic to items that have social capabilities.
 */
const ACTION_HANDLERS = {
  [LIKED_CONTENT]: (state, action) => {
    let contentId = action.payload;

    return modifyLikeStatusOfContent(state, contentId, true);
  },
  [UNLIKED_CONTENT]: (state, action) => {
    let contentId = action.payload;

    return modifyLikeStatusOfContent(state, contentId, false);
  },
  [ADDED_COMMENT]: (state, action) => {
    let { contentId } = action.payload;
    if(contentId !== state.id) {
      return state;
    }

    let updatedState = _.cloneDeep(state);
    updatedState.commentCount++;
    return updatedState;
  },
  [RETRIEVED_COMMENTS]: (state, action) => {
    const { contentId, totalCount } = action.payload;
    if(contentId !== state.id) {
      return state;
    }

    let updatedState = _.cloneDeep(state);
    updatedState.commentCount = totalCount;
    return updatedState;
  }
};

const modifyLikeStatusOfContent = (contentState: Object, contentId: number, doesLike: boolean) => {
  // Non matching content will pass through here, just don't do anything with it
  if(contentId !== contentState.id) {
    return contentState;
  }

  let updatedState = _.cloneDeep(contentState);
  doesLike ? updatedState.likeCount++
    : updatedState.likeCount--;

  return updatedState;
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {};

// TODO - state should be ContentItemRaw 
const itemReducer = (state: Object = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default itemReducer;
