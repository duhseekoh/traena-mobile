// @flow
import {
  TOAST_CLEAR,
  TOAST_CREATE,
} from './actions';
import type { Toast } from 'app/types/Toast';
import type { Action } from 'app/types/Action';

export type ReduxStateToast = {
  toast: ?Toast,
};

const initialState: ReduxStateToast = {
  toast: null,
};

function toast(
  state: ReduxStateToast = initialState,
  { type, payload }: Action,
) {
  // eslint-disable-line
  switch (type) {
    case TOAST_CLEAR: {
      return {
        ...state,
        toast: null,
      };
    }

    case TOAST_CREATE:
      return {
        ...state,
        toast: payload,
      };

    default:
      return state;
  }
}

export default toast;
