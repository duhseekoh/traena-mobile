// @flow
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Animated,
  LayoutAnimation
} from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import styleVars from 'app/style/variables';

type Props = {
  data: any[],
  style?: ViewStyleProp,
  renderItem: (item: any) => React.DOM.Node,
};

type State = {
  galleryWidth: number,
};

class HorizontalSlider extends Component<Props, State> {
  scrollX = new Animated.Value(0);

  constructor(props: Props) {
    super(props);

    this.state = {
      galleryWidth: 0,
    };
  }

  onScrollViewLayout = ({nativeEvent}: *) => {
    // Don't update if width is the same
    if (nativeEvent.layout.width === this.state.galleryWidth) {
      return;
    }
    LayoutAnimation.easeInEaseOut();
    this.setState({
      galleryWidth: nativeEvent.layout.width,
    });
  }

  renderPager(count: number) {
    if(!this.state.galleryWidth || count === 1) {
      return null;
    }

    const position = Animated.divide(this.scrollX, this.state.galleryWidth);

    let pages = [];
    for(let i = 0; i < count; i++) {
      const opacity = position.interpolate({
        inputRange: [i - 1, i, i + 1],
        outputRange: [0.3, 1, 0.3],
        extrapolate: 'clamp'
      });
     pages.push(
       <Animated.View
         key={i}
         style={{ opacity, height: 6, width: 6, backgroundColor: styleVars.color.darkGrey4, margin: 4, borderRadius: 3 }}
       />);
    }
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
        {pages}
      </View>);
  }

  render() {
    const { data } = this.props;
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1}}>
          <ScrollView
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { x: this.scrollX } } }]
            )}
            // when there is no width yet, the container inside will match the width of the scrollview
            contentContainerStyle={[styles.slideContainer, !this.state.galleryWidth && { flex: 1 }]}
            style={styles.scrollView}
            showsHorizontalScrollIndicator={false}
            horizontal
            bounces={true}
            scrollEventThrottle={16}
            onLayout={this.onScrollViewLayout}
            pagingEnabled>
            { // if the width hasn't been determined yet, then display only one card that takes up
              // the entire inside of the scrollview.
              data.map((item, i) => {
                if (!this.state.galleryWidth && i !== 0) {
                  return null;
                }
                return this.renderPage(item, this.state.galleryWidth || '100%');
              })
            }
          </ScrollView>
          {this.renderPager(data.length)}
        </View>
      </View>
    )
  }

  renderPage = (item: any, pageWidth: string | number) => {
    return (
      <View
        key={item.id}
        style={{flex: 1, width: pageWidth, paddingHorizontal: 10}}
      >
        {this.props.renderItem(item)}
      </View>
    )
  }
}

export default HorizontalSlider;
const styles = StyleSheet.create({
  slideContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollView: {
    flex: 1,
  },
});
