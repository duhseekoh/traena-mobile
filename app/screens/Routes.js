// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View, Platform } from 'react-native';

import type {
  NavigationNavigatorProps,
  NavigationAction,
  NavigationStackScreenOptions,
} from 'react-navigation';
import { createDrawerNavigator, createStackNavigator } from 'react-navigation';
import type { Dispatch } from 'app/redux/constants';

// Drawer screens
import Drawer from './drawer/Drawer';
import BookmarkScreen from './drawer/screens/BookmarkScreen';
import FeedScreen from './drawer/screens/FeedScreen';
import NotificationsScreen from './drawer/screens/NotificationsScreen';
import SearchScreen from './drawer/screens/SearchScreen';
import ProfileScreen from './drawer/screens/ProfileScreen';
import SeriesListScreen from './drawer/screens/SeriesListScreen';

// Standalone screens
import UserSearchScreen from './UserSearchScreen';
import CommentsScreen from './CommentsScreen';
import ContentStandaloneScreen from './ContentStandaloneScreen';
import VideoAndroidFullscreen from './VideoAndroidFullscreen';
import ImageFullscreen from './ImageFullscreen';
import ActivityScreen from './ActivityScreen';
import TagStandaloneScreen from './TagStandaloneScreen';
import ChannelStandaloneScreen from './ChannelStandaloneScreen';
import SeriesStandaloneScreen from './SeriesStandaloneScreen';
import ChannelListScreen from './ChannelListScreen';

import { ROUTES } from 'app/redux/modules/navigation/constants';

import RootScreensHeaderLeft from './drawer/components/RootScreensHeaderLeft';

import {
  navigationService,
  trackNavigationEvent,
  initializeSceneOnNavigate,
} from 'app/redux/modules/navigation/actions';

import styleVars from 'app/style/variables';

/**
 * All screens get this default configuration. It's mainly to style the header
 * of each screen consistently. Overrides can be given at the screen level.
 */
const defaultStackOptions: NavigationStackScreenOptions = {
  headerTitleStyle: {
    fontSize: styleVars.fontSize.medium,
    fontFamily: styleVars.fontFamily.bold,
    // react navigation must be setting some default font weight. to get a bold
    // font in the correct family to display here, need to have different weights
    // for the two platforms.
    fontWeight: Platform.OS === 'android' ? 'normal' : 'bold',
    // by default react navigation for android aligns titles to left side.
    // this centers them across platforms.
    flex: 1,
    textAlign: 'center',
  },
  headerStyle: {
    backgroundColor: styleVars.color.white,
    borderBottomWidth: 0,
    ...styleVars.shadow.shadowOne,
  },
  headerTintColor: styleVars.color.black,
  headerBackTitle: ' ',
  // because :( -- https://github.com/react-navigation/react-navigation/issues/1728#issue-232311323
  headerRight: <View />,
};

/**
 * The top level routes are accessible from the drawer nav. This gives default
 * configuratin for each of those. Namely, the placement of the hamburger menu
 * on the left.
 */
const defaultDrawerItemScreenOptions = ({
  navigation,
}): NavigationStackScreenOptions => ({
  headerLeft: (
    <RootScreensHeaderLeft onPressHamburger={() => navigation.toggleDrawer && navigation.toggleDrawer()} />
  ),
});

const UniversalRoutes = {
  [ROUTES.SEARCH]: {
    screen: SearchScreen,
  },
  [ROUTES.COMMENTS]: {
    screen: CommentsScreen,
  },
  [ROUTES.TAG_STANDALONE]: {
    screen: TagStandaloneScreen,
  },
  [ROUTES.CHANNEL_STANDALONE]: {
    screen: ChannelStandaloneScreen,
  },
  [ROUTES.SERIES_STANDALONE]: {
    screen: SeriesStandaloneScreen,
  },
  [ROUTES.USER_SEARCH]: {
    screen: UserSearchScreen,
  },
  [ROUTES.CONTENT_STANDALONE]: {
    screen: ContentStandaloneScreen,
    path: '/content/:contentId/view',
  },
  [ROUTES.VIDEO_ANDROID_FULLSCREEN]: {
    screen: VideoAndroidFullscreen,
  },
  [ROUTES.IMAGE_FULLSCREEN]: {
    screen: ImageFullscreen,
  },
  [ROUTES.ACTIVITY]: {
    screen: ActivityScreen,
  },
  [ROUTES.SERIES_LIST]: {
    screen: SeriesListScreen,
  },
  [ROUTES.CHANNEL_LIST]: {
    screen: ChannelListScreen,
  },
};

const DrawerRoutes = createDrawerNavigator(
  {
    [ROUTES.HOME]: createStackNavigator({
      'main': {
        screen: FeedScreen,
        // root drawer items get special treatment
        navigationOptions: defaultDrawerItemScreenOptions,
      },
      ...UniversalRoutes,
    }, {
      // defaults for all screens in stack
      navigationOptions: defaultStackOptions,
    }),
    [ROUTES.BOOKMARKS]: createStackNavigator({
      'main': {
        screen: BookmarkScreen,
        navigationOptions: defaultDrawerItemScreenOptions,
      },
      ...UniversalRoutes,
    }, {
      navigationOptions: defaultStackOptions,
    }),
    [ROUTES.NOTIFICATIONS]: createStackNavigator({
      'main': {
        screen: NotificationsScreen,
        navigationOptions: defaultDrawerItemScreenOptions,
      },
      ...UniversalRoutes,
    }, {
      navigationOptions: defaultStackOptions,
    }),
    [ROUTES.PROFILE]: createStackNavigator({
      'main': {
        screen: ProfileScreen,
        navigationOptions: defaultDrawerItemScreenOptions,
      },
      ...UniversalRoutes,
    }, {
      navigationOptions: defaultStackOptions,
    }),
    [ROUTES.CHANNEL_LIST_DRAWER]: createStackNavigator({
     'main': {
       screen: ChannelListScreen,
       navigationOptions: defaultDrawerItemScreenOptions,
     },
     ...UniversalRoutes,
   }, {
     navigationOptions: defaultStackOptions,
   }),
    [ROUTES.SERIES_LIST_DRAWER]: createStackNavigator({
      'main': {
        screen: SeriesListScreen,
        navigationOptions: defaultDrawerItemScreenOptions,
      },
      ...UniversalRoutes,
    }, {
      navigationOptions: defaultStackOptions,
    }),
  },
  {
    initialRouteName: ROUTES.HOME,
    contentComponent: Drawer,
    drawerBackgroundColor: styleVars.color.forestGreen,
  },
);

const Routes = DrawerRoutes;

const defaultGetStateForAction = Routes.router.getStateForAction;
Routes.router.getStateForAction = (action, state) => {
  // We used to block double navigation to your own route here. react-navigation
  // now supports providing a `key` to each route and knows to precent double
  // navigation automatically. So this does nothing now, but could be used to
  // block navigation for some other reason down the road.
  // https://reactnavigation.org/docs/en/routers.html#blocking-navigation-actions

  return defaultGetStateForAction(action, state);
};

type Props = {
  initializeSceneOnNavigate: () => any,
  trackNavigationEvent: (action: NavigationAction) => any,
} & NavigationNavigatorProps<*, {}>;

class TopLevelNavigator extends Component<Props, *> {
  onNavigationStateChange = (prevState, newState, action) => {
    this.props.trackNavigationEvent(action);
    this.props.initializeSceneOnNavigate();
  };

  render() {
    return (
      <Routes
        ref={navigatorRef => {
          navigationService.setTopLevelNavigator(navigatorRef);
        }}
        onNavigationStateChange={this.onNavigationStateChange}
      />
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
  trackNavigationEvent: (action: NavigationAction) =>
    dispatch(trackNavigationEvent(action)),
  initializeSceneOnNavigate: () => dispatch(initializeSceneOnNavigate()),
});

export default connect(null, mapDispatchToProps)(TopLevelNavigator);
