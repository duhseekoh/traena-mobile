import { createSelector } from 'reselect';
import { contentItemsSelectorFactory } from 'app/redux/modules/content/selectors';
import { REQUEST_STATUSES } from 'app/redux/constants';

const STATE_KEY = 'tagStandalone';
const stateFromRoot = (state) => state[STATE_KEY];

const contentIdsSelector = createSelector([
  stateFromRoot,
  (_, { tag }) => tag,
], (state, tag) => {
  const uiSlices = state.uiSlices;
  return (uiSlices && uiSlices[tag]) ? uiSlices[tag].contentIds : [];
});

const contentSelector = contentItemsSelectorFactory(contentIdsSelector);

const isLoadingMoreSelector = (state, { tag }) => {
  const uiSlice = stateFromRoot(state).uiSlices[tag];
  return uiSlice && uiSlice.loadingStatus === REQUEST_STATUSES.IN_PROGRESS;
}

const isRefreshingSelector = (state, { tag }) => {
  const uiSlice = stateFromRoot(state).uiSlices[tag];
  return uiSlice && uiSlice.refreshingStatus === REQUEST_STATUSES.IN_PROGRESS;
}

const hasErrorSelector = (state, { tag }) => {
  const uiSlice = stateFromRoot(state).uiSlices[tag];
  return uiSlice && (
    uiSlice.loadingStatus === REQUEST_STATUSES.ERROR || uiSlice.refreshingStatus === REQUEST_STATUSES.ERROR);
}

const canLoadMoreSelector = (state, { tag }) => {
  const uiSlice = stateFromRoot(state).uiSlices[tag];
  return uiSlice && stateFromRoot(state).uiSlices[tag].canLoadMore;
}

const currentPageSelector = (state, { tag }) => {
  const uiSlice = stateFromRoot(state).uiSlices[tag];
  return uiSlice ? stateFromRoot(state).uiSlices[tag].currentPage : 0;
}

/**
 * Are we at the bottom of the results list? Can't be loading, cant be
 refreshing, and make sure that we have hit the end of paging.
 */
const displayNoMoreResultsSelector = createSelector([
  isLoadingMoreSelector,
  isRefreshingSelector,
  canLoadMoreSelector,
], (isLoadingMore, isRefreshing, canLoadMore) => {
  return !isLoadingMore && !isRefreshing && !canLoadMore;
});

export {
  STATE_KEY,
  contentSelector,
  isLoadingMoreSelector,
  isRefreshingSelector,
  hasErrorSelector,
  canLoadMoreSelector,
  currentPageSelector,
  displayNoMoreResultsSelector,
};
