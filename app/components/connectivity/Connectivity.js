// @flow
import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { FeatherIcon, TRAText, TRATextStrong } from 'app/components';
import styleVars from 'app/style/variables';

import { connect } from 'react-redux';
import { isConnectedSelector } from 'app/redux/modules/app-state/selectors';

type Props = {
  isConnected?: boolean,
};

type State = {
  dismissed: boolean,
};

class Connectivity extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      dismissed: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isConnected !== this.props.isConnected) {
      this.setState({ dismissed: false });
    }
  }

  render() {
    const { isConnected } = this.props;
    const { dismissed } = this.state;
    if (!isConnected && !dismissed) {
      return (
        <View style={styles.connectivityComponent}>
          <View style={styles.messageHolder}>
            <TRAText style={styles.connectivityComponentMessage}>
              <FeatherIcon
                fontSize={styleVars.iconSize.small}
                color={styleVars.color.black}
                name={'alert-triangle'}
              /> You are not connected to the internet.
            </TRAText>
          </View>
          <View style={styles.dismissHolder}>
            <TouchableOpacity onPress={this.dismiss}>
              <TRATextStrong style={styles.dismissText}>DISMISS</TRATextStrong>
            </TouchableOpacity>
          </View>
        </View>
      )
    }
    return null;
  }

  dismiss = () => {
    this.setState({ dismissed: true });
  }
}

const styles = StyleSheet.create({
  connectivityComponent: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: styleVars.color.white,
    paddingVertical: 20,
    width: '100%',
    opacity: .85,
  },
  messageHolder: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 5,
  },
  connectivityComponentMessage: {
    textAlign: 'center',
    fontSize: styleVars.fontSize.medium,
  },
  dismissHolder: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 10,
  },
  dismissText: {
    color: styleVars.color.traenaPrimaryGreen,
    fontSize: styleVars.fontSize.small,
  },
});

const mapStateToProps = (state) => ({
  isConnected: isConnectedSelector(state),
});

export default connect(mapStateToProps, null)(Connectivity);
