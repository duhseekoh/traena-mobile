import 'react-native';
import tagStandaloneReducer from './reducer';
import { REFRESHED_TAG_CONTENT } from './actions';

it('action REFRESHED_TAG_CONTENT sets the tag associated content', () => {
  const awesomeFeed = ['some-content-id', 'some-content-id-2'];
  const awesomeTag = 'awesome';
  const state = tagStandaloneReducer(undefined, {type: REFRESHED_TAG_CONTENT, payload: { ids: awesomeFeed, tag: awesomeTag }});
  expect(state.uiSlices[awesomeTag].contentIds).toEqual(awesomeFeed);
});
