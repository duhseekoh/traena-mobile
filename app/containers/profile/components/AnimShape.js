// @flow
/* Inspiration from: https://medium.com/the-react-native-log/animated-charts-in-react-native-using-d3-and-art-21cd9ccf6c58 */

import React from 'react';
import {
  ART,
} from 'react-native';

const {
  Shape,
} = ART;

import Morph from 'art/morph/path';


type Props = {
  d: string,
};

type State = {
  path: any,
};

const AnimationDurationMs = 250;

export default class AnimShape extends React.Component<Props, State> {
  previousGraph: any;
  animating: any;
  constructor(props: Props) {
    super(props);
    this.state = {
      path: '',
    }
  }

  componentWillMount() {
    this.computeNextState(this.props);
  }

  componentWillReceiveProps(nextProps: Props) {
    this.computeNextState(nextProps);
  }

  // Animations based on: https://github.com/hswolff/BetterWeather
  computeNextState(nextProps: Props) {
    const graph = this.props.d;

    // The first time this function is hit we need to set the initial
    // this.previousGraph value.
    if (!this.previousGraph) {
      this.previousGraph = graph;
    }

    // Only animate if our properties change. Typically this is when our
    // yAccessor function changes.
    if (this.props !== nextProps && graph !== this.previousGraph) {
      const pathFrom = this.previousGraph;

      const pathTo = graph;

      cancelAnimationFrame(this.animating);
      this.animating = null;

      this.setState({
        // Create the ART Morph.Tween instance.
        path: Morph.Tween( // eslint-disable-line new-cap
          pathFrom,
          pathTo,
        ),
      }, () => {
        // Kick off our animations!
        this.animate();
      });

      this.previousGraph = graph;
    }
  }

  // This is where we animate our graph's path value.
  animate(start: any) {
    this.animating = requestAnimationFrame((timestamp) => {
      if (!start) {
        start = timestamp;
      }

      // Get the delta on how far long in our animation we are.
      const delta = (timestamp - start) / AnimationDurationMs;

      // If we're above 1 then our animation should be complete.
      if (delta > 1) {

        this.animating = null;
        // Just to be safe set our final value to the new graph path.
        this.setState({
          path: this.previousGraph,
        });

        // Stop our animation loop.
        return;
      }

      // Tween the SVG path value according to what delta we're currently at.
      this.state.path.tween(delta);

      this.setState(this.state, () => {
        this.animate(start);
      });
    });
  }

  render() {
    const path = this.state.path; //eslint-disable-line
    return (
       <Shape
         {...this.props}
         d={path}
        />
    );
  }
}
