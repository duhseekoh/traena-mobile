// @flow
import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { TRAText } from 'app/components';
import styleVars from 'app/style/variables';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  style?: ViewStyleProp,
  imageSource: string,
  title: string,
  message: string,
};

/**
 * This component renders a card on the feed that
 * can be used to show empty states and loading states for feed content,
 */
const FeedStatusCard = ({ style, imageSource, title, message }: Props) => {
  return (
    <View style={[styles.card, style]}>
      <Image source={imageSource} style={styles.image}  />
      <TRAText style={styles.cardTitle}>{title}</TRAText>
      <View style={{ paddingHorizontal: 20 }}>
        <TRAText style={styles.cardText}>{message}</TRAText>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    aspectRatio: 1.6,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: styleVars.color.lightGrey3,
    backgroundColor: styleVars.color.white,
    paddingVertical: 10,
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
  },
  cardTitle: {
    fontFamily: styleVars.fontFamily.medium,
    fontSize: styleVars.fontSize.large,
    color: styleVars.color.forestGreen,
    textAlign: 'center',
  },
  cardText: {
    fontFamily: styleVars.fontFamily.medium,
    fontSize: styleVars.fontSize.medium,
    color: styleVars.color.forestGreen,
    textAlign: 'center',
  },
});

export default FeedStatusCard;
