// @flow
import Icon from 'react-native-vector-icons/Ionicons';
import React from 'react';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import type { Color, Size } from './types';

type Props = {
  style?: ViewStyleProp,
  name: 'ios-stats',
  fontSize: Size,
  color: Color,
};

const IONIcon = ({ fontSize, color, style, name }: Props) => {

  return (
    <Icon
      style={[
        { fontSize, color },
        style,
      ]}
      name={name}
    />
  );
};

export default IONIcon;
