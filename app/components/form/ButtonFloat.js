// @flow
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { TraenaIcon, TRATextStrong } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  title: string,
  style?: ViewStyleProp,
  icon: string,
  onPress: () => *,
  disabled?: boolean,
};

const ButtonFloat = ({title, style, onPress, disabled, icon}: Props) => {
  const buttonOnPress = disabled ? () => {} : onPress;
  const buttonWrapperStyle = disabled ? [style, styles.button, styles.buttonDisabled]
    : [style, styles.button];

  return (
    <TouchableOpacity style={styles.buttonWrap} onPress={buttonOnPress}>
      <View style={buttonWrapperStyle}>
        <TraenaIcon name={icon} fontSize={18}
        color={styleVars.color.white} />
        <TRATextStrong style={styles.buttonText}>{title}</TRATextStrong>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  buttonText: {
    textAlign: 'center',
    fontSize: 18,
    color: styleVars.color.white,
    paddingLeft: 10,
  },
  button: {
    backgroundColor: styleVars.color.traenaOne,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 14,
  },
  buttonWrap: {
    width: '100%',
  },
  buttonDisabled: {
    backgroundColor: styleVars.color.lightGrey,
  }
});

export default ButtonFloat;
