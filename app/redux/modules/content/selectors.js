// @flow
import { createSelector } from 'reselect';
import { completeContentIdsSelector } from 'app/redux/modules/traena-task/selectors';
import { bookmarkContentIdsSelector } from 'app/redux/modules/bookmark/selectors';
import { REQUEST_STATUSES } from 'app/redux/constants';
import type { ReduxState } from 'app/redux/constants';
import type { ContentItem } from './types';

const STATE_KEY = 'content';
const stateFromRoot = (state: ReduxState) => state[STATE_KEY];
const allContentIdsSelector = (state: ReduxState): number[] => Object.keys(stateFromRoot(state).contentItems).map(id => parseInt(id, 10));
const rawContentItemsSelector = (state: ReduxState) => stateFromRoot(state).contentItems;
const likedContentIdsSelector = (state: ReduxState) => stateFromRoot(state).likedContentIds;
const isContentItemLoadingSelector = (state: ReduxState, { contentId }: { contentId: number}) => {
  const request = stateFromRoot(state).contentItemRequests[contentId];
  return request && request.status === REQUEST_STATUSES.IN_PROGRESS;
};

/**
 * Create a selector for content items. Allows another module to specify a selector
 * for supplying the content ids.
 * @param  {selector} contentIdsSelector selector that supplys an array of content ids
 * @return {[type]}                    [description]
 */
const contentItemsSelectorFactory = (contentIdsSelector: Function): (state: ReduxState , ...rest: any) => ContentItem[] => {
  return createSelector(
    [
      contentIdsSelector,
      rawContentItemsSelector,
      completeContentIdsSelector,
      likedContentIdsSelector,
      bookmarkContentIdsSelector
    ],
    contentItemsSelectorResultFunc
  );
};

/**
 * Result function for selector above
 */
const contentItemsSelectorResultFunc = (
  contentIds = [],
  normalizedContentItems = {},
  completeContentIds = [],
  likedContentIds = {},
  bookmarkContentIds = []
) => {
  const decoratedContentItems = contentIds.map((contentId: number) => {
    const contentItem = normalizedContentItems[contentId];
    if (!contentItem) {
      return; // skip for no matching content
    }

    const combined = contentItem ? contentItemSelector({
      contentItem: contentItem,
      isCompleted: completeContentIds.includes(contentId),
      isBookmarked: bookmarkContentIds.includes(contentId),
      doesUserLike: !!likedContentIds[contentId],
    }) : null;

    return combined;
  });
  // flow doesn't understand filtering out nulls and undefineds, so cast it.
  const noNulls: ContentItem[] = (decoratedContentItems.filter(c => !!c): any);
  return noNulls;
};

/**
 * Takes a single item from a raw api content item and turns it into the
 * format that react components expect
 */
const contentItemSelector = createSelector(
  [
    (state) => state.contentItem,
    (state) => state.isCompleted,
    (state) => state.isBookmarked,
    (state) => state.doesUserLike,
  ],
  (rawContentItem = {}, isCompleted, isBookmarked, doesUserLike) => {
    const isSeries = !!rawContentItem.series;
    const reformattedItem = Object.assign(
      {},
      {
        id: rawContentItem.id,
        author: rawContentItem.author,
        channel: rawContentItem.channel,
        activityCount: rawContentItem.activityCount,
        social: {
          likes: {
            count: rawContentItem.likeCount,
            doesUserLike: doesUserLike
          },
          comments: {
            count: rawContentItem.commentCount
          },
          tags: rawContentItem.tags
        },
        isBookmarked,
        isCompleted,
        isSeries,
        series: isSeries ? rawContentItem.series : null,
        publishedTimestamp: rawContentItem.publishedTimestamp,
        raw: { // so we can access the raw elements if we need them in an action creator or something like that
          contentItem: rawContentItem,
        },
        actions: [],
        ...rawContentItem.body
      });
    return reformattedItem;
  }
);

const allContentItemsSelector = contentItemsSelectorFactory(allContentIdsSelector);

/**
 * Grab content items more easily than using the contentItemsSelectorFactory.
 * This may not be as performant, but works well in cases where you don't have a
 * selector for contentIds to supply to the contentItemsSelectorFactory.
 */
const contentItemsSelector = (state: ReduxState, props: { contentIds: number[] }) => {
  return allContentItemsSelector(state).filter(c => props.contentIds.includes(c.id));
};

export {
  STATE_KEY,
  contentItemsSelector,
  contentItemsSelectorFactory,
  contentItemSelector,
  isContentItemLoadingSelector,
};
