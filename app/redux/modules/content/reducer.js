// @flow
import {
  CONTENTS_FETCH_STARTED,
  CONTENTS_FETCH_SUCCEEDED,
  CONTENTS_FETCH_FAILED,
  LIKES_RETRIEVED,
  LIKED_CONTENT,
  UNLIKED_CONTENT,
} from './actions';
import { ADDED_COMMENT, RETRIEVED_COMMENTS } from 'app/redux/modules/comment/actions';
import { REQUEST_STATUSES } from 'app/redux/constants';
import itemReducer from './itemReducer';
import type { Action } from 'app/types/Action';
import type { ContentItem } from './types';

export type ReduxStateContent = {
  // TODO this is incorrect - ContentItem represents whats returned from the selector
  // not what is stored in redux.
  contentItems: {[number]: ContentItem},
  contentItemRequests: Object,
  likedContentIds: {[number]: boolean},
};

const ACTION_HANDLERS = {
  [CONTENTS_FETCH_STARTED]: (state: ReduxStateContent, action: Action) => {
    const contentIds: number[] = action.payload;
    const updatedRequests = contentIds.reduce((map, contentId) => ({
      ...map,
      [contentId]: {
        status: REQUEST_STATUSES.IN_PROGRESS,
      }
    }), {});

    return {
      ...state,
      contentItemRequests: {
        ...state.contentItemRequests,
        ...updatedRequests,
      },
    }
  },
  [CONTENTS_FETCH_SUCCEEDED]: (state: ReduxStateContent, action: Action) => {
    const newContentItems: Object = action.payload;
    const updatedRequests = Object.values(newContentItems).reduce((map, contentItem) => ({
      ...map,
      // $FlowFixMe: suppressing this error
      [contentItem.id]: {
        status: REQUEST_STATUSES.COMPLETE,
      }
    }), {});

    return {
      ...state,
      contentItems: {
        ...state.contentItems,
        ...newContentItems,
      },
      contentItemRequests: {
        ...state.contentItemRequests,
        ...updatedRequests,
      },
    }
  },
  [CONTENTS_FETCH_FAILED]: (state: ReduxStateContent, action: Action) => {
    const contentIds: number[] = action.payload.contentIds;
    const error = action.payload.error;
    const updatedRequests = contentIds.reduce((map, contentId) => ({
      ...map,
      [contentId]: {
        status: REQUEST_STATUSES.IN_PROGRESS,
        error,
      }
    }), {});

    return {
      ...state,
      contentItemRequests: {
        ...state.contentItemRequests,
        ...updatedRequests,
      },
    }
  },
  [LIKES_RETRIEVED]: (state: ReduxStateContent, action: Action) => {
    const likes: Array<number> = action.payload;

    return Object.assign({}, state, {
      likedContentIds: likes.reduce((map, contentId) => {
        map[contentId] = true;
        return map;
      }, {}),
    });
  },
  [LIKED_CONTENT]: (state: ReduxStateContent, action: Action) => {
    const contentId = action.payload,
      updatedContentItem = itemReducer(state.contentItems[contentId], action);

    const updatedContentItems = Object.assign({}, state.contentItems, {
      [contentId]: updatedContentItem,
    });

    let clonedState = Object.assign({}, state, {
      contentItems: updatedContentItems,
      likedContentIds: {
        ...state.likedContentIds,
        [contentId]: true,
      }
    });

    return clonedState;
  },
  [UNLIKED_CONTENT]: (state: ReduxStateContent, action: Action) => {
    const contentId = action.payload,
      updatedContentItem = itemReducer(state.contentItems[contentId], action);

    const updatedContentItems = Object.assign({}, state.contentItems, {
      [contentId]: updatedContentItem
    });

    let clonedState = Object.assign({}, state, {
      contentItems: updatedContentItems,
      likedContentIds: {
        ...state.likedContentIds,
        [contentId]: false,
      }
    });

    return clonedState;
  },
  [RETRIEVED_COMMENTS]: (state: ReduxStateContent, action: Action) => {
    const { contentId } = action.payload,
      updatedContentItem = itemReducer(state.contentItems[contentId], action);

    const updatedContentItems = Object.assign({}, state.contentItems, {
      [contentId]: updatedContentItem
    });

    let clonedState = Object.assign({}, state, {
      contentItems: updatedContentItems
    });

    return clonedState;
  },
  [ADDED_COMMENT]: (state: ReduxStateContent, action: Action) => {
    const { contentId } = action.payload,
      updatedContentItem = itemReducer(state.contentItems[contentId], action);
    const updatedContentItems = Object.assign({}, state.contentItems, {
      [contentId]: updatedContentItem
    });

    let clonedState = Object.assign({}, state, {
      contentItems: updatedContentItems
    });

    return clonedState;
  }
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  contentItems: {},
  contentItemRequests: {},
  likedContentIds: {},
};

const contentReducer = (state: ReduxStateContent = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default contentReducer;
