// @flow
import api from 'app/redux/api';
import { contentsFetchSucceeded } from 'app/redux/modules/content/actions';
import { currentPageSelector } from './selectors';
import type { Dispatch, GetState } from 'app/redux/constants';

export const REFRESH_BOOKMARKS_REQUEST_STARTED = 'REFRESH_BOOKMARKS_REQUEST_STARTED';
const startedRefreshBookmarksRequest = () => {
  return {
    type: REFRESH_BOOKMARKS_REQUEST_STARTED,
  };
};

export const REFRESH_BOOKMARKS_REQUEST_FAILED = 'REFRESH_BOOKMARKS_REQUEST_FAILED';
const failedRefreshBookmarksRequest = () => {
  return {
    type: REFRESH_BOOKMARKS_REQUEST_FAILED,
  };
};

export const LOAD_MORE_BOOKMARKS_REQUEST_STARTED = 'LOAD_MORE_BOOKMARKS_REQUEST_STARTED';
const startedLoadMoreBookmarksRequest = () => {
  return {
    type: LOAD_MORE_BOOKMARKS_REQUEST_STARTED,
  };
};

export const LOAD_MORE_BOOKMARKS_REQUEST_FAILED = 'LOAD_MORE_BOOKMARKS_REQUEST_FAILED';
const failedLoadMoreBookmarksRequest = () => {
  return {
    type: LOAD_MORE_BOOKMARKS_REQUEST_FAILED,
  };
};

export const LOADED_MORE_BOOKMARKS = 'LOADED_MORE_BOOKMARKS';
function loadedMoreBookmarks(bookmarks) {
  return {
    type: LOADED_MORE_BOOKMARKS,
    payload: bookmarks
  };
}

export const REFRESHED_BOOKMARKS = 'REFRESHED_BOOKMARKS';
function refreshedBookmarks(bookmarks) {
  return {
    type: REFRESHED_BOOKMARKS,
    payload: bookmarks
  };
}

export const refreshBookmarks = () => {
  return (dispatch: Dispatch) => {
    dispatch(startedRefreshBookmarksRequest());
    return api.bookmark.retrieveBookmarkContent()
      .then(results => {
        dispatch(contentsFetchSucceeded(results.index));
        dispatch(refreshedBookmarks(results));
      })
      .catch(() => {
        dispatch(failedRefreshBookmarksRequest());
      });
  };
}

export const loadMoreBookmarks = () => {
  return (dispatch: Dispatch, getState: GetState) => {
    dispatch(startedLoadMoreBookmarksRequest());
    const pageNumber = currentPageSelector(getState()) + 1;
    return api.bookmark.retrieveBookmarkContent(pageNumber)
      .then(results => {
        dispatch(contentsFetchSucceeded(results.index));
        dispatch(loadedMoreBookmarks(results));
      })
      .catch(() => {
        dispatch(failedLoadMoreBookmarksRequest());
      });
  };
};
