// @flow
import React, { Component } from 'react';
import type {
  NavigationNavigatorProps,
  NavigationStackScreenOptions,
} from 'react-navigation';
import {
  SafeAreaView
} from 'react-native';
import ActivityContainer from 'app/containers/activity/ActivityContainer';
import type { Activity } from 'app/redux/modules/activity/types';
import styleVars from 'app/style/variables';

type NavigationStateParams = {
  params: {
    activity: Activity,
  },
} & *;

type Props = NavigationNavigatorProps<*, NavigationStateParams>;
class ActivityScreen extends Component<Props> {
  static navigationOptions: (*) => NavigationStackScreenOptions = () => ({
    header: null,
  })

  render() {
    const { params } = this.props.navigation.state;
    const { activity } = params;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: styleVars.color.white}}>
        <ActivityContainer
          activity={activity}
        />
      </SafeAreaView>
    );
  }
}

export default ActivityScreen;
