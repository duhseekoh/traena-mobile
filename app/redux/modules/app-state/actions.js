// @flow
import AppStorage from 'app/repository/AppStorage';
import { refreshJwt } from 'app/redux/modules/auth/actions';
import { retrieveBookmarkIds } from 'app/redux/modules/bookmark/actions';
import { isJwtValidSelector } from 'app/redux/modules/auth/selectors';
import { persistedAppStateSelector } from './selectors';
import { userStateLoaded } from 'app/redux/modules/user/actions';
import {
  configurePushNotifications,
  getUnacknowledgedNotificationsCount,
} from 'app/redux/modules/notification/actions';
import { getContentCompletions } from 'app/redux/modules/traena-task/actions';
import { getUserLikes } from 'app/redux/modules/content/actions';
import {
  createAppActivatedEvent,
  createAppDeactivatedEvent,
  createAppLaunchedEvent,
} from 'app/redux/middleware/eventFactory';
import logger from 'app/logging/logger';
import type { Dispatch, GetState } from 'app/redux/constants';

export const APP_STATE_LOADED = 'APP_STATE_LOADED';
function appStateLoaded(appState) {
  return {
    type: APP_STATE_LOADED,
    payload: appState,
  };
}

export const APP_LAUNCHED = 'APP_LAUNCHED';
export function appLaunched() {
  return {
    type: APP_LAUNCHED,
    meta: {
      event: createAppLaunchedEvent(),
    },
  };
}

export const APP_ACTIVATED = 'APP_ACTIVATED';
export function appActivated() {
  return {
    type: APP_ACTIVATED,
    meta: {
      event: createAppActivatedEvent(),
    }
  }
}

export const APP_DEACTIVATED = 'APP_DEACTIVATED';
export function appDeactivated() {
  return {
    type: APP_DEACTIVATED,
    meta: {
      event: createAppDeactivatedEvent(),
    }
  }
}

export const APP_STATE_SAVED = 'APP_STATE_SAVED';
function appStateSaved() {
  return {
    type: APP_STATE_SAVED,
  };
}

export const loadAppState = () => {
  return (dispatch: Dispatch) => {
    logger.logBreadcrumb('loadAppState');
    return AppStorage.loadState()
      .then((storedAppState) => {
        if(storedAppState) {
          dispatch(appStateLoaded(storedAppState));
          dispatch(userStateLoaded()); // hook so we can perform some user setup
        }
      });
  };
};

export const saveAppState = () =>  {
  return (dispatch: Dispatch, getState: GetState) => {
    logger.logBreadcrumb('saveAppState');
    AppStorage.saveState(persistedAppStateSelector(getState()));
    dispatch(appStateSaved());
  };
};

/**
 * When the app launches and user is logged in, this gets called.
 * If JWT is valid now, refresh feed, traena list, jwt in parallel
 * If JWT is not valid now, refresh jwt, THEN traena list and feed
 * @return {[type]} [description]
 */
export const makeInitialNetworkRequests = () => {
  return (dispatch: Dispatch, getState: GetState) => {
    if(isJwtValidSelector(getState())) {
      logger.logBreadcrumb('init requests-JWT not expired');
      dispatch(appLaunched());
      dispatch(getUnacknowledgedNotificationsCount());
      dispatch(configurePushNotifications());
      dispatch(getContentCompletions());
      dispatch(retrieveBookmarkIds());
      dispatch(getUserLikes());
      dispatch(refreshJwt());
    } else {
      dispatch(refreshJwt()).then(() => {
        logger.logBreadcrumb('init requests-JWT expired');
        dispatch(appLaunched());
        dispatch(getUnacknowledgedNotificationsCount());
        dispatch(configurePushNotifications());
        dispatch(getContentCompletions());
        dispatch(retrieveBookmarkIds());
        dispatch(getUserLikes());
      });
    }
  };
};

/**
 * When app is brought to foreground get a new jwt
 * Can still make other calls in parallel, because the jwt *should* be good stil.
 * We are just premptively refreshing it.
 */
export const makeAppToForegroundRequests = () => {
  return (dispatch: Dispatch) => {
    logger.logBreadcrumb('foregrounding requests');
    dispatch(refreshJwt());
    dispatch(getUnacknowledgedNotificationsCount());
  };
};
