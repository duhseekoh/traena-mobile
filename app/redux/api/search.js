// @flow
import {
  getSearchTpl,
  getUserSearchTpl,
  getCommonSearchesTpl,
  getContentTagsTpl,
  getTagSearchTpl,
  getChannelSearchTpl,
} from './util/endpoints';

import { getQueryString } from './util/queryString';

import type { ArrayPage } from 'app/types/Pagination';
import type { TagAggregation } from 'app/redux/modules/trending-tags/types';

const retrieveTagContent = (
  tag: string,
  page: number = 0,
  size: number = 10,
) => {
  return fetch(getTagSearchTpl({ page, tag, size }));
};

const retrieveSearchResults = (
  searchText: string = '',
  page: number = 0,
  size: number = 10,
) => {
  return fetch(
    getSearchTpl({
      searchText: encodeURIComponent(searchText),
      page,
      size,
    }),
  );
};

const retrieveUserSearchResults = (searchText: string = '') => {
  return fetch(
    getUserSearchTpl({
      searchText,
    }),
  );
};

const retrieveCommonSearches = () => {
  return fetch(getCommonSearchesTpl());
};

const retrieveContentTags = (
  options?: {| daysAgo?: number, size?: number |} = {
    size: 10,
  },
): Promise<ArrayPage<TagAggregation>> => {
  const qp = {
    page: 0,
    ...options,
  };
  const qs = getQueryString(qp);
  return fetch(getContentTagsTpl({qs}));
};

const retrieveChannelContent = (
  channelId: number,
  page: number = 0,
  size: number = 10,
) => {
  return fetch(getChannelSearchTpl({ page, channelId, size }));
};

export default {
  retrieveCommonSearches,
  retrieveSearchResults,
  retrieveUserSearchResults,
  retrieveContentTags,
  retrieveTagContent,
  retrieveChannelContent,
};
