// @flow
import React from 'react';
import { TRAText } from 'app/components';
import { getFullName } from 'app/redux/modules/user/util';
import type { User } from 'app/redux/modules/user/types';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';

type Props = {
  user: User,
  style?: ViewStyleProp,
};
const UserFullName = (props: Props) => {
  const { user } = props;
  const fullName = getFullName(user);

  return (
    <TRAText {...props}>{fullName}</TRAText>
  )
}

export default UserFullName;
