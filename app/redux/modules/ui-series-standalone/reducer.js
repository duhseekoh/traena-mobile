// @flow
import _ from 'lodash';
import {
  REFRESH_SERIES_CONTENT_REQUEST_STARTED,
  REFRESH_SERIES_CONTENT_REQUEST_FAILED,
  LOAD_MORE_SERIES_CONTENT_REQUEST_STARTED,
  LOAD_MORE_SERIES_CONTENT_REQUEST_FAILED,
  LOADED_MORE_SERIES_CONTENT,
  REFRESHED_SERIES_CONTENT,
} from './actions';
import { REQUEST_STATUSES, type RequestStatus } from 'app/redux/constants';

export type ReduxStateSeriesStandalone = {
  uiSlices: {
    [seriesId: number]: {
      loadingStatus: RequestStatus,
      refreshingStatus: RequestStatus,
      contentIds: string[],
      currentPage: number,
      canLoadMore: boolean,
    }
  }
};

const ACTION_HANDLERS = {
  [REFRESHED_SERIES_CONTENT]: (state, action) => {
    const { contentPage, seriesId } = action.payload;
    const { ids, totalPages, number } = contentPage;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [seriesId]: {
          ...state.uiSlices[seriesId],
          contentIds: ids,
          canLoadMore: number < totalPages - 1,
          currentPage: number,
          loadingStatus: REQUEST_STATUSES.COMPLETE,
          refreshingStatus: REQUEST_STATUSES.COMPLETE,
        }
      }
    }
  },
  [LOADED_MORE_SERIES_CONTENT]: (state, action) => {
    const { contentPage, seriesId } = action.payload;
    const { ids, totalPages, number } = contentPage;
    const mergedContentIds = _.uniq([...state.uiSlices[seriesId].contentIds, ...ids]);
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [seriesId]: {
          ...state.uiSlices[seriesId],
          contentIds: mergedContentIds,
          canLoadMore: number < totalPages - 1,
          currentPage: number,
          loadingStatus: REQUEST_STATUSES.COMPLETE,
          refreshingStatus: REQUEST_STATUSES.COMPLETE,
        }
      }
    }
  },
  [REFRESH_SERIES_CONTENT_REQUEST_STARTED]: (state, action) => {
    const seriesId = action.payload;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [seriesId]: {
          ...state.uiSlices[seriesId],
          refreshingStatus: REQUEST_STATUSES.IN_PROGRESS,
        }
      }
    }
  },
  [REFRESH_SERIES_CONTENT_REQUEST_FAILED]: (state, action) => {
    const seriesId = action.payload;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [seriesId]: {
          ...state.uiSlices[seriesId],
          refreshingStatus: REQUEST_STATUSES.ERROR,
        }
      }
    };
  },
  [LOAD_MORE_SERIES_CONTENT_REQUEST_STARTED]: (state, action) => {
    const seriesId = action.payload;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [seriesId]: {
          ...state.uiSlices[seriesId],
          refreshingStatus: REQUEST_STATUSES.IN_PROGRESS,
        }
      }
    };
  },
  [LOAD_MORE_SERIES_CONTENT_REQUEST_FAILED]: (state, action) => {
    const seriesId = action.payload;
    return {
      ...state,
      uiSlices: {
        ...state.uiSlices,
        [seriesId]: {
          ...state.uiSlices[seriesId],
          refreshingStatus: REQUEST_STATUSES.ERROR,
        }
      }
    };
  },
};

const initialState: ReduxStateSeriesStandalone = {
  uiSlices: {}
};

const seriesStandaloneReducer = (state: ReduxStateSeriesStandalone = initialState, action: {type: string, payload: any}) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

export default seriesStandaloneReducer;
