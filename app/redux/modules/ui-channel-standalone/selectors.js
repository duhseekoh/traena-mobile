// @flow
import { createSelector } from 'reselect';
import { contentItemsSelectorFactory } from 'app/redux/modules/content/selectors';
import * as fromSeries from 'app/redux/modules/series/selectors';
import { REQUEST_STATUSES } from 'app/redux/constants';
import type { ReduxState } from 'app/redux/constants';

const STATE_KEY = 'channelStandalone';
const stateFromRoot = (state: ReduxState) => state[STATE_KEY];

const contentIdsSelector = createSelector([
  stateFromRoot,
  (_, props) => props.channelId,
], (state, channelId) => {
  const uiSlices = state.uiSlices;
  return (uiSlices && uiSlices[channelId] && uiSlices[channelId].content) ? uiSlices[channelId].content.contentIds : [];
});

const contentSelector = contentItemsSelectorFactory(contentIdsSelector);

/**
 * Given a channelId, return the series ids associated with that channel.
 */
const seriesIdsForChannelSelector = createSelector([
  stateFromRoot,
  // $FlowFixMe: Figure out why ContentStandaloneContainer mapStateToProps seriesWithContent is triggering an error here
  (_, { channelId }) => channelId,
], (state, channelId) => {
  const uiSlices = state.uiSlices;
  return (uiSlices && uiSlices[channelId] && uiSlices[channelId].series && uiSlices[channelId].series.seriesIds) ? uiSlices[channelId].series.seriesIds : [];
});

/**
 * Given a channelId return the associated series for that channel.
 * Also includes a preview of content
 */
const seriesListWithContentSelector = (state: ReduxState, props: { channelId: number }) => {
  const seriesIds = seriesIdsForChannelSelector(state, props);
  const seriesListWithContent = fromSeries.seriesListWithContentSelectorv2(state, {
    seriesIds,
  });
  return seriesListWithContent;
};

const isLoadingMoreContentSelector = (state: ReduxState, { channelId }: { channelId: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[channelId];
  return uiSlice && uiSlice.content && uiSlice.content.loadingStatus === REQUEST_STATUSES.IN_PROGRESS;
}

const isRefreshingContentSelector = (state: ReduxState, { channelId }: { channelId: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[channelId];
  return uiSlice && uiSlice.content && uiSlice.content.refreshingStatus === REQUEST_STATUSES.IN_PROGRESS;
}

const hasErrorContentSelector = (state: ReduxState, { channelId }: { channelId: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[channelId];
  return uiSlice && uiSlice.content && (
    uiSlice.content.loadingStatus === REQUEST_STATUSES.ERROR || uiSlice.content.refreshingStatus === REQUEST_STATUSES.ERROR);
}

const canLoadMoreContentSelector = (state: ReduxState, { channelId }: { channelId: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[channelId];
  return uiSlice && uiSlice.content && uiSlice.content.canLoadMore;
}

const currentContentPageSelector = (state: ReduxState, { channelId }: { channelId: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[channelId];
  return uiSlice && uiSlice.content ? uiSlice.content.currentPage : 0;
}

/**
 * Are we at the bottom of the results list? Can't be loading, cant be
 refreshing, and make sure that we have hit the end of paging.
 */
const displayNoMoreContentResultsSelector = createSelector([
  isLoadingMoreContentSelector,
  isRefreshingContentSelector,
  canLoadMoreContentSelector,
], (isLoadingMore, isRefreshing, canLoadMore) => {
  return !isLoadingMore && !isRefreshing && !canLoadMore;
});

const isLoadingMoreSeriesSelector = (state: ReduxState, { channelId }: { channelId: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[channelId];
  return uiSlice && uiSlice.series && uiSlice.series.loadingStatus === REQUEST_STATUSES.IN_PROGRESS;
}

const isRefreshingSeriesSelector = (state: ReduxState, { channelId }: { channelId: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[channelId];
  return uiSlice && uiSlice.series && uiSlice.series.refreshingStatus === REQUEST_STATUSES.IN_PROGRESS;
}

const hasErrorSeriesSelector = (state: ReduxState, { channelId }: { channelId: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[channelId];
  return uiSlice && uiSlice.series && (
    uiSlice.series.loadingStatus === REQUEST_STATUSES.ERROR || uiSlice.series.refreshingStatus === REQUEST_STATUSES.ERROR);
}

const canLoadMoreSeriesSelector = (state: ReduxState, { channelId }: { channelId: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[channelId];
  return uiSlice && uiSlice.series && uiSlice.series.canLoadMore;
}

const currentSeriesPageSelector = (state: ReduxState, { channelId }: { channelId: number }) => {
  const uiSlice = stateFromRoot(state).uiSlices[channelId];
  return uiSlice && uiSlice.series ? uiSlice.series.currentPage : 0;
}

/**
 * Are we at the bottom of the results list? Can't be loading, cant be
 refreshing, and make sure that we have hit the end of paging.
 */
const displayNoMoreSeriesResultsSelector = createSelector([
  isLoadingMoreSeriesSelector,
  isRefreshingSeriesSelector,
  canLoadMoreSeriesSelector,
], (isLoadingMore, isRefreshing, canLoadMore) => {
  return !isLoadingMore && !isRefreshing && !canLoadMore;
});

export {
  STATE_KEY,
  contentSelector,
  isLoadingMoreContentSelector,
  isRefreshingContentSelector,
  hasErrorContentSelector,
  canLoadMoreContentSelector,
  currentContentPageSelector,
  displayNoMoreContentResultsSelector,
  isLoadingMoreSeriesSelector,
  isRefreshingSeriesSelector,
  hasErrorSeriesSelector,
  canLoadMoreSeriesSelector,
  currentSeriesPageSelector,
  displayNoMoreSeriesResultsSelector,
  seriesListWithContentSelector,
};
