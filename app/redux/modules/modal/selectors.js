const modalTypeSelector = (state) => state.modal.modalType;
const modalPropsSelector = (state) => state.modal.modalProps;

export {
  modalTypeSelector,
  modalPropsSelector
}
