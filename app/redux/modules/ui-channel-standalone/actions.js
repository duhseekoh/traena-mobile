// @flow
import api from 'app/redux/api';
import { contentsFetchSucceeded } from 'app/redux/modules/content/actions';
import { getSeriesForChannel } from 'app/redux/modules/series/actions';
import { currentContentPageSelector, currentSeriesPageSelector } from './selectors';
import type { IndexedPage } from '../../../types/Pagination';
import type { Series } from '../series/types';
import type { ContentItem } from '../content/types';
import type { Dispatch, GetState } from 'app/redux/constants';

export const REFRESH_CHANNEL_CONTENT_REQUEST_STARTED = 'REFRESH_CHANNEL_CONTENT_REQUEST_STARTED';
const startedRefreshChannelContentRequest = (channelId: number) => {
  return {
    type: REFRESH_CHANNEL_CONTENT_REQUEST_STARTED,
    payload: channelId,
  };
};

export const REFRESH_CHANNEL_CONTENT_REQUEST_FAILED = 'REFRESH_CHANNEL_CONTENT_REQUEST_FAILED';
const failedRefreshChannelContentRequest = (channelId: number) => {
  return {
    type: REFRESH_CHANNEL_CONTENT_REQUEST_FAILED,
    payload: channelId,
  };
};

export const LOAD_MORE_CHANNEL_CONTENT_REQUEST_STARTED = 'LOAD_MORE_CHANNEL_CONTENT_REQUEST_STARTED';
const startedLoadMoreChannelContentRequest = (channelId: number) => {
  return {
    type: LOAD_MORE_CHANNEL_CONTENT_REQUEST_STARTED,
    payload: channelId,
  };
};

export const LOAD_MORE_CHANNEL_CONTENT_REQUEST_FAILED = 'LOAD_MORE_CHANNEL_CONTENT_REQUEST_FAILED';
const failedLoadMoreChannelContentRequest = (channelId: number) => {
  return {
    type: LOAD_MORE_CHANNEL_CONTENT_REQUEST_FAILED,
    payload: channelId,
  };
};

export const LOADED_MORE_CHANNEL_CONTENT = 'LOADED_MORE_CHANNEL_CONTENT';
function loadedMoreChannelContent(content: IndexedPage<*, ContentItem>, channelId: number) {
  return {
    type: LOADED_MORE_CHANNEL_CONTENT,
    payload: { ...content, channelId }
  };
}

export const REFRESHED_CHANNEL_CONTENT = 'REFRESHED_CHANNEL_CONTENT';
function refreshedChannelContent(content: IndexedPage<*, ContentItem>, channelId: number) {
  return {
    type: REFRESHED_CHANNEL_CONTENT,
    payload: { ...content, channelId }
  };
}

export const refreshChannelContent = (channelId: number) => {
  return async (dispatch: Dispatch) => {
    dispatch(startedRefreshChannelContentRequest(channelId));
    return api.search.retrieveChannelContent(channelId)
      .then(results => {
        dispatch(contentsFetchSucceeded(results.index));
        dispatch(refreshedChannelContent(results, channelId));
      })
      .catch(() => {
        dispatch(failedRefreshChannelContentRequest(channelId));
      });
  };
}

export const loadMoreChannelContent = (channelId: number) => {
  return async (dispatch: Dispatch, getState: GetState) => {
    dispatch(startedLoadMoreChannelContentRequest(channelId));
    const page = currentContentPageSelector(getState(), { channelId }) + 1;
    return api.search.retrieveChannelContent(channelId, page)
      .then(results => {
        dispatch(contentsFetchSucceeded(results.index));
        dispatch(loadedMoreChannelContent(results, channelId));
      })
      .catch(() => {
        dispatch(failedLoadMoreChannelContentRequest(channelId));
      });
  };
};

export const REFRESH_SERIES_REQUEST_STARTED = 'REFRESH_SERIES_REQUEST_STARTED';
const startedRefreshSeriesRequest = (channelId: number) => {
  return {
    type: REFRESH_SERIES_REQUEST_STARTED,
    payload: channelId,
  };
};

export const REFRESH_SERIES_REQUEST_FAILED = 'REFRESH_SERIES_REQUEST_FAILED';
const failedRefreshSeriesRequest = (channelId: number) => {
  return {
    type: REFRESH_SERIES_REQUEST_FAILED,
    payload: channelId,
  };
};

export const LOAD_MORE_SERIES_REQUEST_STARTED = 'LOAD_MORE_SERIES_REQUEST_STARTED';
const startedLoadMoreSeriesRequest = (channelId: number) => {
  return {
    type: LOAD_MORE_SERIES_REQUEST_STARTED,
    payload: channelId,
  };
};

export const LOAD_MORE_SERIES_REQUEST_FAILED = 'LOAD_MORE_SERIES_REQUEST_FAILED';
const failedLoadMoreSeriesRequest = (channelId: number) => {
  return {
    type: LOAD_MORE_SERIES_REQUEST_FAILED,
    payload: channelId,
  };
};

export const LOADED_MORE_SERIES = 'LOADED_MORE_SERIES';
function loadedMoreSeries(seriesPage: IndexedPage<number, Series>, channelId: number) {
  return {
    type: LOADED_MORE_SERIES,
    payload: { seriesPage, channelId }
  };
}

export const REFRESHED_SERIES = 'REFRESHED_SERIES';
function refreshedSeries(seriesPage: IndexedPage<number, Series>, channelId: number) {
  return {
    type: REFRESHED_SERIES,
    payload: { seriesPage, channelId }
  };
}

export const refreshSeriesList = (channelId: number) => {
  return async (dispatch: Dispatch) => {
    dispatch(startedRefreshSeriesRequest(channelId));
    try {
      const page: IndexedPage<number, Series> = await dispatch(getSeriesForChannel(channelId));
      return dispatch(refreshedSeries(page, channelId));
    } catch(error) {
      dispatch(failedRefreshSeriesRequest(channelId));
    }
  };
}

export const loadMoreSeries = (channelId: number) => {
  return async (dispatch: Dispatch, getState: GetState) => {
    dispatch(startedLoadMoreSeriesRequest(channelId));
    const pageNumber = currentSeriesPageSelector(getState(), { channelId }) + 1;
    try {
      const page: IndexedPage<number, Series> = await dispatch(getSeriesForChannel(channelId, pageNumber));
      return dispatch(loadedMoreSeries(page, channelId));
    } catch(error) {
      dispatch(failedLoadMoreSeriesRequest(channelId));
    }
  };
};
