// @flow

type InfoModalProps = {
  title: string,
  description: string,
};

export type ModalProps = InfoModalProps;
