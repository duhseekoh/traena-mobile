// @flow
import React, { Component } from 'react';
import {
  Platform,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import { FeatherIcon } from 'app/components';
import styleVars from 'app/style/variables';


type Props = {
  searchQuery?: string,
  onClearSearch?: () => *,
  onSearchQueryTyping?: (text: string) => *,
  onSubmit?: () => *,
};

class SearchBar extends Component<Props> {

  constructor(props: Props) {
    super(props);
  }

  _onChange = (event) => {
    this.props.onSearchQueryTyping && this.props.onSearchQueryTyping(event.nativeEvent.text);
  }

  _onSubmit = () => {
    this.props.onSubmit && this.props.onSubmit();
  }

  _onClearSearch = () => {
    this.props.onClearSearch && this.props.onClearSearch();
  }

  render() {
    let { searchQuery } = this.props;
    return (
      <View style={styles.container}>
        <TextInput
          clearButtonMode={'always'}
          style={styles.searchInput}
          value={searchQuery}
          returnKeyType={'search'}
          placeholder={'Search...'}
          underlineColorAndroid='transparent'
          onChange={this._onChange}
          onSubmitEditing={this._onSubmit}
          autoFocus
        />
        <FeatherIcon name='search' style={styles.searchIcon} />
        {
          Platform.OS === 'android' &&
          <TouchableOpacity style={styles.clearButton} onPress={this._onClearSearch}>
            <FeatherIcon name='x-circle' style={styles.clearIcon} />
          </TouchableOpacity>
        }
      </View>
    )
  }
}

export default SearchBar;

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    borderBottomWidth: 1,
    borderColor: styleVars.color.lightGrey3,
    backgroundColor: styleVars.color.white,
  },
  searchInput: {
    height: styleVars.textInputHeights.medium,
    color: styleVars.color.black,
    paddingLeft: 30,
    paddingRight: 15,
    fontFamily: styleVars.fontFamily.base
  },
  searchIcon: {
    position: 'absolute',
    left: 10,
    top: 10,
    color: styleVars.color.lightGrey2,
    backgroundColor: styleVars.color.transparent,
    fontSize: styleVars.iconSize.medium
  },
  clearButton: {
    position: 'absolute',
    padding: 15,
    right: 4,
    top: -5
  },
  clearIcon: {
    fontSize: styleVars.fontSize.large,
    color: styleVars.color.lightGrey2,
  }
});
