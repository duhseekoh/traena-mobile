// @flow
import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import { FeatherIcon } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  onPressClose: () => *,
};

const ModalTitleBar = ({onPressClose}: Props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPressClose} style={styles.closeButton}>
        <FeatherIcon name='x' style={styles.closeIcon} />
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  closeButton: {
    paddingHorizontal: 10
  },
  closeIcon: {
    fontSize: styleVars.iconSize.large
  }
});

export default ModalTitleBar;
