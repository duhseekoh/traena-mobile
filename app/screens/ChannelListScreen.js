// @flow
import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import type {
  NavigationStackScreenOptions,
  NavigationNavigatorProps,
} from 'react-navigation';
import ChannelListContainer from 'app/containers/channel/ChannelListContainer';
import styleVars from 'app/style/variables';
 type NavigationStateParams = *;
 type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;
 class ChannelListScreen extends Component<NavigationProps> {
  static navigationOptions: (*) => NavigationStackScreenOptions = () => {
    return {
      headerTitle: 'Channels',
    };
  };
   render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: styleVars.color.white }}>
        <ChannelListContainer />
      </SafeAreaView>
    );
  }
}
 export default ChannelListScreen;
