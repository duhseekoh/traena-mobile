// @flow
import { RETRIEVED_ACTIVITIES, RETRIEVED_ACTIVITY_RESULTS } from './actions';
import type { Action } from 'app/types/Action';
import type { Activity, ActivityResults } from './types';

export type ReduxStateActivity = {
  activitiesById: {[activityId: string]: Activity},
  activityResults: {[activityId: string]: ActivityResults},
};

const ACTION_HANDLERS = {
  [RETRIEVED_ACTIVITIES]: (state: ReduxStateActivity, action: Action) => {
    return {
      ...state,
      activitiesById: {
        ...state.activitiesById,
        ...action.payload,
      },
    };
  },
  [RETRIEVED_ACTIVITY_RESULTS]: (state: ReduxStateActivity, action: Action) => {
    return {
      ...state,
      activityResults: {
        ...state.activityResults,
        ...action.payload,
      },
    };
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  activitiesById: {},
  activityResults: {},
};

const activityReducer = (state: ReduxStateActivity = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default activityReducer;
