// @flow
import config from 'app/config';
import Auth0Client from 'react-native-auth0';
import logger from 'app/logging/logger';
import { errors } from './util/errorHandler';

const auth0Client = new Auth0Client({
  domain: config.auth0.domain,
  clientId: config.auth0.clientId,
});

export const loginDB = (username: string, password: string) => {
  return auth0Client.auth
    .passwordRealm({
      username, password,
      audience: config.auth0.audience,
      realm: config.auth0.dbConnectionName,
      scope: config.auth0.scope,
      device: 'mobile'
    })
    .then((response) => {
      logger.logBreadcrumb('Login response', { response });
      return response;
    })
    .catch(error => {
      // special case error response handling since we are not communicating with our API
      logger.logBreadcrumb('Error logging in', { error });
      if(error.json && error.json.error === 'invalid_grant') {
        throw errors.BAD_CREDENTIALS;
      } else {
        throw errors.UNKNOWN;
      }
    });
};

export const refreshIdToken = (refreshToken: string) => {
  if (!refreshToken) {
    logger.logBreadcrumb('warn: refresh attempt no token');
  }

  return auth0Client.auth
    .refreshToken({
      refreshToken,
      scope: config.auth0.scope,
    })
    .then((response) => {
      logger.logBreadcrumb('Refresh JWT response', { response });
      return response;
    })
    .catch(error => {
      // special case error response handling since we are not communicating with our API
      logger.logBreadcrumb('Error refreshing JWT', { error });
      if (error.json && error.json.error_description === 'Authorization Extension: Blocked event loop') {
        // tracking this error to see how often it is occuring:
        //  https://github.com/auth0/auth0-authorization-extension/issues/165
        logger.logError(error);
        throw error;
      } else if (error.status === 401 || (error.json && error.json.statusCode === 401)) {
        throw errors.BAD_CREDENTIALS;
      } else {
        throw new Error('Could not refresh for unexpected reason');
      }
    });
};
