// @flow
import React from 'react';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import type moment from 'moment';
import { TRAText } from 'app/components';

type Props = {
  date: moment,
  style?: ViewStyleProp,
  isUpperCase?: boolean,
};

const DateLong = ({date, style, isUpperCase = false}: Props) => {
  return (
    <TRAText style={style}>
      {isUpperCase ?
        date.format('MMMM D, YYYY').toUpperCase() :
        date.format('MMMM D, YYYY')
      }
    </TRAText>
  )
}

export default DateLong;
