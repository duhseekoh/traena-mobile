// @flow
import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import {
  TRAText,
  TRATextStrong,
  SocialTags
} from 'app/components';
import styleVars from 'app/style/variables';
import type { ContentItem } from 'app/redux/modules/content/types';
import ContentMeta from 'app/containers/content/components/ContentMeta';

type Props = {
  contentItem: ContentItem,
  typeIcon?: React$Node,
  style?: ViewStyleProp,
}

/**
 * All Content Previews should compose themselves of this and pass content type
 * as a prop
 */
const GenericContentPreview = ({ contentItem, typeIcon, style }: Props) => {
  const { title, social, author, isCompleted, activityCount, channel, isSeries } = contentItem;
  const { tags } = social;
  return (
    <View style={[styles.itemContainer, style]}>
      <View style={styles.contentMetaContainer}>
        <ContentMeta
          title={channel.name}
          isCompleted={isCompleted}
          isSeries={isSeries}
          hasActivity={activityCount > 0}/>
      </View>
      <View style={styles.content}>
        <View style={styles.typeIcon}>
          {typeIcon}
        </View>
        <View style={{flex: 1}}>
          <View style={styles.titleHolder}>
            <TRATextStrong style={styles.titleText} numberOfLines={1} ellipsizeMode={'tail'}>
              {title}
            </TRATextStrong>
            <TRAText numberOfLines={1} ellipsizeMode={'tail'}>by {author.firstName} {author.lastName} </TRAText>
          </View>
          <SocialTags isDetailView={false} tags={tags} textStyle={styles.tagText} />
        </View>
      </View>
    </View>
  )
}

export default GenericContentPreview;

const styles = StyleSheet.create({
  itemContainer: {
    backgroundColor: styleVars.color.white,
    padding: 8,
    borderRadius: 4,
    shadowColor: styleVars.color.mediumGrey2,
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 5,
    elevation: 2,
    marginVertical: 2,
  },
  contentMetaContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 8,
    borderBottomWidth: 1,
    borderBottomColor: styleVars.color.lightGrey,
    marginHorizontal: -8,
    paddingVertical: 10,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    marginVertical: 16,
    marginHorizontal: 12,
  },
  titleHolder: {
    marginTop: 5,
    marginBottom: 10,
  },
  typeIcon: {
    paddingTop: 10,
    paddingRight: 8,
  },
  titleText: {
    fontSize: styleVars.fontSize.large,
  },
  tagText: {
    color: styleVars.color.mediumGrey,
  }
});
