// @flow
import React, { Component } from 'react';
import { View } from 'react-native';
import ActivityButton from 'app/containers/content/components/ActivityButton';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import type { Activity } from 'app/redux/modules/activity/types';

type Props = {
  activities: Activity[],
  style?: ViewStyleProp,
};

class ActivityPreviewList extends Component<Props> {
  props: Props;

  render() {
    const { activities, style } = this.props;
    return (
      <View
        style={style}
      >
        {activities &&
          activities.length > 0 && (
            <View>
              {activities.map(activity => {
                return (
                  <View key={activity.id} style={{ alignItems: 'center' }}>
                    <ActivityButton activity={activity} />
                  </View>
                );
              })}
            </View>
          )}
      </View>
    );
  }
}

export default ActivityPreviewList;
