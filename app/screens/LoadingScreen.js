// @flow
import React from 'react';
import {
  View,
  ImageBackground,
  StyleSheet,
} from 'react-native';
import {
  TraenaIcon,
} from 'app/components';

import styleVars from 'app/style/variables';

const LoadingScreen = () => {
  return (
    <View style={{flex: 1}}>
      <ImageBackground style={styles.heroImage} resizeMode='cover' source={require('../images/login-gradient.png')}>
        <TraenaIcon name={'t'} style={styles.heroIcon} />
      </ImageBackground>
    </View>
  );
};

export default LoadingScreen;

const styles = StyleSheet.create({
  heroImage: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  heroIcon: {
    backgroundColor: styleVars.color.transparent,
    fontSize: 100,
    color: styleVars.color.white,
    opacity: .5,
  }

});
