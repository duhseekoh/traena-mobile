// @flow
import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import type {
  NavigationStackScreenOptions,
  NavigationNavigatorProps,
} from 'react-navigation';
import SearchContainer from 'app/containers/search/SearchContainer';
import styleVars from 'app/style/variables';

type NavigationStateParams = *;

type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

class SearchScreen extends Component<NavigationProps> {
  static navigationOptions: (*) => NavigationStackScreenOptions = () => {
    return {
      headerTitle: 'SEARCH',
    };
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: styleVars.color.white }}>
        <SearchContainer />
      </SafeAreaView>
    );
  }
}

export default SearchScreen;
