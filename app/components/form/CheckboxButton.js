// @flow
import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { TRAText } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  isDisabled?: boolean,
  isSelected?: boolean,
  title: string,
  onPress: () => *,
  style?: ViewStyleProp,
};

type State = { };

class CheckboxButton extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    const { title, onPress, isSelected, isDisabled, style } = this.props;

    let buttonStyles = [styles.button];
    if(isSelected) {
      buttonStyles.push(styles.buttonSelected);
    }
    if(isDisabled) {
      buttonStyles.push(styles.buttonDisabled);
    }

    return (
      <TouchableOpacity style={style} onPress={onPress} disabled={isDisabled}>
        <TRAText style={buttonStyles}>{title}</TRAText>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    textAlign: 'center',
    fontSize: styleVars.fontSize.medium,
    color: styleVars.color.darkGrey,
    // color: '#F00',
    padding: 10,
    borderWidth: 2,
    borderColor: styleVars.color.lightGrey2,
  },
  buttonSelected: {
    backgroundColor: styleVars.color.traenaOne,
    color: styleVars.color.white,
    borderColor: styleVars.color.traenaOne
  },
  buttonDisabled: {
    backgroundColor: styleVars.color.lightGrey3,
    color: styleVars.color.darkGrey,
    borderColor: styleVars.color.lightGrey3,
  }
});

export default CheckboxButton;
