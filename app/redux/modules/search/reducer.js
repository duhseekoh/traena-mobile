// @flow
import _ from 'lodash';
import {
  CLEARED_SEARCH,
  RETRIEVED_SEARCH_RESULTS,
  UI_SEARCH_TEXT_UPDATED,
  ADDED_RECENT_SEARCH,
  NEW_SEARCH_REQUEST_STARTED,
  MORE_SEARCH_REQUEST_STARTED,
  RETRIEVED_RECOMMENDED_SEARCHES,
  SEARCH_REQUEST_FINISHED,
  RETRIEVED_CONTENT_TAGS,
} from './actions';

import type { Action } from 'app/types/Action';

export type ReduxStateSearch = {
  isLoading: boolean,
  queryHasNoResults: boolean,
  canLoadMore: boolean,
  queryText: string,
  currentPage: number,
  resultContentIds: number[],
  recentSearches: string[],
  commonSearches: string[],
  contentTags: string[],
};

const ACTION_HANDLERS = {
  [RETRIEVED_SEARCH_RESULTS]: (state: ReduxStateSearch, action: Action) => {
    const { ids, totalPages, number } = action.payload;
    const queryHasText = !!state.queryText;
    const updatedResultContentIds = [...state.resultContentIds, ...ids];
    const isResultsEmpty = updatedResultContentIds.length === 0;
    const canLoadMore = number < totalPages - 1;

    return Object.assign({}, state, {
      resultContentIds: updatedResultContentIds,
      queryHasNoResults: queryHasText && isResultsEmpty, // if something was searched for and no results came back
      currentPage: number,
      canLoadMore
    });
  },
  [UI_SEARCH_TEXT_UPDATED]: (state: ReduxStateSearch, action: Action) => {
    const queryText = action.payload;
    return Object.assign({}, state, {
      queryText,
    });
  },
  [ADDED_RECENT_SEARCH]: (state: ReduxStateSearch, action: Action) => {
    console.log(`ADDED_RECENT_SEARCH: ${action.payload}`);
    const queryText = action.payload;
    const recentSearchs = state.recentSearches || [];
    const updated = _.uniq([queryText, ...recentSearchs]);
    const latestFive = _.take(updated, 5);

    return Object.assign({}, state, {
      recentSearches: latestFive
    });
  },
  [NEW_SEARCH_REQUEST_STARTED]: (state: ReduxStateSearch) => {
    return Object.assign({}, state, {
      isLoading: true,
      currentPage: 0,
      resultContentIds: [],
    });
  },
  [MORE_SEARCH_REQUEST_STARTED]: (state: ReduxStateSearch) => {
    return Object.assign({}, state, {
      isLoading: true,
    });
  },
  [SEARCH_REQUEST_FINISHED]: (state: ReduxStateSearch) => {
    return Object.assign({}, state, {
      isLoading: false
    });
  },
  [CLEARED_SEARCH]: (state: ReduxStateSearch) => {
    return Object.assign({}, state, {
      isLoading: false,
      currentPage: 0,
      resultContentIds: [],
      queryHasNoResults: false,
      queryText: '',
      canLoadMore: false,
    });
  },
  [RETRIEVED_RECOMMENDED_SEARCHES]: (state: ReduxStateSearch, { payload }) => {
    return Object.assign({}, state, {
      commonSearches: payload
    });
  },
  [RETRIEVED_CONTENT_TAGS]: (state: ReduxStateSearch, { payload }) => {
    return {
      ...state,
      contentTags: payload,
    }
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isLoading: false,
  queryHasNoResults: false, // if true, the query text
  canLoadMore: false,
  queryText: '',
  currentPage: 0,
  resultContentIds: [],
  recentSearches: [],
  commonSearches: [],
  trendingSearches: [],
  contentTags: [],
};

const searchReducer = (state: ReduxStateSearch = initialState, action: Action) => {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
};

export default searchReducer;
