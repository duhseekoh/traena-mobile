// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  View
} from 'react-native';
import {
  TRATextStrong,
} from 'app/components';
import {
  publishedContentSelector,
  isCompletedSelector,
  isLoadingSelector,
  hasErrorSelector,
  seriesWithContentSelector,
} from 'app/redux/modules/ui-content-standalone/selectors';
import { activitiesByContentIdSelector } from 'app/redux/modules/activity/selectors';
import { getStandaloneContentItem } from 'app/redux/modules/ui-content-standalone/actions';
import { getActivitiesByContentId } from 'app/redux/modules/activity/actions';
import { getSeriesById } from 'app/redux/modules/series/actions';
import ContentContainer from './ContentContainer';
import SeriesContentPositionContainer from 'app/containers/series-content-position/SeriesContentPositionContainer';
import ActivityPreviewList from 'app/containers/content/components/ActivityPreviewList';
import type { ContentItem } from 'app/redux/modules/content/types';
import styleVars from 'app/style/variables';
import type { Activity } from 'app/redux/modules/activity/types';
import type { SeriesWithPreview } from 'app/redux/modules/series/types';

type State = {
  markedComplete: boolean
};

type OwnProps = {
  contentId: number,
};

type ConnectedProps = {
  getStandaloneContentItem: (contentId: number) => void,
  getActivitiesByContentId: (contentId: number) => void,
  getSeriesById: (seriesId: number) => void,
  contentItem: ContentItem,
  isLoading?: boolean,
  isCompleted: boolean,
  hasError?: boolean,
  activities: Activity[],
  seriesWithContent: ?SeriesWithPreview,
};

type Props = OwnProps & ConnectedProps;

class ContentStandaloneContainer extends Component<Props, State> {
  props: Props;
  componentDidMount() {
    const { contentId } = this.props;
    this.props.getStandaloneContentItem(contentId);
    this.props.getActivitiesByContentId(contentId);
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.contentItem == null && nextProps.contentItem && nextProps.contentItem.series) {
      this.props.getSeriesById(nextProps.contentItem.series.id);
    }
  }

  render() {
    const { isLoading, contentItem, hasError } = this.props;
    if(isLoading) {
      return this._renderLoading();
    } else if(!contentItem || hasError) {
      return this._renderError();
    } else {
      return this._renderContent();
    }
  }

  _renderError = () => {
    return (
      <View
        style={styles.scrollViewWrapper}>
        <TRATextStrong style={styles.errorText}>
          Sorry, this post could not be loaded. You are either offline or the post may have been removed by the author.
        </TRATextStrong>
      </View>
    );
  }

  _renderLoading = () => {
    return (
      <View
        style={styles.scrollViewWrapper}>
        <ActivityIndicator size="small" color={styleVars.color.traenaPrimaryGreen} />
      </View>
    );
  }

  _renderContent = () => {
    const { contentItem, activities } = this.props;
    return (
      <View
        style={styles.scrollViewWrapper}>
        <ScrollView
          style={styles.scrollView}>
          <ContentContainer
            contentItem={contentItem}
            isDetailView={true}
          />
          { activities && activities.length > 0 &&
            <View style={styles.activityHolder}>
            {
                <ActivityPreviewList
                  style={styles.activityPreview}
                  activities={activities}
                />
            }
            </View>
          }
        </ScrollView>

        { this._renderSeriesPositionContainer() }
      </View>
    );
  }

  _renderSeriesPositionContainer = () => {
    const { seriesWithContent } = this.props;
    if (!seriesWithContent) {
      return null;
    }

    return (
      <SeriesContentPositionContainer
        content={this.props.contentItem}
        series={seriesWithContent.series}
      />
    )
  }
}

const mapStateToProps = (state, ownProps: OwnProps) => {
  return {
    contentItem: publishedContentSelector(state, ownProps),
    isCompleted: isCompletedSelector(state, ownProps),
    isLoading: isLoadingSelector(state, ownProps),
    hasError: hasErrorSelector(state, ownProps),
    activities: activitiesByContentIdSelector(state, ownProps),
    seriesWithContent: seriesWithContentSelector(state, ownProps),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getStandaloneContentItem: (contentId: number) =>
      dispatch(getStandaloneContentItem(contentId)),
    getActivitiesByContentId: (contentId: number) => {
      return dispatch(getActivitiesByContentId(contentId))
    },
    getSeriesById: (seriesId: number) => {
      return dispatch(getSeriesById(seriesId))
    },
  };
};

const ConnectedContentStandaloneContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ContentStandaloneContainer);

export default ConnectedContentStandaloneContainer;

const styles = StyleSheet.create({
  scrollViewWrapper: {
    flex: 1,
    backgroundColor: styleVars.color.white,
    justifyContent: 'space-between',
  },
  scrollView: {
    flex: 1,
    backgroundColor: styleVars.color.white,
  },
  errorText: {
    marginTop: styleVars.padding.edgeOfScreen,
    textAlign: 'center',
    paddingHorizontal: styleVars.padding.edgeOfScreen,
  },
  activityHolder: {
    backgroundColor: styleVars.color.transparent,
    paddingHorizontal: styleVars.padding.edgeOfScreen,
    marginBottom: 20,
  },
  activityPreview: {
    width: '100%',
  },
});
