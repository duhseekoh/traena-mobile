// @flow
import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import type { ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet';
import { TRATextStrong } from 'app/components';
import styleVars from 'app/style/variables';

type Props = {
  style?: ViewStyleProp,
  onPress: () => any,
  focused?: boolean,
  disabled?: boolean,
  label: string,
  extraContent?: ?React$Node,
};

const NavButton = (props: Props) => {
  const { focused, disabled, label, extraContent, onPress, style } = props;
  const buttonStyle = focused ? styles.activeButton : styles.inactiveButton;
  const labelStyle = focused ? styles.activeLabel : styles.inactiveLabel;

  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={[styles.defaultButton, buttonStyle, style]}
    >
      <TRATextStrong style={[styles.defaultLabel, labelStyle]}>
        {label}
      </TRATextStrong>
      {extraContent && <View style={styles.extraContent}>{extraContent}</View>}
    </TouchableOpacity>
  );
};

NavButton.defaultProps = {
  focused: false,
  disabled: false,
};

const styles = StyleSheet.create({
  defaultButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  activeButton: {
    backgroundColor: styleVars.color.paleGreen,
  },
  inactiveButton: {},
  defaultLabel: {
    flex: 0,
    fontSize: styleVars.fontSize.large,
    color: styleVars.color.white,
  },
  activeLabel: {},
  inactiveLabel: {},
  extraContent: {
    flex: 0,
  },
});

export default NavButton;
